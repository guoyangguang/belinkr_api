# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Activity
    class Collection
      extend Forwardable
      include Virtus
      include Aequitas
      include Enumerable

      MODEL_NAME = "activity"

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, 
                     :merge, :score

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Activity::Member, storage_key)
      end

      private

      def storage_key
        "activities"
      end
    end # Collection
  end # Activity
end # Belinkr
