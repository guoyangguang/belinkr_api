# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require_relative "../config"
require_relative "../tinto/member"
require_relative "../polymorphic/polymorphic"

module Belinkr
  module Activity
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME  = "activity"
      ACTIONS     = Config::ACTIVITY_ACTIONS + 
                    Config::ACTIVITY_ACTIONS_EXTENSIONS
                    
      attribute :id,            Integer
      attribute :actor,         Polymorphic
      attribute :action,        String
      attribute :object,        Polymorphic
      attribute :target,        Polymorphic
      attribute :description,   String
      attribute :created_at,    Time
      attribute :updated_at,    Time

      validates_presence_of     :actor, :action, :object
      validates_within          :action, set: ACTIONS
      validates_length_of       :description, max: 250

      def_delegators :@member, :==, :score, :read, :save, :update, :to_json,
                                :delete, :undelete 

      alias_method :to_hash, :attributes

      def initialize(*args)
        super(*args)
        @member = Tinto::Member.new self
      end

      def storage_key
        "activities"
      end
    end # Member
  end # Activity
end # Belinkr
