# encoding: utf-8


module Belinkr
  class API < Sinatra::Base
    get "/appointments" do
      dispatch :collection do
        Appointment::Orchestrator.collection(
            Appointment::Collection.new(
                entity_id: current_user.entity_id
            ).page(params[:page]), current_user
        )
      end
    end # get /appointments

    post "/appointments" do
      dispatch :create do
        Appointment::Orchestrator.create(
            Appointment::Member.new(
                sanitized_payload.merge(entity_id: current_user.entity_id)
            ), current_user
        )
      end
    end # post /appointments

    put "/appointments/:id" do
      appointment         =  Appointment::Member.new(
                              entity_id: current_user.entity_id,
                              id:        params[:id]
                            )
      changed_appointment = Appointment::Member.new(
                             sanitized_payload.merge(
                               entity_id: current_user.entity_id
                             )
                           )
      changed_appointment.id = appointment.id
      dispatch :update, changed_appointment do
        Appointment::Orchestrator.update(changed_appointment, current_user)
      end
    end # put /appointments/:id

    get "/appointments/:id" do
      appointment         =  Appointment::Member.new(
                              entity_id: current_user.entity_id,
                              id:        params[:id]
                            )
      dispatch :read do
        Appointment::Orchestrator.read(appointment, current_user)
      end
    end # get /appointments/:id

    delete "/appointments/:id" do
      appointment         =  Appointment::Member.new(
                              entity_id: current_user.entity_id,
                              id:        params[:id]
                            )
      dispatch :delete do
        Appointment::Orchestrator.delete(appointment, current_user)
      end
    end # delete /appointments/:id

  end
end