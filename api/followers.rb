# encoding: utf-8
module Belinkr
  class API < Sinatra::Base
    get "/followings" do
      dispatch :collection do
        User::Orchestrator.followings(
          Follow::Collection.new(
            user_id:    current_user.id,
            entity_id:  current_user.entity_id,
            kind:       Belinkr::Follow::Collection::KINDS[1]
          ).page(params[:page]),
          current_user
        )
      end
    end # get /following

    get "/followers" do
      dispatch :collection do
        User::Orchestrator.followers(
          Follow::Collection.new(
            user_id:    current_user.id,
            entity_id:  current_user.entity_id,
            kind:       Belinkr::Follow::Collection::KINDS[0]
          ).page(params[:page]),
          current_user
        )
      end
    end # get /followers

    get "/users/:id/followers" do
      user = User::Member.new(id: params[:id], entity_id: session[:entity_id])
      
      dispatch :collection do
        User::Orchestrator.followers(
          Follow::Collection.new(
            user_id:    user.id,
            entity_id:  user.entity_id,
            kind:       Belinkr::Follow::Collection::KINDS[0] 
          ).page(params[:page]),
          current_user
        )
      end
    end # get /users/:id/followers

    post "/users/:id/followers" do
      user    = User::Member.new(
                  id:           params[:id], 
                  entity_id:    session[:entity_id]
                )
      follow  = Follow::Member.new(
                  follower_id:  current_user.id,
                  followed_id:  user.id,
                )

      dispatch :create, follow do
        Follow::Orchestrator.create(follow, current_user)
      end
    end # post /users/:id/followers

    delete "/users/:followed_id/followers/:id" do
      followed  = User::Member.new(  
                    id:           params[:followed_id],
                    entity_id:    current_user.entity_id 
                  )

      follow    = Follow::Member.new(
                    id:           params[:id], 
                    follower_id:  current_user.id, 
                    entity_id:    current_user.entity_id, 
                    followed_id:  followed.id
                  )
      dispatch :delete do
        Follow::Orchestrator.delete(follow, current_user)
      end
    end # delete /users/:followed_id/followers/:id
  end # API
end # Belinkr
