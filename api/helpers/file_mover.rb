# encoding: utf-8
require_relative "../../tinto/file_scheduler"

module Belinkr
  module ApiHelpers

    def file_mover
      lambda { |*args|
        caller_0, caller_1, caller_2 = *caller[0..2]
        block_caller = get_caller(caller_0)
        block_caller = get_caller(caller_2) if block_caller == "call"
        expect_methods = %w(create update delete destroy)
        unless expect_methods.include?(block_caller)
          raise "Unsupported caller #{block_caller}," \
                "caller must be one of #{expect_methods}"
        end
        args.flatten!
        schedule_method_name = "enqueue_for_#{block_caller}".to_sym
        Tinto::FileScheduler.send(schedule_method_name, *args)
      }
    end

    def get_caller(caller_string)
      if /^(.+?):(\d+)(?::in `(.*)')?/ =~ caller_string
        $3
      else
        ""
      end
    end

  end # ApiHelpers
end # Belinkr