# encoding: utf-8
module Belinkr
  class API < Sinatra::Base
    get "/requests" do
      dispatch :collection do
        Request::Orchestrator.collection(Request::Collection.new(
            user_id:    current_user.id, 
            entity_id:  current_user.entity_id,
            # kind: from
            kind:       Belinkr::Request::Collection::KINDS[0] 
          ).page(params[:page]), current_user)
      end
    end # get /requests

    get "/requests/:id" do
      entity_id, user_id = session.fetch(:entity_id), session.fetch(:user_id)
      dispatch :read do
        Request::Orchestrator.read(Request::Member.new(params.merge!(
          {entity_id: entity_id, user_id: user_id}
        )), current_user) 
      end
    end # get /requests/:id

    post "/requests" do
      request = Request::Member.new(sanitized_payload)
      dispatch :create, request do 
        Request::Orchestrator.create(request, current_user)
      end
    end # post /requests

    put "/requests/:id" do
      entity_id, user_id = session.fetch(:entity_id), session.fetch(:user_id)
      request     = Request::Member.new(params.merge!(
        {entity_id: entity_id, user_id: user_id}
      ))
      changed     = Request::Member.new(sanitized_payload)
      changed.id  = params[:id]
      dispatch :update, request do 
        Request::Orchestrator.update(request, changed, current_user)
      end
    end # put /requests/:id

    delete "/requests/:id" do
      entity_id, user_id = session.fetch(:entity_id), session.fetch(:user_id)
      dispatch :delete do
        Request::Orchestrator.delete(Request::Member.new(params.merge!(
            {entity_id: entity_id, user_id: user_id}
          )), current_user) 
      end
    end # delete /requests/:id

   post "/requests/:id/approvers" do
     entity_id, user_id = session.fetch(:entity_id), session.fetch(:user_id)
     request  = Request::Member.new(params.merge!(
       {entity_id: entity_id, user_id: user_id}
     ))
     actor_id, description = sanitized_payload.values
     dispatch :create  do
       Request::Orchestrator.activate(request, actor_id, description)
     end
   end

   get "/requests/:id/activities" do
     entity_id, user_id = session.fetch(:entity_id), session.fetch(:user_id)
     request  = Request::Member.new(params.merge!(
       {entity_id: entity_id, user_id: user_id}
     ))
     dispatch :collection, request do
       Request::Orchestrator.activities(request, current_user) 
     end
   end # get /activities

  end # API
end # Belinkr
