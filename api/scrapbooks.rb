# encoding: utf-8
module Belinkr
  class API < Sinatra::Base
    get "/scrapbooks" do
      dispatch :collection do
        Scrapbook::Orchestrator.collection(
          Scrapbook::Collection.new(
            user_id:    current_user.id,
            kind:       "own"
          ).page(params[:page]),
          current_user
        )
      end
    end # get /scrapbooks

    get "/scrapbooks/:id" do
      dispatch :read do
        Scrapbook::Orchestrator.read(
          Scrapbook::Member.new(
            id:       params[:id],
            user_id:  current_user.id
          ), 
          current_user
        )
      end
    end # get /scrapbooks/:id

    post "/scrapbooks" do
      scrapbook = Scrapbook::Member.new(sanitized_payload)
      dispatch :create, scrapbook do
        Scrapbook::Orchestrator.create(scrapbook, current_user)
      end
    end # post /scrapbooks

    put "/scrapbooks/:id" do
      scrapbook  = Scrapbook::Member.new(
        id:       params[:id],
        user_id:  current_user.id
      )
      updated    = Scrapbook::Member.new(sanitized_payload)

      dispatch :update, scrapbook do
        Scrapbook::Orchestrator.update(scrapbook, updated, current_user)
      end
    end # put /scrapbooks/:id
    
    delete "/scrapbooks/:id" do
      scrapbook = Scrapbook::Member.new(
        id:       params[:id], 
        user_id:  current_user.id
      )

      dispatch :delete do
        Scrapbook::Orchestrator.delete(scrapbook, current_user)
      end
    end # delete /scrapbooks/:id

    get "/scrapbooks/:scrapbook_id/scraps" do
      scrapbook = Scrapbook::Member.new(
        id:       params[:scrapbook_id],
        user_id:  current_user.id
      )

      dispatch :collection do
        Scrapbook::Scrap::Orchestrator.collection(
          scrapbook,
          Scrapbook::Scrap::Collection.new(
            scrapbook_id: scrapbook.id,
            user_id:      current_user.id
          ).page(params[:page]),
          current_user
        )
      end
    end # get /scrapbooks/:scrapbook_id/scraps

    get "/scrapbooks/:scrapbook_id/scraps/:id" do
      scrapbook = Scrapbook::Member.new(
        id:       params[:scrapbook_id],
        user_id:  current_user.id
      )
      scrap = Scrapbook::Scrap::Member.new(
        id:           params[:id],
        user_id:      current_user.id,
        scrapbook_id: scrapbook.id 
      )

      dispatch :read do
        Scrapbook::Scrap::Orchestrator.read(scrapbook, scrap, current_user)
      end
    end # get /scrapbooks/:scrapbook_id/scraps/:id

    post "/scrapbooks/:scrapbook_id/scraps" do
      scrapbook = Scrapbook::Member.new(
        id:       params[:scrapbook_id],
        user_id:  current_user.id
      )
      scrap = Scrapbook::Scrap::Member.new(sanitized_payload)

      dispatch :create, scrap do 
        Belinkr::Scrapbook::Scrap::Orchestrator
          .create(scrapbook, scrap, current_user)
      end
    end # post /scrapbooks/:scrapbook_id/scraps

    put "/scrapbooks/:scrapbook_id/scraps/:scrap_id" do
      scrapbook = Scrapbook::Member.new(
        id:       params[:scrapbook_id],
        user_id:  current_user.id
      )
      scrap   = Scrapbook::Scrap::Member.new(
        id:             params[:scrap_id],
        user_id:        current_user.id,
        scrapbook_id:   params[:scrapbook_id]
      )
      updated    = Scrapbook::Scrap::Member.new(sanitized_payload)
      updated.id = params[:scrap_id]

      dispatch :update, scrap do 
        Scrapbook::Scrap::Orchestrator
          .update(scrapbook, scrap, updated, current_user)
      end
    end # put /scrapbooks/:scrapbook_id/scraps/:id
    
    delete "/scrapbooks/:scrapbook_id/scraps/:scrap_id" do
      scrapbook = Scrapbook::Member.new(
        id:       params[:scrapbook_id],
        user_id:  current_user.id
      )
      scrap = Scrapbook::Scrap::Member.new(
          id:             params[:scrap_id],
          user_id:        current_user.id,
          scrapbook_id:   scrapbook.id
      )

      dispatch :delete do
        Scrapbook::Scrap::Orchestrator.delete(scrapbook, scrap, current_user)
      end
    end # delete /scrapbooks/:scrapbook_id/scraps/:id
  end # API
end # Belinkr
