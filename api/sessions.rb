# encoding: utf-8
require_relative "../session/orchestrator"

module Belinkr
  class API < Sinatra::Base
    post "/sessions" do
      payload   = sanitized_payload
      email     = payload["email"]
      plaintext = payload["password"]

      dispatch :create, session do
        Session::Orchestrator.create(session, email, plaintext)
      end
    end # post /sessions

    delete "/sessions/:id" do
      dispatch :delete do
        Session::Orchestrator.delete(session)
      end
    end
  end # API
end # Belinkr
