# encoding: utf-8

module Belinkr
  class API < Sinatra::Base
    get "/statuses" do
      dispatch :collection do
        Status::Orchestrator.collection(
          Status::Collection.new(
            user_id:    current_user.id, 
            entity_id:  current_user.entity_id,
            kind:       "general"
          ).page(params[:page]),
          current_user
        )
      end
    end # get /statuses

    get "/statuses/:id" do
      dispatch :read do
        Status::Orchestrator.read(
          Status::Member
            .new(id: params[:id], entity_id: current_user.entity_id),
          current_user
        )
      end
    end # get /statuses/:id

    post "/statuses" do
      status = Status::Member.new(sanitized_payload)
      dispatch :create, status do 
        Status::Orchestrator.create(status, current_user) 
      end
    end # post /statuses

    put "/statuses/:id" do
      status     = Status::Member.new(
                     id: params[:id], entity_id: current_user.entity_id
                   )
      changed    = Status::Member.new(
        sanitized_payload.merge!(entity_id: current_user.entity_id)
      )
      changed.files += status.files
      changed.id = status.id

      dispatch :update, status do 
        Status::Orchestrator.update(status, changed, current_user)
      end
    end # put /statuses/:id

    delete "/statuses/:id" do
      dispatch :delete do
        Status::Orchestrator.delete(
          Status::Member
            .new(id: params[:id], entity_id: current_user.entity_id),
          current_user
        )
      end
    end # delete /statuses/:id

    get "/statuses/:status_id/replies" do
      status  = Status::Member.new(
                  id: params[:status_id], entity_id: current_user.entity_id
                )
      replies =  Reply::Collection.new(status.embedded_replies)

      dispatch :collection do
        Reply::Orchestrator.collection(status, replies, current_user)
      end
    end # get /statuses/:status_id/replies

    get "/statuses/:status_id/replies/:id" do
      status  = Status::Member.new(
                  id: params[:status_id], entity_id: current_user.entity_id
                )
      reply     = status.replies.to_a[params[:id].to_i]

      dispatch :read, reply do
        Reply::Orchestrator.read(status, reply, current_user)
      end
    end # get /statuses/:status_id/replies/:id
    
    post "/statuses/:status_id/replies" do
      status  = Status::Member.new(
                  id: params[:status_id], entity_id: current_user.entity_id
                )
      reply   = Reply::Member.new(sanitized_payload)

      dispatch :create, reply do 
        Reply::Orchestrator.create(status, reply, current_user) 
      end
    end # post /statuses/:status_id/replies

    put "/statuses/:status_id/replies/:id" do
      status  = Status::Member.new(
                  id: params[:status_id], entity_id: current_user.entity_id
                )
      reply     = Reply::Member.new(sanitized_payload.merge!(params))
      reply.id  = params[:id]

      dispatch :update, reply do
        Reply::Orchestrator.update(status, reply, current_user)
      end
    end # put /statuses/:status_id/replies/:id

    delete "/statuses/:status_id/replies/:id" do
      status  = Status::Member.new(
                  id: params[:status_id], entity_id: current_user.entity_id
                )
      reply   = status.replies.to_a[params[:id].to_i]

      dispatch :delete, reply do
        Reply::Orchestrator.delete(status, reply, current_user)
      end
    end # delete /statuses/:status_id/replies/:id
  end # API
end # Belinkr
