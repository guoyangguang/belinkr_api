# encoding: utf-8
require_relative "workspaces/invitations"
require_relative "workspaces/autoinvitations"
require_relative "workspaces/collaborators"
require_relative "workspaces/administrators"
require_relative "workspaces/statuses"
require_relative "workspaces/replies"
require_relative "workspaces/boards"

module Belinkr
  class API < Sinatra::Base
    get "/workspaces" do
      dispatch :collection do
        Workspace::Orchestrator.collection(
          Workspace::Collection.new(
              entity_id: current_user.entity_id
          ).page(params[:page]), current_user
        )
      end
    end # get /workspaces

    get "/workspaces/:id" do
      dispatch :read do
        Workspace::Orchestrator.read(
         Workspace::Member
          .new(id: params[:id], entity_id: current_user.entity_id),
         current_user)
      end
    end # get /workspaces/:id

    post "/workspaces" do
      dispatch :create do
        Workspace::Orchestrator.create(
          Workspace::Member.new(
            sanitized_payload.merge(entity_id: current_user.entity_id)
        ), current_user)
      end
    end # post /workspaces/:id

    put "/workspaces/:id" do
      workspace  = Workspace::Member.new(params.merge!(
        entity_id: current_user.entity_id))
      changed    = Workspace::Member.new(sanitized_payload.merge!(
        entity_id: current_user.entity_id))
      changed.id = workspace.id

      dispatch :update, workspace do
        Workspace::Orchestrator.update(workspace, changed, current_user)
      end
    end # put /workspaces/:id
    
    delete "/workspaces/:id" do
      workspace = Workspace::Member
        .new(entity_id: current_user.entity_id, id: params[:id])
      
      dispatch :delete do
        Workspace::Orchestrator.delete(workspace, current_user)
      end
    end

    delete "/workspaces/:workspace_id/users/:user_id" do
      workspace = Workspace::Member.new(
                    id:         params[:workspace_id], 
                    entity_id:  session[:entity_id]
                  )
      user      = User::Member.new(
                    id:         params[:user_id],
                    entity_id:  session[:entity_id]
                  )
      dispatch :delete do
        Workspace::Orchestrator.leave(workspace, user)
      end
    end # delete /workspaces/:workspace_id/users/:user_id
  end # API
end # Belinkr
