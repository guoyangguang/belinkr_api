# encoding: utf-8
require_relative "../../workspace/administrator/presenter"
require_relative '../../user/member'

module Belinkr
  class API < Sinatra::Base
    get "/workspaces/:workspace_id/administrators" do
      workspace = Belinkr::Workspace::Member
        .new(id: params[:workspace_id], entity_id: session[:entity_id])

      dispatch :collection do
        Belinkr::Workspace::Orchestrator
          .administrators_for(workspace)
      end
    end # get /workspaces/:workspace_id/administrators

    post "/workspaces/:workspace_id/administrators" do
      workspace = Workspace::Member.new(
          id:        params[:workspace_id],
          entity_id: current_user.entity_id
      )

      dispatch :create, workspace do
        Workspace::Orchestrator.promote(
            workspace,
            User::Member.new(
              id:        sanitized_payload['user_id'],
              entity_id: current_user.entity_id
            ),
            current_user
        )
      end
    end # post /workspaces/:workspace_id/administrators

    delete "/workspaces/:workspace_id/administrators/:id" do
      workspace = Workspace::Member
        .new(entity_id: current_user.entity_id, id: params[:workspace_id])

      admin = User::Member
        .new(entity_id: current_user.entity_id, id: params[:id])
      
      dispatch :delete do
        Workspace::Orchestrator.demote(workspace, admin, current_user)
      end
    end # delete /workspaces/:workspace_id/administrators
  end # API
end # Belinkr
