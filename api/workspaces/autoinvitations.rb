# encoding: utf-8
module Belinkr
  class API < Sinatra::Base
    post "/workspaces/:workspace_id/autoinvitations" do
      workspace = Workspace::Member.new(
          entity_id: current_user.entity_id,
          id:        params[:workspace_id]
      )

      dispatch :create, workspace do
        Workspace::Autoinvitation::Orchestrator.request(
            workspace,
            Workspace::Autoinvitation::Member.new(
                entity_id: workspace.entity_id,
                workspace_id: workspace.id
            ),
            current_user
        )
      end
    end

    get "/workspaces/:workspace_id/autoinvitations" do
      dispatch :collection do
        Workspace::Autoinvitation::Orchestrator.collection(
            Workspace::Member.new(
                entity_id: current_user.entity_id,
                id: params[:workspace_id]
            ),
            Workspace::Autoinvitation::Collection.new(
                entity_id: current_user.entity_id,
                workspace_id: params[:workspace_id]
            ).page(params[:page]),
            current_user
        )
      end
    end

    post "/workspaces/:workspace_id/autoinvitations/allowed" do
      workspace = Workspace::Member
        .new(entity_id: current_user.entity_id, id: params[:workspace_id])
      autoinvitation = Workspace::Autoinvitation::Member
        .new(entity_id: current_user.entity_id, workspace_id: workspace.id,
          id: sanitized_payload['autoinvitation_id'])

      dispatch :create do
        Workspace::Autoinvitation::Orchestrator
          .allow(workspace, autoinvitation, current_user)
      end
    end # post /workspaces/:workspace_id/autoinvitations
  end # API
end # Belinkr
