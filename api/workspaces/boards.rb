# encoding: utf-8
module Belinkr
  class API < Sinatra::Base
    post "/workspaces/:workspace_id/boards" do
      board     = Workspace::Board::Member.new(sanitized_payload)
      workspace = Workspace::Member.new(
                    id:         params[:workspace_id],
                    entity_id:  current_user.entity_id
                  )
      dispatch :create, board do
        Workspace::Board::Orchestrator.create(workspace, board, current_user)
      end
    end
   
    get "/workspaces/:workspace_id/boards/:id" do
      dispatch :read do
        Workspace::Board::Orchestrator.read(
          Workspace::Member.new(
            id:            params[:workspace_id],
            entity_id:     current_user.entity_id
          ),
          Workspace::Board::Member.new(
            id:            params[:id],
            entity_id:     current_user.entity_id,
            workspace_id:  params[:workspace_id]
          ),
          current_user
        )
      end
    end

    get "/workspaces/:workspace_id/boards" do
      workspace = Workspace::Member
        .new(id: params[:workspace_id], entity_id: current_user.entity_id)
      boards = Workspace::Board::Collection
        .new(entity_id: current_user.entity_id, workspace_id: params[:workspace_id])
          .page(params[:page])
          
      dispatch :collection do
        Workspace::Board::Orchestrator
          .collection(workspace, boards, current_user)
      end
    end
  end # API
end # Belinkr
