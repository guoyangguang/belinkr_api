# encoding: utf-8
require_relative "../../workspace/collaborator/presenter"

module Belinkr
  class API < Sinatra::Base
    get "/workspaces/:workspace_id/collaborators" do
      workspace = Belinkr::Workspace::Member
        .new(id: params[:workspace_id], entity_id: session[:entity_id])

      dispatch :collection do
        Belinkr::Workspace::Orchestrator.collaborators_for(workspace)
      end
    end # get /workspaces/:workspace_id/collaborators

    delete "/workspaces/:workspace_id/collaborators/:collaborator_id" do
      dispatch :delete do
        Workspace::Orchestrator.remove(
          Workspace::Member.new(
              id: params[:workspace_id],
              entity_id: current_user.entity_id
          ),
          User::Member.new(
              id: params[:collaborator_id],
              entity_id: current_user.entity_id
          ),
          current_user
        )
      end
    end # delete /workspaces/:workspace_id/collaborators/:collaborator_id
  end # API
end # Belinkr
