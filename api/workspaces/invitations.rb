# encoding: utf-8
require_relative "../../workspace/invitation/presenter"

module Belinkr
  class API < Sinatra::Base
    get "/workspaces/:id/invitations" do
      dispatch :collection do
        Workspace::Invitation::Orchestrator.collection(
            Workspace::Member.new(
                entity_id: current_user.entity_id,
                id: params[:id]
            ),
            Workspace::Invitation::Collection.new(
                entity_id: current_user.entity_id,
                workspace_id: params[:id]
            ).page(params[:page]),
            current_user
        )
      end
    end # get /workspaces/:id/invitations

    post "/workspaces/:id/invitations" do
      dispatch :create do
        Workspace::Invitation::Orchestrator.invite(
          Workspace::Member.new(
            id:         params[:id],
            entity_id:  current_user.entity_id
          ),
          User::Member.new(
            id:         sanitized_payload["invited_id"],
            entity_id:  current_user.entity_id
          ),
          current_user
        )
      end
    end # post /workspaces/:id/invitations

    post "/workspaces/:id/invitations/accepted" do
      workspace   = Workspace::Member.new(
                      id:         params[:id],
                      entity_id:  current_user.entity_id
                    )
      invitation  = Workspace::Invitation::Member.new(
                      id:           sanitized_payload["invitation_id"],
                      entity_id:    current_user.entity_id,
                      workspace_id: params[:id]
                    )

      dispatch :create, invitation do
        Workspace::Invitation::Orchestrator
          .accept(workspace, invitation, current_user)
      end
    end # post /workspaces/:id/invitations/accepted

    post "/workspaces/:id/invitations/rejected" do
      workspace   = Workspace::Member.new(
                      id: params[:id],
                      entity_id: current_user.entity_id
                    )
      invitation  = Workspace::Invitation::Member.new(
                      id:           sanitized_payload["invitation_id"],
                      entity_id:    current_user.entity_id,
                      workspace_id: params[:id]
                    )

      dispatch :create, invitation do
        Workspace::Invitation::Orchestrator
          .reject(workspace, invitation, current_user)
      end
    end # post /workspaces/:id/invitations/rejected
  end # API
end # Belinkr
