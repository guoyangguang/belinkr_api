# encoding: utf-8
require_relative "../../workspace/reply/presenter"

module Belinkr
  class API < Sinatra::Base
    get "/workspaces/:workspace_id/statuses/:status_id/replies" do
      status       = Workspace::Status::Member.new(
                    id: params[:status_id],
                    entity_id: current_user.entity_id,
                    workspace_id: params[:workspace_id]
                  )
      replies      = Workspace::Reply::Collection.new(status.embedded_replies)
      dispatch :collection do
        Workspace::Reply::Orchestrator.collection(
            replies, current_user
        )
      end
    end # get /workspaces/:workspace_id

    post "/workspaces/:workspace_id/statuses/:status_id/replies" do
      reply     = Workspace::Reply::Member.new(sanitized_payload)
      status     = Workspace::Status::Member.new(
                    id: params[:status_id],
                    entity_id: current_user.entity_id,
                    workspace_id: params[:workspace_id]
                  )
      dispatch :create, reply do
        Workspace::Reply::Orchestrator.create(status, reply, current_user)
      end
    end # /workspaces/:workspace_id/statuses/:status_id/replies

    put "/workspaces/:workspace_id/statuses/:status_id/replies/:reply_id" do
      status       = Workspace::Status::Member.new(
                    id: params[:status_id],
                    entity_id: current_user.entity_id,
                    workspace_id: params[:workspace_id]
                  )
      reply        = status.embedded_replies
                         .select { |reply|
                            reply['id'] == params[:reply_id].to_i
                          }[0]
      updated     = Workspace::Reply::Member
                      .new(reply.merge(sanitized_payload))
      dispatch :update, updated do
        Workspace::Reply::Orchestrator.update(status, updated, current_user)
      end
    end # put /workspaces/:workspace_id/statuses/:status_id/replies/:reply_id

    delete "/workspaces/:workspace_id/statuses/:status_id/"\
           + "replies/:reply_id" do
      status       = Workspace::Status::Member.new(
                    id: params[:status_id],
                    entity_id: current_user.entity_id,
                    workspace_id: params[:workspace_id]
                  )
      reply        = status.embedded_replies
                         .select { |reply|
                            reply['id'] == params[:reply_id].to_i
                          }[0]
      dispatch :delete do
        Workspace::Reply::Orchestrator.delete(
          status, reply, current_user
        ) 
      end
    end #delete /workspaces/:workspace_id/statuses/:status_id/replies/:reply_id
  end # API
end # Belinkr
