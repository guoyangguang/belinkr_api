# encoding: utf-8
require_relative "../../workspace/status/presenter"

module Belinkr
  class API < Sinatra::Base
    get "/workspaces/:workspace_id/statuses" do
      workspace = Workspace::Member.new(
                    id:         params[:workspace_id],
                    entity_id:  current_user.entity_id
                  )
      dispatch :read do
        Workspace::Status::Orchestrator.collection(workspace, current_user)
          .page(params[:page])
      end
    end # get /workspace/:workspace_id/statuses

    get "/workspaces/:workspace_id/statuses/:status_id" do
      status    = Workspace::Status::Member.new(
                    id:           params[:status_id],
                    workspace_id: params[:workspace_id],
                    entity_id:    current_user.entity_id
                  )
      workspace = Workspace::Member.new(
                    id:         params[:workspace_id],
                    entity_id:  current_user.entity_id
                  )
      dispatch :read, status do
        Workspace::Status::Orchestrator.read(workspace, status, current_user)
      end
    end # get /workspaces/:workspace_id/statuses/:status_id

    post "/workspaces/:workspace_id/statuses" do
      status    = Workspace::Status::Member.new(sanitized_payload)
      workspace = Workspace::Member.new(
                    id:         params[:workspace_id],
                    entity_id:  current_user.entity_id
                  )
      dispatch :create, status do
        Workspace::Status::Orchestrator.create(workspace, status, current_user)
      end
    end # post /workspaces/:workspace_id/statuses
    
    put "/workspaces/:workspace_id/statuses/:status_id" do
      updated     = Workspace::Status::Member.new(sanitized_payload)
      updated.id  = params[:status_id]
      status      = Workspace::Status::Member.new(
                      id:           params[:status_id],
                      workspace_id: params[:workspace_id],
                      entity_id:    current_user.entity_id
                    )
      workspace   = Workspace::Member.new(
                      id:         params[:workspace_id],
                      entity_id:  current_user.entity_id
                    )
      dispatch :update, status do
        Workspace::Status::Orchestrator
          .update(workspace, status, updated, current_user)
      end
    end # put /workspaces/:workspace_id/statuses/:status_id

    delete "/workspaces/:workspace_id/statuses/:status_id" do
      status    = Workspace::Status::Member.new(
                    id:           params[:status_id],
                    workspace_id: params[:workspace_id],
                    entity_id:    current_user.entity_id
                  )
      workspace = Workspace::Member.new(
                    id:         params[:workspace_id],
                    entity_id:  current_user.entity_id
                  )
      dispatch :delete do
        Workspace::Status::Orchestrator.delete(
          workspace, status, current_user
        ) 
      end
    end # delete /workspaces/:workspace_id/statuses/:status_id

    post "/workspaces/:workspace_id/councils" do
      workspace = Belinkr::Workspace::Member
        .new(entity_id: current_user.entity_id, id: params[:workspace_id])
      status = Belinkr::Workspace::Status::Member
        .new(sanitized_payload['status'])
      
      dispatch :create, status do
        Belinkr::Workspace::Status::Orchestrator
          .create(workspace, status, current_user, {kind: 'council'})
      end
    end
  end # API
end # Belinkr
