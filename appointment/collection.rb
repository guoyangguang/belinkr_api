# encoding: utf-8
require 'virtus'
require 'aequitas'
require 'forwardable'
require_relative 'member'
require_relative '../tinto/sorted_set'

module Belinkr
  module Appointment
    class Collection
      include Virtus
      include Aequitas
      extend Forwardable
      include Enumerable

      MODEL_NAME = 'appointment'

      attribute :entity_id, Integer

      validates_presence_of       :entity_id
      validates_numericalness_of  :entity_id

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete,
                     :merge, :score

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Member, storage_key)
      end

      def member_init_params
        { entity_id: entity_id }
      end

      private

      def storage_key
        "entities:#{entity_id}:appointments"
      end

    end # Collection
  end # Appointment
end # Belinkr