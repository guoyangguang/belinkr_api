#encoding: utf-8
require 'virtus'
require 'aequitas'
require 'forwardable'
require_relative '../tinto/member'

module Belinkr
  module Appointment
    class Member
      include Virtus
      include Aequitas
      extend Forwardable

      MODEL_NAME = 'appointment'

      attribute :id,              Integer
      attribute :entity_id,       Integer
      attribute :user_id,         Integer
      attribute :name,            String
      attribute :description,     String
      attribute :start_at,        Time
      attribute :end_at,          Time
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of :entity_id, :user_id, :name, :description
      validates_numericalness_of :entity_id, :user_id
      validates_length_of :name, min: 3, max: 250
      validates_length_of :description, min: 3, max: 250

      def_delegators :@member, :==, :score, :to_json, :read, :save,
                               :delete, :undelete

      alias_method :to_hash, :attributes


      def initialize(*args)
        super *args
        @member = Tinto::Member.new self
      end

      def storage_key
        "entities:#{entity_id}:appointments"
      end

    end # Member
  end # Appointment
end # Belinkr