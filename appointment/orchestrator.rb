#encoding: utf-8
require_relative 'collection'

module Belinkr
  module Appointment
    class Orchestrator
      def self.collection(appointments, user)
        #TODO enforcer
        appointments
      end

      def self.create(appointment, user)
        #TODO enforcer
        sanitize appointment, user
        appointment.save
        collection_for(appointment).add appointment
        appointment
      end

      def self.update(appointment, user)
        #TODO enforcer
        sanitize appointment, user
        appointment.save
        appointment
      end

      def self.read(appointment, user)
        #TODO enforcer
        appointment
      end

      def self.delete(appointment, user)
        #TODO enforcer
        appointment.delete
        appointment
      end

      def self.collection_for(appointment)
        Belinkr::Appointment::Collection
          .new(entity_id: appointment.entity_id)
      end

      def self.sanitize(appointment, user)
        appointment.entity_id = user.entity_id
        appointment.user_id   = user.id
      end

    end # Orchestrator
  end # Appointment
end # Belinkr