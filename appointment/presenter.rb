# encoding: utf-8
require 'json'

module Belinkr
  module Appointment
    module Presenter
      class Collection
        def initialize(resource)
          @resource = resource
        end

        def as_poro
          @resource.map { |member| Presenter::Member.new(member).as_poro }
        end

        def as_json
          "[#{as_poro.map { |i| i.to_json }.join(",")}]"
        end
      end # Collection

      class Member
        def initialize(resource)
          @resource = resource
        end

        def as_poro
          user = OpenStruct.new(timezone: "Europe/Madrid")
          local_created_at =
            Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
          local_updated_at =
            Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)

          {
            id:           @resource.id,
            entity_id:    @resource.entity_id,
            user_id:      @resource.user_id,
            name:         @resource.name,
            description:  @resource.description,
            start_at:     @resource.start_at,
            end_at:       @resource.end_at,
            created_at:   local_created_at,
            updated_at:   local_updated_at
          }.merge!(errors)
        end

        def as_json
          as_poro.to_json
        end

        def errors
          @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
        end

      end # Member
    end # Presenter
  end # Appointment
end # Belinkr