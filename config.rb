# encoding: utf-8

module Belinkr
  module Config
    AUTHENTICATOR_STORAGE_KEY = "authenticator"
    AVAILABLE_LOCALES = ["en.yml", "es.yml"]
    DEFAULT_LOCALE    = :en
    DEFAULT_TIMEZONE  = "Etc/UTC"

    # See http://activitystrea.ms/head/activity-schema.html#verbs
    ACTIVITY_ACTIONS = %w{
      add cancel checkin delete favorite follow give ignore invite 
      join leave like make-friend play post receive remove remove-friend
      request-friend rsvp-maybe rsvp-no rsvp-yes save share
      stop-following tag unfavorite unlike unsave update
    }

    ACTIVITY_ACTIONS_EXTENSIONS = %w{
      accept reject allow deny promote demote
    }

    INITIAL_SCRAPBOOKS = ["favorites", "drafts"]
    #
    # Polymorphic mapper
    #

    RESOURCE_MAP = { 
      "string"                      => "String",
      "openstruct"                  => "OpenStruct",
      "user"                        => "Belinkr::User::Member", 
      "status"                      => "Belinkr::Status::Member",
      "invitation"                  => "Belinkr::Invitation::Member",
      "follow"                      => "Belinkr::Follow::Member",
      "workspace"                   => "Belinkr::Workspace::Member",
      "workspace::invitation"       => "Belinkr::Workspace::Invitation::Member",
      "workspace::autoinvitation"   => "Belinkr::Workspace::Autoinvitation::Member",
      "request"                     => "Belinkr::Request::Member"
    }

    #
    # Mailer Worker
    #

    SMTP = {
      address:        "smtp.gmail.com",
      port:           587,
      user_name:      ENV.fetch("MAIL_USER"),
      password:       ENV.fetch("MAIL_PASS"),
      authentication: "plain",
      domain:         "belinkr.com"
    }

    INVITATION_TEMPLATE_EN = 
      "#{File.dirname(__FILE__)}/workers/mailer/templates/invitation.en.html"
    INVITATION_TEMPLATE_ES = 
      "#{File.dirname(__FILE__)}/workers/mailer/templates/invitation.es.html"

    MAILER_TEMPLATES = {
      invitation: {
        en: File.open(INVITATION_TEMPLATE_EN, "r") { |f| f.read },
        es: File.open(INVITATION_TEMPLATE_ES, "r") { |f| f.read }
      }
    }

    AUTH_USERNAME = "admin"
    AUTH_PASSWORD = "admin"

    FILES_CLEANUP_KEY = "files:cleanup"

  end # Config
end # Belinkr
