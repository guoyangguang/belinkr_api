# encoding: utf-8
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Credential
    class Collection
      extend Forwardable
      include Aequitas
      include Enumerable
      
      MODEL_NAME = "credential"

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, 
                     :merge, :score

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Member, storage_key)
      end

      private

      def storage_key
        "credentials"
      end
    end # Collection
  end # Credential
end # Belinkr
