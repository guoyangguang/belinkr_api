# encoding: utf-8
require "virtus"
require "aequitas"
require "bcrypt"
require_relative "../tinto/member"
require_relative "../tinto/utils"
require_relative "../user/member"
require_relative "../entity/member"

module Belinkr
  module Credential
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "credential"

      attribute :id,          Integer
      attribute :email,       String
      attribute :password,    String
      attribute :user_ids,    Array[Integer], default: []
      attribute :entity_ids,  Array[Integer], default: []
      attribute :created_at,  Time
      attribute :updated_at,  Time

      validates_presence_of   :email, :password
      validates_format_of     :email, as: :email_address
      validates_length_of     :password, min: 8, max: 150

      def_delegators :@member, :==, :score, :to_json, :read, :update, 
                                :delete, :undelete 

      alias_method :to_hash, :attributes

      def initialize(*args)
        super *args
        @member = Tinto::Member.new self
      end

      def save
        self.password = BCrypt::Password.create(password) unless encrypted?
        @member.save
      end

      def encrypted?
        Tinto::Utils.bcrypted?(password)
      end

      def storage_key
        "credentials"
      end

      alias_method :search_index, :storage_key
    end # Member
  end # Credential
end # Belinkr
