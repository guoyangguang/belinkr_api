# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "../user/orchestrator"
require_relative "../session/authenticator"

module Belinkr
  module Credential
    class Orchestrator
      def self.create(credential, user)
        if Authenticator.exists?(credential.email) ||
          credentials.include?(credential)
          raise "credential already exists" 
        end

        Belinkr::User::Orchestrator.create(user)
        credential.user_ids   = [user.id]
        credential.entity_ids = [user.entity_id]
        credential.save

        $redis.multi do
          credentials << credential
          Belinkr::Authenticator.add(credential)
        end

        [credential, user]
      rescue Tinto::Exceptions::InvalidResource => exception
        #if user.valid? && !credential.valid?
        $redis.multi do
          credentials.remove credential
          Belinkr::Authenticator.remove(credential)
          #destroy(credential)
        end
        #end

        raise exception
      end #.create

      private

      def self.credentials
        Credential::Collection.new
      end
    end # Orchestrator
  end # Credential
end # Belinkr
