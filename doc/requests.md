Accessing requests you are involved in
======================================
GET /requests

Accessing a request you are involved in
=======================================
GET /requests/:request_id

Creating a request
==================
POST /requests

Updating a request you are involved in
======================================
PUT /requests/:request_id

Deleting a request you created
==============================
DELETE /requests/:request_id

@pending
Accessing the history of a request you are involved in
======================================================
GET /requests/:request_id/events

@pending
Approving a request in which you are involved as an approver
============================================================
PUT /requests/:request_id/yeas/:approver_id

@pending
Rejecting a request in which you are involved as an approver
============================================================
PUT /requests/:request_id/nays/:approver_id

@pending
Fulfilling a request in which you are involved as its resposible
================================================================
PUT /requests/:request_id

@pending
Involving further approvers in a request
========================================
POST /requests/:request_id/approvers

@pending
Removing approvers from a request
=================================
DELETE /requests/:request_id/approvers/:approver_id

Updating the fulfiller of a request
===================================
PUT /requests/:request_id
