Accessing scrapbooks
====================
GET /scrapbooks

Accessing a specific scrapbook
==============================
GET /scrapbooks/scrapbook_id

Creating a scrapbook
====================
POST /scrapbooks

Updating an existing scrapbook
==============================
PUT /scrapbooks/:scrapbook_id

Deleting an existing scrapbook
==============================
DELETE /scrapbooks/:scrapbook_id

Accessing the list of scraps in a scrapbook
===========================================
GET /scrapbooks/:scrapbook_id/scraps

Accessing a specific scrap
==============================
GET /scrapbooks/:scrapbook_id/scraps/:scrap_id
Adding a new scrap to a scrapbook

=================================
POST /scrapbooks/:scrapbook_id/scraps

Updating an existing scrap
==========================
PUT /scrapbooks/:scrapbook_id/scraps/:scrap_id

Deleting an existing scrap
==========================
DELETE /scrapbooks/:scrapbook_id/scraps/:scrap_id

