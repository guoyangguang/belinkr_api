Accessing the general timeline
==============================

Reuest:
GET /statuses

Response:
200 OK

[
  { 
    "id"          : 52,
    "text"        : "something important",
    "user_id"     : 1,
    "replies"     : [],
    "mentions"    : [],
    "attachments  : [],
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"    : "/statuses/52",
                      "replies" : "/statuses/52/replies",
                      "user"    : "/users/1"
                    }
  },
  {
    "id"          : 55,
    "text"        : "this is interesting",
    "user_id"     : 3,
    "replies"     : [],
    "mentions"    : [],
    "attachments  : [],
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"    : "/statuses/55",
                      "replies" : "/statuses/55/replies",
                      "user"    : "/users/3"
                    }
  }
]


Accessing a specific status
===========================
Request:
GET /statuses/:status_id

Response:
200 OK

{ 
  "id"          : 52,
  "text"        : "something important",
  "user_id"     : 1,
  "replies"     : [],
  "mentions"    : [],
  "attachments  : [],
  "created_at"  : "2012-03-04 15:56:00 +0800",
  "updated_at"  : "2012-03-04 15:56:00 +0800",
  "links"       : {
                    "self"    : "/statuses/52",
                    "replies" : "/statuses/52/replies",
                    "user"    : "/users/1"
                  }
}

Creating a status
=================
Request:
POST /statuses

{ "text"        : "a new status" }

Response:
201 Created

{ 
  "id"          : 88,
  "text"        : "a new status",
  "user_id"     : 1,
  "replies"     : [],
  "mentions"    : [],
  "attachments" : [],
  "created_at"  : "2012-03-04 15:56:00 +0800",
  "updated_at"  : "2012-03-04 15:56:00 +0800",
  "links"       : {
                    "self"    : "/statuses/88",
                    "replies" : "/statuses/88/replies",
                    "user"    : "/users/1"
                  }
}

Updating a status
=================
Request:
PUT /statuses/:status_id

{ 
  "id"          : 88,
  "text"        : "a new status [updated]",
  "user_id"     : 1
}

Response:
200 OK

{ 
  "id"          : 88,
  "text"        : "a new status - [updated]",
  "user_id"     : 1,
  "replies"     : [],
  "mentions"    : [],
  "attachments" : [],
  "created_at"  : "2012-03-04 15:56:00 +0800",
  "updated_at"  : "2012-03-04 16:01:00 +0800",
  "links"       : {
                    "self"    : "/statuses/88",
                    "replies" : "/statuses/88/replies",
                    "user"    : "/users/1"
                  }
}

Deleting a status
=================
Request:
DELETE /statuses/:status_id

Response:
204 No Content


Accessing replies to a specific status
======================================
Request:
GET /statuses/:status_id/replies

Response:
200 OK

[
  { 
    "id"          : 87,
    "text"        : "a reply",
    "user_id"     : 5,
    "mentions"    : [],
    "attachments" : [],
    "created_at"  : "2012-03-04 17:56:00 +0800",
    "updated_at"  : "2012-03-04 17:56:00 +0800",
    "links"       : {
                      "self"    : "/statuses/82/replies/97",
                      "status"  : "/statuses/82",
                      "user"    : "/users/5"
                    }
  },
  { 
    "id"          : 100,
    "text"        : "another reply"
    "user_id"     : 8,
    "mentions"    : [],
    "attachments" : [],
    "created_at"  : "2012-03-04 18:02:00 +0800",
    "updated_at"  : "2012-03-04 18:02:00 +0800",
    "links"       : {
                      "self"    : "/statuses/82/replies/100",
                      "status"  : "/statuses/82",
                      "user"    : "/users/8"
                    }
  },
]

  
@pending
Accessing a specific reply
==========================
Request:
GET /statuses/:status_id/replies/:reply_id

Response: 
200 OK

{ 
  "id"          : 100,
  "text"        : "another reply"
  "user_id"     : 8,
  "mentions"    : [],
  "attachments" : [],
  "created_at"  : "2012-03-04 18:02:00 +0800",
  "updated_at"  : "2012-03-04 18:02:00 +0800",
  "links"       : {
                    "self"    : "/statuses/82/replies/100",
                    "status"  : "/statuses/82",
                    "user"    : "/users/8"
                  }
}

Replying to a status
====================
Request:
POST /statuses/:status_id/replies

{ "text" : " a reply" }

Reponse:
201 Created

Updating an existing reply
==========================
PUT /statuses/:status_id/replies/:reply_id

{ 
  "id"          : 100,
  "text"        : "another reply [updated]"
  "user_id"     : 8,
}

Response:
200 OK

{ 
  "id"          : 100,
  "text"        : "another reply [updated]"
  "user_id"     : 8,
  "mentions"    : [],
  "attachments" : [],
  "created_at"  : "2012-03-04 18:02:00 +0800",
  "updated_at"  : "2012-03-04 18:07:00 +0800",
  "links"       : {
                    "self"    : "/statuses/82/replies/100",
                    "status"  : "/statuses/82",
                    "user"    : "/users/8"
                  }
}

Deleting an existing reply
==========================
DELETE /statuses/:status_id/replies/:reply_id

204 No Content

@pending
Accessing statuses that mention you
===================================
GET /statuses/mentions

201 OK

[
  { 
    "id"          : 53,
    "text"        : "mentioning @LuoRunZhi",
    "user_id"     : 1,
    "replies"     : [],
    "attachments" : [],
    "mentions"    : ["/users/8"],
    "created_at"  : "2012-03-04 16:56:00 +0800",
    "updated_at"  : "2012-03-04 16:56:00 +0800",
    "links"       : {
                      "self"    : "/statuses/53",
                      "replies" : "/statuses/53/replies",
                      "user"    : "/users/1"
                    }
  },
  {
    "id"          : 56,
    "text"        : "I'm also mentioning @LuoRunZhi",
    "user_id"     : 3,
    "replies"     : [],
    "attachments" : [],
    "mentions"    : ["/users/8"],
    "created_at"  : "2012-03-04 18:56:00 +0800",
    "updated_at"  : "2012-03-04 18:56:00 +0800",
    "links"       : {
                      "self"    : "/statuses/56",
                      "replies" : "/statuses/56/replies",
                      "users"   : "/users/3"
                    }
  }
]

@pending
Accessing replies to your statuses, and replies to status you have replied
==========================================================================
GET /statuses/replies

@pending
Accessing statuses with attachments (files or images)
====================================================
GET /statuses/attachments

[
  { 
    "id"          : 82,
    "text"        : "An interesting report",
    "user_id"     : 1,
    "replies"     : [],
    "mentions"    : [],
    "attachments  : [],
    "created_at"  : "2012-03-04 16:56:00 +0800",
    "updated_at"  : "2012-03-04 16:56:00 +0800",
    "links"       : {
                      "self"    : "/statuses/82",
                      "replies" : "/statuses/82/replies",
                      "user"    : "/users/1"
                    }
  },
  {
    "id"          : 89,
    "text"        : "This document is also important",
    "user_id"     : 3,
    "replies"     : [],
    "attachments  : [],
    "created_at"  : "2012-03-04 18:89:00 +0800",
    "updated_at"  : "2012-03-04 18:89:00 +0800",
    "links"       : {
                      "self"    : "/statuses/89",
                      "replies" : "/statuses/89/replies",
                      "user"    : "/users/3"
                    }
  }
]
