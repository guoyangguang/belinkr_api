Accessing a list of the people in this account
=================================================
GET /users

Accessing the profile of a specific user
========================================
GET /users/:user_id

Editing your profile
====================
PUT /users/:user_id

Following a user
================
POST /users/:user_id/followers

Unfollowing a user
==================
DELETE /users/:user_id/followers/:follower_id

Acessing the public timeline of a specific user
===============================================
GET /users/:user_id/statuses

Accessing a list of the followers of a specific user
=======================================================
GET /users/:user_id/followers

Accessing a list of the people a specific user is following
==============================================================
GET /users/:user_id/following

