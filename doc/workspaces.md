Introduction
============
Workspaces 

Accessing a list of workspaces
=================================
Pre-requisites
You must autheticate first. See section xxxx for authentication

Request:
GET /workspaces

Response:
200 OK
[
  { 
    "id"          : 52,
    "name"        : "workspace for project 1",
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"            : "/workspaces/52",
                      "invitations"     : "/workspaces/52/invitations",
                      "users"           : "/workspaces/52/users",
                      "collaborators"   : "/workspaces/52/administrators",
                      "administrators"  : "/workspaces/52/collaborators",
                      "boards"          : "/workspaces/52/boards"
                    }
  },
  { 
    "id"          : 58,
    "name"        : "workspace for project 2"
    "created_at"  : "2012-03-04 18:56:00 +0800",
    "updated_at"  : "2012-03-04 18:56:00 +0800",
    "links"       : {
                      "self"            : "/workspaces/58",
                      "invitations"     : "/workspaces/58/invitations",
                      "users"           : "/workspaces/58/users",
                      "collaborators"   : "/workspaces/58/administrators",
                      "administrators"  : "/workspaces/58/collaborators",
                      "boards"          : "/workspaces/58/boards"
                    }
  }
]

Accessing a specific workspace
=====================
Request:
GET /workspaces/:workspace_id

Response:
200 OK

{ 
  "id"          : 52,
  "name"        : "workspace for project 1",
  "created_at"  : "2012-03-04 15:56:00 +0800",
  "updated_at"  : "2012-03-04 15:56:00 +0800",
  "links"       : {
                    "self"            : "/workspaces/52",
                    "invitations"     : "/workspaces/52/invitations",
                    "users"           : "/workspaces/52/users",
                    "collaborators"   : "/workspaces/52/administrators",
                    "administrators"  : "/workspaces/52/collaborators",
                    "boards"          : "/workspaces/52/boards"
                  }
}

Creating a workspace
====================

{ 
  "id"          : 58,
  "name"        : "workspace for project 2"
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"            : "/workspaces/58",
                    "invitations"     : "/workspaces/58/invitations",
                    "users"           : "/workspaces/58/users",
                    "collaborators"   : "/workspaces/58/administrators",
                    "administrators"  : "/workspaces/58/collaborators",
                    "boards"          : "/workspaces/58/boards"
                  }
}

Updating the details of a workspace
===================================

{ 
  "id"          : 58,
  "name"        : "workspace for project 2"
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"            : "/workspaces/58",
                    "invitations"     : "/workspaces/58/invitations",
                    "users"           : "/workspaces/58/users",
                    "collaborators"   : "/workspaces/58/administrators",
                    "administrators"  : "/workspaces/58/collaborators",
                    "boards"          : "/workspaces/58/boards"
                  }
}

Deleting a workspace
====================
DELETE /workspaces/:workspace_id


Requesting access to a workspace
================================
Request:
POST /workspaces/:workspace_id/autoinvitations
{ "requester_id"  : 3 }

Response:
{
  "id"          : 88,
  "requester_id"  : 3,
  "state"       : "requested",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"          : "/workspaces/58/autoinvitations/88",
                    "workspace"     : "/workspaces/58",
                    "requester_id"  : "/users/3",
                  }
}

Allowing a request for access to workspace
==========================================
POST /workspaces/:workspace_id/autoinvitations/allowed

{
  "id"          : 88,
  "requester_id"  : 3,
  "state"       : "requested",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"          : "/workspaces/58/autoinvitations/88",
                    "workspace"     : "/workspaces/58",
                    "requester_id"  : "/users/3",
                  }
}

Response
{
  "id"          : 88,
  "requester_id"  : 3,
  "state"       : "allowed",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"          : "/workspaces/58/autoinvitations/88",
                    "workspace"     : "/workspaces/58",
                    "requester_id"  : "/users/3",
                  }
}


Denying a request for access to workspace
==========================================
POST /workspaces/:workspace_id/autoinvitations/denied

{
  "id"          : 88,
  "requester_id"  : 3,
  "state"       : "requested",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"          : "/workspaces/58/autoinvitations/88",
                    "workspace"     : "/workspaces/58",
                    "requester_id"  : "/users/3",
                  }
}

Response
{
  "id"          : 88,
  "requester_id"  : 3,
  "state"       : "denied",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"          : "/workspaces/58/autoinvitations/88",
                    "workspace"     : "/workspaces/58",
                    "requester_id"  : "/users/3",
                  }
}

Accessing all requests for access to a workspace
================================================
Request:
GET /workspace/:workspace_id/autoinvitations

Response:
200 OK
[
  {
    "id"          : 88,
    "requester_id"  : 3,
    "state"       : "requested",
    "created_at"  : "2012-03-04 18:56:00 +0800",
    "updated_at"  : "2012-03-04 18:56:00 +0800",
    "links"       : {
                      "self"          : "/workspaces/58/autoinvitations/88",
                      "workspace"     : "/workspaces/58",
                      "requester_id"  : "/users/3",
                    }
  },
  {
    "id"          : 88,
    "requester_id"  : 3,
    "state"       : "denied",
    "created_at"  : "2012-03-04 18:56:00 +0800",
    "updated_at"  : "2012-03-04 18:56:00 +0800",
    "links"       : {
                      "self"          : "/workspaces/58/autoinvitations/88",
                      "workspace"     : "/workspaces/58",
                      "requester_id"  : "/users/3",
                    }
  }
]

Inviting a person to a workspace
================================
Request:
POST /workspaces/:workspace_id/invitations

{
  "inviter_id"  : 2,
  "invited_id"  : 3
}

Response:
201 Created

{
  "id"          : 88,
  "inviter_id"  : 2,
  "invited_id"  : 3,
  "state"       : "pending",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 18:56:00 +0800",
  "links"       : {
                    "self"      : "/workspaces/58/invitations/88",
                    "workspace" : "/workspaces/58",
                    "inviter"   : "/users/2",
                    "invited"   : "/users/3"
                  }
}

Accepting an invitation to a workspace
======================================
POST /workspaces/:workspace_id/invitations/:invitation_id/accepted

{
  "id"          : 88,
  "inviter_id"  : 2,
  "invited_id"  : 3
}

Response:
200 OK

{
  "id"          : 88,
  "inviter_id"  : 2,
  "invited_id"  : 3,
  "state"       : "accepted",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 19:56:00 +0800",
  "links"       : {
                    "self"      : "/workspaces/58/invitations/88",
                    "workspace" : "/workspaces/58",
                    "inviter"   : "/users/2",
                    "invited"   : "/users/3"
                  }
}

Rejecting an invitation to a workspace
======================================
Request: 
POST /workspaces/:workspace_id/invitations/:invitation_id/rejected

Response:
201 Created

{
  "id"          : 88,
  "inviter_id"  : 2,
  "invited_id"  : 3,
  "state"       : "rejected",
  "created_at"  : "2012-03-04 18:56:00 +0800",
  "updated_at"  : "2012-03-04 19:56:00 +0800",
  "links"       : {
                    "self"      : "/workspaces/58/invitations/88",
                    "workspace" : "/workspaces/58",
                    "inviter"   : "/users/2",
                    "invited"   : "/users/3"
                  }
}

Accessing all invitations to a workspace
========================================
Request:
GET /workspaces/:workspace_id/invitations

Response:
200 OK 

[
  {
    "id"          : 88,
    "inviter_id"  : 2,
    "invited_id"  : 3,
    "state"       : "accepted",
    "created_at"  : "2012-03-04 18:56:00 +0800",
    "updated_at"  : "2012-03-04 19:56:00 +0800",
    "links"       : {
                      "self"      : "/workspaces/58/invitations/88",
                      "workspace" : "/workspaces/58",
                      "inviter"   : "/users/2",
                      "invited"   : "/users/3"
                    }
  },
  {
    "id"          : 88,
    "inviter_id"  : 2,
    "invited_id"  : 3,
    "state"       : "rejected",
    "created_at"  : "2012-03-04 18:56:00 +0800",
    "updated_at"  : "2012-03-04 19:56:00 +0800",
    "links"       : {
                      "self"      : "/workspaces/58/invitations/88",
                      "workspace" : "/workspaces/58",
                      "inviter"   : "/users/2",
                      "invited"   : "/users/3"
                    }
  }
]




Accessing the list of users who participate in a workspace
==========================================================
Request:
GET /workspaces/:workspace_id/users

Response:
200 OK

[
  {
    "id"          : 3,
    "first"       : "John",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "foo@foo.com",
    "department"  : "Finance",
    "position"    : "Accountant",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/3",
                      "timeline"  : "/users/3/statuses",
                      "followers" : "/users/3/followers",
                      "following" : "/users/3/following"
                    }
  },
  {
    "id"          : 4,
    "first"       : "Jane",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "bar@foo.com",
    "department"  : "Research and Development",
    "position"    : "Lead Researcher",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/4"
                      "timeline"  : "/users/4/statuses",
                      "followers" : "/users/4/followers",
                      "following" : "/users/4/following"
                    }
  },
  {
    "id"          : 5,
    "first"       : "James",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "woo@foo.com",
    "department"  : "Sales",
    "position"    : "Account Executive",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/5"
                      "timeline"  : "/users/5/statuses",
                      "followers" : "/users/5/followers",
                      "following" : "/users/5/following"
                    }
  },
  {
    "id"          : 6,
    "first"       : "Jillian",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "foobar@foo.com",
    "department"  : "Sales",
    "position"    : "Sales Manager",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/6"
                      "timeline"  : "/users/6/statuses",
                      "followers" : "/users/6/followers",
                      "following" : "/users/6/following"
                    }
  }
]

Accessing the list of administrators of a workspace
===================================================
Request:
GET /workspaces/:workspace_id/administrators

Response:
200 OK

[
  {
    "id"          : 3,
    "first"       : "John",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "foo@foo.com",
    "department"  : "Finance",
    "position"    : "Accountant",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/3",
                      "timeline"  : "/users/3/statuses",
                      "followers" : "/users/3/followers",
                      "following" : "/users/3/following"
                    }
  },
  {
    "id"          : 4,
    "first"       : "Jane",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "bar@foo.com",
    "department"  : "Research and Development",
    "position"    : "Lead Researcher",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/4"
                      "timeline"  : "/users/4/statuses",
                      "followers" : "/users/4/followers",
                      "following" : "/users/4/following"
                    }
  }
]

Accessing the list of collaborators of a workspace
==================================================
Request:
GET /workspaces/:workspace_id/collaborators

Response:
200 OK

[
  {
    "id"          : 5,
    "first"       : "James",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "woo@foo.com",
    "department"  : "Sales",
    "position"    : "Account Executive",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/5"
                      "timeline"  : "/users/5/statuses",
                      "followers" : "/users/5/followers",
                      "following" : "/users/5/following"
                    }
  },
  {
    "id"          : 6,
    "first"       : "Jillian",
    "last"        : "Doe",
    "language"    : "en",
    "timezone"    : "Asia/Shanghai",
    "email"       : "foobar@foo.com",
    "department"  : "Sales",
    "position"    : "Sales Manager",
    "avatar"      : {
                      "small"   : "/files/"
                      "medium"  : "/files/"
                      "large"   : "/files/"
                    },
    "created_at"  : "2012-03-04 15:56:00 +0800",
    "updated_at"  : "2012-03-04 15:56:00 +0800",
    "links"       : {
                      "self"      : "/users/6"
                      "timeline"  : "/users/6/statuses",
                      "followers" : "/users/6/followers",
                      "following" : "/users/6/following"
                    }
  }
]

Promoting a collaborator to administrator
=========================================
Request:
POST /workspaces/:workspace_id/administrators
{
  "id"          : 6,
  "first"       : "Jillian",
  "last"        : "Doe",
  "email"       : "foobar@foo.com"
}

Response:

200 OK
{
  "id"          : 6,
  "first"       : "Jillian",
  "last"        : "Doe",
  "language"    : "en",
  "timezone"    : "Asia/Shanghai",
  "email"       : "foobar@foo.com",
  "department"  : "Sales",
  "position"    : "Sales Manager",
  "avatar"      : {
                    "small"   : "/files/"
                    "medium"  : "/files/"
                    "large"   : "/files/"
                  },
  "created_at"  : "2012-03-04 15:56:00 +0800",
  "updated_at"  : "2012-03-04 15:56:00 +0800",
  "links"       : {
                    "self"      : "/users/6"
                    "timeline"  : "/users/6/statuses",
                    "followers" : "/users/6/followers",
                    "following" : "/users/6/following"
                  }
}

Demoting an administrator to collaborator
=========================================
Request:
DELETE /workspaces/:workspace_id/administrators/:administrator_id

Response:
200 OK

{
  "id"          : 4,
  "first"       : "Jane",
  "last"        : "Doe",
  "email"       : "bar@foo.com",
  "language"    : "en",
  "timezone"    : "Asia/Shanghai",
  "department"  : "Research and Development",
  "position"    : "Lead Researcher",
  "avatar"      : {
                    "small"   : "/files/"
                    "medium"  : "/files/"
                    "large"   : "/files/"
                  },
  "created_at"  : "2012-03-04 15:56:00 +0800",
  "updated_at"  : "2012-03-04 15:56:00 +0800",
  "links"       : {
                    "self"      : "/users/4"
                    "timeline"  : "/users/4/statuses",
                    "followers" : "/users/4/followers",
                    "following" : "/users/4/following"
                  }
}

Removing a collaborator from a workspace
========================================
Request:
DELETE /workspaces/:workspace_id/collaborators/:collaborator_id

Response:
204 No Content

Leaving a workspace
===================
Request:
DELETE /workspaces/:workspace_id/users/:user_id

Response:
204 No Content

Accessing the timeline of a workspace
=====================================
Reuest:
GET /workspaces/:workspace_id/statuses

Response:
200 OK

[
  { 
    "id"            : 52,
    "text"          : "something important",
    "user_id"       : 1,
    "workspace_id"  : 8,
    "replies"       : [],
    "mentions"      : [],
    "attachments    : [],
    "created_at"    : "2012-03-04 15:56:00 +0800",
    "updated_at"    : "2012-03-04 15:56:00 +0800",
    "links"         : {
                        "self"      : "/workspaces/8/statuses/52",
                        "replies"   : "/workspaces/8/statuses/52/replies",
                        "user"      : "/users/1"
                        "workspace" : "/workspaces/8"
                      }
  },
  {
    "id"            : 55,
    "text"          : "this is interesting",
    "user_id"       : 3,
    "workspace_id"  : 8,
    "replies"       : [],
    "mentions"      : [],
    "attachments    : [],
    "created_at"    : "2012-03-04 15:56:00 +0800",
    "updated_at"    : "2012-03-04 15:56:00 +0800",
    "links"         : {
                        "self"      : "/workspaces/8/statuses/55",
                        "replies"   : "/workspaces/8statuses/55/replies",
                        "user"      : "/users/3"
                        "workspace" : "/workspaces/8"
                      }
  }
]

Accessing a specific status in a workspace
==========================================
Request:
GET /workspaces/:workspace_id/statuses/:status_id

Response:
200 OK

{ 
  "id"            : 52,
  "text"          : "something important",
  "user_id"       : 1,
  "workspace_id"  : 8,
  "replies"       : [],
  "mentions"      : [],
  "attachments    : [],
  "created_at"    : "2012-03-04 15:56:00 +0800",
  "updated_at"    : "2012-03-04 15:56:00 +0800",
  "links"         : {
                      "self"      : "/workspaces/8/statuses/52",
                      "replies"   : "/workspaces/8/statuses/52/replies",
                      "user"      : "/users/1"
                      "workspace" : "/workspaces/8"
                    }
}

Creating a status in a workspace
================================
Request:
POST /workspaces/:workspace_id/statuses

{ "text"        : "a new status" }

Response:
201 Created

{ 
  "id"            : 88,
  "text"          : "a new status",
  "user_id"       : 1,
  "workspace_id"  : 8,
  "replies"       : [],
  "mentions"      : [],
  "attachments"   : [],
  "created_at"    : "2012-03-04 15:56:00 +0800",
  "updated_at"    : "2012-03-04 15:56:00 +0800",
  "links"         : {
                      "self"      : "/workspaces/8/statuses/88",
                      "replies"   : "/workspaces/8/statuses/88/replies",
                      "user"      : "/users/1",
                      "workspace" : "/workspaces/8"
                    }
}

Updating a status in a workspace
================================
Request:
PUT /workspaces/:workspace_id/statuses/:status_id

{ 
  "id"            : 88,
  "text"          : "a new status [updated]",
  "user_id"       : 1,
  "workspace_id"  : 8
}

Response:
200 OK

{ 
  "id"            : 88,
  "text"          : "a new status - [updated]",
  "user_id"       : 1,
  "workspace_id"  : 8,
  "replies"       : [],
  "mentions"      : [],
  "attachments"   : [],
  "created_at"    : "2012-03-04 15:56:00 +0800",
  "updated_at"    : "2012-03-04 16:01:00 +0800",
  "links"         : {
                      "self"      : "/workspaces/8/statuses/88",
                      "replies"   : "/workspaces/8/statuses/88/replies",
                      "user"      : "/users/1",
                      "workspace" : "/workspaces/8"
                    }
}

Deleting a status in a workspace
================================
Request:
DELETE /workspaces/:workspace_id/statuses/:status_id

Response:
204 No Content

Replying to a status
====================
Request:
POST /workspaces/:workspace_id/statuses/:status_id/replies

{ "text" : " a reply" }

Reponse:
201 Created

Accessing replies to a specific status in a workspace
=====================================================
Request:
GET /workspaces/:workspace_id/statuses/:status_id/replies

Response:
200 OK

[
  { 
    "id"            : 87,
    "text"          : "a reply",
    "user_id"       : 5,
    "workspace_id"  : 8,
    "mentions"      : [],
    "attachments"   : [],
    "created_at"    : "2012-03-04 17:56:00 +0800",
    "updated_at"    : "2012-03-04 17:56:00 +0800",
    "links"         : {
                        "self"      : "/workspaces/8/statuses/82/replies/97",
                        "status"    : "/workspaces/8/statuses/82",
                        "user"      : "/users/5",
                        "workspace" : "/workspaces/8"
                      }
  },
  { 
    "id"            : 100,
    "text"          : "another reply"
    "user_id"       : 8,
    "workspace_id"  : 8,
    "mentions"      : [],
    "attachments"   : [],
    "created_at"    : "2012-03-04 18:02:00 +0800",
    "updated_at"    : "2012-03-04 18:02:00 +0800",
    "links"         : {
                        "self"      : "/workspaces/8/statuses/82/replies/100",
                        "status"    : "/workspaces/8/statuses/82/replies/100",
                        "user"      : "/users/8"
                        "workspace" : "/workspaces/8"
                    }
  },
]

  

Accessing a specific reply to a status in a workspace
=====================================================
Request:
GET /workspaces/:workspace_id/statuses/:status_id/replies/:reply_id

Response: 
200 OK

{ 
  "id"            : 100,
  "text"          : "another reply"
  "user_id"       : 8,
  "workspace_id"  : 8,
  "mentions"      : [],
  "attachments"   : [],
  "created_at"    : "2012-03-04 18:02:00 +0800",
  "updated_at"    : "2012-03-04 18:02:00 +0800",
  "links"         : {
                      "self"      : "/workspaces/8/statuses/82/replies/100",
                      "status"    : "/workspaces/8/statuses/82/replies/100",
                      "user"      : "/users/8"
                      "workspace" : "/workspaces/8"
                  }
}

Updating an existing reply to a status in a workspace
=====================================================
PUT /workspaces/:workspace_id/statuses/:status_id/replies/:reply_id

{ 
  "id"            : 100,
  "text"          : "another reply [updated]"
  "user_id"       : 8,
  "workspace_id"  : 8
}

Response:
200 OK

{ 
  "id"            : 100,
  "text"          : "another reply [updated]"
  "user_id"       : 8,
  "workspace_id"  : 8,
  "mentions"      : [],
  "attachments"   : [],
  "created_at"    : "2012-03-04 18:02:00 +0800",
  "updated_at"    : "2012-03-04 18:07:00 +0800",
  "links"         : {
                      "self"      : "/statuses/82/replies/100",
                      "status"    : "/statuses/82/replies/100",
                      "user"      : "/users/8",
                      "workspace" : "/workspaces/8"
                    }
}

Deleting an existing reply to a status in a workspace
=====================================================
DELETE /workspaces/:workspace_id/statuses/:status_id/replies/:reply_id

204 No Content

@pending
Accessing statuses with attachments (files or images) from a workspace
======================================================================
GET /workspaces/:workspace_id/statuses/attachments

[
  { 
    "id"            : 82,
    "text"          : "An interesting report",
    "user_id"       : 1,
    "workspace_id"  : 8,
    "replies"       : [],
    "mentions"      : [],
    "attachments    : [],
    "created_at"    : "2012-03-04 16:56:00 +0800",
    "updated_at"    : "2012-03-04 16:56:00 +0800",
    "links"         : {
                        "self"      : "/workspaces/8/statuses/82",
                        "replies"   : "/workspaces/8/statuses/82/replies",
                        "user"      : "/users/1"
                        "workspace" : "/workspaces/8"
                      }
  },
  {
    "id"            : 89,
    "text"          : "This document is also important",
    "user_id"       : 3,
    "workspace_id"  : 8,
    "replies"       : [],
    "attachments    : [],
    "created_at"    : "2012-03-04 18:89:00 +0800",
    "updated_at"    : "2012-03-04 18:89:00 +0800",
    "links"         : {
                        "self"      : "/statuses/89",
                        "replies"   : "/statuses/89/replies",
                        "user"      : "/users/3",
                        "workspace" : "/workspaces/8"
                      }
  }
]
