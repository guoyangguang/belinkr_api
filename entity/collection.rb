# encoding: utf-8
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Entity
    class Collection
      extend Forwardable
      include Enumerable
      include Aequitas

      MODEL_NAME  = "entities"

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, 
                     :merge, :score

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Entity::Member, storage_key)
      end

      private

      def storage_key
        "entities"
      end
    end # Collection
  end # Entity
end # Belinkr
