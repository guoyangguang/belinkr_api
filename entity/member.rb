# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "../tinto/member"

module Belinkr
  module Entity
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "entity"
 
      attribute :id,          Integer
      attribute :name,        String
      attribute :phone,       String
      attribute :address,     String
      attribute :created_at,  Time
      attribute :updated_at,  Time
      attribute :deleted_at,  Time

      validates_presence_of   :name, :phone, :address
      validates_length_of     :name, min: 2, max: 150
      validates_length_of     :phone, min: 5, max: 20
      validates_length_of     :address, min: 10, max: 300

      def_delegators :@member, :==, :score, :to_json, :read, :save, 
                                :delete, :undelete, :destroy

      alias_method :to_hash, :attributes

      def initialize(*args)
        super(*args)
        @member = Tinto::Member.new self
      end

      def storage_key
        "entities"
      end

      alias_method :search_index, :storage_key
    end # Member
  end # Entity
end # Belinkr
