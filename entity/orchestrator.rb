# encoding: utf-8
require_relative "collection"
require_relative "../user/orchestrator"
require_relative "../credential/orchestrator"
require_relative "../tinto/exceptions"

module Belinkr
  module Entity
    class Orchestrator
      def self.create(entity, credential, user)
        entity.save
        collection = Belinkr::Entity::Collection.new
        collection.add entity

        user.entity_id = entity.id
        Belinkr::Credential::Orchestrator.create(credential ,user)
       
        [entity, credential, user]

      rescue Tinto::Exceptions::InvalidResource => exception
        if entity.valid? && !user.valid?
          $redis.multi do
            collection.remove entity
            delete(entity)
          end
          entity.destroy
        end

        raise exception
      end

      def self.update(entity, updated)
        entity.attributes = updated.attributes
        entity.save
      end

      def self.delete(entity)
        entity.delete
        entity
      end
    end # Orchestrator
  end # Entity
end # Belinkr
