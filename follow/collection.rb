# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Follow
    class Collection
      extend Forwardable
      include Virtus
      include Aequitas
      include Enumerable

      MODEL_NAME  = "follow"
      KINDS       = %w{ followers following }

      attribute :user_id,         Integer
      attribute :entity_id,       Integer
      attribute :kind,            String

      validates_presence_of       :user_id, :entity_id, :kind
      validates_numericalness_of  :user_id, :entity_id
      validates_within            :kind, set: KINDS

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, :merge

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Follow::Member, storage_key) 
      end

      def member_init_params
        { entity_id: entity_id }
      end

      private

      def storage_key
        "entities:#{entity_id}:users:#{user_id}:#{kind}"
      end
    end # Collection
  end # Follow
end # Belinkr
