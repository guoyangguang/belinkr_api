# encoding: utf-8
require_relative "../tinto/exceptions"

module Belinkr
  module Follow
    class Enforcer
      include Tinto::Exceptions

      def self.authorize(user, action, resource)
        new(resource).send :"#{action}_by?", user
      end

      def initialize(resource)
        @resource = resource
      end

      def create_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @resource.entity_id
        raise NotAllowed unless user.id == @resource.follower_id
      end

      def read_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @resource.entity_id
      end

      def delete_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @resource.entity_id
        raise NotAllowed unless user.id == @resource.follower_id
      end

      alias_method :undelete_by?, :delete_by?
      alias_method :destroy_by?,  :delete_by?

    end # Enforcer
  end # Follow
end # Belinkr
