# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "../tinto/member"

module Belinkr
  module Follow
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "follow"
      
      attribute :id,              Integer
      attribute :follower_id,     Integer
      attribute :followed_id,     Integer
      attribute :entity_id,       Integer
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of       :follower_id, :followed_id, :entity_id
      validates_numericalness_of  :follower_id, :followed_id, :entity_id

      def_delegators :@member, :==, :score, :to_json, :read, :save, :delete
      alias_method :to_hash, :attributes

      def initialize(*args)
        super *args
        @member = Tinto::Member.new self
      end

      def storage_key
        "entities:#{entity_id}:follows"
      end
    end # Member
  end # Follow
end # Belinkr
