# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "enforcer"
require_relative "../status/collection"
require_relative "../activity/collection"  

module Belinkr
  module Follow
    class Orchestrator
      def self.create(follow, user)
        follow    = sanitize(follow, user)
        follow.save
        Enforcer.authorize user, :create, follow

        followers = followers_from(follow)
        following = following_from(follow)
        timeline  = follower_timeline_from(follow)
        latest    = latest_statuses_from(follow)
        
        followed  = User::Member.new(
          entity_id: follow.entity_id,
          id:        follow.followed_id      
        )
        activity  = Activity::Member.new(
          actor:  user,
          action: "follow", 
          object: followed
        ).save

        $redis.multi do
          followers.add follow
          following.add follow
          activities.add activity
          timeline.merge latest
        end

        user
      end # .create

      def self.delete(follow, user)
        Enforcer.authorize user, :delete, follow
        followers = followers_from(follow)
        following = following_from(follow)

        $redis.multi do
          follow.delete
          followers.remove follow
          following.remove follow
        end

        user
      end # .delete

      private

      def self.sanitize(follow, user)
        follow.follower_id  = user.id
        follow.entity_id    = user.entity_id
        follow
      end

      def self.followers_from(follow)
        Follow::Collection.new(
          user_id:    follow.followed_id,
          entity_id:  follow.entity_id,
          kind:       "followers"
        )
      end

      def self.following_from(follow)
        Follow::Collection.new(
          user_id:    follow.follower_id,
          entity_id:  follow.entity_id,
          kind:       "following"
        )
      end

      def self.follower_timeline_from(follow)
        Status::Collection.new(
          user_id:    follow.follower_id,
          entity_id:  follow.entity_id,
          kind:       "general"
        )
      end

      def self.latest_statuses_from(follow)
        Status::Collection.new(
          user_id:    follow.followed_id,
          entity_id:  follow.entity_id,
          kind:       "own"
        ).page(0).to_a
      end

      def self.activities
        Activity::Collection.new
      end
    end # Orchestrator
  end # Follow
end # Belinkr
