# encoding: utf-8
require "i18n"
require_relative "tinto/transformer"
require_relative "config"

Belinkr::Config::AVAILABLE_LOCALES.each do |locale|
  I18n.load_path << ["#{File.dirname(__FILE__)}/locales/#{locale}"]
end

I18n.locale = Belinkr::Config::DEFAULT_LOCALE

