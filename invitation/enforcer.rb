# encoding: utf-8
require_relative "../tinto/exceptions"

module Belinkr
  module Invitation
    class Enforcer
      include Tinto::Exceptions

      def self.authorize(user, action, resource)
        new(resource).send :"#{action}_by?", user
      end

      def initialize(resource)
        @resource = resource
      end

      def create_by?(inviter)
        raise NotAllowed unless inviter.id
        raise NotAllowed unless inviter.id        == @resource.inviter_id
        raise NotAllowed unless inviter.entity_id == @resource.entity_id
      end

      alias_method :delete_by?,   :create_by?
      alias_method :undelete_by?, :create_by?
      alias_method :destroy_by?,  :create_by?

      def accept_by?(invited)
        raise NotAllowed unless invited.entity_id == @resource.entity_id
      end
    end # Enforcer
  end # Invitation
end # Belinkr
