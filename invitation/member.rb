# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require "statemachine"
require "digest"
require_relative "../tinto/member"

module Belinkr
  module Invitation
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME        = "invitation"
      MIN_TOKEN_LENGTH  = 20
      STATES            = %w{ pending accepted }

      attribute :id,              Integer
      attribute :token,           String
      attribute :entity_id,       Integer
      attribute :inviter_id,      Integer
      attribute :invited_name,    String
      attribute :invited_email,   String
      attribute :locale,          String
      attribute :state,           String
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of       :token, :entity_id, :inviter_id,
                                  :invited_name, :invited_email, :locale 
      validates_length_of         :invited_name, min: 3, max: 150
      validates_format_of         :invited_email, as: :email_address
      validates_numericalness_of  :entity_id, :inviter_id
      validates_within            :locale, set: 
                                    I18n.available_locales.map(&:to_s)
      validates_with_method       :token, method: :token_is_sha256

      def_delegators :@state_machine, :accept
      def_delegators :@member, :==, :score, :to_json, :read, :save, :update, 
                                :delete, :undelete, :destroy

      alias_method :to_hash, :attributes

      def initialize(*args)
        super(*args)
        @member     = Tinto::Member.new self
        self.token  ||= generate_token

        @state_machine = Statemachine.build do
          state :pending
          state :accepted

          trans :pending, :accept, :accepted
        end

        @state_machine.state = @state if @state
      end

      def pending?
        state == "pending"
      end

      def accepted?
        state == "accepted"
      end

      def storage_key
        "entities:#{entity_id}:invitations"
      end

      def state
        sync_state
        super
      end

      def state=(state)
        @state_machine.state = state if @state_machine
        super(state)
      end

      private

      def sync_state
        self.state = @state_machine.state.to_s
      end

      def generate_token
        Digest::SHA2.hexdigest([Time.now, rand].join)
      end

      def token_is_sha256
        return true if token && token.length > MIN_TOKEN_LENGTH
        message = I18n::t "validation.errors.must_be_sha256",
                          attribute:  I18n::t("invitation.attributes.token")
        return [false, message]
      end
    end # Member
  end # Invitation
end # Belinkr
