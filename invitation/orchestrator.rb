# encoding: utf-8
require "i18n"
require_relative "collection"
require_relative "enforcer"
require_relative "../session/authenticator"
require_relative "../activity/collection"
require_relative "../workers/mailer/message"

module Belinkr
  module Invitation
    class Orchestrator
      BASE_PATH = "https://www.belinkr.com/invitations"

      def self.create(invitation, inviter)
        sanitize(invitation, inviter)
        Enforcer.authorize(inviter, :create, invitation)
        check_not_registered_yet(invitation)

        message = message_for(invitation)
        invitation.save
        
        activity = Activity::Member.new(
          actor:  inviter, 
          action: "invite", 
          # Since invited person doesn't have User::Member yet,
          # we use the invited_name
          object: invitation.invited_name 
        ).save


        $redis.multi do
          invitations_from(invitation).add invitation
          activities.add activity
          Mailer::Message.new(message).queue
        end

        invitation
      end

      def self.accept(invitation, invited)
        sanitize(invitation, invited)
        Enforcer.authorize(invited, :accept, invitation)

        invitation.accept

        activity =  Activity::Member.new(
                      actor: invited, action: "accept", object: invitation
                    ).save

        $redis.multi do
          invited.save
          invitation.save
          activities.add activity
        end

        invitation
      end

      def self.delete(invitation, user)
        Enforcer.authorize(user, :delete, invitation)
        $redis.multi do
          invitation.delete
          Collection.new(entity_id: invitation.entity_id).remove invitation
        end
        invitation
      end

      def self.undelete(invitation, user)
        Enforcer.authorize(user, :undelete, invitation)
        $redis.multi do
          invitation.undelete
          Collection.new(entity_id: invitation.entity_id).add invitation
        end
        invitation
      end

      def self.destroy(invitation, user)
        Enforcer.authorize(user, :destroy, invitation)
        delete(invitation, user).destroy
      end

      def self.sanitize(invitation, user)
        invitation.entity_id  = user.entity_id
      end

      def self.check_not_registered_yet(invitation)
        credential = Authenticator.get(invitation.invited_email)

        if credential && credential.entity_ids.include?(invitation.entity_id)
          message   = "#{invitation.invited_email} " + 
                      I18n::t("validation.errors.already_registered")
          violation = Aequitas::Violation.new invitation, message

          invitation.errors[:invited_email] << violation
          raise Tinto::Exceptions::InvalidResource
        end
      end

      def self.message_for(invitation)
        inviter_name  = inviter_name_from(invitation)

        {
          from:           "#{inviter_name} via belinkr <lp@belinkr.com>",
          to:             invitation.invited_name +
                          " <#{invitation.invited_email}>",
          template:       "invitation",
          locale:         invitation.locale,
          substitutions:  {
                            inviter_name:     inviter_name,
                            invited_name:     invitation.invited_name,
                            entity_name:      "belinkr",
                            invitation_link:  "#{BASE_PATH}/#{invitation.token}"
                          }
        }
      end

      def self.inviter_name_from(invitation)
        inviter = User::Member.new(
                    id:         invitation.inviter_id, 
                    entity_id:  invitation.entity_id
                  )
        "#{inviter.first} #{inviter.last}"
      end

      def self.activities
        Activity::Collection.new
      end

      def self.invitations_from(invitation)
        Invitation::Collection.new(entity_id: invitation.entity_id)
      end
    end # Orchestrator
  end # Invitation
end # Belinkr
