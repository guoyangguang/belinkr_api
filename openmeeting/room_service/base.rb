# encoding: utf-8
require "typhoeus"
require "uri"
require "json"

module Belinkr
  module RoomService
    class Base

      OPENMEETINGS_ROOM_BASE_URL = "http://localhost:5080/openmeetings/services/RoomService"

      def self.addRoomWithModeration(sid, name, type, comment, number, ispublic, appointment, isdemoroom, demotime, ismoderatedroom)
        url = OPENMEETINGS_ROOM_BASE_URL + "/addRoomWithModeration?SID=#{sid}&name=#{name}&roomtypes_id=#{type}&comment=#{comment}&numberOfPartizipants=#{number}&ispublic=#{ispublic}&appointment=#{appointment}&isDemoRoom=#{isdemoroom}&demoTime=#{demotime}&isModeratedRoom=#{ismoderatedroom}"
        Typhoeus::Request.post(url)
      end

      def self.getRoomById(sid, room_id)
        url = OPENMEETINGS_ROOM_BASE_URL + "/getRoomById?SID=#{sid}&rooms_id=#{room_id}"
        Typhoeus::Request.get(url)
      end

      def self.sendInvitationHash(sid, username, message, baseurl, email, subject, room_id, conferencedomain, ispasswordprotected, invitationpass, valid, validfromdate, validfromtime, validtodate, validtotime, language_id, sendmail)
        url = OPENMEETINGS_ROOM_BASE_URL + "/sendInvitationHash?SID=#{sid}&username=#{username}&message=#{message}&baseurl=#{baseurl}&email=#{email}&subject=#{subject}&room_id=#{room_id}&conferencedomain=#{conferencedomain}&isPasswordProtected=#{ispasswordprotected}&invitationpass=#{invitationpass}&valid=#{valid}&validFromDate=#{validfromdate}&validFromTime=#{validfromtime}&validToDate=#{validtodate}&validToTime=#{validtotime}&language_id=#{language_id}&sendMail=#{sendmail}"
        Typhoeus::Request.post(URI.encode(url))
      end
    end
  end
end

