# encoding: utf-8
require "typhoeus"
require "json"

module Belinkr
  module UserService
    class Base

      OPENMEETINGS_USER_BASE_URL = "http://localhost:5080/openmeetings/services/UserService"

      def self.get_session
        url = OPENMEETINGS_USER_BASE_URL+ "/getSession"
        Typhoeus::Request.get(url)
      end

      def self.login_user(sid, username, password)
        url = OPENMEETINGS_USER_BASE_URL + "/loginUser?SID=#{sid}&username=#{username}&userpass=#{password}"
        Typhoeus::Request.post(url)
      end
    
    end
  end
end

