#!/usr/bin/env ruby
require_relative 'defaultDriver'

endpoint_url = ARGV.shift
obj = RoomServicePortType.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   getInvitationHash(parameters)
#
# ARGS
#   parameters      GetInvitationHash - {http://services.axis.openmeetings.org}getInvitationHash
#
# RETURNS
#   parameters      GetInvitationHashResponse - {http://services.axis.openmeetings.org}getInvitationHashResponse
#
parameters = nil
puts obj.getInvitationHash(parameters)

# SYNOPSIS
#   sendInvitationHash(parameters)
#
# ARGS
#   parameters      SendInvitationHash - {http://services.axis.openmeetings.org}sendInvitationHash
#
# RETURNS
#   parameters      SendInvitationHashResponse - {http://services.axis.openmeetings.org}sendInvitationHashResponse
#
parameters = nil
puts obj.sendInvitationHash(parameters)

# SYNOPSIS
#   addRoomWithModerationAndQuestions(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndQuestions - {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestions
#
# RETURNS
#   parameters      AddRoomWithModerationAndQuestionsResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestionsResponse
#
parameters = nil
puts obj.addRoomWithModerationAndQuestions(parameters)

# SYNOPSIS
#   getRooms(parameters)
#
# ARGS
#   parameters      GetRooms - {http://services.axis.openmeetings.org}getRooms
#
# RETURNS
#   parameters      GetRoomsResponse - {http://services.axis.openmeetings.org}getRoomsResponse
#
parameters = nil
puts obj.getRooms(parameters)

# SYNOPSIS
#   getFlvRecordingByRoomId(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByRoomId - {http://services.axis.openmeetings.org}getFlvRecordingByRoomId
#
# RETURNS
#   parameters      GetFlvRecordingByRoomIdResponse - {http://services.axis.openmeetings.org}getFlvRecordingByRoomIdResponse
#
parameters = nil
puts obj.getFlvRecordingByRoomId(parameters)

# SYNOPSIS
#   addRoomWithModerationQuestionsAndAudioType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationQuestionsAndAudioType - {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioType
#
# RETURNS
#   parameters      AddRoomWithModerationQuestionsAndAudioTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationQuestionsAndAudioType(parameters)

# SYNOPSIS
#   sendInvitationHashWithDateObject(parameters)
#
# ARGS
#   parameters      SendInvitationHashWithDateObject - {http://services.axis.openmeetings.org}sendInvitationHashWithDateObject
#
# RETURNS
#   parameters      SendInvitationHashWithDateObjectResponse - {http://services.axis.openmeetings.org}sendInvitationHashWithDateObjectResponse
#
parameters = nil
puts obj.sendInvitationHashWithDateObject(parameters)

# SYNOPSIS
#   kickUser(parameters)
#
# ARGS
#   parameters      KickUser - {http://services.axis.openmeetings.org}kickUser
#
# RETURNS
#   parameters      KickUserResponse - {http://services.axis.openmeetings.org}kickUserResponse
#
parameters = nil
puts obj.kickUser(parameters)

# SYNOPSIS
#   updateRoomWithModeration(parameters)
#
# ARGS
#   parameters      UpdateRoomWithModeration - {http://services.axis.openmeetings.org}updateRoomWithModeration
#
# RETURNS
#   parameters      UpdateRoomWithModerationResponse - {http://services.axis.openmeetings.org}updateRoomWithModerationResponse
#
parameters = nil
puts obj.updateRoomWithModeration(parameters)

# SYNOPSIS
#   addRoomWithModerationAndExternalType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndExternalType - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalType
#
# RETURNS
#   parameters      AddRoomWithModerationAndExternalTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationAndExternalType(parameters)

# SYNOPSIS
#   getRoomCounters(parameters)
#
# ARGS
#   parameters      GetRoomCounters - {http://services.axis.openmeetings.org}getRoomCounters
#
# RETURNS
#   parameters      GetRoomCountersResponse - {http://services.axis.openmeetings.org}getRoomCountersResponse
#
parameters = nil
puts obj.getRoomCounters(parameters)

# SYNOPSIS
#   addRoom(parameters)
#
# ARGS
#   parameters      AddRoom - {http://services.axis.openmeetings.org}addRoom
#
# RETURNS
#   parameters      AddRoomResponse - {http://services.axis.openmeetings.org}addRoomResponse
#
parameters = nil
puts obj.addRoom(parameters)

# SYNOPSIS
#   deleteRoom(parameters)
#
# ARGS
#   parameters      DeleteRoom - {http://services.axis.openmeetings.org}deleteRoom
#
# RETURNS
#   parameters      DeleteRoomResponse - {http://services.axis.openmeetings.org}deleteRoomResponse
#
parameters = nil
puts obj.deleteRoom(parameters)

# SYNOPSIS
#   getRoomsPublic(parameters)
#
# ARGS
#   parameters      GetRoomsPublic - {http://services.axis.openmeetings.org}getRoomsPublic
#
# RETURNS
#   parameters      GetRoomsPublicResponse - {http://services.axis.openmeetings.org}getRoomsPublicResponse
#
parameters = nil
puts obj.getRoomsPublic(parameters)

# SYNOPSIS
#   getFlvRecordingByExternalRoomType(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByExternalRoomType - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomType
#
# RETURNS
#   parameters      GetFlvRecordingByExternalRoomTypeResponse - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeResponse
#
parameters = nil
puts obj.getFlvRecordingByExternalRoomType(parameters)

# SYNOPSIS
#   addRoomWithModerationExternalTypeAndAudioType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationExternalTypeAndAudioType - {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioType
#
# RETURNS
#   parameters      AddRoomWithModerationExternalTypeAndAudioTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationExternalTypeAndAudioType(parameters)

# SYNOPSIS
#   addRoomWithModerationAndExternalTypeAndStartEnd(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndExternalTypeAndStartEnd - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEnd
#
# RETURNS
#   parameters      AddRoomWithModerationAndExternalTypeAndStartEndResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEndResponse
#
parameters = nil
puts obj.addRoomWithModerationAndExternalTypeAndStartEnd(parameters)

# SYNOPSIS
#   getFlvRecordingByExternalRoomTypeByList(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByExternalRoomTypeByList - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByList
#
# RETURNS
#   parameters      GetFlvRecordingByExternalRoomTypeByListResponse - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByListResponse
#
parameters = nil
puts obj.getFlvRecordingByExternalRoomTypeByList(parameters)

# SYNOPSIS
#   addRoomWithModeration(parameters)
#
# ARGS
#   parameters      AddRoomWithModeration - {http://services.axis.openmeetings.org}addRoomWithModeration
#
# RETURNS
#   parameters      AddRoomWithModerationResponse - {http://services.axis.openmeetings.org}addRoomWithModerationResponse
#
parameters = nil
puts obj.addRoomWithModeration(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsers(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsers - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsers
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsers(parameters)

# SYNOPSIS
#   getRoomIdByExternalId(parameters)
#
# ARGS
#   parameters      GetRoomIdByExternalId - {http://services.axis.openmeetings.org}getRoomIdByExternalId
#
# RETURNS
#   parameters      GetRoomIdByExternalIdResponse - {http://services.axis.openmeetings.org}getRoomIdByExternalIdResponse
#
parameters = nil
puts obj.getRoomIdByExternalId(parameters)

# SYNOPSIS
#   updateRoom(parameters)
#
# ARGS
#   parameters      UpdateRoom - {http://services.axis.openmeetings.org}updateRoom
#
# RETURNS
#   parameters      UpdateRoomResponse - {http://services.axis.openmeetings.org}updateRoomResponse
#
parameters = nil
puts obj.updateRoom(parameters)

# SYNOPSIS
#   getRoomTypes(parameters)
#
# ARGS
#   parameters      GetRoomTypes - {http://services.axis.openmeetings.org}getRoomTypes
#
# RETURNS
#   parameters      GetRoomTypesResponse - {http://services.axis.openmeetings.org}getRoomTypesResponse
#
parameters = nil
puts obj.getRoomTypes(parameters)

# SYNOPSIS
#   addMeetingMemberRemindToRoom(parameters)
#
# ARGS
#   parameters      AddMeetingMemberRemindToRoom - {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoom
#
# RETURNS
#   parameters      AddMeetingMemberRemindToRoomResponse - {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoomResponse
#
parameters = nil
puts obj.addMeetingMemberRemindToRoom(parameters)

# SYNOPSIS
#   getRoomWithClientObjectsById(parameters)
#
# ARGS
#   parameters      GetRoomWithClientObjectsById - {http://services.axis.openmeetings.org}getRoomWithClientObjectsById
#
# RETURNS
#   parameters      GetRoomWithClientObjectsByIdResponse - {http://services.axis.openmeetings.org}getRoomWithClientObjectsByIdResponse
#
parameters = nil
puts obj.getRoomWithClientObjectsById(parameters)

# SYNOPSIS
#   getRoomWithCurrentUsersById(parameters)
#
# ARGS
#   parameters      GetRoomWithCurrentUsersById - {http://services.axis.openmeetings.org}getRoomWithCurrentUsersById
#
# RETURNS
#   parameters      GetRoomWithCurrentUsersByIdResponse - {http://services.axis.openmeetings.org}getRoomWithCurrentUsersByIdResponse
#
parameters = nil
puts obj.getRoomWithCurrentUsersById(parameters)

# SYNOPSIS
#   closeRoom(parameters)
#
# ARGS
#   parameters      CloseRoom - {http://services.axis.openmeetings.org}closeRoom
#
# RETURNS
#   parameters      CloseRoomResponse - {http://services.axis.openmeetings.org}closeRoomResponse
#
parameters = nil
puts obj.closeRoom(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsersByList(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsersByList - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByList
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersByListResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsersByList(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsersByListAndType(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsersByListAndType - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndType
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersByListAndTypeResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndTypeResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsersByListAndType(parameters)

# SYNOPSIS
#   getRoomById(parameters)
#
# ARGS
#   parameters      GetRoomById - {http://services.axis.openmeetings.org}getRoomById
#
# RETURNS
#   parameters      GetRoomByIdResponse - {http://services.axis.openmeetings.org}getRoomByIdResponse
#
parameters = nil
puts obj.getRoomById(parameters)

# SYNOPSIS
#   updateRoomWithModerationAndQuestions(parameters)
#
# ARGS
#   parameters      UpdateRoomWithModerationAndQuestions - {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestions
#
# RETURNS
#   parameters      UpdateRoomWithModerationAndQuestionsResponse - {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestionsResponse
#
parameters = nil
puts obj.updateRoomWithModerationAndQuestions(parameters)

# SYNOPSIS
#   addExternalMeetingMemberRemindToRoom(parameters)
#
# ARGS
#   parameters      AddExternalMeetingMemberRemindToRoom - {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoom
#
# RETURNS
#   parameters      AddExternalMeetingMemberRemindToRoomResponse - {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoomResponse
#
parameters = nil
puts obj.addExternalMeetingMemberRemindToRoom(parameters)


endpoint_url = ARGV.shift
obj = RoomServicePortType.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   getInvitationHash(parameters)
#
# ARGS
#   parameters      GetInvitationHash - {http://services.axis.openmeetings.org}getInvitationHash
#
# RETURNS
#   parameters      GetInvitationHashResponse - {http://services.axis.openmeetings.org}getInvitationHashResponse
#
parameters = nil
puts obj.getInvitationHash(parameters)

# SYNOPSIS
#   sendInvitationHash(parameters)
#
# ARGS
#   parameters      SendInvitationHash - {http://services.axis.openmeetings.org}sendInvitationHash
#
# RETURNS
#   parameters      SendInvitationHashResponse - {http://services.axis.openmeetings.org}sendInvitationHashResponse
#
parameters = nil
puts obj.sendInvitationHash(parameters)

# SYNOPSIS
#   addRoomWithModerationAndQuestions(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndQuestions - {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestions
#
# RETURNS
#   parameters      AddRoomWithModerationAndQuestionsResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestionsResponse
#
parameters = nil
puts obj.addRoomWithModerationAndQuestions(parameters)

# SYNOPSIS
#   getRooms(parameters)
#
# ARGS
#   parameters      GetRooms - {http://services.axis.openmeetings.org}getRooms
#
# RETURNS
#   parameters      GetRoomsResponse - {http://services.axis.openmeetings.org}getRoomsResponse
#
parameters = nil
puts obj.getRooms(parameters)

# SYNOPSIS
#   getFlvRecordingByRoomId(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByRoomId - {http://services.axis.openmeetings.org}getFlvRecordingByRoomId
#
# RETURNS
#   parameters      GetFlvRecordingByRoomIdResponse - {http://services.axis.openmeetings.org}getFlvRecordingByRoomIdResponse
#
parameters = nil
puts obj.getFlvRecordingByRoomId(parameters)

# SYNOPSIS
#   addRoomWithModerationQuestionsAndAudioType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationQuestionsAndAudioType - {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioType
#
# RETURNS
#   parameters      AddRoomWithModerationQuestionsAndAudioTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationQuestionsAndAudioType(parameters)

# SYNOPSIS
#   sendInvitationHashWithDateObject(parameters)
#
# ARGS
#   parameters      SendInvitationHashWithDateObject - {http://services.axis.openmeetings.org}sendInvitationHashWithDateObject
#
# RETURNS
#   parameters      SendInvitationHashWithDateObjectResponse - {http://services.axis.openmeetings.org}sendInvitationHashWithDateObjectResponse
#
parameters = nil
puts obj.sendInvitationHashWithDateObject(parameters)

# SYNOPSIS
#   kickUser(parameters)
#
# ARGS
#   parameters      KickUser - {http://services.axis.openmeetings.org}kickUser
#
# RETURNS
#   parameters      KickUserResponse - {http://services.axis.openmeetings.org}kickUserResponse
#
parameters = nil
puts obj.kickUser(parameters)

# SYNOPSIS
#   updateRoomWithModeration(parameters)
#
# ARGS
#   parameters      UpdateRoomWithModeration - {http://services.axis.openmeetings.org}updateRoomWithModeration
#
# RETURNS
#   parameters      UpdateRoomWithModerationResponse - {http://services.axis.openmeetings.org}updateRoomWithModerationResponse
#
parameters = nil
puts obj.updateRoomWithModeration(parameters)

# SYNOPSIS
#   addRoomWithModerationAndExternalType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndExternalType - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalType
#
# RETURNS
#   parameters      AddRoomWithModerationAndExternalTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationAndExternalType(parameters)

# SYNOPSIS
#   getRoomCounters(parameters)
#
# ARGS
#   parameters      GetRoomCounters - {http://services.axis.openmeetings.org}getRoomCounters
#
# RETURNS
#   parameters      GetRoomCountersResponse - {http://services.axis.openmeetings.org}getRoomCountersResponse
#
parameters = nil
puts obj.getRoomCounters(parameters)

# SYNOPSIS
#   addRoom(parameters)
#
# ARGS
#   parameters      AddRoom - {http://services.axis.openmeetings.org}addRoom
#
# RETURNS
#   parameters      AddRoomResponse - {http://services.axis.openmeetings.org}addRoomResponse
#
parameters = nil
puts obj.addRoom(parameters)

# SYNOPSIS
#   deleteRoom(parameters)
#
# ARGS
#   parameters      DeleteRoom - {http://services.axis.openmeetings.org}deleteRoom
#
# RETURNS
#   parameters      DeleteRoomResponse - {http://services.axis.openmeetings.org}deleteRoomResponse
#
parameters = nil
puts obj.deleteRoom(parameters)

# SYNOPSIS
#   getRoomsPublic(parameters)
#
# ARGS
#   parameters      GetRoomsPublic - {http://services.axis.openmeetings.org}getRoomsPublic
#
# RETURNS
#   parameters      GetRoomsPublicResponse - {http://services.axis.openmeetings.org}getRoomsPublicResponse
#
parameters = nil
puts obj.getRoomsPublic(parameters)

# SYNOPSIS
#   getFlvRecordingByExternalRoomType(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByExternalRoomType - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomType
#
# RETURNS
#   parameters      GetFlvRecordingByExternalRoomTypeResponse - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeResponse
#
parameters = nil
puts obj.getFlvRecordingByExternalRoomType(parameters)

# SYNOPSIS
#   addRoomWithModerationExternalTypeAndAudioType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationExternalTypeAndAudioType - {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioType
#
# RETURNS
#   parameters      AddRoomWithModerationExternalTypeAndAudioTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationExternalTypeAndAudioType(parameters)

# SYNOPSIS
#   addRoomWithModerationAndExternalTypeAndStartEnd(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndExternalTypeAndStartEnd - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEnd
#
# RETURNS
#   parameters      AddRoomWithModerationAndExternalTypeAndStartEndResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEndResponse
#
parameters = nil
puts obj.addRoomWithModerationAndExternalTypeAndStartEnd(parameters)

# SYNOPSIS
#   getFlvRecordingByExternalRoomTypeByList(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByExternalRoomTypeByList - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByList
#
# RETURNS
#   parameters      GetFlvRecordingByExternalRoomTypeByListResponse - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByListResponse
#
parameters = nil
puts obj.getFlvRecordingByExternalRoomTypeByList(parameters)

# SYNOPSIS
#   addRoomWithModeration(parameters)
#
# ARGS
#   parameters      AddRoomWithModeration - {http://services.axis.openmeetings.org}addRoomWithModeration
#
# RETURNS
#   parameters      AddRoomWithModerationResponse - {http://services.axis.openmeetings.org}addRoomWithModerationResponse
#
parameters = nil
puts obj.addRoomWithModeration(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsers(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsers - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsers
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsers(parameters)

# SYNOPSIS
#   getRoomIdByExternalId(parameters)
#
# ARGS
#   parameters      GetRoomIdByExternalId - {http://services.axis.openmeetings.org}getRoomIdByExternalId
#
# RETURNS
#   parameters      GetRoomIdByExternalIdResponse - {http://services.axis.openmeetings.org}getRoomIdByExternalIdResponse
#
parameters = nil
puts obj.getRoomIdByExternalId(parameters)

# SYNOPSIS
#   updateRoom(parameters)
#
# ARGS
#   parameters      UpdateRoom - {http://services.axis.openmeetings.org}updateRoom
#
# RETURNS
#   parameters      UpdateRoomResponse - {http://services.axis.openmeetings.org}updateRoomResponse
#
parameters = nil
puts obj.updateRoom(parameters)

# SYNOPSIS
#   getRoomTypes(parameters)
#
# ARGS
#   parameters      GetRoomTypes - {http://services.axis.openmeetings.org}getRoomTypes
#
# RETURNS
#   parameters      GetRoomTypesResponse - {http://services.axis.openmeetings.org}getRoomTypesResponse
#
parameters = nil
puts obj.getRoomTypes(parameters)

# SYNOPSIS
#   addMeetingMemberRemindToRoom(parameters)
#
# ARGS
#   parameters      AddMeetingMemberRemindToRoom - {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoom
#
# RETURNS
#   parameters      AddMeetingMemberRemindToRoomResponse - {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoomResponse
#
parameters = nil
puts obj.addMeetingMemberRemindToRoom(parameters)

# SYNOPSIS
#   getRoomWithClientObjectsById(parameters)
#
# ARGS
#   parameters      GetRoomWithClientObjectsById - {http://services.axis.openmeetings.org}getRoomWithClientObjectsById
#
# RETURNS
#   parameters      GetRoomWithClientObjectsByIdResponse - {http://services.axis.openmeetings.org}getRoomWithClientObjectsByIdResponse
#
parameters = nil
puts obj.getRoomWithClientObjectsById(parameters)

# SYNOPSIS
#   getRoomWithCurrentUsersById(parameters)
#
# ARGS
#   parameters      GetRoomWithCurrentUsersById - {http://services.axis.openmeetings.org}getRoomWithCurrentUsersById
#
# RETURNS
#   parameters      GetRoomWithCurrentUsersByIdResponse - {http://services.axis.openmeetings.org}getRoomWithCurrentUsersByIdResponse
#
parameters = nil
puts obj.getRoomWithCurrentUsersById(parameters)

# SYNOPSIS
#   closeRoom(parameters)
#
# ARGS
#   parameters      CloseRoom - {http://services.axis.openmeetings.org}closeRoom
#
# RETURNS
#   parameters      CloseRoomResponse - {http://services.axis.openmeetings.org}closeRoomResponse
#
parameters = nil
puts obj.closeRoom(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsersByList(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsersByList - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByList
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersByListResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsersByList(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsersByListAndType(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsersByListAndType - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndType
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersByListAndTypeResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndTypeResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsersByListAndType(parameters)

# SYNOPSIS
#   getRoomById(parameters)
#
# ARGS
#   parameters      GetRoomById - {http://services.axis.openmeetings.org}getRoomById
#
# RETURNS
#   parameters      GetRoomByIdResponse - {http://services.axis.openmeetings.org}getRoomByIdResponse
#
parameters = nil
puts obj.getRoomById(parameters)

# SYNOPSIS
#   updateRoomWithModerationAndQuestions(parameters)
#
# ARGS
#   parameters      UpdateRoomWithModerationAndQuestions - {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestions
#
# RETURNS
#   parameters      UpdateRoomWithModerationAndQuestionsResponse - {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestionsResponse
#
parameters = nil
puts obj.updateRoomWithModerationAndQuestions(parameters)

# SYNOPSIS
#   addExternalMeetingMemberRemindToRoom(parameters)
#
# ARGS
#   parameters      AddExternalMeetingMemberRemindToRoom - {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoom
#
# RETURNS
#   parameters      AddExternalMeetingMemberRemindToRoomResponse - {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoomResponse
#
parameters = nil
puts obj.addExternalMeetingMemberRemindToRoom(parameters)


endpoint_url = ARGV.shift
obj = RoomServicePortType.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   getInvitationHash(parameters)
#
# ARGS
#   parameters      GetInvitationHash - {http://services.axis.openmeetings.org}getInvitationHash
#
# RETURNS
#   parameters      GetInvitationHashResponse - {http://services.axis.openmeetings.org}getInvitationHashResponse
#
parameters = nil
puts obj.getInvitationHash(parameters)

# SYNOPSIS
#   sendInvitationHash(parameters)
#
# ARGS
#   parameters      SendInvitationHash - {http://services.axis.openmeetings.org}sendInvitationHash
#
# RETURNS
#   parameters      SendInvitationHashResponse - {http://services.axis.openmeetings.org}sendInvitationHashResponse
#
parameters = nil
puts obj.sendInvitationHash(parameters)

# SYNOPSIS
#   addRoomWithModerationAndQuestions(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndQuestions - {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestions
#
# RETURNS
#   parameters      AddRoomWithModerationAndQuestionsResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestionsResponse
#
parameters = nil
puts obj.addRoomWithModerationAndQuestions(parameters)

# SYNOPSIS
#   getRooms(parameters)
#
# ARGS
#   parameters      GetRooms - {http://services.axis.openmeetings.org}getRooms
#
# RETURNS
#   parameters      GetRoomsResponse - {http://services.axis.openmeetings.org}getRoomsResponse
#
parameters = nil
puts obj.getRooms(parameters)

# SYNOPSIS
#   getFlvRecordingByRoomId(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByRoomId - {http://services.axis.openmeetings.org}getFlvRecordingByRoomId
#
# RETURNS
#   parameters      GetFlvRecordingByRoomIdResponse - {http://services.axis.openmeetings.org}getFlvRecordingByRoomIdResponse
#
parameters = nil
puts obj.getFlvRecordingByRoomId(parameters)

# SYNOPSIS
#   addRoomWithModerationQuestionsAndAudioType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationQuestionsAndAudioType - {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioType
#
# RETURNS
#   parameters      AddRoomWithModerationQuestionsAndAudioTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationQuestionsAndAudioType(parameters)

# SYNOPSIS
#   sendInvitationHashWithDateObject(parameters)
#
# ARGS
#   parameters      SendInvitationHashWithDateObject - {http://services.axis.openmeetings.org}sendInvitationHashWithDateObject
#
# RETURNS
#   parameters      SendInvitationHashWithDateObjectResponse - {http://services.axis.openmeetings.org}sendInvitationHashWithDateObjectResponse
#
parameters = nil
puts obj.sendInvitationHashWithDateObject(parameters)

# SYNOPSIS
#   kickUser(parameters)
#
# ARGS
#   parameters      KickUser - {http://services.axis.openmeetings.org}kickUser
#
# RETURNS
#   parameters      KickUserResponse - {http://services.axis.openmeetings.org}kickUserResponse
#
parameters = nil
puts obj.kickUser(parameters)

# SYNOPSIS
#   updateRoomWithModeration(parameters)
#
# ARGS
#   parameters      UpdateRoomWithModeration - {http://services.axis.openmeetings.org}updateRoomWithModeration
#
# RETURNS
#   parameters      UpdateRoomWithModerationResponse - {http://services.axis.openmeetings.org}updateRoomWithModerationResponse
#
parameters = nil
puts obj.updateRoomWithModeration(parameters)

# SYNOPSIS
#   addRoomWithModerationAndExternalType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndExternalType - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalType
#
# RETURNS
#   parameters      AddRoomWithModerationAndExternalTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationAndExternalType(parameters)

# SYNOPSIS
#   getRoomCounters(parameters)
#
# ARGS
#   parameters      GetRoomCounters - {http://services.axis.openmeetings.org}getRoomCounters
#
# RETURNS
#   parameters      GetRoomCountersResponse - {http://services.axis.openmeetings.org}getRoomCountersResponse
#
parameters = nil
puts obj.getRoomCounters(parameters)

# SYNOPSIS
#   addRoom(parameters)
#
# ARGS
#   parameters      AddRoom - {http://services.axis.openmeetings.org}addRoom
#
# RETURNS
#   parameters      AddRoomResponse - {http://services.axis.openmeetings.org}addRoomResponse
#
parameters = nil
puts obj.addRoom(parameters)

# SYNOPSIS
#   deleteRoom(parameters)
#
# ARGS
#   parameters      DeleteRoom - {http://services.axis.openmeetings.org}deleteRoom
#
# RETURNS
#   parameters      DeleteRoomResponse - {http://services.axis.openmeetings.org}deleteRoomResponse
#
parameters = nil
puts obj.deleteRoom(parameters)

# SYNOPSIS
#   getRoomsPublic(parameters)
#
# ARGS
#   parameters      GetRoomsPublic - {http://services.axis.openmeetings.org}getRoomsPublic
#
# RETURNS
#   parameters      GetRoomsPublicResponse - {http://services.axis.openmeetings.org}getRoomsPublicResponse
#
parameters = nil
puts obj.getRoomsPublic(parameters)

# SYNOPSIS
#   getFlvRecordingByExternalRoomType(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByExternalRoomType - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomType
#
# RETURNS
#   parameters      GetFlvRecordingByExternalRoomTypeResponse - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeResponse
#
parameters = nil
puts obj.getFlvRecordingByExternalRoomType(parameters)

# SYNOPSIS
#   addRoomWithModerationExternalTypeAndAudioType(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationExternalTypeAndAudioType - {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioType
#
# RETURNS
#   parameters      AddRoomWithModerationExternalTypeAndAudioTypeResponse - {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioTypeResponse
#
parameters = nil
puts obj.addRoomWithModerationExternalTypeAndAudioType(parameters)

# SYNOPSIS
#   addRoomWithModerationAndExternalTypeAndStartEnd(parameters)
#
# ARGS
#   parameters      AddRoomWithModerationAndExternalTypeAndStartEnd - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEnd
#
# RETURNS
#   parameters      AddRoomWithModerationAndExternalTypeAndStartEndResponse - {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEndResponse
#
parameters = nil
puts obj.addRoomWithModerationAndExternalTypeAndStartEnd(parameters)

# SYNOPSIS
#   getFlvRecordingByExternalRoomTypeByList(parameters)
#
# ARGS
#   parameters      GetFlvRecordingByExternalRoomTypeByList - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByList
#
# RETURNS
#   parameters      GetFlvRecordingByExternalRoomTypeByListResponse - {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByListResponse
#
parameters = nil
puts obj.getFlvRecordingByExternalRoomTypeByList(parameters)

# SYNOPSIS
#   addRoomWithModeration(parameters)
#
# ARGS
#   parameters      AddRoomWithModeration - {http://services.axis.openmeetings.org}addRoomWithModeration
#
# RETURNS
#   parameters      AddRoomWithModerationResponse - {http://services.axis.openmeetings.org}addRoomWithModerationResponse
#
parameters = nil
puts obj.addRoomWithModeration(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsers(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsers - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsers
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsers(parameters)

# SYNOPSIS
#   getRoomIdByExternalId(parameters)
#
# ARGS
#   parameters      GetRoomIdByExternalId - {http://services.axis.openmeetings.org}getRoomIdByExternalId
#
# RETURNS
#   parameters      GetRoomIdByExternalIdResponse - {http://services.axis.openmeetings.org}getRoomIdByExternalIdResponse
#
parameters = nil
puts obj.getRoomIdByExternalId(parameters)

# SYNOPSIS
#   updateRoom(parameters)
#
# ARGS
#   parameters      UpdateRoom - {http://services.axis.openmeetings.org}updateRoom
#
# RETURNS
#   parameters      UpdateRoomResponse - {http://services.axis.openmeetings.org}updateRoomResponse
#
parameters = nil
puts obj.updateRoom(parameters)

# SYNOPSIS
#   getRoomTypes(parameters)
#
# ARGS
#   parameters      GetRoomTypes - {http://services.axis.openmeetings.org}getRoomTypes
#
# RETURNS
#   parameters      GetRoomTypesResponse - {http://services.axis.openmeetings.org}getRoomTypesResponse
#
parameters = nil
puts obj.getRoomTypes(parameters)

# SYNOPSIS
#   addMeetingMemberRemindToRoom(parameters)
#
# ARGS
#   parameters      AddMeetingMemberRemindToRoom - {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoom
#
# RETURNS
#   parameters      AddMeetingMemberRemindToRoomResponse - {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoomResponse
#
parameters = nil
puts obj.addMeetingMemberRemindToRoom(parameters)

# SYNOPSIS
#   getRoomWithClientObjectsById(parameters)
#
# ARGS
#   parameters      GetRoomWithClientObjectsById - {http://services.axis.openmeetings.org}getRoomWithClientObjectsById
#
# RETURNS
#   parameters      GetRoomWithClientObjectsByIdResponse - {http://services.axis.openmeetings.org}getRoomWithClientObjectsByIdResponse
#
parameters = nil
puts obj.getRoomWithClientObjectsById(parameters)

# SYNOPSIS
#   getRoomWithCurrentUsersById(parameters)
#
# ARGS
#   parameters      GetRoomWithCurrentUsersById - {http://services.axis.openmeetings.org}getRoomWithCurrentUsersById
#
# RETURNS
#   parameters      GetRoomWithCurrentUsersByIdResponse - {http://services.axis.openmeetings.org}getRoomWithCurrentUsersByIdResponse
#
parameters = nil
puts obj.getRoomWithCurrentUsersById(parameters)

# SYNOPSIS
#   closeRoom(parameters)
#
# ARGS
#   parameters      CloseRoom - {http://services.axis.openmeetings.org}closeRoom
#
# RETURNS
#   parameters      CloseRoomResponse - {http://services.axis.openmeetings.org}closeRoomResponse
#
parameters = nil
puts obj.closeRoom(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsersByList(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsersByList - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByList
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersByListResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsersByList(parameters)

# SYNOPSIS
#   getRoomsWithCurrentUsersByListAndType(parameters)
#
# ARGS
#   parameters      GetRoomsWithCurrentUsersByListAndType - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndType
#
# RETURNS
#   parameters      GetRoomsWithCurrentUsersByListAndTypeResponse - {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndTypeResponse
#
parameters = nil
puts obj.getRoomsWithCurrentUsersByListAndType(parameters)

# SYNOPSIS
#   getRoomById(parameters)
#
# ARGS
#   parameters      GetRoomById - {http://services.axis.openmeetings.org}getRoomById
#
# RETURNS
#   parameters      GetRoomByIdResponse - {http://services.axis.openmeetings.org}getRoomByIdResponse
#
parameters = nil
puts obj.getRoomById(parameters)

# SYNOPSIS
#   updateRoomWithModerationAndQuestions(parameters)
#
# ARGS
#   parameters      UpdateRoomWithModerationAndQuestions - {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestions
#
# RETURNS
#   parameters      UpdateRoomWithModerationAndQuestionsResponse - {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestionsResponse
#
parameters = nil
puts obj.updateRoomWithModerationAndQuestions(parameters)

# SYNOPSIS
#   addExternalMeetingMemberRemindToRoom(parameters)
#
# ARGS
#   parameters      AddExternalMeetingMemberRemindToRoom - {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoom
#
# RETURNS
#   parameters      AddExternalMeetingMemberRemindToRoomResponse - {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoomResponse
#
parameters = nil
puts obj.addExternalMeetingMemberRemindToRoom(parameters)


