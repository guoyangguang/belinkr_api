require 'xsd/qname'

# {http://basic.beans.data.app.openmeetings.org/xsd}SearchResult
#   errorId - SOAP::SOAPLong
#   objectName - SOAP::SOAPString
#   records - SOAP::SOAPLong
#   result - (any)
class SearchResult
  attr_accessor :errorId
  attr_accessor :objectName
  attr_accessor :records
  attr_accessor :result

  def initialize(errorId = nil, objectName = nil, records = nil, result = nil)
    @errorId = errorId
    @objectName = objectName
    @records = records
    @result = result
  end
end

# {http://rooms.beans.hibernate.app.openmeetings.org/xsd}Rooms
#   allowUserQuestions - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   comment - SOAP::SOAPString
#   conferencePin - SOAP::SOAPString
#   currentusers - (any)
#   deleted - SOAP::SOAPString
#   demoTime - SOAP::SOAPInt
#   externalRoomId - SOAP::SOAPLong
#   externalRoomType - SOAP::SOAPString
#   isAudioOnly - SOAP::SOAPBoolean
#   isClosed - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   isModeratedRoom - SOAP::SOAPBoolean
#   ispublic - SOAP::SOAPBoolean
#   name - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   redirectURL - SOAP::SOAPString
#   rooms_id - SOAP::SOAPLong
#   roomtype - RoomTypes
#   sipNumber - SOAP::SOAPString
#   starttime - SOAP::SOAPDateTime
#   updatetime - SOAP::SOAPDateTime
class Rooms
  attr_accessor :allowUserQuestions
  attr_accessor :appointment
  attr_accessor :comment
  attr_accessor :conferencePin
  attr_accessor :currentusers
  attr_accessor :deleted
  attr_accessor :demoTime
  attr_accessor :externalRoomId
  attr_accessor :externalRoomType
  attr_accessor :isAudioOnly
  attr_accessor :isClosed
  attr_accessor :isDemoRoom
  attr_accessor :isModeratedRoom
  attr_accessor :ispublic
  attr_accessor :name
  attr_accessor :numberOfPartizipants
  attr_accessor :redirectURL
  attr_accessor :rooms_id
  attr_accessor :roomtype
  attr_accessor :sipNumber
  attr_accessor :starttime
  attr_accessor :updatetime

  def initialize(allowUserQuestions = nil, appointment = nil, comment = nil, conferencePin = nil, currentusers = nil, deleted = nil, demoTime = nil, externalRoomId = nil, externalRoomType = nil, isAudioOnly = nil, isClosed = nil, isDemoRoom = nil, isModeratedRoom = nil, ispublic = nil, name = nil, numberOfPartizipants = nil, redirectURL = nil, rooms_id = nil, roomtype = nil, sipNumber = nil, starttime = nil, updatetime = nil)
    @allowUserQuestions = allowUserQuestions
    @appointment = appointment
    @comment = comment
    @conferencePin = conferencePin
    @currentusers = currentusers
    @deleted = deleted
    @demoTime = demoTime
    @externalRoomId = externalRoomId
    @externalRoomType = externalRoomType
    @isAudioOnly = isAudioOnly
    @isClosed = isClosed
    @isDemoRoom = isDemoRoom
    @isModeratedRoom = isModeratedRoom
    @ispublic = ispublic
    @name = name
    @numberOfPartizipants = numberOfPartizipants
    @redirectURL = redirectURL
    @rooms_id = rooms_id
    @roomtype = roomtype
    @sipNumber = sipNumber
    @starttime = starttime
    @updatetime = updatetime
  end
end

# {http://rooms.beans.hibernate.app.openmeetings.org/xsd}RoomTypes
#   deleted - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   starttime - SOAP::SOAPDateTime
#   updatetime - SOAP::SOAPDateTime
class RoomTypes
  attr_accessor :deleted
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :starttime
  attr_accessor :updatetime

  def initialize(deleted = nil, name = nil, roomtypes_id = nil, starttime = nil, updatetime = nil)
    @deleted = deleted
    @name = name
    @roomtypes_id = roomtypes_id
    @starttime = starttime
    @updatetime = updatetime
  end
end

# {http://services.axis.openmeetings.org}Exception
#   exception - (any)
class C_Exception
  attr_accessor :exception

  def initialize(exception = nil)
    @exception = exception
  end
end

# {http://user.beans.hibernate.app.openmeetings.org/xsd}Users
#   activatehash - SOAP::SOAPString
#   adresses - Adresses
#   age - SOAP::SOAPDateTime
#   availible - SOAP::SOAPInt
#   deleted - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   forceTimeZoneCheck - SOAP::SOAPBoolean
#   language_id - SOAP::SOAPLong
#   lastlogin - SOAP::SOAPDateTime
#   lastname - SOAP::SOAPString
#   lasttrans - SOAP::SOAPLong
#   level_id - SOAP::SOAPLong
#   lieferadressen - Userdata
#   login - SOAP::SOAPString
#   omTimeZone - OmTimeZone
#   organisation_users - Set
#   password - SOAP::SOAPString
#   pictureuri - SOAP::SOAPString
#   rechnungsaddressen - Userdata
#   regdate - SOAP::SOAPDateTime
#   resethash - SOAP::SOAPString
#   sessionData - Sessiondata
#   showContactData - SOAP::SOAPBoolean
#   showContactDataToContacts - SOAP::SOAPBoolean
#   starttime - SOAP::SOAPDateTime
#   status - SOAP::SOAPInt
#   title_id - SOAP::SOAPInt
#   updatetime - SOAP::SOAPDateTime
#   userOffers - SOAP::SOAPString
#   userSearchs - SOAP::SOAPString
#   userSipData - UserSipData
#   user_id - SOAP::SOAPLong
#   usergroups - Usergroups
#   userlevel - Userlevel
class Users
  attr_accessor :activatehash
  attr_accessor :adresses
  attr_accessor :age
  attr_accessor :availible
  attr_accessor :deleted
  attr_accessor :externalUserId
  attr_accessor :externalUserType
  attr_accessor :firstname
  attr_accessor :forceTimeZoneCheck
  attr_accessor :language_id
  attr_accessor :lastlogin
  attr_accessor :lastname
  attr_accessor :lasttrans
  attr_accessor :level_id
  attr_accessor :lieferadressen
  attr_accessor :login
  attr_accessor :omTimeZone
  attr_accessor :organisation_users
  attr_accessor :password
  attr_accessor :pictureuri
  attr_accessor :rechnungsaddressen
  attr_accessor :regdate
  attr_accessor :resethash
  attr_accessor :sessionData
  attr_accessor :showContactData
  attr_accessor :showContactDataToContacts
  attr_accessor :starttime
  attr_accessor :status
  attr_accessor :title_id
  attr_accessor :updatetime
  attr_accessor :userOffers
  attr_accessor :userSearchs
  attr_accessor :userSipData
  attr_accessor :user_id
  attr_accessor :usergroups
  attr_accessor :userlevel

  def initialize(activatehash = nil, adresses = nil, age = nil, availible = nil, deleted = nil, externalUserId = nil, externalUserType = nil, firstname = nil, forceTimeZoneCheck = nil, language_id = nil, lastlogin = nil, lastname = nil, lasttrans = nil, level_id = nil, lieferadressen = nil, login = nil, omTimeZone = nil, organisation_users = nil, password = nil, pictureuri = nil, rechnungsaddressen = nil, regdate = nil, resethash = nil, sessionData = nil, showContactData = nil, showContactDataToContacts = nil, starttime = nil, status = nil, title_id = nil, updatetime = nil, userOffers = nil, userSearchs = nil, userSipData = nil, user_id = nil, usergroups = [], userlevel = nil)
    @activatehash = activatehash
    @adresses = adresses
    @age = age
    @availible = availible
    @deleted = deleted
    @externalUserId = externalUserId
    @externalUserType = externalUserType
    @firstname = firstname
    @forceTimeZoneCheck = forceTimeZoneCheck
    @language_id = language_id
    @lastlogin = lastlogin
    @lastname = lastname
    @lasttrans = lasttrans
    @level_id = level_id
    @lieferadressen = lieferadressen
    @login = login
    @omTimeZone = omTimeZone
    @organisation_users = organisation_users
    @password = password
    @pictureuri = pictureuri
    @rechnungsaddressen = rechnungsaddressen
    @regdate = regdate
    @resethash = resethash
    @sessionData = sessionData
    @showContactData = showContactData
    @showContactDataToContacts = showContactDataToContacts
    @starttime = starttime
    @status = status
    @title_id = title_id
    @updatetime = updatetime
    @userOffers = userOffers
    @userSearchs = userSearchs
    @userSipData = userSipData
    @user_id = user_id
    @usergroups = usergroups
    @userlevel = userlevel
  end
end

# {http://user.beans.hibernate.app.openmeetings.org/xsd}Userdata
#   comment - SOAP::SOAPString
#   data - SOAP::SOAPString
#   data_id - SOAP::SOAPLong
#   data_key - SOAP::SOAPString
#   deleted - SOAP::SOAPString
#   starttime - SOAP::SOAPDateTime
#   updatetime - SOAP::SOAPDateTime
#   user_id - SOAP::SOAPLong
class Userdata
  attr_accessor :comment
  attr_accessor :data
  attr_accessor :data_id
  attr_accessor :data_key
  attr_accessor :deleted
  attr_accessor :starttime
  attr_accessor :updatetime
  attr_accessor :user_id

  def initialize(comment = nil, data = nil, data_id = nil, data_key = nil, deleted = nil, starttime = nil, updatetime = nil, user_id = nil)
    @comment = comment
    @data = data
    @data_id = data_id
    @data_key = data_key
    @deleted = deleted
    @starttime = starttime
    @updatetime = updatetime
    @user_id = user_id
  end
end

# {http://user.beans.hibernate.app.openmeetings.org/xsd}UserSipData
#   authId - SOAP::SOAPString
#   inserted - SOAP::SOAPDateTime
#   updated - SOAP::SOAPDateTime
#   userSipDataId - SOAP::SOAPLong
#   username - SOAP::SOAPString
#   userpass - SOAP::SOAPString
class UserSipData
  attr_accessor :authId
  attr_accessor :inserted
  attr_accessor :updated
  attr_accessor :userSipDataId
  attr_accessor :username
  attr_accessor :userpass

  def initialize(authId = nil, inserted = nil, updated = nil, userSipDataId = nil, username = nil, userpass = nil)
    @authId = authId
    @inserted = inserted
    @updated = updated
    @userSipDataId = userSipDataId
    @username = username
    @userpass = userpass
  end
end

# {http://user.beans.hibernate.app.openmeetings.org/xsd}Usergroups
#   comment - SOAP::SOAPString
#   deleted - SOAP::SOAPString
#   description - SOAP::SOAPString
#   level_id - SOAP::SOAPLong
#   name - SOAP::SOAPString
#   starttime - SOAP::SOAPDateTime
#   updatetime - SOAP::SOAPDateTime
#   user_id - SOAP::SOAPLong
#   usergroup_id - SOAP::SOAPLong
class Usergroups
  attr_accessor :comment
  attr_accessor :deleted
  attr_accessor :description
  attr_accessor :level_id
  attr_accessor :name
  attr_accessor :starttime
  attr_accessor :updatetime
  attr_accessor :user_id
  attr_accessor :usergroup_id

  def initialize(comment = nil, deleted = nil, description = nil, level_id = nil, name = nil, starttime = nil, updatetime = nil, user_id = nil, usergroup_id = nil)
    @comment = comment
    @deleted = deleted
    @description = description
    @level_id = level_id
    @name = name
    @starttime = starttime
    @updatetime = updatetime
    @user_id = user_id
    @usergroup_id = usergroup_id
  end
end

# {http://user.beans.hibernate.app.openmeetings.org/xsd}Userlevel
#   deleted - SOAP::SOAPString
#   description - SOAP::SOAPString
#   level_id - SOAP::SOAPLong
#   starttime - SOAP::SOAPDateTime
#   statuscode - SOAP::SOAPInt
#   updatetime - SOAP::SOAPDateTime
class Userlevel
  attr_accessor :deleted
  attr_accessor :description
  attr_accessor :level_id
  attr_accessor :starttime
  attr_accessor :statuscode
  attr_accessor :updatetime

  def initialize(deleted = nil, description = nil, level_id = nil, starttime = nil, statuscode = nil, updatetime = nil)
    @deleted = deleted
    @description = description
    @level_id = level_id
    @starttime = starttime
    @statuscode = statuscode
    @updatetime = updatetime
  end
end

# {http://services.axis.openmeetings.org/xsd}RoomCountBean
#   maxUser - SOAP::SOAPInt
#   roomCount - SOAP::SOAPInt
#   roomId - SOAP::SOAPLong
#   roomName - SOAP::SOAPString
class RoomCountBean
  attr_accessor :maxUser
  attr_accessor :roomCount
  attr_accessor :roomId
  attr_accessor :roomName

  def initialize(maxUser = nil, roomCount = nil, roomId = nil, roomName = nil)
    @maxUser = maxUser
    @roomCount = roomCount
    @roomId = roomId
    @roomName = roomName
  end
end

# {http://services.axis.openmeetings.org/xsd}RoomReturn
#   created - SOAP::SOAPDateTime
#   creator - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomUser - RoomUser
#   room_id - SOAP::SOAPLong
class RoomReturn
  attr_accessor :created
  attr_accessor :creator
  attr_accessor :name
  attr_accessor :roomUser
  attr_accessor :room_id

  def initialize(created = nil, creator = nil, name = nil, roomUser = [], room_id = nil)
    @created = created
    @creator = creator
    @name = name
    @roomUser = roomUser
    @room_id = room_id
  end
end

# {http://services.axis.openmeetings.org/xsd}RoomUser
#   avsettings - SOAP::SOAPString
#   broadcastId - SOAP::SOAPLong
#   firstname - SOAP::SOAPString
#   isBroadCasting - SOAP::SOAPBoolean
#   lastname - SOAP::SOAPString
#   publicSID - SOAP::SOAPString
class RoomUser
  attr_accessor :avsettings
  attr_accessor :broadcastId
  attr_accessor :firstname
  attr_accessor :isBroadCasting
  attr_accessor :lastname
  attr_accessor :publicSID

  def initialize(avsettings = nil, broadcastId = nil, firstname = nil, isBroadCasting = nil, lastname = nil, publicSID = nil)
    @avsettings = avsettings
    @broadcastId = broadcastId
    @firstname = firstname
    @isBroadCasting = isBroadCasting
    @lastname = lastname
    @publicSID = publicSID
  end
end

# {http://basic.beans.hibernate.app.openmeetings.org/xsd}OmTimeZone
#   frontEndLabel - SOAP::SOAPString
#   ical - SOAP::SOAPString
#   inserted - SOAP::SOAPDateTime
#   jname - SOAP::SOAPString
#   label - SOAP::SOAPString
#   omtimezoneId - SOAP::SOAPLong
#   orderId - SOAP::SOAPInt
class OmTimeZone
  attr_accessor :frontEndLabel
  attr_accessor :ical
  attr_accessor :inserted
  attr_accessor :jname
  attr_accessor :label
  attr_accessor :omtimezoneId
  attr_accessor :orderId

  def initialize(frontEndLabel = nil, ical = nil, inserted = nil, jname = nil, label = nil, omtimezoneId = nil, orderId = nil)
    @frontEndLabel = frontEndLabel
    @ical = ical
    @inserted = inserted
    @jname = jname
    @label = label
    @omtimezoneId = omtimezoneId
    @orderId = orderId
  end
end

# {http://basic.beans.hibernate.app.openmeetings.org/xsd}Sessiondata
#   id - SOAP::SOAPLong
#   language_id - SOAP::SOAPLong
#   organization_id - SOAP::SOAPLong
#   refresh_time - SOAP::SOAPDateTime
#   sessionXml - SOAP::SOAPString
#   session_id - SOAP::SOAPString
#   starttermin_time - SOAP::SOAPDateTime
#   storePermanent - SOAP::SOAPBoolean
#   user_id - SOAP::SOAPLong
class Sessiondata
  attr_accessor :id
  attr_accessor :language_id
  attr_accessor :organization_id
  attr_accessor :refresh_time
  attr_accessor :sessionXml
  attr_accessor :session_id
  attr_accessor :starttermin_time
  attr_accessor :storePermanent
  attr_accessor :user_id

  def initialize(id = nil, language_id = nil, organization_id = nil, refresh_time = nil, sessionXml = nil, session_id = nil, starttermin_time = nil, storePermanent = nil, user_id = nil)
    @id = id
    @language_id = language_id
    @organization_id = organization_id
    @refresh_time = refresh_time
    @sessionXml = sessionXml
    @session_id = session_id
    @starttermin_time = starttermin_time
    @storePermanent = storePermanent
    @user_id = user_id
  end
end

# {http://adresses.beans.hibernate.app.openmeetings.org/xsd}Adresses
#   additionalname - SOAP::SOAPString
#   adresses_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   deleted - SOAP::SOAPString
#   email - SOAP::SOAPString
#   fax - SOAP::SOAPString
#   phone - SOAP::SOAPString
#   starttime - SOAP::SOAPDateTime
#   states - States
#   street - SOAP::SOAPString
#   town - SOAP::SOAPString
#   updatetime - SOAP::SOAPDateTime
#   zip - SOAP::SOAPString
class Adresses
  attr_accessor :additionalname
  attr_accessor :adresses_id
  attr_accessor :comment
  attr_accessor :deleted
  attr_accessor :email
  attr_accessor :fax
  attr_accessor :phone
  attr_accessor :starttime
  attr_accessor :states
  attr_accessor :street
  attr_accessor :town
  attr_accessor :updatetime
  attr_accessor :zip

  def initialize(additionalname = nil, adresses_id = nil, comment = nil, deleted = nil, email = nil, fax = nil, phone = nil, starttime = nil, states = nil, street = nil, town = nil, updatetime = nil, zip = nil)
    @additionalname = additionalname
    @adresses_id = adresses_id
    @comment = comment
    @deleted = deleted
    @email = email
    @fax = fax
    @phone = phone
    @starttime = starttime
    @states = states
    @street = street
    @town = town
    @updatetime = updatetime
    @zip = zip
  end
end

# {http://adresses.beans.hibernate.app.openmeetings.org/xsd}States
#   deleted - SOAP::SOAPString
#   name - SOAP::SOAPString
#   starttime - SOAP::SOAPDateTime
#   state_id - SOAP::SOAPLong
#   updatetime - SOAP::SOAPDateTime
class States
  attr_accessor :deleted
  attr_accessor :name
  attr_accessor :starttime
  attr_accessor :state_id
  attr_accessor :updatetime

  def initialize(deleted = nil, name = nil, starttime = nil, state_id = nil, updatetime = nil)
    @deleted = deleted
    @name = name
    @starttime = starttime
    @state_id = state_id
    @updatetime = updatetime
  end
end

# {http://flvrecord.beans.hibernate.app.openmeetings.org/xsd}FlvRecording
#   alternateDownload - SOAP::SOAPString
#   comment - SOAP::SOAPString
#   creator - Users
#   deleted - SOAP::SOAPString
#   fileHash - SOAP::SOAPString
#   fileName - SOAP::SOAPString
#   fileSize - SOAP::SOAPLong
#   flvHeight - SOAP::SOAPInt
#   flvRecordingId - SOAP::SOAPLong
#   flvRecordingLog - (any)
#   flvRecordingMetaData - (any)
#   flvWidth - SOAP::SOAPInt
#   height - SOAP::SOAPInt
#   inserted - SOAP::SOAPDateTime
#   insertedBy - SOAP::SOAPLong
#   isFolder - SOAP::SOAPBoolean
#   isImage - SOAP::SOAPBoolean
#   isInterview - SOAP::SOAPBoolean
#   isPresentation - SOAP::SOAPBoolean
#   isRecording - SOAP::SOAPBoolean
#   organization_id - SOAP::SOAPLong
#   ownerId - SOAP::SOAPLong
#   parentFileExplorerItemId - SOAP::SOAPLong
#   previewImage - SOAP::SOAPString
#   progressPostProcessing - SOAP::SOAPInt
#   recordEnd - SOAP::SOAPDateTime
#   recordStart - SOAP::SOAPDateTime
#   recorderStreamId - SOAP::SOAPString
#   room - Rooms
#   room_id - SOAP::SOAPLong
#   updated - SOAP::SOAPDateTime
#   width - SOAP::SOAPInt
class FlvRecording
  attr_accessor :alternateDownload
  attr_accessor :comment
  attr_accessor :creator
  attr_accessor :deleted
  attr_accessor :fileHash
  attr_accessor :fileName
  attr_accessor :fileSize
  attr_accessor :flvHeight
  attr_accessor :flvRecordingId
  attr_accessor :flvRecordingLog
  attr_accessor :flvRecordingMetaData
  attr_accessor :flvWidth
  attr_accessor :height
  attr_accessor :inserted
  attr_accessor :insertedBy
  attr_accessor :isFolder
  attr_accessor :isImage
  attr_accessor :isInterview
  attr_accessor :isPresentation
  attr_accessor :isRecording
  attr_accessor :organization_id
  attr_accessor :ownerId
  attr_accessor :parentFileExplorerItemId
  attr_accessor :previewImage
  attr_accessor :progressPostProcessing
  attr_accessor :recordEnd
  attr_accessor :recordStart
  attr_accessor :recorderStreamId
  attr_accessor :room
  attr_accessor :room_id
  attr_accessor :updated
  attr_accessor :width

  def initialize(alternateDownload = nil, comment = nil, creator = nil, deleted = nil, fileHash = nil, fileName = nil, fileSize = nil, flvHeight = nil, flvRecordingId = nil, flvRecordingLog = nil, flvRecordingMetaData = nil, flvWidth = nil, height = nil, inserted = nil, insertedBy = nil, isFolder = nil, isImage = nil, isInterview = nil, isPresentation = nil, isRecording = nil, organization_id = nil, ownerId = nil, parentFileExplorerItemId = nil, previewImage = nil, progressPostProcessing = nil, recordEnd = nil, recordStart = nil, recorderStreamId = nil, room = nil, room_id = nil, updated = nil, width = nil)
    @alternateDownload = alternateDownload
    @comment = comment
    @creator = creator
    @deleted = deleted
    @fileHash = fileHash
    @fileName = fileName
    @fileSize = fileSize
    @flvHeight = flvHeight
    @flvRecordingId = flvRecordingId
    @flvRecordingLog = flvRecordingLog
    @flvRecordingMetaData = flvRecordingMetaData
    @flvWidth = flvWidth
    @height = height
    @inserted = inserted
    @insertedBy = insertedBy
    @isFolder = isFolder
    @isImage = isImage
    @isInterview = isInterview
    @isPresentation = isPresentation
    @isRecording = isRecording
    @organization_id = organization_id
    @ownerId = ownerId
    @parentFileExplorerItemId = parentFileExplorerItemId
    @previewImage = previewImage
    @progressPostProcessing = progressPostProcessing
    @recordEnd = recordEnd
    @recordStart = recordStart
    @recorderStreamId = recorderStreamId
    @room = room
    @room_id = room_id
    @updated = updated
    @width = width
  end
end

# {http://util.java/xsd}Set
#   empty - SOAP::SOAPBoolean
class Set
  attr_accessor :empty

  def initialize(empty = nil)
    @empty = empty
  end
end

# {http://services.axis.openmeetings.org}closeRoom
#   sID - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   status - SOAP::SOAPBoolean
class CloseRoom
  attr_accessor :sID
  attr_accessor :room_id
  attr_accessor :status

  def initialize(sID = nil, room_id = nil, status = nil)
    @sID = sID
    @room_id = room_id
    @status = status
  end
end

# {http://services.axis.openmeetings.org}closeRoomResponse
#   m_return - SOAP::SOAPInt
class CloseRoomResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}kickUser
#   sID_Admin - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
class KickUser
  attr_accessor :sID_Admin
  attr_accessor :room_id

  def initialize(sID_Admin = nil, room_id = nil)
    @sID_Admin = sID_Admin
    @room_id = room_id
  end
end

# {http://services.axis.openmeetings.org}kickUserResponse
#   m_return - SOAP::SOAPBoolean
class KickUserResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoom
#   sID - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   email - SOAP::SOAPString
#   baseUrl - SOAP::SOAPString
#   language_id - SOAP::SOAPLong
#   jNameTimeZone - SOAP::SOAPString
#   invitorName - SOAP::SOAPString
class AddExternalMeetingMemberRemindToRoom
  attr_accessor :sID
  attr_accessor :room_id
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :email
  attr_accessor :baseUrl
  attr_accessor :language_id
  attr_accessor :jNameTimeZone
  attr_accessor :invitorName

  def initialize(sID = nil, room_id = nil, firstname = nil, lastname = nil, email = nil, baseUrl = nil, language_id = nil, jNameTimeZone = nil, invitorName = nil)
    @sID = sID
    @room_id = room_id
    @firstname = firstname
    @lastname = lastname
    @email = email
    @baseUrl = baseUrl
    @language_id = language_id
    @jNameTimeZone = jNameTimeZone
    @invitorName = invitorName
  end
end

# {http://services.axis.openmeetings.org}addExternalMeetingMemberRemindToRoomResponse
#   m_return - SOAP::SOAPLong
class AddExternalMeetingMemberRemindToRoomResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoom
#   sID - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   email - SOAP::SOAPString
#   baseUrl - SOAP::SOAPString
#   language_id - SOAP::SOAPLong
class AddMeetingMemberRemindToRoom
  attr_accessor :sID
  attr_accessor :room_id
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :email
  attr_accessor :baseUrl
  attr_accessor :language_id

  def initialize(sID = nil, room_id = nil, firstname = nil, lastname = nil, email = nil, baseUrl = nil, language_id = nil)
    @sID = sID
    @room_id = room_id
    @firstname = firstname
    @lastname = lastname
    @email = email
    @baseUrl = baseUrl
    @language_id = language_id
  end
end

# {http://services.axis.openmeetings.org}addMeetingMemberRemindToRoomResponse
#   m_return - SOAP::SOAPLong
class AddMeetingMemberRemindToRoomResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoom
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   videoPodWidth - SOAP::SOAPInt
#   videoPodHeight - SOAP::SOAPInt
#   videoPodXPosition - SOAP::SOAPInt
#   videoPodYPosition - SOAP::SOAPInt
#   moderationPanelXPosition - SOAP::SOAPInt
#   showWhiteBoard - SOAP::SOAPBoolean
#   whiteBoardPanelXPosition - SOAP::SOAPInt
#   whiteBoardPanelYPosition - SOAP::SOAPInt
#   whiteBoardPanelHeight - SOAP::SOAPInt
#   whiteBoardPanelWidth - SOAP::SOAPInt
#   showFilesPanel - SOAP::SOAPBoolean
#   filesPanelXPosition - SOAP::SOAPInt
#   filesPanelYPosition - SOAP::SOAPInt
#   filesPanelHeight - SOAP::SOAPInt
#   filesPanelWidth - SOAP::SOAPInt
class AddRoom
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :videoPodWidth
  attr_accessor :videoPodHeight
  attr_accessor :videoPodXPosition
  attr_accessor :videoPodYPosition
  attr_accessor :moderationPanelXPosition
  attr_accessor :showWhiteBoard
  attr_accessor :whiteBoardPanelXPosition
  attr_accessor :whiteBoardPanelYPosition
  attr_accessor :whiteBoardPanelHeight
  attr_accessor :whiteBoardPanelWidth
  attr_accessor :showFilesPanel
  attr_accessor :filesPanelXPosition
  attr_accessor :filesPanelYPosition
  attr_accessor :filesPanelHeight
  attr_accessor :filesPanelWidth

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, videoPodWidth = nil, videoPodHeight = nil, videoPodXPosition = nil, videoPodYPosition = nil, moderationPanelXPosition = nil, showWhiteBoard = nil, whiteBoardPanelXPosition = nil, whiteBoardPanelYPosition = nil, whiteBoardPanelHeight = nil, whiteBoardPanelWidth = nil, showFilesPanel = nil, filesPanelXPosition = nil, filesPanelYPosition = nil, filesPanelHeight = nil, filesPanelWidth = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @videoPodWidth = videoPodWidth
    @videoPodHeight = videoPodHeight
    @videoPodXPosition = videoPodXPosition
    @videoPodYPosition = videoPodYPosition
    @moderationPanelXPosition = moderationPanelXPosition
    @showWhiteBoard = showWhiteBoard
    @whiteBoardPanelXPosition = whiteBoardPanelXPosition
    @whiteBoardPanelYPosition = whiteBoardPanelYPosition
    @whiteBoardPanelHeight = whiteBoardPanelHeight
    @whiteBoardPanelWidth = whiteBoardPanelWidth
    @showFilesPanel = showFilesPanel
    @filesPanelXPosition = filesPanelXPosition
    @filesPanelYPosition = filesPanelYPosition
    @filesPanelHeight = filesPanelHeight
    @filesPanelWidth = filesPanelWidth
  end
end

# {http://services.axis.openmeetings.org}addRoomResponse
#   m_return - SOAP::SOAPLong
class AddRoomResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModeration
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
class AddRoomWithModeration
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationResponse
#   m_return - SOAP::SOAPLong
class AddRoomWithModerationResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalType
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   externalRoomType - SOAP::SOAPString
class AddRoomWithModerationAndExternalType
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :externalRoomType

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, externalRoomType = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @externalRoomType = externalRoomType
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeResponse
#   m_return - SOAP::SOAPLong
class AddRoomWithModerationAndExternalTypeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEnd
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   externalRoomType - SOAP::SOAPString
#   validFromDate - SOAP::SOAPString
#   validFromTime - SOAP::SOAPString
#   validToDate - SOAP::SOAPString
#   validToTime - SOAP::SOAPString
#   isPasswordProtected - SOAP::SOAPBoolean
#   password - SOAP::SOAPString
#   reminderTypeId - SOAP::SOAPLong
#   redirectURL - SOAP::SOAPString
class AddRoomWithModerationAndExternalTypeAndStartEnd
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :externalRoomType
  attr_accessor :validFromDate
  attr_accessor :validFromTime
  attr_accessor :validToDate
  attr_accessor :validToTime
  attr_accessor :isPasswordProtected
  attr_accessor :password
  attr_accessor :reminderTypeId
  attr_accessor :redirectURL

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, externalRoomType = nil, validFromDate = nil, validFromTime = nil, validToDate = nil, validToTime = nil, isPasswordProtected = nil, password = nil, reminderTypeId = nil, redirectURL = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @externalRoomType = externalRoomType
    @validFromDate = validFromDate
    @validFromTime = validFromTime
    @validToDate = validToDate
    @validToTime = validToTime
    @isPasswordProtected = isPasswordProtected
    @password = password
    @reminderTypeId = reminderTypeId
    @redirectURL = redirectURL
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationAndExternalTypeAndStartEndResponse
#   m_return - SOAP::SOAPLong
class AddRoomWithModerationAndExternalTypeAndStartEndResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestions
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   allowUserQuestions - SOAP::SOAPBoolean
class AddRoomWithModerationAndQuestions
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :allowUserQuestions

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, allowUserQuestions = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @allowUserQuestions = allowUserQuestions
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationAndQuestionsResponse
#   m_return - SOAP::SOAPLong
class AddRoomWithModerationAndQuestionsResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioType
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   externalRoomType - SOAP::SOAPString
#   allowUserQuestions - SOAP::SOAPBoolean
#   isAudioOnly - SOAP::SOAPBoolean
class AddRoomWithModerationExternalTypeAndAudioType
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :externalRoomType
  attr_accessor :allowUserQuestions
  attr_accessor :isAudioOnly

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, externalRoomType = nil, allowUserQuestions = nil, isAudioOnly = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @externalRoomType = externalRoomType
    @allowUserQuestions = allowUserQuestions
    @isAudioOnly = isAudioOnly
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationExternalTypeAndAudioTypeResponse
#   m_return - SOAP::SOAPLong
class AddRoomWithModerationExternalTypeAndAudioTypeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioType
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   allowUserQuestions - SOAP::SOAPBoolean
#   isAudioOnly - SOAP::SOAPBoolean
class AddRoomWithModerationQuestionsAndAudioType
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :allowUserQuestions
  attr_accessor :isAudioOnly

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, allowUserQuestions = nil, isAudioOnly = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @allowUserQuestions = allowUserQuestions
    @isAudioOnly = isAudioOnly
  end
end

# {http://services.axis.openmeetings.org}addRoomWithModerationQuestionsAndAudioTypeResponse
#   m_return - SOAP::SOAPLong
class AddRoomWithModerationQuestionsAndAudioTypeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}deleteRoom
#   sID - SOAP::SOAPString
#   rooms_id - SOAP::SOAPLong
class DeleteRoom
  attr_accessor :sID
  attr_accessor :rooms_id

  def initialize(sID = nil, rooms_id = nil)
    @sID = sID
    @rooms_id = rooms_id
  end
end

# {http://services.axis.openmeetings.org}deleteRoomResponse
#   m_return - SOAP::SOAPLong
class DeleteRoomResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRoomIdByExternalId
#   sID - SOAP::SOAPString
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   externalRoomId - SOAP::SOAPLong
#   externalRoomType - SOAP::SOAPString
class GetRoomIdByExternalId
  attr_accessor :sID
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :externalRoomId
  attr_accessor :externalRoomType

  def initialize(sID = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, externalRoomId = nil, externalRoomType = nil)
    @sID = sID
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @externalRoomId = externalRoomId
    @externalRoomType = externalRoomType
  end
end

# {http://services.axis.openmeetings.org}getRoomIdByExternalIdResponse
#   m_return - SOAP::SOAPLong
class GetRoomIdByExternalIdResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}updateRoom
#   sID - SOAP::SOAPString
#   rooms_id - SOAP::SOAPLong
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   videoPodWidth - SOAP::SOAPInt
#   videoPodHeight - SOAP::SOAPInt
#   videoPodXPosition - SOAP::SOAPInt
#   videoPodYPosition - SOAP::SOAPInt
#   moderationPanelXPosition - SOAP::SOAPInt
#   showWhiteBoard - SOAP::SOAPBoolean
#   whiteBoardPanelXPosition - SOAP::SOAPInt
#   whiteBoardPanelYPosition - SOAP::SOAPInt
#   whiteBoardPanelHeight - SOAP::SOAPInt
#   whiteBoardPanelWidth - SOAP::SOAPInt
#   showFilesPanel - SOAP::SOAPBoolean
#   filesPanelXPosition - SOAP::SOAPInt
#   filesPanelYPosition - SOAP::SOAPInt
#   filesPanelHeight - SOAP::SOAPInt
#   filesPanelWidth - SOAP::SOAPInt
#   appointment - SOAP::SOAPBoolean
class UpdateRoom
  attr_accessor :sID
  attr_accessor :rooms_id
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :videoPodWidth
  attr_accessor :videoPodHeight
  attr_accessor :videoPodXPosition
  attr_accessor :videoPodYPosition
  attr_accessor :moderationPanelXPosition
  attr_accessor :showWhiteBoard
  attr_accessor :whiteBoardPanelXPosition
  attr_accessor :whiteBoardPanelYPosition
  attr_accessor :whiteBoardPanelHeight
  attr_accessor :whiteBoardPanelWidth
  attr_accessor :showFilesPanel
  attr_accessor :filesPanelXPosition
  attr_accessor :filesPanelYPosition
  attr_accessor :filesPanelHeight
  attr_accessor :filesPanelWidth
  attr_accessor :appointment

  def initialize(sID = nil, rooms_id = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, videoPodWidth = nil, videoPodHeight = nil, videoPodXPosition = nil, videoPodYPosition = nil, moderationPanelXPosition = nil, showWhiteBoard = nil, whiteBoardPanelXPosition = nil, whiteBoardPanelYPosition = nil, whiteBoardPanelHeight = nil, whiteBoardPanelWidth = nil, showFilesPanel = nil, filesPanelXPosition = nil, filesPanelYPosition = nil, filesPanelHeight = nil, filesPanelWidth = nil, appointment = nil)
    @sID = sID
    @rooms_id = rooms_id
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @videoPodWidth = videoPodWidth
    @videoPodHeight = videoPodHeight
    @videoPodXPosition = videoPodXPosition
    @videoPodYPosition = videoPodYPosition
    @moderationPanelXPosition = moderationPanelXPosition
    @showWhiteBoard = showWhiteBoard
    @whiteBoardPanelXPosition = whiteBoardPanelXPosition
    @whiteBoardPanelYPosition = whiteBoardPanelYPosition
    @whiteBoardPanelHeight = whiteBoardPanelHeight
    @whiteBoardPanelWidth = whiteBoardPanelWidth
    @showFilesPanel = showFilesPanel
    @filesPanelXPosition = filesPanelXPosition
    @filesPanelYPosition = filesPanelYPosition
    @filesPanelHeight = filesPanelHeight
    @filesPanelWidth = filesPanelWidth
    @appointment = appointment
  end
end

# {http://services.axis.openmeetings.org}updateRoomResponse
#   m_return - SOAP::SOAPLong
class UpdateRoomResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}updateRoomWithModeration
#   sID - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
class UpdateRoomWithModeration
  attr_accessor :sID
  attr_accessor :room_id
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom

  def initialize(sID = nil, room_id = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil)
    @sID = sID
    @room_id = room_id
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
  end
end

# {http://services.axis.openmeetings.org}updateRoomWithModerationResponse
#   m_return - SOAP::SOAPLong
class UpdateRoomWithModerationResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestions
#   sID - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   name - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
#   comment - SOAP::SOAPString
#   numberOfPartizipants - SOAP::SOAPLong
#   ispublic - SOAP::SOAPBoolean
#   appointment - SOAP::SOAPBoolean
#   isDemoRoom - SOAP::SOAPBoolean
#   demoTime - SOAP::SOAPInt
#   isModeratedRoom - SOAP::SOAPBoolean
#   allowUserQuestions - SOAP::SOAPBoolean
class UpdateRoomWithModerationAndQuestions
  attr_accessor :sID
  attr_accessor :room_id
  attr_accessor :name
  attr_accessor :roomtypes_id
  attr_accessor :comment
  attr_accessor :numberOfPartizipants
  attr_accessor :ispublic
  attr_accessor :appointment
  attr_accessor :isDemoRoom
  attr_accessor :demoTime
  attr_accessor :isModeratedRoom
  attr_accessor :allowUserQuestions

  def initialize(sID = nil, room_id = nil, name = nil, roomtypes_id = nil, comment = nil, numberOfPartizipants = nil, ispublic = nil, appointment = nil, isDemoRoom = nil, demoTime = nil, isModeratedRoom = nil, allowUserQuestions = nil)
    @sID = sID
    @room_id = room_id
    @name = name
    @roomtypes_id = roomtypes_id
    @comment = comment
    @numberOfPartizipants = numberOfPartizipants
    @ispublic = ispublic
    @appointment = appointment
    @isDemoRoom = isDemoRoom
    @demoTime = demoTime
    @isModeratedRoom = isModeratedRoom
    @allowUserQuestions = allowUserQuestions
  end
end

# {http://services.axis.openmeetings.org}updateRoomWithModerationAndQuestionsResponse
#   m_return - SOAP::SOAPLong
class UpdateRoomWithModerationAndQuestionsResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getInvitationHash
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   isPasswordProtected - SOAP::SOAPBoolean
#   invitationpass - SOAP::SOAPString
#   valid - SOAP::SOAPInt
#   validFromDate - SOAP::SOAPString
#   validFromTime - SOAP::SOAPString
#   validToDate - SOAP::SOAPString
#   validToTime - SOAP::SOAPString
class GetInvitationHash
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :room_id
  attr_accessor :isPasswordProtected
  attr_accessor :invitationpass
  attr_accessor :valid
  attr_accessor :validFromDate
  attr_accessor :validFromTime
  attr_accessor :validToDate
  attr_accessor :validToTime

  def initialize(sID = nil, username = nil, room_id = nil, isPasswordProtected = nil, invitationpass = nil, valid = nil, validFromDate = nil, validFromTime = nil, validToDate = nil, validToTime = nil)
    @sID = sID
    @username = username
    @room_id = room_id
    @isPasswordProtected = isPasswordProtected
    @invitationpass = invitationpass
    @valid = valid
    @validFromDate = validFromDate
    @validFromTime = validFromTime
    @validToDate = validToDate
    @validToTime = validToTime
  end
end

# {http://services.axis.openmeetings.org}getInvitationHashResponse
#   m_return - SOAP::SOAPString
class GetInvitationHashResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}sendInvitationHash
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   message - SOAP::SOAPString
#   baseurl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   subject - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   conferencedomain - SOAP::SOAPString
#   isPasswordProtected - SOAP::SOAPBoolean
#   invitationpass - SOAP::SOAPString
#   valid - SOAP::SOAPInt
#   validFromDate - SOAP::SOAPString
#   validFromTime - SOAP::SOAPString
#   validToDate - SOAP::SOAPString
#   validToTime - SOAP::SOAPString
#   language_id - SOAP::SOAPLong
#   sendMail - SOAP::SOAPBoolean
class SendInvitationHash
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :message
  attr_accessor :baseurl
  attr_accessor :email
  attr_accessor :subject
  attr_accessor :room_id
  attr_accessor :conferencedomain
  attr_accessor :isPasswordProtected
  attr_accessor :invitationpass
  attr_accessor :valid
  attr_accessor :validFromDate
  attr_accessor :validFromTime
  attr_accessor :validToDate
  attr_accessor :validToTime
  attr_accessor :language_id
  attr_accessor :sendMail

  def initialize(sID = nil, username = nil, message = nil, baseurl = nil, email = nil, subject = nil, room_id = nil, conferencedomain = nil, isPasswordProtected = nil, invitationpass = nil, valid = nil, validFromDate = nil, validFromTime = nil, validToDate = nil, validToTime = nil, language_id = nil, sendMail = nil)
    @sID = sID
    @username = username
    @message = message
    @baseurl = baseurl
    @email = email
    @subject = subject
    @room_id = room_id
    @conferencedomain = conferencedomain
    @isPasswordProtected = isPasswordProtected
    @invitationpass = invitationpass
    @valid = valid
    @validFromDate = validFromDate
    @validFromTime = validFromTime
    @validToDate = validToDate
    @validToTime = validToTime
    @language_id = language_id
    @sendMail = sendMail
  end
end

# {http://services.axis.openmeetings.org}sendInvitationHashResponse
#   m_return - SOAP::SOAPString
class SendInvitationHashResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}sendInvitationHashWithDateObject
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   message - SOAP::SOAPString
#   baseurl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   subject - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   conferencedomain - SOAP::SOAPString
#   isPasswordProtected - SOAP::SOAPBoolean
#   invitationpass - SOAP::SOAPString
#   valid - SOAP::SOAPInt
#   fromDate - SOAP::SOAPDateTime
#   toDate - SOAP::SOAPDateTime
#   language_id - SOAP::SOAPLong
#   sendMail - SOAP::SOAPBoolean
class SendInvitationHashWithDateObject
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :message
  attr_accessor :baseurl
  attr_accessor :email
  attr_accessor :subject
  attr_accessor :room_id
  attr_accessor :conferencedomain
  attr_accessor :isPasswordProtected
  attr_accessor :invitationpass
  attr_accessor :valid
  attr_accessor :fromDate
  attr_accessor :toDate
  attr_accessor :language_id
  attr_accessor :sendMail

  def initialize(sID = nil, username = nil, message = nil, baseurl = nil, email = nil, subject = nil, room_id = nil, conferencedomain = nil, isPasswordProtected = nil, invitationpass = nil, valid = nil, fromDate = nil, toDate = nil, language_id = nil, sendMail = nil)
    @sID = sID
    @username = username
    @message = message
    @baseurl = baseurl
    @email = email
    @subject = subject
    @room_id = room_id
    @conferencedomain = conferencedomain
    @isPasswordProtected = isPasswordProtected
    @invitationpass = invitationpass
    @valid = valid
    @fromDate = fromDate
    @toDate = toDate
    @language_id = language_id
    @sendMail = sendMail
  end
end

# {http://services.axis.openmeetings.org}sendInvitationHashWithDateObjectResponse
#   m_return - SOAP::SOAPString
class SendInvitationHashWithDateObjectResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByList
#   sID - SOAP::SOAPString
#   externalRoomType - SOAP::SOAPString
class GetFlvRecordingByExternalRoomTypeByList
  attr_accessor :sID
  attr_accessor :externalRoomType

  def initialize(sID = nil, externalRoomType = nil)
    @sID = sID
    @externalRoomType = externalRoomType
  end
end

# {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeByListResponse
#   m_return - (any)
class GetFlvRecordingByExternalRoomTypeByListResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByList
#   sID - SOAP::SOAPString
#   start - SOAP::SOAPInt
#   max - SOAP::SOAPInt
#   orderby - SOAP::SOAPString
#   asc - SOAP::SOAPBoolean
class GetRoomsWithCurrentUsersByList
  attr_accessor :sID
  attr_accessor :start
  attr_accessor :max
  attr_accessor :orderby
  attr_accessor :asc

  def initialize(sID = nil, start = nil, max = nil, orderby = nil, asc = nil)
    @sID = sID
    @start = start
    @max = max
    @orderby = orderby
    @asc = asc
  end
end

# {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListResponse
#   m_return - (any)
class GetRoomsWithCurrentUsersByListResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndType
#   sID - SOAP::SOAPString
#   start - SOAP::SOAPInt
#   max - SOAP::SOAPInt
#   orderby - SOAP::SOAPString
#   asc - SOAP::SOAPBoolean
#   externalRoomType - SOAP::SOAPString
class GetRoomsWithCurrentUsersByListAndType
  attr_accessor :sID
  attr_accessor :start
  attr_accessor :max
  attr_accessor :orderby
  attr_accessor :asc
  attr_accessor :externalRoomType

  def initialize(sID = nil, start = nil, max = nil, orderby = nil, asc = nil, externalRoomType = nil)
    @sID = sID
    @start = start
    @max = max
    @orderby = orderby
    @asc = asc
    @externalRoomType = externalRoomType
  end
end

# {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersByListAndTypeResponse
#   m_return - (any)
class GetRoomsWithCurrentUsersByListAndTypeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRooms
#   sID - SOAP::SOAPString
#   start - SOAP::SOAPInt
#   max - SOAP::SOAPInt
#   orderby - SOAP::SOAPString
#   asc - SOAP::SOAPBoolean
class GetRooms
  attr_accessor :sID
  attr_accessor :start
  attr_accessor :max
  attr_accessor :orderby
  attr_accessor :asc

  def initialize(sID = nil, start = nil, max = nil, orderby = nil, asc = nil)
    @sID = sID
    @start = start
    @max = max
    @orderby = orderby
    @asc = asc
  end
end

# {http://services.axis.openmeetings.org}getRoomsResponse
#   m_return - SearchResult
class GetRoomsResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRoomsWithCurrentUsers
#   sID - SOAP::SOAPString
#   start - SOAP::SOAPInt
#   max - SOAP::SOAPInt
#   orderby - SOAP::SOAPString
#   asc - SOAP::SOAPBoolean
class GetRoomsWithCurrentUsers
  attr_accessor :sID
  attr_accessor :start
  attr_accessor :max
  attr_accessor :orderby
  attr_accessor :asc

  def initialize(sID = nil, start = nil, max = nil, orderby = nil, asc = nil)
    @sID = sID
    @start = start
    @max = max
    @orderby = orderby
    @asc = asc
  end
end

# {http://services.axis.openmeetings.org}getRoomsWithCurrentUsersResponse
#   m_return - SearchResult
class GetRoomsWithCurrentUsersResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomType
#   sID - SOAP::SOAPString
#   externalRoomType - SOAP::SOAPString
class GetFlvRecordingByExternalRoomType
  attr_accessor :sID
  attr_accessor :externalRoomType

  def initialize(sID = nil, externalRoomType = nil)
    @sID = sID
    @externalRoomType = externalRoomType
  end
end

# {http://services.axis.openmeetings.org}getFlvRecordingByExternalRoomTypeResponse
class GetFlvRecordingByExternalRoomTypeResponse < ::Array
end

# {http://services.axis.openmeetings.org}getFlvRecordingByRoomId
#   sID - SOAP::SOAPString
#   roomId - SOAP::SOAPLong
class GetFlvRecordingByRoomId
  attr_accessor :sID
  attr_accessor :roomId

  def initialize(sID = nil, roomId = nil)
    @sID = sID
    @roomId = roomId
  end
end

# {http://services.axis.openmeetings.org}getFlvRecordingByRoomIdResponse
class GetFlvRecordingByRoomIdResponse < ::Array
end

# {http://services.axis.openmeetings.org}getRoomTypes
#   sID - SOAP::SOAPString
class GetRoomTypes
  attr_accessor :sID

  def initialize(sID = nil)
    @sID = sID
  end
end

# {http://services.axis.openmeetings.org}getRoomTypesResponse
class GetRoomTypesResponse < ::Array
end

# {http://services.axis.openmeetings.org}getRoomById
#   sID - SOAP::SOAPString
#   rooms_id - SOAP::SOAPLong
class GetRoomById
  attr_accessor :sID
  attr_accessor :rooms_id

  def initialize(sID = nil, rooms_id = nil)
    @sID = sID
    @rooms_id = rooms_id
  end
end

# {http://services.axis.openmeetings.org}getRoomByIdResponse
#   m_return - Rooms
class GetRoomByIdResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRoomWithCurrentUsersById
#   sID - SOAP::SOAPString
#   rooms_id - SOAP::SOAPLong
class GetRoomWithCurrentUsersById
  attr_accessor :sID
  attr_accessor :rooms_id

  def initialize(sID = nil, rooms_id = nil)
    @sID = sID
    @rooms_id = rooms_id
  end
end

# {http://services.axis.openmeetings.org}getRoomWithCurrentUsersByIdResponse
#   m_return - Rooms
class GetRoomWithCurrentUsersByIdResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getRoomsPublic
#   sID - SOAP::SOAPString
#   roomtypes_id - SOAP::SOAPLong
class GetRoomsPublic
  attr_accessor :sID
  attr_accessor :roomtypes_id

  def initialize(sID = nil, roomtypes_id = nil)
    @sID = sID
    @roomtypes_id = roomtypes_id
  end
end

# {http://services.axis.openmeetings.org}getRoomsPublicResponse
class GetRoomsPublicResponse < ::Array
end

# {http://services.axis.openmeetings.org}getRoomCounters
#   sID - SOAP::SOAPString
#   roomId1 - SOAP::SOAPInt
#   roomId2 - SOAP::SOAPInt
#   roomId3 - SOAP::SOAPInt
#   roomId4 - SOAP::SOAPInt
#   roomId5 - SOAP::SOAPInt
#   roomId6 - SOAP::SOAPInt
#   roomId7 - SOAP::SOAPInt
#   roomId8 - SOAP::SOAPInt
#   roomId9 - SOAP::SOAPInt
#   roomId10 - SOAP::SOAPInt
class GetRoomCounters
  attr_accessor :sID
  attr_accessor :roomId1
  attr_accessor :roomId2
  attr_accessor :roomId3
  attr_accessor :roomId4
  attr_accessor :roomId5
  attr_accessor :roomId6
  attr_accessor :roomId7
  attr_accessor :roomId8
  attr_accessor :roomId9
  attr_accessor :roomId10

  def initialize(sID = nil, roomId1 = nil, roomId2 = nil, roomId3 = nil, roomId4 = nil, roomId5 = nil, roomId6 = nil, roomId7 = nil, roomId8 = nil, roomId9 = nil, roomId10 = nil)
    @sID = sID
    @roomId1 = roomId1
    @roomId2 = roomId2
    @roomId3 = roomId3
    @roomId4 = roomId4
    @roomId5 = roomId5
    @roomId6 = roomId6
    @roomId7 = roomId7
    @roomId8 = roomId8
    @roomId9 = roomId9
    @roomId10 = roomId10
  end
end

# {http://services.axis.openmeetings.org}getRoomCountersResponse
class GetRoomCountersResponse < ::Array
end

# {http://services.axis.openmeetings.org}getRoomWithClientObjectsById
#   sID - SOAP::SOAPString
#   rooms_id - SOAP::SOAPLong
class GetRoomWithClientObjectsById
  attr_accessor :sID
  attr_accessor :rooms_id

  def initialize(sID = nil, rooms_id = nil)
    @sID = sID
    @rooms_id = rooms_id
  end
end

# {http://services.axis.openmeetings.org}getRoomWithClientObjectsByIdResponse
#   m_return - RoomReturn
class GetRoomWithClientObjectsByIdResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end
