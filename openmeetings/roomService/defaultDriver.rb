require_relative 'default'
require_relative 'defaultMappingRegistry'
require 'soap/rpc/driver'

class RoomServicePortType < ::SOAP::RPC::Driver
  DefaultEndpointUrl = "http://localhost:5080/openmeetings/services/RoomService"

  Methods = [
    [ "urn:getInvitationHash",
      "getInvitationHash",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getInvitationHash"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getInvitationHashResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:sendInvitationHash",
      "sendInvitationHash",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "sendInvitationHash"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "sendInvitationHashResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoomWithModerationAndQuestions",
      "addRoomWithModerationAndQuestions",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationAndQuestions"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationAndQuestionsResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRooms",
      "getRooms",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRooms"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getFlvRecordingByRoomId",
      "getFlvRecordingByRoomId",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getFlvRecordingByRoomId"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getFlvRecordingByRoomIdResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoomWithModerationQuestionsAndAudioType",
      "addRoomWithModerationQuestionsAndAudioType",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationQuestionsAndAudioType"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationQuestionsAndAudioTypeResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:sendInvitationHashWithDateObject",
      "sendInvitationHashWithDateObject",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "sendInvitationHashWithDateObject"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "sendInvitationHashWithDateObjectResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:kickUser",
      "kickUser",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "kickUser"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "kickUserResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:updateRoomWithModeration",
      "updateRoomWithModeration",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "updateRoomWithModeration"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "updateRoomWithModerationResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoomWithModerationAndExternalType",
      "addRoomWithModerationAndExternalType",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationAndExternalType"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationAndExternalTypeResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomCounters",
      "getRoomCounters",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomCounters"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomCountersResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoom",
      "addRoom",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoom"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:deleteRoom",
      "deleteRoom",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "deleteRoom"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "deleteRoomResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomsPublic",
      "getRoomsPublic",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsPublic"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsPublicResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getFlvRecordingByExternalRoomType",
      "getFlvRecordingByExternalRoomType",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getFlvRecordingByExternalRoomType"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getFlvRecordingByExternalRoomTypeResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoomWithModerationExternalTypeAndAudioType",
      "addRoomWithModerationExternalTypeAndAudioType",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationExternalTypeAndAudioType"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationExternalTypeAndAudioTypeResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoomWithModerationAndExternalTypeAndStartEnd",
      "addRoomWithModerationAndExternalTypeAndStartEnd",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationAndExternalTypeAndStartEnd"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationAndExternalTypeAndStartEndResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getFlvRecordingByExternalRoomTypeByList",
      "getFlvRecordingByExternalRoomTypeByList",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getFlvRecordingByExternalRoomTypeByList"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getFlvRecordingByExternalRoomTypeByListResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addRoomWithModeration",
      "addRoomWithModeration",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModeration"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addRoomWithModerationResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomsWithCurrentUsers",
      "getRoomsWithCurrentUsers",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsWithCurrentUsers"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsWithCurrentUsersResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomIdByExternalId",
      "getRoomIdByExternalId",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomIdByExternalId"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomIdByExternalIdResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:updateRoom",
      "updateRoom",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "updateRoom"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "updateRoomResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomTypes",
      "getRoomTypes",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomTypes"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomTypesResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addMeetingMemberRemindToRoom",
      "addMeetingMemberRemindToRoom",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addMeetingMemberRemindToRoom"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addMeetingMemberRemindToRoomResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomWithClientObjectsById",
      "getRoomWithClientObjectsById",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomWithClientObjectsById"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomWithClientObjectsByIdResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomWithCurrentUsersById",
      "getRoomWithCurrentUsersById",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomWithCurrentUsersById"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomWithCurrentUsersByIdResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:closeRoom",
      "closeRoom",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "closeRoom"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "closeRoomResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomsWithCurrentUsersByList",
      "getRoomsWithCurrentUsersByList",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsWithCurrentUsersByList"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsWithCurrentUsersByListResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomsWithCurrentUsersByListAndType",
      "getRoomsWithCurrentUsersByListAndType",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsWithCurrentUsersByListAndType"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomsWithCurrentUsersByListAndTypeResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getRoomById",
      "getRoomById",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomById"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getRoomByIdResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:updateRoomWithModerationAndQuestions",
      "updateRoomWithModerationAndQuestions",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "updateRoomWithModerationAndQuestions"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "updateRoomWithModerationAndQuestionsResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addExternalMeetingMemberRemindToRoom",
      "addExternalMeetingMemberRemindToRoom",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addExternalMeetingMemberRemindToRoom"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addExternalMeetingMemberRemindToRoomResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]

  def initialize(endpoint_url = nil)
    endpoint_url ||= DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = DefaultMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = DefaultMappingRegistry::LiteralRegistry
    init_methods
  end

private

  def init_methods
    Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name and qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end

