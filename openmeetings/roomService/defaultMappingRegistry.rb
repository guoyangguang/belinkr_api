require_relative 'default'
require 'soap/mapping'

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new
  NsServicesAxisOpenmeetingsOrg = "http://services.axis.openmeetings.org"
  NsXsd = "http://basic.beans.hibernate.app.openmeetings.org/xsd"
  NsXsd_0 = "http://basic.beans.data.app.openmeetings.org/xsd"
  NsXsd_1 = "http://user.beans.hibernate.app.openmeetings.org/xsd"
  NsXsd_2 = "http://services.axis.openmeetings.org/xsd"
  NsXsd_3 = "http://basic.beans.hibernate.app.openmeetings.org/xsd"
  NsXsd_4 = "http://adresses.beans.hibernate.app.openmeetings.org/xsd"
  NsXsd_5 = "http://flvrecord.beans.hibernate.app.openmeetings.org/xsd"
  NsXsd_6 = "http://util.java/xsd"

  EncodedRegistry.register(
    :class => SearchResult,
    :schema_type => XSD::QName.new(NsXsd, "SearchResult"),
    :schema_element => [
      ["errorId", "SOAP::SOAPLong", [0, 1]],
      ["objectName", "SOAP::SOAPString", [0, 1]],
      ["records", "SOAP::SOAPLong", [0, 1]],
      ["result", nil, [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Rooms,
    :schema_type => XSD::QName.new(NsXsd_0, "Rooms"),
    :schema_element => [
      ["allowUserQuestions", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["conferencePin", "SOAP::SOAPString", [0, 1]],
      ["currentusers", nil, [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["externalRoomId", "SOAP::SOAPLong", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]],
      ["isAudioOnly", "SOAP::SOAPBoolean", [0, 1]],
      ["isClosed", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["redirectURL", "SOAP::SOAPString", [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]],
      ["roomtype", "RoomTypes", [0, 1]],
      ["sipNumber", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RoomTypes,
    :schema_type => XSD::QName.new(NsXsd_0, "RoomTypes"),
    :schema_element => [
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => C_Exception,
    :schema_type => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception"),
    :schema_element => [
      ["exception", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception")], [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Users,
    :schema_type => XSD::QName.new(NsXsd_1, "Users"),
    :schema_element => [
      ["activatehash", "SOAP::SOAPString", [0, 1]],
      ["adresses", "Adresses", [0, 1]],
      ["age", "SOAP::SOAPDateTime", [0, 1]],
      ["availible", "SOAP::SOAPInt", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["forceTimeZoneCheck", "SOAP::SOAPBoolean", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["lastlogin", "SOAP::SOAPDateTime", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["lasttrans", "SOAP::SOAPLong", [0, 1]],
      ["level_id", "SOAP::SOAPLong", [0, 1]],
      ["lieferadressen", "Userdata", [0, 1]],
      ["login", "SOAP::SOAPString", [0, 1]],
      ["omTimeZone", "OmTimeZone", [0, 1]],
      ["organisation_users", "Set", [0, 1]],
      ["password", "SOAP::SOAPString", [0, 1]],
      ["pictureuri", "SOAP::SOAPString", [0, 1]],
      ["rechnungsaddressen", "Userdata", [0, 1]],
      ["regdate", "SOAP::SOAPDateTime", [0, 1]],
      ["resethash", "SOAP::SOAPString", [0, 1]],
      ["sessionData", "Sessiondata", [0, 1]],
      ["showContactData", "SOAP::SOAPBoolean", [0, 1]],
      ["showContactDataToContacts", "SOAP::SOAPBoolean", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["status", "SOAP::SOAPInt", [0, 1]],
      ["title_id", "SOAP::SOAPInt", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["userOffers", "SOAP::SOAPString", [0, 1]],
      ["userSearchs", "SOAP::SOAPString", [0, 1]],
      ["userSipData", "UserSipData", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]],
      ["usergroups", "Usergroups[]", [0, nil]],
      ["userlevel", "Userlevel", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Userdata,
    :schema_type => XSD::QName.new(NsXsd_1, "Userdata"),
    :schema_element => [
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["data", "SOAP::SOAPString", [0, 1]],
      ["data_id", "SOAP::SOAPLong", [0, 1]],
      ["data_key", "SOAP::SOAPString", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => UserSipData,
    :schema_type => XSD::QName.new(NsXsd_1, "UserSipData"),
    :schema_element => [
      ["authId", "SOAP::SOAPString", [0, 1]],
      ["inserted", "SOAP::SOAPDateTime", [0, 1]],
      ["updated", "SOAP::SOAPDateTime", [0, 1]],
      ["userSipDataId", "SOAP::SOAPLong", [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["userpass", "SOAP::SOAPString", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Usergroups,
    :schema_type => XSD::QName.new(NsXsd_1, "Usergroups"),
    :schema_element => [
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["description", "SOAP::SOAPString", [0, 1]],
      ["level_id", "SOAP::SOAPLong", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]],
      ["usergroup_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Userlevel,
    :schema_type => XSD::QName.new(NsXsd_1, "Userlevel"),
    :schema_element => [
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["description", "SOAP::SOAPString", [0, 1]],
      ["level_id", "SOAP::SOAPLong", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["statuscode", "SOAP::SOAPInt", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RoomCountBean,
    :schema_type => XSD::QName.new(NsXsd_2, "RoomCountBean"),
    :schema_element => [
      ["maxUser", "SOAP::SOAPInt", [0, 1]],
      ["roomCount", "SOAP::SOAPInt", [0, 1]],
      ["roomId", "SOAP::SOAPLong", [0, 1]],
      ["roomName", "SOAP::SOAPString", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RoomReturn,
    :schema_type => XSD::QName.new(NsXsd_2, "RoomReturn"),
    :schema_element => [
      ["created", "SOAP::SOAPDateTime", [0, 1]],
      ["creator", "SOAP::SOAPString", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomUser", "RoomUser[]", [0, nil]],
      ["room_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => RoomUser,
    :schema_type => XSD::QName.new(NsXsd_2, "RoomUser"),
    :schema_element => [
      ["avsettings", "SOAP::SOAPString", [0, 1]],
      ["broadcastId", "SOAP::SOAPLong", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["isBroadCasting", "SOAP::SOAPBoolean", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["publicSID", "SOAP::SOAPString", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => OmTimeZone,
    :schema_type => XSD::QName.new(NsXsd_3, "OmTimeZone"),
    :schema_element => [
      ["frontEndLabel", "SOAP::SOAPString", [0, 1]],
      ["ical", "SOAP::SOAPString", [0, 1]],
      ["inserted", "SOAP::SOAPDateTime", [0, 1]],
      ["jname", "SOAP::SOAPString", [0, 1]],
      ["label", "SOAP::SOAPString", [0, 1]],
      ["omtimezoneId", "SOAP::SOAPLong", [0, 1]],
      ["orderId", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Sessiondata,
    :schema_type => XSD::QName.new(NsXsd_3, "Sessiondata"),
    :schema_element => [
      ["id", "SOAP::SOAPLong", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["organization_id", "SOAP::SOAPLong", [0, 1]],
      ["refresh_time", "SOAP::SOAPDateTime", [0, 1]],
      ["sessionXml", "SOAP::SOAPString", [0, 1]],
      ["session_id", "SOAP::SOAPString", [0, 1]],
      ["starttermin_time", "SOAP::SOAPDateTime", [0, 1]],
      ["storePermanent", "SOAP::SOAPBoolean", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Adresses,
    :schema_type => XSD::QName.new(NsXsd_4, "Adresses"),
    :schema_element => [
      ["additionalname", "SOAP::SOAPString", [0, 1]],
      ["adresses_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["fax", "SOAP::SOAPString", [0, 1]],
      ["phone", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["states", "States", [0, 1]],
      ["street", "SOAP::SOAPString", [0, 1]],
      ["town", "SOAP::SOAPString", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["zip", "SOAP::SOAPString", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => States,
    :schema_type => XSD::QName.new(NsXsd_4, "States"),
    :schema_element => [
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["state_id", "SOAP::SOAPLong", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => FlvRecording,
    :schema_type => XSD::QName.new(NsXsd_5, "FlvRecording"),
    :schema_element => [
      ["alternateDownload", "SOAP::SOAPString", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["creator", "Users", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["fileHash", "SOAP::SOAPString", [0, 1]],
      ["fileName", "SOAP::SOAPString", [0, 1]],
      ["fileSize", "SOAP::SOAPLong", [0, 1]],
      ["flvHeight", "SOAP::SOAPInt", [0, 1]],
      ["flvRecordingId", "SOAP::SOAPLong", [0, 1]],
      ["flvRecordingLog", nil, [0, 1]],
      ["flvRecordingMetaData", nil, [0, 1]],
      ["flvWidth", "SOAP::SOAPInt", [0, 1]],
      ["height", "SOAP::SOAPInt", [0, 1]],
      ["inserted", "SOAP::SOAPDateTime", [0, 1]],
      ["insertedBy", "SOAP::SOAPLong", [0, 1]],
      ["isFolder", "SOAP::SOAPBoolean", [0, 1]],
      ["isImage", "SOAP::SOAPBoolean", [0, 1]],
      ["isInterview", "SOAP::SOAPBoolean", [0, 1]],
      ["isPresentation", "SOAP::SOAPBoolean", [0, 1]],
      ["isRecording", "SOAP::SOAPBoolean", [0, 1]],
      ["organization_id", "SOAP::SOAPLong", [0, 1]],
      ["ownerId", "SOAP::SOAPLong", [0, 1]],
      ["parentFileExplorerItemId", "SOAP::SOAPLong", [0, 1]],
      ["previewImage", "SOAP::SOAPString", [0, 1]],
      ["progressPostProcessing", "SOAP::SOAPInt", [0, 1]],
      ["recordEnd", "SOAP::SOAPDateTime", [0, 1]],
      ["recordStart", "SOAP::SOAPDateTime", [0, 1]],
      ["recorderStreamId", "SOAP::SOAPString", [0, 1]],
      ["room", "Rooms", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["updated", "SOAP::SOAPDateTime", [0, 1]],
      ["width", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => Set,
    :schema_type => XSD::QName.new(NsXsd_6, "Set"),
    :schema_element => [
      ["empty", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SearchResult,
    :schema_type => XSD::QName.new(NsXsd, "SearchResult"),
    :schema_element => [
      ["errorId", "SOAP::SOAPLong", [0, 1]],
      ["objectName", "SOAP::SOAPString", [0, 1]],
      ["records", "SOAP::SOAPLong", [0, 1]],
      ["result", nil, [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Rooms,
    :schema_type => XSD::QName.new(NsXsd_0, "Rooms"),
    :schema_element => [
      ["allowUserQuestions", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["conferencePin", "SOAP::SOAPString", [0, 1]],
      ["currentusers", nil, [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["externalRoomId", "SOAP::SOAPLong", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]],
      ["isAudioOnly", "SOAP::SOAPBoolean", [0, 1]],
      ["isClosed", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["redirectURL", "SOAP::SOAPString", [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]],
      ["roomtype", "RoomTypes", [0, 1]],
      ["sipNumber", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RoomTypes,
    :schema_type => XSD::QName.new(NsXsd_0, "RoomTypes"),
    :schema_element => [
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => C_Exception,
    :schema_type => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception"),
    :schema_element => [
      ["exception", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Users,
    :schema_type => XSD::QName.new(NsXsd_1, "Users"),
    :schema_element => [
      ["activatehash", "SOAP::SOAPString", [0, 1]],
      ["adresses", "Adresses", [0, 1]],
      ["age", "SOAP::SOAPDateTime", [0, 1]],
      ["availible", "SOAP::SOAPInt", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["forceTimeZoneCheck", "SOAP::SOAPBoolean", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["lastlogin", "SOAP::SOAPDateTime", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["lasttrans", "SOAP::SOAPLong", [0, 1]],
      ["level_id", "SOAP::SOAPLong", [0, 1]],
      ["lieferadressen", "Userdata", [0, 1]],
      ["login", "SOAP::SOAPString", [0, 1]],
      ["omTimeZone", "OmTimeZone", [0, 1]],
      ["organisation_users", "Set", [0, 1]],
      ["password", "SOAP::SOAPString", [0, 1]],
      ["pictureuri", "SOAP::SOAPString", [0, 1]],
      ["rechnungsaddressen", "Userdata", [0, 1]],
      ["regdate", "SOAP::SOAPDateTime", [0, 1]],
      ["resethash", "SOAP::SOAPString", [0, 1]],
      ["sessionData", "Sessiondata", [0, 1]],
      ["showContactData", "SOAP::SOAPBoolean", [0, 1]],
      ["showContactDataToContacts", "SOAP::SOAPBoolean", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["status", "SOAP::SOAPInt", [0, 1]],
      ["title_id", "SOAP::SOAPInt", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["userOffers", "SOAP::SOAPString", [0, 1]],
      ["userSearchs", "SOAP::SOAPString", [0, 1]],
      ["userSipData", "UserSipData", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]],
      ["usergroups", "Usergroups[]", [0, nil]],
      ["userlevel", "Userlevel", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Userdata,
    :schema_type => XSD::QName.new(NsXsd_1, "Userdata"),
    :schema_element => [
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["data", "SOAP::SOAPString", [0, 1]],
      ["data_id", "SOAP::SOAPLong", [0, 1]],
      ["data_key", "SOAP::SOAPString", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UserSipData,
    :schema_type => XSD::QName.new(NsXsd_1, "UserSipData"),
    :schema_element => [
      ["authId", "SOAP::SOAPString", [0, 1]],
      ["inserted", "SOAP::SOAPDateTime", [0, 1]],
      ["updated", "SOAP::SOAPDateTime", [0, 1]],
      ["userSipDataId", "SOAP::SOAPLong", [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["userpass", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Usergroups,
    :schema_type => XSD::QName.new(NsXsd_1, "Usergroups"),
    :schema_element => [
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["description", "SOAP::SOAPString", [0, 1]],
      ["level_id", "SOAP::SOAPLong", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]],
      ["usergroup_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Userlevel,
    :schema_type => XSD::QName.new(NsXsd_1, "Userlevel"),
    :schema_element => [
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["description", "SOAP::SOAPString", [0, 1]],
      ["level_id", "SOAP::SOAPLong", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["statuscode", "SOAP::SOAPInt", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RoomCountBean,
    :schema_type => XSD::QName.new(NsXsd_2, "RoomCountBean"),
    :schema_element => [
      ["maxUser", "SOAP::SOAPInt", [0, 1]],
      ["roomCount", "SOAP::SOAPInt", [0, 1]],
      ["roomId", "SOAP::SOAPLong", [0, 1]],
      ["roomName", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RoomReturn,
    :schema_type => XSD::QName.new(NsXsd_2, "RoomReturn"),
    :schema_element => [
      ["created", "SOAP::SOAPDateTime", [0, 1]],
      ["creator", "SOAP::SOAPString", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomUser", "RoomUser[]", [0, nil]],
      ["room_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => RoomUser,
    :schema_type => XSD::QName.new(NsXsd_2, "RoomUser"),
    :schema_element => [
      ["avsettings", "SOAP::SOAPString", [0, 1]],
      ["broadcastId", "SOAP::SOAPLong", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["isBroadCasting", "SOAP::SOAPBoolean", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["publicSID", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => OmTimeZone,
    :schema_type => XSD::QName.new(NsXsd_3, "OmTimeZone"),
    :schema_element => [
      ["frontEndLabel", "SOAP::SOAPString", [0, 1]],
      ["ical", "SOAP::SOAPString", [0, 1]],
      ["inserted", "SOAP::SOAPDateTime", [0, 1]],
      ["jname", "SOAP::SOAPString", [0, 1]],
      ["label", "SOAP::SOAPString", [0, 1]],
      ["omtimezoneId", "SOAP::SOAPLong", [0, 1]],
      ["orderId", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Sessiondata,
    :schema_type => XSD::QName.new(NsXsd_3, "Sessiondata"),
    :schema_element => [
      ["id", "SOAP::SOAPLong", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["organization_id", "SOAP::SOAPLong", [0, 1]],
      ["refresh_time", "SOAP::SOAPDateTime", [0, 1]],
      ["sessionXml", "SOAP::SOAPString", [0, 1]],
      ["session_id", "SOAP::SOAPString", [0, 1]],
      ["starttermin_time", "SOAP::SOAPDateTime", [0, 1]],
      ["storePermanent", "SOAP::SOAPBoolean", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Adresses,
    :schema_type => XSD::QName.new(NsXsd_4, "Adresses"),
    :schema_element => [
      ["additionalname", "SOAP::SOAPString", [0, 1]],
      ["adresses_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["fax", "SOAP::SOAPString", [0, 1]],
      ["phone", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["states", "States", [0, 1]],
      ["street", "SOAP::SOAPString", [0, 1]],
      ["town", "SOAP::SOAPString", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]],
      ["zip", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => States,
    :schema_type => XSD::QName.new(NsXsd_4, "States"),
    :schema_element => [
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["starttime", "SOAP::SOAPDateTime", [0, 1]],
      ["state_id", "SOAP::SOAPLong", [0, 1]],
      ["updatetime", "SOAP::SOAPDateTime", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => FlvRecording,
    :schema_type => XSD::QName.new(NsXsd_5, "FlvRecording"),
    :schema_element => [
      ["alternateDownload", "SOAP::SOAPString", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["creator", "Users", [0, 1]],
      ["deleted", "SOAP::SOAPString", [0, 1]],
      ["fileHash", "SOAP::SOAPString", [0, 1]],
      ["fileName", "SOAP::SOAPString", [0, 1]],
      ["fileSize", "SOAP::SOAPLong", [0, 1]],
      ["flvHeight", "SOAP::SOAPInt", [0, 1]],
      ["flvRecordingId", "SOAP::SOAPLong", [0, 1]],
      ["flvRecordingLog", nil, [0, 1]],
      ["flvRecordingMetaData", nil, [0, 1]],
      ["flvWidth", "SOAP::SOAPInt", [0, 1]],
      ["height", "SOAP::SOAPInt", [0, 1]],
      ["inserted", "SOAP::SOAPDateTime", [0, 1]],
      ["insertedBy", "SOAP::SOAPLong", [0, 1]],
      ["isFolder", "SOAP::SOAPBoolean", [0, 1]],
      ["isImage", "SOAP::SOAPBoolean", [0, 1]],
      ["isInterview", "SOAP::SOAPBoolean", [0, 1]],
      ["isPresentation", "SOAP::SOAPBoolean", [0, 1]],
      ["isRecording", "SOAP::SOAPBoolean", [0, 1]],
      ["organization_id", "SOAP::SOAPLong", [0, 1]],
      ["ownerId", "SOAP::SOAPLong", [0, 1]],
      ["parentFileExplorerItemId", "SOAP::SOAPLong", [0, 1]],
      ["previewImage", "SOAP::SOAPString", [0, 1]],
      ["progressPostProcessing", "SOAP::SOAPInt", [0, 1]],
      ["recordEnd", "SOAP::SOAPDateTime", [0, 1]],
      ["recordStart", "SOAP::SOAPDateTime", [0, 1]],
      ["recorderStreamId", "SOAP::SOAPString", [0, 1]],
      ["room", "Rooms", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["updated", "SOAP::SOAPDateTime", [0, 1]],
      ["width", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Set,
    :schema_type => XSD::QName.new(NsXsd_6, "Set"),
    :schema_element => [
      ["empty", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CloseRoom,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "closeRoom"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["status", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => CloseRoomResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "closeRoomResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPInt", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => KickUser,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "kickUser"),
    :schema_element => [
      ["sID_Admin", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID_Admin")], [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => KickUserResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "kickUserResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPBoolean", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddExternalMeetingMemberRemindToRoom,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addExternalMeetingMemberRemindToRoom"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["baseUrl", "SOAP::SOAPString", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["jNameTimeZone", "SOAP::SOAPString", [0, 1]],
      ["invitorName", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddExternalMeetingMemberRemindToRoomResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addExternalMeetingMemberRemindToRoomResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddMeetingMemberRemindToRoom,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addMeetingMemberRemindToRoom"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["baseUrl", "SOAP::SOAPString", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddMeetingMemberRemindToRoomResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addMeetingMemberRemindToRoomResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoom,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoom"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["videoPodWidth", "SOAP::SOAPInt", [0, 1]],
      ["videoPodHeight", "SOAP::SOAPInt", [0, 1]],
      ["videoPodXPosition", "SOAP::SOAPInt", [0, 1]],
      ["videoPodYPosition", "SOAP::SOAPInt", [0, 1]],
      ["moderationPanelXPosition", "SOAP::SOAPInt", [0, 1]],
      ["showWhiteBoard", "SOAP::SOAPBoolean", [0, 1]],
      ["whiteBoardPanelXPosition", "SOAP::SOAPInt", [0, 1]],
      ["whiteBoardPanelYPosition", "SOAP::SOAPInt", [0, 1]],
      ["whiteBoardPanelHeight", "SOAP::SOAPInt", [0, 1]],
      ["whiteBoardPanelWidth", "SOAP::SOAPInt", [0, 1]],
      ["showFilesPanel", "SOAP::SOAPBoolean", [0, 1]],
      ["filesPanelXPosition", "SOAP::SOAPInt", [0, 1]],
      ["filesPanelYPosition", "SOAP::SOAPInt", [0, 1]],
      ["filesPanelHeight", "SOAP::SOAPInt", [0, 1]],
      ["filesPanelWidth", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModeration,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModeration"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationAndExternalType,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationAndExternalType"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationAndExternalTypeResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationAndExternalTypeResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationAndExternalTypeAndStartEnd,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationAndExternalTypeAndStartEnd"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]],
      ["validFromDate", "SOAP::SOAPString", [0, 1]],
      ["validFromTime", "SOAP::SOAPString", [0, 1]],
      ["validToDate", "SOAP::SOAPString", [0, 1]],
      ["validToTime", "SOAP::SOAPString", [0, 1]],
      ["isPasswordProtected", "SOAP::SOAPBoolean", [0, 1]],
      ["password", "SOAP::SOAPString", [0, 1]],
      ["reminderTypeId", "SOAP::SOAPLong", [0, 1]],
      ["redirectURL", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationAndExternalTypeAndStartEndResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationAndExternalTypeAndStartEndResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationAndQuestions,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationAndQuestions"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["allowUserQuestions", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationAndQuestionsResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationAndQuestionsResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationExternalTypeAndAudioType,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationExternalTypeAndAudioType"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]],
      ["allowUserQuestions", "SOAP::SOAPBoolean", [0, 1]],
      ["isAudioOnly", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationExternalTypeAndAudioTypeResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationExternalTypeAndAudioTypeResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationQuestionsAndAudioType,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationQuestionsAndAudioType"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["allowUserQuestions", "SOAP::SOAPBoolean", [0, 1]],
      ["isAudioOnly", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddRoomWithModerationQuestionsAndAudioTypeResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addRoomWithModerationQuestionsAndAudioTypeResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => DeleteRoom,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "deleteRoom"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => DeleteRoomResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "deleteRoomResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomIdByExternalId,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomIdByExternalId"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["externalRoomId", "SOAP::SOAPLong", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomIdByExternalIdResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomIdByExternalIdResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UpdateRoom,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "updateRoom"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["videoPodWidth", "SOAP::SOAPInt", [0, 1]],
      ["videoPodHeight", "SOAP::SOAPInt", [0, 1]],
      ["videoPodXPosition", "SOAP::SOAPInt", [0, 1]],
      ["videoPodYPosition", "SOAP::SOAPInt", [0, 1]],
      ["moderationPanelXPosition", "SOAP::SOAPInt", [0, 1]],
      ["showWhiteBoard", "SOAP::SOAPBoolean", [0, 1]],
      ["whiteBoardPanelXPosition", "SOAP::SOAPInt", [0, 1]],
      ["whiteBoardPanelYPosition", "SOAP::SOAPInt", [0, 1]],
      ["whiteBoardPanelHeight", "SOAP::SOAPInt", [0, 1]],
      ["whiteBoardPanelWidth", "SOAP::SOAPInt", [0, 1]],
      ["showFilesPanel", "SOAP::SOAPBoolean", [0, 1]],
      ["filesPanelXPosition", "SOAP::SOAPInt", [0, 1]],
      ["filesPanelYPosition", "SOAP::SOAPInt", [0, 1]],
      ["filesPanelHeight", "SOAP::SOAPInt", [0, 1]],
      ["filesPanelWidth", "SOAP::SOAPInt", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UpdateRoomResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "updateRoomResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UpdateRoomWithModeration,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "updateRoomWithModeration"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UpdateRoomWithModerationResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "updateRoomWithModerationResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UpdateRoomWithModerationAndQuestions,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "updateRoomWithModerationAndQuestions"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["name", "SOAP::SOAPString", [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]],
      ["numberOfPartizipants", "SOAP::SOAPLong", [0, 1]],
      ["ispublic", "SOAP::SOAPBoolean", [0, 1]],
      ["appointment", "SOAP::SOAPBoolean", [0, 1]],
      ["isDemoRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["demoTime", "SOAP::SOAPInt", [0, 1]],
      ["isModeratedRoom", "SOAP::SOAPBoolean", [0, 1]],
      ["allowUserQuestions", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => UpdateRoomWithModerationAndQuestionsResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "updateRoomWithModerationAndQuestionsResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetInvitationHash,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getInvitationHash"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["isPasswordProtected", "SOAP::SOAPBoolean", [0, 1]],
      ["invitationpass", "SOAP::SOAPString", [0, 1]],
      ["valid", "SOAP::SOAPInt", [0, 1]],
      ["validFromDate", "SOAP::SOAPString", [0, 1]],
      ["validFromTime", "SOAP::SOAPString", [0, 1]],
      ["validToDate", "SOAP::SOAPString", [0, 1]],
      ["validToTime", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetInvitationHashResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getInvitationHashResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SendInvitationHash,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "sendInvitationHash"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["message", "SOAP::SOAPString", [0, 1]],
      ["baseurl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["subject", "SOAP::SOAPString", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["conferencedomain", "SOAP::SOAPString", [0, 1]],
      ["isPasswordProtected", "SOAP::SOAPBoolean", [0, 1]],
      ["invitationpass", "SOAP::SOAPString", [0, 1]],
      ["valid", "SOAP::SOAPInt", [0, 1]],
      ["validFromDate", "SOAP::SOAPString", [0, 1]],
      ["validFromTime", "SOAP::SOAPString", [0, 1]],
      ["validToDate", "SOAP::SOAPString", [0, 1]],
      ["validToTime", "SOAP::SOAPString", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["sendMail", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SendInvitationHashResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "sendInvitationHashResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SendInvitationHashWithDateObject,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "sendInvitationHashWithDateObject"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["message", "SOAP::SOAPString", [0, 1]],
      ["baseurl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["subject", "SOAP::SOAPString", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["conferencedomain", "SOAP::SOAPString", [0, 1]],
      ["isPasswordProtected", "SOAP::SOAPBoolean", [0, 1]],
      ["invitationpass", "SOAP::SOAPString", [0, 1]],
      ["valid", "SOAP::SOAPInt", [0, 1]],
      ["fromDate", "SOAP::SOAPDateTime", [0, 1]],
      ["toDate", "SOAP::SOAPDateTime", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["sendMail", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SendInvitationHashWithDateObjectResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "sendInvitationHashWithDateObjectResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetFlvRecordingByExternalRoomTypeByList,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getFlvRecordingByExternalRoomTypeByList"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetFlvRecordingByExternalRoomTypeByListResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getFlvRecordingByExternalRoomTypeByListResponse"),
    :schema_element => [
      ["v_return", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsWithCurrentUsersByList,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsWithCurrentUsersByList"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["start", "SOAP::SOAPInt", [0, 1]],
      ["max", "SOAP::SOAPInt", [0, 1]],
      ["orderby", "SOAP::SOAPString", [0, 1]],
      ["asc", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsWithCurrentUsersByListResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsWithCurrentUsersByListResponse"),
    :schema_element => [
      ["v_return", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsWithCurrentUsersByListAndType,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsWithCurrentUsersByListAndType"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["start", "SOAP::SOAPInt", [0, 1]],
      ["max", "SOAP::SOAPInt", [0, 1]],
      ["orderby", "SOAP::SOAPString", [0, 1]],
      ["asc", "SOAP::SOAPBoolean", [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsWithCurrentUsersByListAndTypeResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsWithCurrentUsersByListAndTypeResponse"),
    :schema_element => [
      ["v_return", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRooms,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRooms"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["start", "SOAP::SOAPInt", [0, 1]],
      ["max", "SOAP::SOAPInt", [0, 1]],
      ["orderby", "SOAP::SOAPString", [0, 1]],
      ["asc", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsResponse"),
    :schema_element => [
      ["v_return", ["SearchResult", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsWithCurrentUsers,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsWithCurrentUsers"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["start", "SOAP::SOAPInt", [0, 1]],
      ["max", "SOAP::SOAPInt", [0, 1]],
      ["orderby", "SOAP::SOAPString", [0, 1]],
      ["asc", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsWithCurrentUsersResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsWithCurrentUsersResponse"),
    :schema_element => [
      ["v_return", ["SearchResult", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetFlvRecordingByExternalRoomType,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getFlvRecordingByExternalRoomType"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["externalRoomType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetFlvRecordingByExternalRoomTypeResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getFlvRecordingByExternalRoomTypeResponse"),
    :schema_element => [
      ["v_return", ["FlvRecording[]", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => GetFlvRecordingByRoomId,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getFlvRecordingByRoomId"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["roomId", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetFlvRecordingByRoomIdResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getFlvRecordingByRoomIdResponse"),
    :schema_element => [
      ["v_return", ["FlvRecording[]", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomTypes,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomTypes"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomTypesResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomTypesResponse"),
    :schema_element => [
      ["v_return", ["RoomTypes[]", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomById,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomById"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomByIdResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomByIdResponse"),
    :schema_element => [
      ["v_return", ["Rooms", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomWithCurrentUsersById,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomWithCurrentUsersById"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomWithCurrentUsersByIdResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomWithCurrentUsersByIdResponse"),
    :schema_element => [
      ["v_return", ["Rooms", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsPublic,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsPublic"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["roomtypes_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomsPublicResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomsPublicResponse"),
    :schema_element => [
      ["v_return", ["Rooms[]", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomCounters,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomCounters"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["roomId1", "SOAP::SOAPInt", [0, 1]],
      ["roomId2", "SOAP::SOAPInt", [0, 1]],
      ["roomId3", "SOAP::SOAPInt", [0, 1]],
      ["roomId4", "SOAP::SOAPInt", [0, 1]],
      ["roomId5", "SOAP::SOAPInt", [0, 1]],
      ["roomId6", "SOAP::SOAPInt", [0, 1]],
      ["roomId7", "SOAP::SOAPInt", [0, 1]],
      ["roomId8", "SOAP::SOAPInt", [0, 1]],
      ["roomId9", "SOAP::SOAPInt", [0, 1]],
      ["roomId10", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomCountersResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomCountersResponse"),
    :schema_element => [
      ["v_return", ["RoomCountBean[]", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, nil]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomWithClientObjectsById,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomWithClientObjectsById"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["rooms_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetRoomWithClientObjectsByIdResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getRoomWithClientObjectsByIdResponse"),
    :schema_element => [
      ["v_return", ["RoomReturn", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )
end
