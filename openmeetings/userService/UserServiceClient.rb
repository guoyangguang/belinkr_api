#!/usr/bin/env ruby
require_relative 'defaultDriver'

endpoint_url = ARGV.shift
obj = UserServicePortType.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   setUserObjectAndGenerateRecordingHashByURL(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRecordingHashByURL - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURL
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRecordingHashByURLResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURLResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRecordingHashByURL(parameters)

# SYNOPSIS
#   kickUserByPublicSID(parameters)
#
# ARGS
#   parameters      KickUserByPublicSID - {http://services.axis.openmeetings.org}kickUserByPublicSID
#
# RETURNS
#   parameters      KickUserByPublicSIDResponse - {http://services.axis.openmeetings.org}kickUserByPublicSIDResponse
#
parameters = nil
puts obj.kickUserByPublicSID(parameters)

# SYNOPSIS
#   setUserObjectMainLandingZone(parameters)
#
# ARGS
#   parameters      SetUserObjectMainLandingZone - {http://services.axis.openmeetings.org}setUserObjectMainLandingZone
#
# RETURNS
#   parameters      SetUserObjectMainLandingZoneResponse - {http://services.axis.openmeetings.org}setUserObjectMainLandingZoneResponse
#
parameters = nil
puts obj.setUserObjectMainLandingZone(parameters)

# SYNOPSIS
#   setUserObjectWithExternalUser(parameters)
#
# ARGS
#   parameters      SetUserObjectWithExternalUser - {http://services.axis.openmeetings.org}setUserObjectWithExternalUser
#
# RETURNS
#   parameters      SetUserObjectWithExternalUserResponse - {http://services.axis.openmeetings.org}setUserObjectWithExternalUserResponse
#
parameters = nil
puts obj.setUserObjectWithExternalUser(parameters)

# SYNOPSIS
#   addNewUserWithTimeZone(parameters)
#
# ARGS
#   parameters      AddNewUserWithTimeZone - {http://services.axis.openmeetings.org}addNewUserWithTimeZone
#
# RETURNS
#   parameters      AddNewUserWithTimeZoneResponse - {http://services.axis.openmeetings.org}addNewUserWithTimeZoneResponse
#
parameters = nil
puts obj.addNewUserWithTimeZone(parameters)

# SYNOPSIS
#   setUserObject(parameters)
#
# ARGS
#   parameters      SetUserObject - {http://services.axis.openmeetings.org}setUserObject
#
# RETURNS
#   parameters      SetUserObjectResponse - {http://services.axis.openmeetings.org}setUserObjectResponse
#
parameters = nil
puts obj.setUserObject(parameters)

# SYNOPSIS
#   setUserAndNickName(parameters)
#
# ARGS
#   parameters      SetUserAndNickName - {http://services.axis.openmeetings.org}setUserAndNickName
#
# RETURNS
#   parameters      SetUserAndNickNameResponse - {http://services.axis.openmeetings.org}setUserAndNickNameResponse
#
parameters = nil
puts obj.setUserAndNickName(parameters)

# SYNOPSIS
#   getUsersByOrganisation(parameters)
#
# ARGS
#   parameters      GetUsersByOrganisation - {http://services.axis.openmeetings.org}getUsersByOrganisation
#
# RETURNS
#   parameters      GetUsersByOrganisationResponse - {http://services.axis.openmeetings.org}getUsersByOrganisationResponse
#
parameters = nil
puts obj.getUsersByOrganisation(parameters)

# SYNOPSIS
#   setUserObjectAndGenerateRoomHash(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRoomHash - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHash
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRoomHashResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRoomHash(parameters)

# SYNOPSIS
#   setUserObjectAndGenerateRoomHashByURL(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRoomHashByURL - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURL
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRoomHashByURLResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURLResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRoomHashByURL(parameters)

# SYNOPSIS
#   addUserToOrganisation(parameters)
#
# ARGS
#   parameters      AddUserToOrganisation - {http://services.axis.openmeetings.org}addUserToOrganisation
#
# RETURNS
#   parameters      AddUserToOrganisationResponse - {http://services.axis.openmeetings.org}addUserToOrganisationResponse
#
parameters = nil
puts obj.addUserToOrganisation(parameters)

# SYNOPSIS
#   addNewUser(parameters)
#
# ARGS
#   parameters      AddNewUser - {http://services.axis.openmeetings.org}addNewUser
#
# RETURNS
#   parameters      AddNewUserResponse - {http://services.axis.openmeetings.org}addNewUserResponse
#
parameters = nil
puts obj.addNewUser(parameters)

# SYNOPSIS
#   getSession
#
# ARGS
#   N/A
#
# RETURNS
#   parameters      GetSessionResponse - {http://services.axis.openmeetings.org}getSessionResponse
#

puts obj.getSession

# SYNOPSIS
#   getErrorByCode(parameters)
#
# ARGS
#   parameters      GetErrorByCode - {http://services.axis.openmeetings.org}getErrorByCode
#
# RETURNS
#   parameters      GetErrorByCodeResponse - {http://services.axis.openmeetings.org}getErrorByCodeResponse
#
parameters = nil
puts obj.getErrorByCode(parameters)

# SYNOPSIS
#   loginUser(parameters)
#
# ARGS
#   parameters      LoginUser - {http://services.axis.openmeetings.org}loginUser
#
# RETURNS
#   parameters      LoginUserResponse - {http://services.axis.openmeetings.org}loginUserResponse
#
parameters = nil
puts obj.loginUser(parameters)


endpoint_url = ARGV.shift
obj = UserServicePortType.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   setUserObjectAndGenerateRecordingHashByURL(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRecordingHashByURL - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURL
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRecordingHashByURLResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURLResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRecordingHashByURL(parameters)

# SYNOPSIS
#   kickUserByPublicSID(parameters)
#
# ARGS
#   parameters      KickUserByPublicSID - {http://services.axis.openmeetings.org}kickUserByPublicSID
#
# RETURNS
#   parameters      KickUserByPublicSIDResponse - {http://services.axis.openmeetings.org}kickUserByPublicSIDResponse
#
parameters = nil
puts obj.kickUserByPublicSID(parameters)

# SYNOPSIS
#   setUserObjectMainLandingZone(parameters)
#
# ARGS
#   parameters      SetUserObjectMainLandingZone - {http://services.axis.openmeetings.org}setUserObjectMainLandingZone
#
# RETURNS
#   parameters      SetUserObjectMainLandingZoneResponse - {http://services.axis.openmeetings.org}setUserObjectMainLandingZoneResponse
#
parameters = nil
puts obj.setUserObjectMainLandingZone(parameters)

# SYNOPSIS
#   setUserObjectWithExternalUser(parameters)
#
# ARGS
#   parameters      SetUserObjectWithExternalUser - {http://services.axis.openmeetings.org}setUserObjectWithExternalUser
#
# RETURNS
#   parameters      SetUserObjectWithExternalUserResponse - {http://services.axis.openmeetings.org}setUserObjectWithExternalUserResponse
#
parameters = nil
puts obj.setUserObjectWithExternalUser(parameters)

# SYNOPSIS
#   addNewUserWithTimeZone(parameters)
#
# ARGS
#   parameters      AddNewUserWithTimeZone - {http://services.axis.openmeetings.org}addNewUserWithTimeZone
#
# RETURNS
#   parameters      AddNewUserWithTimeZoneResponse - {http://services.axis.openmeetings.org}addNewUserWithTimeZoneResponse
#
parameters = nil
puts obj.addNewUserWithTimeZone(parameters)

# SYNOPSIS
#   setUserObject(parameters)
#
# ARGS
#   parameters      SetUserObject - {http://services.axis.openmeetings.org}setUserObject
#
# RETURNS
#   parameters      SetUserObjectResponse - {http://services.axis.openmeetings.org}setUserObjectResponse
#
parameters = nil
puts obj.setUserObject(parameters)

# SYNOPSIS
#   setUserAndNickName(parameters)
#
# ARGS
#   parameters      SetUserAndNickName - {http://services.axis.openmeetings.org}setUserAndNickName
#
# RETURNS
#   parameters      SetUserAndNickNameResponse - {http://services.axis.openmeetings.org}setUserAndNickNameResponse
#
parameters = nil
puts obj.setUserAndNickName(parameters)

# SYNOPSIS
#   getUsersByOrganisation(parameters)
#
# ARGS
#   parameters      GetUsersByOrganisation - {http://services.axis.openmeetings.org}getUsersByOrganisation
#
# RETURNS
#   parameters      GetUsersByOrganisationResponse - {http://services.axis.openmeetings.org}getUsersByOrganisationResponse
#
parameters = nil
puts obj.getUsersByOrganisation(parameters)

# SYNOPSIS
#   setUserObjectAndGenerateRoomHash(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRoomHash - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHash
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRoomHashResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRoomHash(parameters)

# SYNOPSIS
#   setUserObjectAndGenerateRoomHashByURL(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRoomHashByURL - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURL
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRoomHashByURLResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURLResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRoomHashByURL(parameters)

# SYNOPSIS
#   addUserToOrganisation(parameters)
#
# ARGS
#   parameters      AddUserToOrganisation - {http://services.axis.openmeetings.org}addUserToOrganisation
#
# RETURNS
#   parameters      AddUserToOrganisationResponse - {http://services.axis.openmeetings.org}addUserToOrganisationResponse
#
parameters = nil
puts obj.addUserToOrganisation(parameters)

# SYNOPSIS
#   addNewUser(parameters)
#
# ARGS
#   parameters      AddNewUser - {http://services.axis.openmeetings.org}addNewUser
#
# RETURNS
#   parameters      AddNewUserResponse - {http://services.axis.openmeetings.org}addNewUserResponse
#
parameters = nil
puts obj.addNewUser(parameters)

# SYNOPSIS
#   getSession
#
# ARGS
#   N/A
#
# RETURNS
#   parameters      GetSessionResponse - {http://services.axis.openmeetings.org}getSessionResponse
#

puts obj.getSession

# SYNOPSIS
#   getErrorByCode(parameters)
#
# ARGS
#   parameters      GetErrorByCode - {http://services.axis.openmeetings.org}getErrorByCode
#
# RETURNS
#   parameters      GetErrorByCodeResponse - {http://services.axis.openmeetings.org}getErrorByCodeResponse
#
parameters = nil
puts obj.getErrorByCode(parameters)

# SYNOPSIS
#   loginUser(parameters)
#
# ARGS
#   parameters      LoginUser - {http://services.axis.openmeetings.org}loginUser
#
# RETURNS
#   parameters      LoginUserResponse - {http://services.axis.openmeetings.org}loginUserResponse
#
parameters = nil
puts obj.loginUser(parameters)


endpoint_url = ARGV.shift
obj = UserServicePortType.new(endpoint_url)

# run ruby with -d to see SOAP wiredumps.
obj.wiredump_dev = STDERR if $DEBUG

# SYNOPSIS
#   setUserObjectAndGenerateRecordingHashByURL(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRecordingHashByURL - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURL
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRecordingHashByURLResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURLResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRecordingHashByURL(parameters)

# SYNOPSIS
#   kickUserByPublicSID(parameters)
#
# ARGS
#   parameters      KickUserByPublicSID - {http://services.axis.openmeetings.org}kickUserByPublicSID
#
# RETURNS
#   parameters      KickUserByPublicSIDResponse - {http://services.axis.openmeetings.org}kickUserByPublicSIDResponse
#
parameters = nil
puts obj.kickUserByPublicSID(parameters)

# SYNOPSIS
#   setUserObjectMainLandingZone(parameters)
#
# ARGS
#   parameters      SetUserObjectMainLandingZone - {http://services.axis.openmeetings.org}setUserObjectMainLandingZone
#
# RETURNS
#   parameters      SetUserObjectMainLandingZoneResponse - {http://services.axis.openmeetings.org}setUserObjectMainLandingZoneResponse
#
parameters = nil
puts obj.setUserObjectMainLandingZone(parameters)

# SYNOPSIS
#   setUserObjectWithExternalUser(parameters)
#
# ARGS
#   parameters      SetUserObjectWithExternalUser - {http://services.axis.openmeetings.org}setUserObjectWithExternalUser
#
# RETURNS
#   parameters      SetUserObjectWithExternalUserResponse - {http://services.axis.openmeetings.org}setUserObjectWithExternalUserResponse
#
parameters = nil
puts obj.setUserObjectWithExternalUser(parameters)

# SYNOPSIS
#   addNewUserWithTimeZone(parameters)
#
# ARGS
#   parameters      AddNewUserWithTimeZone - {http://services.axis.openmeetings.org}addNewUserWithTimeZone
#
# RETURNS
#   parameters      AddNewUserWithTimeZoneResponse - {http://services.axis.openmeetings.org}addNewUserWithTimeZoneResponse
#
parameters = nil
puts obj.addNewUserWithTimeZone(parameters)

# SYNOPSIS
#   setUserObject(parameters)
#
# ARGS
#   parameters      SetUserObject - {http://services.axis.openmeetings.org}setUserObject
#
# RETURNS
#   parameters      SetUserObjectResponse - {http://services.axis.openmeetings.org}setUserObjectResponse
#
parameters = nil
puts obj.setUserObject(parameters)

# SYNOPSIS
#   setUserAndNickName(parameters)
#
# ARGS
#   parameters      SetUserAndNickName - {http://services.axis.openmeetings.org}setUserAndNickName
#
# RETURNS
#   parameters      SetUserAndNickNameResponse - {http://services.axis.openmeetings.org}setUserAndNickNameResponse
#
parameters = nil
puts obj.setUserAndNickName(parameters)

# SYNOPSIS
#   getUsersByOrganisation(parameters)
#
# ARGS
#   parameters      GetUsersByOrganisation - {http://services.axis.openmeetings.org}getUsersByOrganisation
#
# RETURNS
#   parameters      GetUsersByOrganisationResponse - {http://services.axis.openmeetings.org}getUsersByOrganisationResponse
#
parameters = nil
puts obj.getUsersByOrganisation(parameters)

# SYNOPSIS
#   setUserObjectAndGenerateRoomHash(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRoomHash - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHash
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRoomHashResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRoomHash(parameters)

# SYNOPSIS
#   setUserObjectAndGenerateRoomHashByURL(parameters)
#
# ARGS
#   parameters      SetUserObjectAndGenerateRoomHashByURL - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURL
#
# RETURNS
#   parameters      SetUserObjectAndGenerateRoomHashByURLResponse - {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURLResponse
#
parameters = nil
puts obj.setUserObjectAndGenerateRoomHashByURL(parameters)

# SYNOPSIS
#   addUserToOrganisation(parameters)
#
# ARGS
#   parameters      AddUserToOrganisation - {http://services.axis.openmeetings.org}addUserToOrganisation
#
# RETURNS
#   parameters      AddUserToOrganisationResponse - {http://services.axis.openmeetings.org}addUserToOrganisationResponse
#
parameters = nil
puts obj.addUserToOrganisation(parameters)

# SYNOPSIS
#   addNewUser(parameters)
#
# ARGS
#   parameters      AddNewUser - {http://services.axis.openmeetings.org}addNewUser
#
# RETURNS
#   parameters      AddNewUserResponse - {http://services.axis.openmeetings.org}addNewUserResponse
#
parameters = nil
puts obj.addNewUser(parameters)

# SYNOPSIS
#   getSession
#
# ARGS
#   N/A
#
# RETURNS
#   parameters      GetSessionResponse - {http://services.axis.openmeetings.org}getSessionResponse
#

puts obj.getSession

# SYNOPSIS
#   getErrorByCode(parameters)
#
# ARGS
#   parameters      GetErrorByCode - {http://services.axis.openmeetings.org}getErrorByCode
#
# RETURNS
#   parameters      GetErrorByCodeResponse - {http://services.axis.openmeetings.org}getErrorByCodeResponse
#
parameters = nil
puts obj.getErrorByCode(parameters)

# SYNOPSIS
#   loginUser(parameters)
#
# ARGS
#   parameters      LoginUser - {http://services.axis.openmeetings.org}loginUser
#
# RETURNS
#   parameters      LoginUserResponse - {http://services.axis.openmeetings.org}loginUserResponse
#
parameters = nil
puts obj.loginUser(parameters)


