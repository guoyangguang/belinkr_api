require 'xsd/qname'

# {http://basic.beans.hibernate.app.openmeetings.org/xsd}Sessiondata
#   id - SOAP::SOAPLong
#   language_id - SOAP::SOAPLong
#   organization_id - SOAP::SOAPLong
#   refresh_time - SOAP::SOAPDateTime
#   sessionXml - SOAP::SOAPString
#   session_id - SOAP::SOAPString
#   starttermin_time - SOAP::SOAPDateTime
#   storePermanent - SOAP::SOAPBoolean
#   user_id - SOAP::SOAPLong
class Sessiondata
  attr_accessor :id
  attr_accessor :language_id
  attr_accessor :organization_id
  attr_accessor :refresh_time
  attr_accessor :sessionXml
  attr_accessor :session_id
  attr_accessor :starttermin_time
  attr_accessor :storePermanent
  attr_accessor :user_id

  def initialize(id = nil, language_id = nil, organization_id = nil, refresh_time = nil, sessionXml = nil, session_id = nil, starttermin_time = nil, storePermanent = nil, user_id = nil)
    @id = id
    @language_id = language_id
    @organization_id = organization_id
    @refresh_time = refresh_time
    @sessionXml = sessionXml
    @session_id = session_id
    @starttermin_time = starttermin_time
    @storePermanent = storePermanent
    @user_id = user_id
  end
end

# {http://basic.beans.data.app.openmeetings.org/xsd}ErrorResult
#   errmessage - SOAP::SOAPString
#   errorId - SOAP::SOAPLong
#   errortype - SOAP::SOAPString
class ErrorResult
  attr_accessor :errmessage
  attr_accessor :errorId
  attr_accessor :errortype

  def initialize(errmessage = nil, errorId = nil, errortype = nil)
    @errmessage = errmessage
    @errorId = errorId
    @errortype = errortype
  end
end

# {http://basic.beans.data.app.openmeetings.org/xsd}SearchResult
#   errorId - SOAP::SOAPLong
#   objectName - SOAP::SOAPString
#   records - SOAP::SOAPLong
#   result - (any)
class SearchResult
  attr_accessor :errorId
  attr_accessor :objectName
  attr_accessor :records
  attr_accessor :result

  def initialize(errorId = nil, objectName = nil, records = nil, result = nil)
    @errorId = errorId
    @objectName = objectName
    @records = records
    @result = result
  end
end

# {http://services.axis.openmeetings.org}Exception
#   exception - (any)
class C_Exception
  attr_accessor :exception

  def initialize(exception = nil)
    @exception = exception
  end
end

# {http://services.axis.openmeetings.org}kickUserByPublicSID
#   sID - SOAP::SOAPString
#   publicSID - SOAP::SOAPString
class KickUserByPublicSID
  attr_accessor :sID
  attr_accessor :publicSID

  def initialize(sID = nil, publicSID = nil)
    @sID = sID
    @publicSID = publicSID
  end
end

# {http://services.axis.openmeetings.org}kickUserByPublicSIDResponse
#   m_return - SOAP::SOAPBoolean
class KickUserByPublicSIDResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addNewUser
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   userpass - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   email - SOAP::SOAPString
#   additionalname - SOAP::SOAPString
#   street - SOAP::SOAPString
#   zip - SOAP::SOAPString
#   fax - SOAP::SOAPString
#   states_id - SOAP::SOAPLong
#   town - SOAP::SOAPString
#   language_id - SOAP::SOAPLong
#   baseURL - SOAP::SOAPString
class AddNewUser
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :userpass
  attr_accessor :lastname
  attr_accessor :firstname
  attr_accessor :email
  attr_accessor :additionalname
  attr_accessor :street
  attr_accessor :zip
  attr_accessor :fax
  attr_accessor :states_id
  attr_accessor :town
  attr_accessor :language_id
  attr_accessor :baseURL

  def initialize(sID = nil, username = nil, userpass = nil, lastname = nil, firstname = nil, email = nil, additionalname = nil, street = nil, zip = nil, fax = nil, states_id = nil, town = nil, language_id = nil, baseURL = nil)
    @sID = sID
    @username = username
    @userpass = userpass
    @lastname = lastname
    @firstname = firstname
    @email = email
    @additionalname = additionalname
    @street = street
    @zip = zip
    @fax = fax
    @states_id = states_id
    @town = town
    @language_id = language_id
    @baseURL = baseURL
  end
end

# {http://services.axis.openmeetings.org}addNewUserResponse
#   m_return - SOAP::SOAPLong
class AddNewUserResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addNewUserWithTimeZone
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   userpass - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   email - SOAP::SOAPString
#   additionalname - SOAP::SOAPString
#   street - SOAP::SOAPString
#   zip - SOAP::SOAPString
#   fax - SOAP::SOAPString
#   states_id - SOAP::SOAPLong
#   town - SOAP::SOAPString
#   language_id - SOAP::SOAPLong
#   baseURL - SOAP::SOAPString
#   jNameTimeZone - SOAP::SOAPString
class AddNewUserWithTimeZone
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :userpass
  attr_accessor :lastname
  attr_accessor :firstname
  attr_accessor :email
  attr_accessor :additionalname
  attr_accessor :street
  attr_accessor :zip
  attr_accessor :fax
  attr_accessor :states_id
  attr_accessor :town
  attr_accessor :language_id
  attr_accessor :baseURL
  attr_accessor :jNameTimeZone

  def initialize(sID = nil, username = nil, userpass = nil, lastname = nil, firstname = nil, email = nil, additionalname = nil, street = nil, zip = nil, fax = nil, states_id = nil, town = nil, language_id = nil, baseURL = nil, jNameTimeZone = nil)
    @sID = sID
    @username = username
    @userpass = userpass
    @lastname = lastname
    @firstname = firstname
    @email = email
    @additionalname = additionalname
    @street = street
    @zip = zip
    @fax = fax
    @states_id = states_id
    @town = town
    @language_id = language_id
    @baseURL = baseURL
    @jNameTimeZone = jNameTimeZone
  end
end

# {http://services.axis.openmeetings.org}addNewUserWithTimeZoneResponse
#   m_return - SOAP::SOAPLong
class AddNewUserWithTimeZoneResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}addUserToOrganisation
#   sID - SOAP::SOAPString
#   user_id - SOAP::SOAPLong
#   organisation_id - SOAP::SOAPLong
#   insertedby - SOAP::SOAPLong
#   comment - SOAP::SOAPString
class AddUserToOrganisation
  attr_accessor :sID
  attr_accessor :user_id
  attr_accessor :organisation_id
  attr_accessor :insertedby
  attr_accessor :comment

  def initialize(sID = nil, user_id = nil, organisation_id = nil, insertedby = nil, comment = nil)
    @sID = sID
    @user_id = user_id
    @organisation_id = organisation_id
    @insertedby = insertedby
    @comment = comment
  end
end

# {http://services.axis.openmeetings.org}addUserToOrganisationResponse
#   m_return - SOAP::SOAPLong
class AddUserToOrganisationResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}loginUser
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   userpass - SOAP::SOAPString
class LoginUser
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :userpass

  def initialize(sID = nil, username = nil, userpass = nil)
    @sID = sID
    @username = username
    @userpass = userpass
  end
end

# {http://services.axis.openmeetings.org}loginUserResponse
#   m_return - SOAP::SOAPLong
class LoginUserResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserObject
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   profilePictureUrl - SOAP::SOAPString
#   email - SOAP::SOAPString
class SetUserObject
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :profilePictureUrl
  attr_accessor :email

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, profilePictureUrl = nil, email = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @profilePictureUrl = profilePictureUrl
    @email = email
  end
end

# {http://services.axis.openmeetings.org}setUserObjectResponse
#   m_return - SOAP::SOAPLong
class SetUserObjectResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserObjectWithExternalUser
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   profilePictureUrl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
class SetUserObjectWithExternalUser
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :profilePictureUrl
  attr_accessor :email
  attr_accessor :externalUserId
  attr_accessor :externalUserType

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, profilePictureUrl = nil, email = nil, externalUserId = nil, externalUserType = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @profilePictureUrl = profilePictureUrl
    @email = email
    @externalUserId = externalUserId
    @externalUserType = externalUserType
  end
end

# {http://services.axis.openmeetings.org}setUserObjectWithExternalUserResponse
#   m_return - SOAP::SOAPLong
class SetUserObjectWithExternalUserResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserAndNickName
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   profilePictureUrl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   becomeModeratorAsInt - SOAP::SOAPInt
#   showAudioVideoTestAsInt - SOAP::SOAPInt
#   showNickNameDialogAsInt - SOAP::SOAPInt
class SetUserAndNickName
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :profilePictureUrl
  attr_accessor :email
  attr_accessor :externalUserId
  attr_accessor :externalUserType
  attr_accessor :room_id
  attr_accessor :becomeModeratorAsInt
  attr_accessor :showAudioVideoTestAsInt
  attr_accessor :showNickNameDialogAsInt

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, profilePictureUrl = nil, email = nil, externalUserId = nil, externalUserType = nil, room_id = nil, becomeModeratorAsInt = nil, showAudioVideoTestAsInt = nil, showNickNameDialogAsInt = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @profilePictureUrl = profilePictureUrl
    @email = email
    @externalUserId = externalUserId
    @externalUserType = externalUserType
    @room_id = room_id
    @becomeModeratorAsInt = becomeModeratorAsInt
    @showAudioVideoTestAsInt = showAudioVideoTestAsInt
    @showNickNameDialogAsInt = showNickNameDialogAsInt
  end
end

# {http://services.axis.openmeetings.org}setUserAndNickNameResponse
#   m_return - SOAP::SOAPString
class SetUserAndNickNameResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURL
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
#   recording_id - SOAP::SOAPLong
class SetUserObjectAndGenerateRecordingHashByURL
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :externalUserId
  attr_accessor :externalUserType
  attr_accessor :recording_id

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, externalUserId = nil, externalUserType = nil, recording_id = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @externalUserId = externalUserId
    @externalUserType = externalUserType
    @recording_id = recording_id
  end
end

# {http://services.axis.openmeetings.org}setUserObjectAndGenerateRecordingHashByURLResponse
#   m_return - SOAP::SOAPString
class SetUserObjectAndGenerateRecordingHashByURLResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHash
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   profilePictureUrl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   becomeModeratorAsInt - SOAP::SOAPInt
#   showAudioVideoTestAsInt - SOAP::SOAPInt
class SetUserObjectAndGenerateRoomHash
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :profilePictureUrl
  attr_accessor :email
  attr_accessor :externalUserId
  attr_accessor :externalUserType
  attr_accessor :room_id
  attr_accessor :becomeModeratorAsInt
  attr_accessor :showAudioVideoTestAsInt

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, profilePictureUrl = nil, email = nil, externalUserId = nil, externalUserType = nil, room_id = nil, becomeModeratorAsInt = nil, showAudioVideoTestAsInt = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @profilePictureUrl = profilePictureUrl
    @email = email
    @externalUserId = externalUserId
    @externalUserType = externalUserType
    @room_id = room_id
    @becomeModeratorAsInt = becomeModeratorAsInt
    @showAudioVideoTestAsInt = showAudioVideoTestAsInt
  end
end

# {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashResponse
#   m_return - SOAP::SOAPString
class SetUserObjectAndGenerateRoomHashResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURL
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   profilePictureUrl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
#   room_id - SOAP::SOAPLong
#   becomeModeratorAsInt - SOAP::SOAPInt
#   showAudioVideoTestAsInt - SOAP::SOAPInt
class SetUserObjectAndGenerateRoomHashByURL
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :profilePictureUrl
  attr_accessor :email
  attr_accessor :externalUserId
  attr_accessor :externalUserType
  attr_accessor :room_id
  attr_accessor :becomeModeratorAsInt
  attr_accessor :showAudioVideoTestAsInt

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, profilePictureUrl = nil, email = nil, externalUserId = nil, externalUserType = nil, room_id = nil, becomeModeratorAsInt = nil, showAudioVideoTestAsInt = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @profilePictureUrl = profilePictureUrl
    @email = email
    @externalUserId = externalUserId
    @externalUserType = externalUserType
    @room_id = room_id
    @becomeModeratorAsInt = becomeModeratorAsInt
    @showAudioVideoTestAsInt = showAudioVideoTestAsInt
  end
end

# {http://services.axis.openmeetings.org}setUserObjectAndGenerateRoomHashByURLResponse
#   m_return - SOAP::SOAPString
class SetUserObjectAndGenerateRoomHashByURLResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}setUserObjectMainLandingZone
#   sID - SOAP::SOAPString
#   username - SOAP::SOAPString
#   firstname - SOAP::SOAPString
#   lastname - SOAP::SOAPString
#   profilePictureUrl - SOAP::SOAPString
#   email - SOAP::SOAPString
#   externalUserId - SOAP::SOAPLong
#   externalUserType - SOAP::SOAPString
class SetUserObjectMainLandingZone
  attr_accessor :sID
  attr_accessor :username
  attr_accessor :firstname
  attr_accessor :lastname
  attr_accessor :profilePictureUrl
  attr_accessor :email
  attr_accessor :externalUserId
  attr_accessor :externalUserType

  def initialize(sID = nil, username = nil, firstname = nil, lastname = nil, profilePictureUrl = nil, email = nil, externalUserId = nil, externalUserType = nil)
    @sID = sID
    @username = username
    @firstname = firstname
    @lastname = lastname
    @profilePictureUrl = profilePictureUrl
    @email = email
    @externalUserId = externalUserId
    @externalUserType = externalUserType
  end
end

# {http://services.axis.openmeetings.org}setUserObjectMainLandingZoneResponse
#   m_return - SOAP::SOAPString
class SetUserObjectMainLandingZoneResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getErrorByCode
#   sID - SOAP::SOAPString
#   errorid - SOAP::SOAPLong
#   language_id - SOAP::SOAPLong
class GetErrorByCode
  attr_accessor :sID
  attr_accessor :errorid
  attr_accessor :language_id

  def initialize(sID = nil, errorid = nil, language_id = nil)
    @sID = sID
    @errorid = errorid
    @language_id = language_id
  end
end

# {http://services.axis.openmeetings.org}getErrorByCodeResponse
#   m_return - ErrorResult
class GetErrorByCodeResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getUsersByOrganisation
#   sID - SOAP::SOAPString
#   organisation_id - SOAP::SOAPLong
#   start - SOAP::SOAPInt
#   max - SOAP::SOAPInt
#   orderby - SOAP::SOAPString
#   asc - SOAP::SOAPBoolean
class GetUsersByOrganisation
  attr_accessor :sID
  attr_accessor :organisation_id
  attr_accessor :start
  attr_accessor :max
  attr_accessor :orderby
  attr_accessor :asc

  def initialize(sID = nil, organisation_id = nil, start = nil, max = nil, orderby = nil, asc = nil)
    @sID = sID
    @organisation_id = organisation_id
    @start = start
    @max = max
    @orderby = orderby
    @asc = asc
  end
end

# {http://services.axis.openmeetings.org}getUsersByOrganisationResponse
#   m_return - SearchResult
class GetUsersByOrganisationResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end

# {http://services.axis.openmeetings.org}getSessionResponse
#   m_return - Sessiondata
class GetSessionResponse
  def m_return
    @v_return
  end

  def m_return=(value)
    @v_return = value
  end

  def initialize(v_return = nil)
    @v_return = v_return
  end
end
