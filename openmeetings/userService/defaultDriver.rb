require_relative 'default'
require_relative 'defaultMappingRegistry'
require 'soap/rpc/driver'

class UserServicePortType < ::SOAP::RPC::Driver
  DefaultEndpointUrl = "http://localhost:5080/openmeetings/services/UserService"

  Methods = [
    [ "urn:setUserObjectAndGenerateRecordingHashByURL",
      "setUserObjectAndGenerateRecordingHashByURL",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectAndGenerateRecordingHashByURL"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectAndGenerateRecordingHashByURLResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:kickUserByPublicSID",
      "kickUserByPublicSID",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "kickUserByPublicSID"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "kickUserByPublicSIDResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:setUserObjectMainLandingZone",
      "setUserObjectMainLandingZone",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectMainLandingZone"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectMainLandingZoneResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:setUserObjectWithExternalUser",
      "setUserObjectWithExternalUser",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectWithExternalUser"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectWithExternalUserResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addNewUserWithTimeZone",
      "addNewUserWithTimeZone",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addNewUserWithTimeZone"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addNewUserWithTimeZoneResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:setUserObject",
      "setUserObject",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObject"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:setUserAndNickName",
      "setUserAndNickName",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserAndNickName"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserAndNickNameResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getUsersByOrganisation",
      "getUsersByOrganisation",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getUsersByOrganisation"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getUsersByOrganisationResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:setUserObjectAndGenerateRoomHash",
      "setUserObjectAndGenerateRoomHash",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectAndGenerateRoomHash"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectAndGenerateRoomHashResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:setUserObjectAndGenerateRoomHashByURL",
      "setUserObjectAndGenerateRoomHashByURL",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectAndGenerateRoomHashByURL"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "setUserObjectAndGenerateRoomHashByURLResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addUserToOrganisation",
      "addUserToOrganisation",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addUserToOrganisation"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addUserToOrganisationResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:addNewUser",
      "addNewUser",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addNewUser"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "addNewUserResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getSession",
      "getSession",
      [ [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getSessionResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:getErrorByCode",
      "getErrorByCode",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getErrorByCode"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "getErrorByCodeResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ],
    [ "urn:loginUser",
      "loginUser",
      [ [:in, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "loginUser"]],
        [:out, "parameters", ["::SOAP::SOAPElement", "http://services.axis.openmeetings.org", "loginUserResponse"]] ],
      { :request_style =>  :document, :request_use =>  :literal,
        :response_style => :document, :response_use => :literal,
        :faults => {} }
    ]
  ]

  def initialize(endpoint_url = nil)
    endpoint_url ||= DefaultEndpointUrl
    super(endpoint_url, nil)
    self.mapping_registry = DefaultMappingRegistry::EncodedRegistry
    self.literal_mapping_registry = DefaultMappingRegistry::LiteralRegistry
    init_methods
  end

private

  def init_methods
    Methods.each do |definitions|
      opt = definitions.last
      if opt[:request_style] == :document
        add_document_operation(*definitions)
      else
        add_rpc_operation(*definitions)
        qname = definitions[0]
        name = definitions[2]
        if qname.name != name and qname.name.capitalize == name.capitalize
          ::SOAP::Mapping.define_singleton_method(self, qname.name) do |*arg|
            __send__(name, *arg)
          end
        end
      end
    end
  end
end

