require_relative 'default'
require 'soap/mapping'

module DefaultMappingRegistry
  EncodedRegistry = ::SOAP::Mapping::EncodedRegistry.new
  LiteralRegistry = ::SOAP::Mapping::LiteralRegistry.new
  NsServicesAxisOpenmeetingsOrg = "http://services.axis.openmeetings.org"
  NsXsd = "http://basic.beans.hibernate.app.openmeetings.org/xsd"
  NsXsd_0 = "http://basic.beans.data.app.openmeetings.org/xsd"

  EncodedRegistry.register(
    :class => Sessiondata,
    :schema_type => XSD::QName.new(NsXsd, "Sessiondata"),
    :schema_element => [
      ["id", "SOAP::SOAPLong", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["organization_id", "SOAP::SOAPLong", [0, 1]],
      ["refresh_time", "SOAP::SOAPDateTime", [0, 1]],
      ["sessionXml", "SOAP::SOAPString", [0, 1]],
      ["session_id", "SOAP::SOAPString", [0, 1]],
      ["starttermin_time", "SOAP::SOAPDateTime", [0, 1]],
      ["storePermanent", "SOAP::SOAPBoolean", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => ErrorResult,
    :schema_type => XSD::QName.new(NsXsd_0, "ErrorResult"),
    :schema_element => [
      ["errmessage", "SOAP::SOAPString", [0, 1]],
      ["errorId", "SOAP::SOAPLong", [0, 1]],
      ["errortype", "SOAP::SOAPString", [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => SearchResult,
    :schema_type => XSD::QName.new(NsXsd_0, "SearchResult"),
    :schema_element => [
      ["errorId", "SOAP::SOAPLong", [0, 1]],
      ["objectName", "SOAP::SOAPString", [0, 1]],
      ["records", "SOAP::SOAPLong", [0, 1]],
      ["result", nil, [0, 1]]
    ]
  )

  EncodedRegistry.register(
    :class => C_Exception,
    :schema_type => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception"),
    :schema_element => [
      ["exception", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => Sessiondata,
    :schema_type => XSD::QName.new(NsXsd, "Sessiondata"),
    :schema_element => [
      ["id", "SOAP::SOAPLong", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["organization_id", "SOAP::SOAPLong", [0, 1]],
      ["refresh_time", "SOAP::SOAPDateTime", [0, 1]],
      ["sessionXml", "SOAP::SOAPString", [0, 1]],
      ["session_id", "SOAP::SOAPString", [0, 1]],
      ["starttermin_time", "SOAP::SOAPDateTime", [0, 1]],
      ["storePermanent", "SOAP::SOAPBoolean", [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => ErrorResult,
    :schema_type => XSD::QName.new(NsXsd_0, "ErrorResult"),
    :schema_element => [
      ["errmessage", "SOAP::SOAPString", [0, 1]],
      ["errorId", "SOAP::SOAPLong", [0, 1]],
      ["errortype", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SearchResult,
    :schema_type => XSD::QName.new(NsXsd_0, "SearchResult"),
    :schema_element => [
      ["errorId", "SOAP::SOAPLong", [0, 1]],
      ["objectName", "SOAP::SOAPString", [0, 1]],
      ["records", "SOAP::SOAPLong", [0, 1]],
      ["result", nil, [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => C_Exception,
    :schema_type => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception"),
    :schema_element => [
      ["exception", [nil, XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "Exception")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => KickUserByPublicSID,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "kickUserByPublicSID"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["publicSID", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => KickUserByPublicSIDResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "kickUserByPublicSIDResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPBoolean", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddNewUser,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addNewUser"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["userpass", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["additionalname", "SOAP::SOAPString", [0, 1]],
      ["street", "SOAP::SOAPString", [0, 1]],
      ["zip", "SOAP::SOAPString", [0, 1]],
      ["fax", "SOAP::SOAPString", [0, 1]],
      ["states_id", "SOAP::SOAPLong", [0, 1]],
      ["town", "SOAP::SOAPString", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["baseURL", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddNewUserResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addNewUserResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddNewUserWithTimeZone,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addNewUserWithTimeZone"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["userpass", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["additionalname", "SOAP::SOAPString", [0, 1]],
      ["street", "SOAP::SOAPString", [0, 1]],
      ["zip", "SOAP::SOAPString", [0, 1]],
      ["fax", "SOAP::SOAPString", [0, 1]],
      ["states_id", "SOAP::SOAPLong", [0, 1]],
      ["town", "SOAP::SOAPString", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]],
      ["baseURL", "SOAP::SOAPString", [0, 1]],
      ["jNameTimeZone", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddNewUserWithTimeZoneResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addNewUserWithTimeZoneResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddUserToOrganisation,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addUserToOrganisation"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["user_id", "SOAP::SOAPLong", [0, 1]],
      ["organisation_id", "SOAP::SOAPLong", [0, 1]],
      ["insertedby", "SOAP::SOAPLong", [0, 1]],
      ["comment", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => AddUserToOrganisationResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "addUserToOrganisationResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => LoginUser,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "loginUser"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["userpass", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => LoginUserResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "loginUserResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObject,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObject"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["profilePictureUrl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectWithExternalUser,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectWithExternalUser"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["profilePictureUrl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectWithExternalUserResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectWithExternalUserResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPLong", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserAndNickName,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserAndNickName"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["profilePictureUrl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["becomeModeratorAsInt", "SOAP::SOAPInt", [0, 1]],
      ["showAudioVideoTestAsInt", "SOAP::SOAPInt", [0, 1]],
      ["showNickNameDialogAsInt", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserAndNickNameResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserAndNickNameResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectAndGenerateRecordingHashByURL,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectAndGenerateRecordingHashByURL"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]],
      ["recording_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectAndGenerateRecordingHashByURLResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectAndGenerateRecordingHashByURLResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectAndGenerateRoomHash,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectAndGenerateRoomHash"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["profilePictureUrl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["becomeModeratorAsInt", "SOAP::SOAPInt", [0, 1]],
      ["showAudioVideoTestAsInt", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectAndGenerateRoomHashResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectAndGenerateRoomHashResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectAndGenerateRoomHashByURL,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectAndGenerateRoomHashByURL"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["profilePictureUrl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]],
      ["room_id", "SOAP::SOAPLong", [0, 1]],
      ["becomeModeratorAsInt", "SOAP::SOAPInt", [0, 1]],
      ["showAudioVideoTestAsInt", "SOAP::SOAPInt", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectAndGenerateRoomHashByURLResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectAndGenerateRoomHashByURLResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectMainLandingZone,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectMainLandingZone"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["username", "SOAP::SOAPString", [0, 1]],
      ["firstname", "SOAP::SOAPString", [0, 1]],
      ["lastname", "SOAP::SOAPString", [0, 1]],
      ["profilePictureUrl", "SOAP::SOAPString", [0, 1]],
      ["email", "SOAP::SOAPString", [0, 1]],
      ["externalUserId", "SOAP::SOAPLong", [0, 1]],
      ["externalUserType", "SOAP::SOAPString", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => SetUserObjectMainLandingZoneResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "setUserObjectMainLandingZoneResponse"),
    :schema_element => [
      ["v_return", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetErrorByCode,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getErrorByCode"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["errorid", "SOAP::SOAPLong", [0, 1]],
      ["language_id", "SOAP::SOAPLong", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetErrorByCodeResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getErrorByCodeResponse"),
    :schema_element => [
      ["v_return", ["ErrorResult", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetUsersByOrganisation,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getUsersByOrganisation"),
    :schema_element => [
      ["sID", ["SOAP::SOAPString", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "SID")], [0, 1]],
      ["organisation_id", "SOAP::SOAPLong", [0, 1]],
      ["start", "SOAP::SOAPInt", [0, 1]],
      ["max", "SOAP::SOAPInt", [0, 1]],
      ["orderby", "SOAP::SOAPString", [0, 1]],
      ["asc", "SOAP::SOAPBoolean", [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetUsersByOrganisationResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getUsersByOrganisationResponse"),
    :schema_element => [
      ["v_return", ["SearchResult", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )

  LiteralRegistry.register(
    :class => GetSessionResponse,
    :schema_name => XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "getSessionResponse"),
    :schema_element => [
      ["v_return", ["Sessiondata", XSD::QName.new(NsServicesAxisOpenmeetingsOrg, "return")], [0, 1]]
    ]
  )
end
