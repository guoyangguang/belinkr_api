# encoding: utf-8
require_relative "../follow/collection"
require_relative "../status/collection"
require_relative "../tinto/exceptions"

module Belinkr
  module Reply
    class Enforcer
      include Tinto::Exceptions

      def self.authorize(user, action, status, reply)
        new(status, reply).send :"#{action}_by?", user
      end

      def initialize(status, reply)
        @status = status
        @reply  = reply
      end

      def create_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @status.entity_id
      end

      def read_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @status.entity_id
      end

      def update_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.id == @reply.user_id
        raise NotAllowed unless user.entity_id == @status.entity_id
      end

      alias_method :collection_by?, :read_by?
      alias_method :delete_by?,     :update_by?
      alias_method :undelete_by?,   :update_by?
      alias_method :destroy_by?,    :update_by?

    end # Enforcer
  end # Reply
end # Belinkr
