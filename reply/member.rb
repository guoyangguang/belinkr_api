# encoding: utf-8
require "json"
require "aequitas"
require "virtus"

module Belinkr
  module Reply
    class Member
      include Virtus
      include Aequitas

      MODEL_NAME = "reply"

      attribute :id,              Integer
      attribute :text,            String
      attribute :status_id,       Integer
      attribute :user_id,         Integer
      attribute :entity_id,       Integer
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of       :id, :text, :status_id, :user_id, :entity_id
      validates_length_of         :text, min: 3, max: 5000
      validates_numericalness_of  :id, :status_id, :user_id, :entity_id

      alias_method :to_hash, :attributes

      def initialize(*args)
        super *args
        self.created_at = self.updated_at = Time.now
      end

      def ==(other)
        to_hash.to_s == other.to_hash.to_s
      end

      def to_json(*args)
        attributes.to_json
      end
    end # Reply
  end # Member
end # Belinkr
