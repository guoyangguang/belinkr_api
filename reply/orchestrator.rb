# encoding: utf-8
require_relative "enforcer"
require_relative "../status/collection"

module Belinkr
  module Reply
    class Orchestrator
      def self.read(status, reply, user)
        reply
      end

      def self.collection(status, replies, user)
        replies
      end

      def self.create(status, reply, user)
        Enforcer.authorize(user, :create, status, reply)
        status.embedded_replies = 
          status.replies << sanitize(status, reply, user)
        status.save
        add_status_replies_collection(user, status) { |s_replies,replies_count=nil|
          exec_status_replies_action(s_replies, :add, status, Time.now.to_f)
        }
        reply
      end

      def self.update(status, reply, user)
        Enforcer.authorize(user, :update, status, reply)
        status.embedded_replies = 
          status.replies.update sanitize(status, reply, user)
        status.save
        reply
      end

      def self.delete(status, reply, user)
        Enforcer.authorize(user, :delete, status, reply)
        status.embedded_replies = status.replies.delete reply
        status.save
        add_status_replies_collection(user, status) { |s_replies,replies_count=nil|
          unless replies_count && replies_count >= 2
            exec_status_replies_action(s_replies, :remove, status)
          end
        }
        reply
      end

      def self.undelete(status, reply, user)
        Enforcer.authorize(user, :undelete, status, reply)
        status.embedded_replies = status.replies.undelete reply
        status.save
        add_status_replies_collection(user, status) { |s_replies,replies_count=nil|
          exec_status_replies_action(s_replies, :add, status, Time.now.to_f)
        }
        reply
      end

      private

      def self.sanitize(status, reply, user)
        reply.status_id = status.id
        reply.user_id   = user.id
        reply.entity_id = user.entity_id
        reply
      end

      def self.status_replies(status, user=nil)
        user_id = user ? user.id : status.user_id
        Status::Collection.new(
                              kind: 'replies',
                              entity_id: status.entity_id,
                              user_id: user_id
                              )
      end

      def self.add_status_replies_collection(user, status)
        return false if user.id == status.user_id
        if block_given?
          yield(status_replies(status), status.replies.count)
        end
        yield(status_replies(status, user)) if block_given?
      end

      def self.exec_status_replies_action(status_replies, action, *args)
        status_replies.send(action, *args)
      end
    end # Orchestrator
  end # Reply
end # Belinkr
