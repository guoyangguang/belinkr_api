# encoding: utf-8
require "json"

module Belinkr
  module Reply
    module Presenter
      class Collection
        def initialize(resource)
          @resource = resource
        end
        
        def as_poro
          replies = @resource.select { |reply| !reply.deleted_at }
          replies.map { |reply| Presenter::Member.new(reply).as_poro }
        end

        def as_json
          replies = as_poro.map { |reply| reply.to_json }.compact.join(", ") 
          "[#{replies}]"
        end
      end # Collection

      class Member
        BASE_PATH = "/statuses"

        def initialize(resource)
          @resource = resource
        end

        def as_json
          as_poro.to_json
        end

        def as_poro
          {
            id:             @resource.id,
            text:           @resource.text,
            user_id:        @resource.user_id,
            status_id:      @resource.status_id,
            created_at:     @resource.created_at,
            updated_at:     @resource.updated_at
          }
          .merge! mentions
          .merge! attachments
          .merge! errors
          .merge! links
        end

        private

        def mentions
          {}
        end

        def attachments
          {}
        end

        def links
          status_path = "#{BASE_PATH}/#{@resource.status_id}"
          {
            links: {
              self:    "#{status_path}/replies/#{@resource.id}",
              status:  status_path,
              user:    "/users/#{@resource.id}"
            }
          }
        end

        def errors
          @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
        end
      end # Member
    end # Presenter
  end # Status
end # Belinkr
