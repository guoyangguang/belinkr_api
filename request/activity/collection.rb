# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require_relative "member"

module Belinkr
  module Request
    module Activity
      class Collection
        extend Forwardable
        include Virtus
        include Aequitas
        include Enumerable

        MODEL_NAME = "activity"
      
        attribute :entity_id,        Integer
        attribute :request_id,        Integer

        validates_presence_of       :entity_id
        validates_numericalness_of  :entity_id   

        def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
          :exists?, :all, :page, :<<, :add, :remove, :delete, 
          :merge, :score

        def initialize(*args)
          super *args
          @sorted_set = Tinto::SortedSet.new(self, Activity::Member, storage_key)
        end

        def member_init_params
          { entity_id: entity_id, request_id:  request_id }
        end

        private

        def storage_key
          "entities:#{entity_id}:requests#{request_id}:activities"
        end
      end # Collection
    end # Activity
  end # Request
end # Belinkr
