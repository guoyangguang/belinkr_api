# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require_relative "../../config"
require_relative "../../tinto/member"
require_relative "../../polymorphic/polymorphic"

module Belinkr
  module Request
    module Activity
      class Member
        extend Forwardable
        include Virtus
        include Aequitas

        MODEL_NAME  = "activity"
        ACTIONS     = %w(accept reject)

        attribute :id,            Integer
        attribute :entity_id,     Integer
        attribute :request_id,    Integer
        attribute :actor,         Polymorphic
        attribute :action,        String
        attribute :object,        Polymorphic
        attribute :description,   String
        attribute :created_at,    Time
        attribute :updated_at,    Time

        validates_presence_of     :entity_id, :request_id, :actor, :action, :object 
        validates_within          :action, set: ACTIONS
        validates_length_of       :description, max: 250

        def_delegators :@member, :==, :score, :read, :save, :update, :to_json,
          :delete, :undelete 

        alias_method :to_hash, :attributes

        def initialize(*args)
          super(*args)
          @member = Tinto::Member.new self
        end

        def storage_key
          "entities:#{entity_id}:requests#{request_id}:activities"
        end
      end # Member
    end # Activity
  end # Request
end # Belinkr
