# encoding: utf-8
require "json"
require_relative "../../tinto/utils"

module Belinkr
  module Request
    module Activity
      module Presenter
        class Collection 
          def initialize(resource)
            @resource = resource
          end

          def as_poro
            @resource.all.map { |member| Presenter::Member.new(member).as_poro }
          end

          def as_json
            "[#{as_poro.map { |i| i.to_json }.join(",")}]"
          end
        end #Collection

        class Member
          attr_accessor :resource

          def initialize(resource)
            @resource = resource
          end

          def as_json
            as_poro.to_json
          end

          def as_poro
            user = OpenStruct.new(timezone: "Europe/Madrid")
            local_created_at = 
              Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
            local_updated_at = 
              Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)

            @resource.attributes.merge!({
              created_at: local_created_at,
              updated_at: local_updated_at
            })
            .merge! errors
          end

          def errors
            @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
          end
        end #Member
      end # Presenter
    end # Activity
  end # Request
end # Belinkr
