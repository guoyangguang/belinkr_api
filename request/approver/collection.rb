# encoding: utf-8

require "virtus"
require "aequitas"
require_relative "../../tinto/sorted_set"
require_relative "../../tinto/exceptions"
require_relative "../../user/member"

module Belinkr
  module Request
    module Approver
      class Collection
        extend Forwardable
        include Enumerable
        include Virtus
        include Aequitas

        MODEL_NAME = "user"

        attribute :request_id,    Integer
        attribute :entity_id,       Integer

        validates_presence_of       :request_id, :entity_id
        validates_numericalness_of  :request_id, :entity_id

        def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                       :exists?, :all, :page, :<<,  :remove, :delete, 
                       :merge, :score

        def initialize(*args)
          super(*args)
          @sorted_set = Tinto::SortedSet.new(self, User::Member, storage_key)
        end

        def add(user)
          @sorted_set.add(user)
        end

        def member_init_params
          { entity_id: entity_id, request_id: request_id }
        end

        private

        def storage_key
          "entities:#{entity_id}:requests:#{request_id}:approvers"
        end

      end # Collection
    end # Approver
  end # Request
end # Belinkr
