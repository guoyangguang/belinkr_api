# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Request
    class Collection
      extend Forwardable
      include Virtus
      include Aequitas
      include Enumerable

      MODEL_NAME  = "request"
      KINDS       = %w(from to approver_ids approved_by_ids rejected_by_ids)

      attribute :entity_id,  Integer 
      attribute :user_id,    Integer # id of the user, who may be issuer, operator, or grantor of request
      attribute :kind,       String# will be within of TYPES

      validates_presence_of       :user_id, :kind, :entity_id
      validates_numericalness_of  :user_id, :entity_id
      validates_within            :kind, set: KINDS

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, 
                     :merge, :score, :oldest

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Request::Member, storage_key)
      end

      def member_init_params
        { entity_id: entity_id, user_id: user_id }
      end

      private

      def storage_key
        "entities:#{entity_id}:users:#{user_id}:requests:#{kind}"
      end
    end # Collection
  end # Request
end # Belinkr
