# encoding: utf-8
require "virtus"
require "aequitas"
require "statemachine"
require_relative "../tinto/member"
require_relative "activity/collection"

module Belinkr
  module Request
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "request"

      attribute :id,               Integer
      attribute :entity_id,        Integer
      attribute :title,            String
      attribute :description,      String
      attribute :state,            String, default: "pending"
      attribute :from,             Integer #creator
      attribute :to,               Integer #operator
      attribute :approver_ids,     Array   #grantor
      attribute :approved_by_ids,  Array   #granted
      attribute :rejected_by_ids,  Array   #rejecter
      attribute :events,           Array   #object of Event
      attribute :created_at,       Time
      attribute :updated_at,       Time
      attribute :deleted_at,       Time

      validates_presence_of       :from, :to, :title, :state, :entity_id
      validates_length_of         :title, min: 3, max: 150
      validates_numericalness_of  :from, :to, :entity_id

      def_delegators :@state_machine, :state, :state=, :accept, :reject
      def_delegators :@member, :==, :score, :to_json, :read, :save, :delete, 
                                :undelete, :destroy
      alias_method :to_hash, :attributes

      def initialize(*args)
        super *args
        @member = Tinto::Member.new self

        @state_machine = Statemachine.build do
          trans :pending, :accept, :accepted
          trans :pending, :reject, :rejected
        end
      end

      def accepted?
        state == "accepted"
      end

      def rejected?
        state == "rejected"
      end

      def state
        sync_state
        super
      end

      def state=(state)
        @state_machine.state = state if @state_machine
        super(state)
      end

      def register_the_activity(action, actor, description)
        activity  = Activity::Member.new(
          entity_id:    entity_id,
          request_id:   self.id,
          actor:        actor,
          action:       action,
          object:       self,
          description:  description
        ).save
        activities.add activity
      end

      def activities
        Activity::Collection.new(
          entity_id:    entity_id,
          request_id:   id
        )
      end

      def storage_key
        "entities:#{entity_id}:requests"
      end

      private

      def sync_state
        self.state = @state_machine.state.to_s
      end

    end # Member
  end # Request
end # Belinkr
