# encoding: utf-8
require_relative "collection"
require_relative "approver/collection"


module Belinkr
  module Request
    class Orchestrator

      def self.read(request, user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == request.entity_id 
        request
      end

      def self.collection(requests, user)
        requests
      end

      def self.activities(request, user)
        request.activities
      end

      def self.create(request, user)
        sanitize(request, user)
        request.save
        request_from_collection(request).add request
        request_to_collection(request)  .add request
        request.approver_ids.each do |user_id|
          request_approver_collection(request, user_id).add request
        end
        
        request
      end

      def self.update(request, updated, user)
        request.attributes = updated.attributes
        sanitize(request, user)
        request.save
        request
      end #.update

      def self.delete(request, user)
        sanitize(request, user)
        request.delete
        $redis.multi do
          request_from_collection(request).remove request
          request_to_collection(request)  .remove request
          request.approver_ids.each do |user_id|
            request_approver_collection(request, user_id).remove request
          end
        end
        request
      end

      def self.undelete(request, user)
        sanitize(request, user)
        request.undelete
        $redis.multi do
          request_from_collection(request).add request
          request_to_collection(request)  .add request
          request.approver_ids.each do |user_id|
            request_approver_collection(request, user_id).add request
          end
        end
        request
      end

      def self.destroy(request, user)
        delete(request, user).destroy
        request
      end

      def self.approve(request, approver, description = nil)
        action = Belinkr::Request::Activity::Member::ACTIONS[0] #accept
        request.approver_ids.delete approver.id
        request.approved_by_ids.push approver.id
        request.accept if request.approver_ids.empty?

        request.register_the_activity(action, approver, description)

        request.save
        
        request_approver_collection(request, approver.id).remove request
        request_approved_collection(request, approver.id).add    request
        
        request
      end

      def self.reject(request, rejector, description = nil)
        action = Belinkr::Request::Activity::Member::ACTIONS[1] #reject
        request.approver_ids.delete rejector.id
        request.rejected_by_ids.push rejector.id
        request.reject

        request.register_the_activity(action, rejector, description)

        request.save

        request_approver_collection(request, rejector.id).remove request
        request_rejected_collection(request, rejector.id).add    request
        
        request
      end #.reject

      def self.activate(request, actor_id, description)
        approver = Belinkr::User::Member.new(
          id:        actor_id,
          entity_id: request.entity_id
        )
        approve(request, approver, description)
        Request::Approver::Collection.new(
          entity_id:  request.entity_id,
          request_id: request.id
        ).add approver

        approver
      end

      private

      def self.sanitize(request, user)
        request.from      = user.id
        request.entity_id = user.entity_id
        request
      end

      def self.request_from_collection(request)
        Request::Collection.new(
          entity_id: request.entity_id,
          kind:      "from",
          user_id:   request.from)
      end

      def self.request_to_collection(request)
        Request::Collection.new(
          entity_id: request.entity_id,
          kind:      "to",
          user_id:   request.to)
      end

      def self.request_approver_collection(request, user_id)
        Request::Collection.new(
            entity_id: request.entity_id,
            kind:      "approver_ids",
            user_id:   user_id)
      end

      def self.request_approved_collection(request, user_id)
        Request::Collection.new(
            entity_id: request.entity_id,
            kind:      "approved_by_ids",
            user_id:   user_id)
      end

      def self.request_rejected_collection(request, user_id)
        Belinkr::Request::Collection.new(
          entity_id: request.entity_id,
          kind:      "rejected_by_ids",
          user_id:   user_id
        ).add request
      end
      
    end # Orchestrator
  end # Request
end # Belinkr
