# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Scrapbook
    class Collection
      extend Forwardable
      include Virtus
      include Aequitas
      include Enumerable

      MODEL_NAME  = "scrapbook"
      KINDS       = %w{ own others }

      attribute :user_id,         Integer
      attribute :kind,            String

      validates_presence_of       :user_id, :kind
      validates_numericalness_of  :user_id
      validates_within            :kind, set: KINDS

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, :merge

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet
                        .new(self, Scrapbook::Member, storage_key)
      end

      def member_init_params
        { user_id: user_id }
      end

      private

      def storage_key
        "users:#{user_id}:scrapbooks:#{kind}"
      end
    end # Collection
  end # Scrapbook
end # Belinkr
