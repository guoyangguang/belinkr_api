# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "../tinto/member"

module Belinkr
  module Scrapbook
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "scrapbook"

      attribute :id,              Integer
      attribute :name,            String
      attribute :user_id,         Integer
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of       :name, :user_id
      validates_numericalness_of  :user_id
      validates_length_of         :name, min: 3, max: 250

      def_delegators :@member, :==, :score, :to_json, :read, :save, 
                                :delete, :undelete, :destroy

      alias_method :to_hash, :attributes

      def initialize(*args)
        super(*args)
        @member = Tinto::Member.new self
      end

      def storage_key
        "users:#{user_id}:scrapbooks"
      end
    end # Member
  end # Scrapbook
end # Belinkr
