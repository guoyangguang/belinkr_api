# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "scrap/orchestrator"

module Belinkr
  module Scrapbook
    class Orchestrator
      include Tinto::Exceptions

      def self.read(scrapbook, user)
        raise NotAllowed unless user.id
        scrapbook
      end

      def self.collection(scrapbooks, user)
        raise NotAllowed unless user.id
        scrapbooks
      end
      
      def self.create(scrapbook, user)
        sanitize(scrapbook, user)
        scrapbook.save
        scrapbooks_for(user).add scrapbook
        scrapbook
      end

      def self.update(scrapbook, updated, user)
        sanitize(updated, user)
        scrapbook.name = updated.name
        scrapbook.save
        scrapbook
      end

      def self.delete(scrapbook, user)
        $redis.multi do
          scrapbook.delete
          scrapbooks_for(user).remove scrapbook
        end
        scrapbook
      end
     
      def self.undelete(scrapbook, user)
        $redis.multi do 
          scrapbook.undelete
          scrapbooks_for(user).add scrapbook
        end
        scrapbook
      end

      def self.destroy(scrapbook, user)
        delete(scrapbook, user).destroy
        scrapbook
      end

      private

      def self.scrapbooks_for(user)
        Scrapbook::Collection.new(user_id: user.id, kind: "own")
      end

      def self.sanitize(scrapbook, user)
        scrapbook.user_id = user.id
      end
    end # Orchestrator
  end # Scrapbook
end # Belinkr
