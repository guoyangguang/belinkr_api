# encoding: utf-8
require "json"
require_relative "../tinto/utils"

module Belinkr
  module Scrapbook
    module Presenter

      class Collection
        def initialize(resource)
          @resource = resource
        end

        def as_poro
          @resource.map { |member| Presenter::Member.new(member).as_poro }
        end

        def as_json
          "[#{as_poro.map { |i| i.to_json }.join(",")}]"
        end
      end # Collection

      class Member
         
        BASE_PATH = '/scrapbooks'

        def initialize(resource)
          @resource = resource
        end

        def as_poro
          user = OpenStruct.new(timezone: "Europe/Madrid")
          local_created_at =
            Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
          local_updated_at =
            Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)

          {
            id:         @resource.id,
            name:       @resource.name,
            created_at: local_created_at,
            updated_at: local_updated_at
          }.merge! errors
           .merge! links
        end

        def as_json
          as_poro.to_json
        end
       
        private 

        def links
          {
            links: {
                    self: "#{BASE_PATH}/#{@resource.id}",
                    user: "/users/#{@resource.user_id}",
                    scraps: "#{BASE_PATH}/#{@resource.id}/scraps"
                    }
          }
        end

        def errors
          @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
        end
      end #Member
    end # Presenter
  end # Scrapbook
end #Belinkr
