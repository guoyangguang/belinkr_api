# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../../tinto/sorted_set"

module Belinkr
  module Scrapbook
    module Scrap
      class Collection
        extend Forwardable
        include Virtus
        include Aequitas
        include Enumerable

        MODEL_NAME  = "scrap"

        attribute :scrapbook_id,    Integer
        attribute :user_id,         Integer

        validates_presence_of       :scrapbook_id, :user_id
        validates_numericalness_of  :scrapbook_id, :user_id

        def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                                     :exists?, :all, :page, :<<, :add, :remove, 
                                     :delete, :merge

        def initialize(*args)
          super *args
          @sorted_set = Tinto::SortedSet.new(self, Scrap::Member, storage_key)
        end

        def member_init_params
          { user_id: user_id, scrapbook_id: scrapbook_id }
        end

        private

        def storage_key
          "users:#{user_id}:scrapbooks:#{scrapbook_id}:scraps"
        end
      end # Collection
    end # Scrap
  end # Scrapbook
end # Belinkr
