# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "../../tinto/member"

module Belinkr
  module Scrapbook
    module Scrap
      class Member
        extend Forwardable
        include Virtus
        include Aequitas

        MODEL_NAME = "scrap"

        attribute :id,              Integer
        attribute :text,            String
        attribute :user_id,         Integer
        attribute :scrapbook_id,    Integer
        attribute :created_at,      Time
        attribute :updated_at,      Time
        attribute :deleted_at,      Time

        validates_presence_of       :scrapbook_id, :user_id, :text
        validates_numericalness_of  :scrapbook_id, :user_id
        validates_length_of         :text, min: 3, max: 5000

        def_delegators :@member, :==, :score, :to_json, :read, :save,
                                  :delete, :undelete, :destroy

        alias_method :to_hash, :attributes

        def initialize(*args)
          super *args
          @member = Tinto::Member.new self
        end

        def storage_key
          "users:#{user_id}:scrapbooks:#{scrapbook_id}:scraps"
        end
      end # Member
    end # Scrap
  end # Scrapbook
end # Belinkr
