# encoding: utf-8
require_relative "member"
require_relative "collection"

module Belinkr
  module Scrapbook
    module Scrap
      class Orchestrator
        def self.collection(scrapbook, scraps, user)
          scraps
        end

        def self.read(scrapbook, scrap, user)
          scrap
        end

        def self.create(scrapbook, scrap, user)
          sanitize(scrap, scrapbook, user)
          scrap.save
          scraps_for(scrapbook).add scrap
          scrap
        end

        def self.update(scrapbook, scrap, updated, user)
          sanitize(updated, scrapbook, user)
          scrap.text = updated.text
          scrap.save
          scrap
        end

        def self.delete(scrapbook, scrap, user)
          $redis.multi do
            scrap.delete
            scraps_for(scrapbook).remove scrap
          end

          scrap
        end

        def self.undelete(scrapbook, scrap, user)
          $redis.multi do
            scrap.undelete
            scraps_for(scrapbook).add scrap
          end

          scrap
        end

        def self.destroy(scrapbook, scrap, user)
          delete(scrapbook, scrap, user).destroy 
          scrap
        end

        private
        
        def self.sanitize(scrap, scrapbook, user)
          scrap.scrapbook_id = scrapbook.id
          scrap.user_id = user.id
        end

        def self.scraps_for(scrapbook)
          Collection.new(scrapbook_id: scrapbook.id, user_id: scrapbook.user_id)
        end
      end # Orchestrator
    end # Scrap
  end # Scrapbook
end # Belinkr
