# encoding: utf-8
require "redis"
require "bcrypt"
require_relative "../config"
require_relative "../credential/member"
require_relative "../tinto/exceptions"
require_relative "../tinto/utils"

module Belinkr
  class Authenticator
    STORAGE_KEY = Config::AUTHENTICATOR_STORAGE_KEY

    def self.add(credential)
      raise Tinto::Exceptions::InvalidResource if exists?(credential.email)

      credential.save unless credential.id
      $redis.hset(STORAGE_KEY, credential.email, credential.id) 
    end

    def self.remove(credential)
      $redis.hdel(STORAGE_KEY, credential.email)
    end

    def self.exists?(email)
      $redis.hexists(STORAGE_KEY, email)
    end

    def self.keys
      $redis.hkeys(STORAGE_KEY)
    end

    def self.authenticate(email, plaintext)
      credential = get(email)
      return credential if credential &&  BCrypt::Password
                            .new(credential.password).is_password?(plaintext)
      return false
    end

    def self.get(email)
      return false unless exists?(email)
      Credential::Member.new(id: $redis.hget(STORAGE_KEY, email))
    end
  end # Authenticator
end # Belinkr
