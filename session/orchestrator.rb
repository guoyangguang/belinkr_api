# encoding: utf-8
require_relative "authenticator"
require_relative "../user/member"
require_relative "../tinto/exceptions"

module Belinkr
  module Session
    class Orchestrator
      def self.create(session, email, plaintext)
        credential = Authenticator.authenticate(email, plaintext)
        raise Tinto::Exceptions::NotAllowed unless credential

        user = User::Member.new(
          id:         credential.user_ids.first,
          entity_id:  credential.entity_ids.first
        )
        session[:user_id]   = user.id
        session[:entity_id] = user.entity_id

        user
      end #.create

      def self.delete(session)
        session[:user_id]   = nil
        session[:entity_id] = nil
      end
    end # Orchestrator
  end # Session
end # Belinkr
