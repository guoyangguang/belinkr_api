# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.set :sessions, false

Belinkr::API.get '/__just_for_helpers__' do
  status 200
  respond_to?(params[:helper].to_sym).to_s
end

def session
  {"rack.session" => {:user_id => 1, :entity_id => 1}}
end

def app;Belinkr::API.new;end

describe 'API app helpers' do
  include Rack::Test::Methods

  describe '#file_mover' do
    it "app should respond to the helper" do
      get '/__just_for_helpers__', { helper: :file_mover }, session
      last_response.status.must_equal 200
      last_response.body.must_equal "true"
    end

  end # file_mover

  describe "#authorized?" do
    it "without session passed, returns with status 200 when authorized" do 
      authorize Belinkr::Config::AUTH_USERNAME, Belinkr::Config::AUTH_PASSWORD
      get '/__just_for_helpers__', { helper: :file_mover }
      last_response.status.must_equal 200
    end

    it "without session passed,returns with status 401 when authorized" do 
      get '/__just_for_helpers__', { helper: :file_mover } 
      last_response.status.must_equal 401
    end
  end #authorized?
end # API app helpers
