# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user  = Belinkr::User::Member
               .new(first: "User", last: "111", entity_id: 1).save
  end

  describe "POST /appointments" do
    it "create a appointment" do
      appointment = create_appointment
      appointment['id'].wont_be_nil
      appointment['name'].must_equal 'appointment'
    end
  end # POST /appointments

  describe "GET /appointments" do
    it "it will be return nothing" do
      5.times do
        create_appointment
      end

      get "/appointments", {}, session

      last_response.status.must_equal 200

      appointments = JSON.parse(last_response.body)
      appointments.length  .must_equal 5
      appointments[0]['id'].must_equal 5
      appointments[4]['id'].must_equal 1
    end
  end # GET /appointments

  describe "PUT /appointments/:id" do
    it "replaces a appointment that already exists" do
      appointment = create_appointment

      put "/appointments/#{appointment['id']}", {
          name: 'appointment update name',
          description: 'appointment update description'
      }.to_json, session

      last_response.status.must_equal 200
      update_appointment = JSON.parse(last_response.body)
      update_appointment['name'].must_equal 'appointment update name'
      update_appointment['description']
        .must_equal 'appointment update description'
    end
  end # PUT /appointments/:id

  describe "GET /appointments/:id" do
    it "read a appointment" do
      appointment = create_appointment

      get "/appointments/#{appointment['id']}", {}, session
      last_response.status.must_equal 200
      get_appointment = JSON.parse(last_response.body)
      get_appointment['id'].must_equal appointment['id']
    end
  end # GET /appointments/:id

  describe "DELETE /appointments/:id" do
    it "delete a appointments" do
      appointment = create_appointment
      appointment['deleted_at'].must_be_nil

      delete "/appointments/#{appointment['id']}", {}, session
      last_response.status.must_equal 204
      last_response.body.must_be_empty
    end
  end # DELETE /appointments/:id


  def create_appointment(args={})
    post "/appointments", {
        name: args[:name] || 'appointment',
        description: args[:description] || 'appointment'
    }.to_json, session

    last_response.status.must_equal 201
    JSON.parse(last_response.body)
  end

end # Belinkr::API