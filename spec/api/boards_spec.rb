#encoding: utf-8

require 'minitest/autorun'
require 'rack/test'
require_relative '../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)
def app; Belinkr::API.new;end

include Belinkr

describe Belinkr::API do
  include Rack::Test::Methods
  
  before do
    $redis.flushdb
    
    @user = User::Member
      .new(first: 'user', last: '111', entity_id: 1).save
    
    post "/workspaces", {name: 'Workspace 1'}.to_json, session
    @workspace = JSON.parse(last_response.body)

    post "/workspaces/#{@workspace['id']}/boards", {
        name: 'board 1'}.to_json, session
    @board = JSON.parse(last_response.body)
  end

  describe "POST /workspaces/:workspace_id/boards" do
    it "creates a board if successful" do
      post "/workspaces/#{@workspace['id']}/boards",{
        name: 'Board 1'}.to_json, session 
      board = JSON.parse(last_response.body)

      last_response.status.must_equal 201
      board['id'].wont_be_nil
      board['entity_id'].must_equal @user.entity_id
      board['workspace_id'].must_equal @workspace['id']
      board['user_id'].must_equal @user.id
      board['name'].must_equal 'Board 1'
      board['created_at'].wont_be_nil
    end

    it "presents errors if the creating board is invalid" do
      post "/workspaces/#{@workspace['id']}/boards", {
        name: ''}.to_json, session
      last_response.status.must_equal 400
      board = JSON.parse(last_response.body)
      board['errors'].wont_be_empty
    end
  end #POST /workspaces/:workspace_id/boards
 
  describe "GET /workspaces/:workspace_id/boards/:id" do
    it "returns the board" do
      get "/workspaces/#{@workspace['id']}/boards/#{@board['id']}",
        {}, session 

      last_response.status.must_equal 200 
      board = JSON.parse(last_response.body)
      @board['id'].must_equal board['id']
      @board['name'].must_equal board['name'] 
      @board['user_id'].must_equal board['user_id']
    end
  end

  describe "GET /workspaces/:workspace_id/boards" do
    it "return a board collection" do
      get "/workspaces/#{@workspace['id']}/boards",
        {}, session
     
      last_response.status.must_equal 200
      boards = JSON.parse(last_response.body)
      boards.length.must_equal 1
      board = boards[0]
      board["id"].must_equal @board["id"]
      board["name"].must_equal @board["name"]
      board["errors"].must_be_nil
    end
  end

  def session
    {"rack.session" => {:user_id => @user.id, :entity_id => @user.entity_id}}
  end
end#Belinkr::API
