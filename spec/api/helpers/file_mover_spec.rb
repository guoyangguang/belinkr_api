# encoding: utf-8
require 'minitest/autorun'
require 'mocha'

require_relative "../../../api/helpers/file_mover"

include Belinkr::ApiHelpers

module ApiHelperTest
  extend self

  def test_callee
    get_caller(caller[1])
  end

  # here: no dynamic
  def create(*args)
    yield args if block_given?
    "create"
  end

  def update(*args)
    yield args if block_given?
    "update"
  end

  def delete(*args)
    yield args if block_given?
    "delete"
  end

  def destroy(*args)
    yield args if block_given?
    "destroy"
  end

  def not_support(*args)
    yield args if block_given?
    "not_support"
  end

end

describe Belinkr::ApiHelpers do
  describe "#get_caller" do
    it "return empty if not match the caller" do
      get_caller('not-match').must_be_empty
    end

    describe "return the caller" do
      it "manually spec" do
        caller_string = "rb_file.rb:88:in `here_we_go'"
        ApiHelperTest.get_caller(caller_string).must_equal "here_we_go"
      end

      it "caller is spec unit run" do
        ApiHelperTest.test_callee.must_equal "run"
      end
    end
  end # get_caller

  describe "#file_mover" do
    it "return a proc" do
      obj = file_mover
      obj.is_a?(Proc).must_equal true
    end

    it "the proc has many arguments" do
      obj = file_mover
      obj.is_a?(Proc).must_equal true
      obj.arity.must_equal -1
    end

    describe "caller methods" do
      describe "valid CUDD" do
        before do
          @actions = %w(create update delete destroy)
          prefix = "enqueue_for_"
          # expects invoked twice. one for: &, the other: call.
          @actions.each do |action|
            method_name = "#{prefix}#{action}".to_sym
            Tinto::FileScheduler.expects(method_name)
              .with("some", "arguments").twice
          end
        end

        it "respond to CUDD" do
          @actions.each do |method_name|
            ApiHelperTest.send(method_name.to_sym,
                               "some", "arguments", &file_mover
            ).must_equal method_name

            ApiHelperTest.send(method_name.to_sym, "some", "arguments") {|args|
              file_mover.call(args)
            }.must_equal method_name
          end
        end
      end # valid CUDD

      it "raise a exception if caller method is not supported" do
        lambda {
          ApiHelperTest.not_support("a", "b") do |args|
            file_mover.call(args)
          end
        }.must_raise RuntimeError
        lambda {
          ApiHelperTest.not_support("a", "b", &file_mover)
        }.must_raise RuntimeError
      end
    end # caller methods
  end # file_mover

end # Belinkr::ApiHelper