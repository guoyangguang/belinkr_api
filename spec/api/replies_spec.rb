# encoding: utf-8
require "minitest/autorun"
require "rack/test"
require_relative "../../api"

ENV["RACK_ENV"] = "test"
Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

describe Belinkr::API do
  include Rack::Test::Methods

  before do 
    $redis.flushdb 
    @user = Belinkr::User::Member
      .new(first: "User", last: "111", entity_id: 1).save
  end

  describe "GET /statuses/:id/replies" do
    it "retrieves all replies of a given status, 
    sorted by the creation time" do
      status, reply = create_status_and_reply
      post "/statuses/#{status["id"]}/replies", 
            { text: "another reply", user_id: 2 }.to_json, session

      get "/statuses/#{status["id"]}/replies", {}, session
      replies = JSON.parse(last_response.body)

      replies.length                .must_equal 2
      replies[0]["text"]            .must_equal "my reply"
      replies[0]["status_id"]       .must_equal status["id"]
      replies[1]["text"]            .must_equal "another reply"
      replies[1]["status_id"]       .must_equal status["id"]
    end

    it "are empty if no replies posted" do
      status = create_status
     
      get "/statuses/#{status["id"]}", {}, session
      status = JSON.parse(last_response.body)

      status["replies"].must_be_empty
    end
  end # GET /statuses/:status_id/:id/replies

  describe "GET /statuses/:status_id/replies/:id" do
    it "returns the reply" do
      status, reply = create_status_and_reply

      get "/statuses/#{status["id"]}/replies/#{reply["id"]}", {}, session
      reply = JSON.parse(last_response.body)

      last_response.status  .must_equal 200

      reply["id"]             .wont_be_nil
      reply["text"]           .must_equal "my reply"
      reply["status_id"]      .must_equal status["id"]
      reply["created_at"]     .wont_be_nil
      reply["updated_at"]     .wont_be_nil

      check_links_for(status, reply) 
    end 
  end # GET /statuses/:status_id/replies/:id

  describe "POST /statuses/:status_id/replies" do
    it "creates a new reply if sucessful" do
      status, reply = create_status_and_reply
      last_response.status.must_equal 201

      reply["id"]             .wont_be_nil
      reply["text"]           .must_equal "my reply"
      reply["status_id"]      .must_equal status["id"]
      reply["created_at"]     .wont_be_nil
      reply["updated_at"]     .wont_be_nil
      
      check_links_for(status, reply)
    end
  end # POST /statuses/:status_id/replies

  describe "PUT /statuses/:status_id/replies/:id" do
    it "replaces a reply if sucessful" do
      status, reply = create_status_and_reply
      reply["text"] = "my updated reply"
      
      put "/statuses/#{status["id"]}/replies/#{reply["id"]}", 
            reply.to_json, session

      last_response.status.must_equal 200
      reply["text"].must_equal "my updated reply"

      check_links_for(status, reply)
    end

    it "presents an errors hash if resource invalid" do
      status, reply = create_status_and_reply

      put "/statuses/#{status["id"]}/replies/#{reply["id"]}", 
        { id: reply["id"], text: "", user_id: 1, entity_id: 1 }.to_json, session

      last_response.status.must_equal 400
      reply = JSON.parse(last_response.body)

      reply["errors"].wont_be_empty
    end

    it "localizes errors when an Accept-Language header is passed" do
      header "Accept-Language", "es_ES"
      status, reply = create_status_and_reply

      put "/statuses/#{status["id"]}/replies/#{reply["id"]}", 
        { id: reply["id"], text: "", user_id: 1, entity_id: 1 }.to_json, session
      last_response.status.must_equal 400
      reply = JSON.parse(last_response.body)

      reply["errors"].wont_be_empty
      reply["errors"]["text"].must_include "texto no debe quedar vacío"
    end
  end # PUT /statuses/:status_id/replies/:id

  describe "DELETE /statuses/:id/replies/:id" do
    it "deletes an existing reply" do
      status, reply = create_status_and_reply

      get "/statuses/#{status["id"]}/replies", {}, session
      last_response.status.must_equal 200
      replies = JSON.parse(last_response.body)
      replies.wont_be_empty

      delete "/statuses/#{status["id"]}/replies/#{reply["id"]}", {}, session
      last_response.status.must_equal 204
      last_response.body.must_be_empty

      get "/statuses/#{status["id"]}/replies", {}, session
      last_response.status.must_equal 200
      replies = JSON.parse(last_response.body)
      replies.must_be_empty
    end
  end # DELETE /statuses/:status_id/replies:id
end # Belinkr::API

def session
  { "rack.session" => { user_id: 1, entity_id: 1 } }
end

def create_status
  post "/statuses", { text: "my status" }.to_json, session
  status = JSON.parse(last_response.body)
end

def create_status_and_reply
  status = create_status

  post "/statuses/#{status["id"]}/replies", 
    { text: "my reply" }.to_json, session
  reply = JSON.parse(last_response.body)

  [status, reply]
end

def check_links_for(status, reply)
  links = reply["links"]
  links["self"]
    .must_equal "/statuses/#{status["id"]}/replies/#{reply["id"]}"
  links["status"].must_equal "/statuses/#{status["id"]}"
end
