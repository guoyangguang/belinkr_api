# encoding: utf-8
require "minitest/autorun"
require "rack/test"
require_relative "../../api"

ENV["RACK_ENV"] = "test"
Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

describe Belinkr::API do
  include Rack::Test::Methods

  before do 
    $redis.flushdb 
    @user = Belinkr::User::Member
      .new(first: "User", last: "111", entity_id: 1).save
    @entity = OpenStruct.new(id: 1)
    @boss = Belinkr::User::Member
      .new(first: "Boss first name", last: "Boss last name", entity_id: 1).save
    @dean   = OpenStruct.new(id: 2)
    @worker = OpenStruct.new(id: 3)
  end

  describe "GET /requests" do
    it "gets requests for current user" do
      25.times {|i| create_request title: "title #{i} placeholder"}

      get "/requests", {}, session

      last_response.status.must_equal 200
      requests = JSON.parse(last_response.body)   
      requests.length        .must_equal 20
      requests[0]["title"]   .must_equal "title 24 placeholder"
      requests.last["title"] .must_equal "title 5 placeholder"

    end
  end # GET /requests

  describe "GET /requests/:id" do
    it "returns the request" do
      request = create_request

      get "/requests/#{request["id"]}", {}, session
      get_request = JSON.parse(last_response.body)
      last_response.status  .must_equal 200
      get_request["id"]     .wont_be_nil
      get_request["id"]     .must_equal request["id"]
    end

    it "returns 404 if the resource doesn't exist" do
      get "/requests/999", {}, session
      last_response.status.must_equal 404
      last_response.body.must_be_empty
    end
  end # GET /requests/:id

  describe "POST /requests" do
    it "creates a new request if sucessful" do
      request = create_request
      last_response.status.must_equal 201

      request["created_at"]  .wont_be_nil
      request["id"]          .wont_be_nil
      request["entity_id"]   .must_equal @entity.id
    end
  end # POST /requests

  describe "PUT /requests/:id" do
    it "replaces the request if it already exists" do
      request           = create_request
      updated_title     = "my updated title"
      request["title"]  = updated_title

      put "/requests/#{request["id"]}", request.to_json, session
      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)

      updated["id"]         .must_equal request["id"]
      updated["title"]      .must_equal updated_title
    end

    it "presents an errors hash if resource invalid" do
      request          = create_request
      request["title"] = nil

      put "/requests/#{request["id"]}", request.to_json, session
      last_response.status.must_equal 400
      request = JSON.parse(last_response.body)
      request["errors"].wont_be_empty
    end
  end # PUT /requests/:id

  describe "DELETE /requests/:id" do
    it "deletes a request" do
      request = create_request

      delete "/requests/#{request["id"]}", {}, session
      last_response.status.must_equal 204
      last_response.body.must_be_empty
    end
  end # DELETE /requests/:id

  describe "POST /requests/:id/approvers" do
    it "add the user who just approve request to approvers collection" do
      request = create_request
      params  = {user_id: @boss.id, description: "ok, go ahead"}
      post "/requests/#{request["id"]}/approvers", params.to_json, session
      last_response.status.must_equal 201
      approver = JSON.parse(last_response.body)
      approver["last"]    .must_equal @boss.last
      approver["first"]   .must_equal @boss.first
    end
  end # POST /requests/:id/approvers

  describe "GET /requests/:id/activities" do
    it "gets all activities related to this request" do
      request = create_request
      params  = [{user_id: @boss.id, description: "ok, go ahead"},
                 {user_id: @dean.id, description: "Go on, Buddy"}]
      params.each do |param|
        post "/requests/#{request["id"]}/approvers", param.to_json, session
      end

      get "/requests/#{request["id"]}/activities", {}.to_json, session
      last_response.status.must_equal 200
      activities = JSON.parse(last_response.body)   
      activities.length                       .must_equal 2
      activities[0]["actor"]["resource"]["id"].must_equal params[1][:user_id]
      activities[0]["description"]            .must_equal params[1][:description]
      activities[1]["actor"]["resource"]["id"].must_equal params[0][:user_id]
      activities[1]["description"]            .must_equal params[0][:description]
    end
  end # GET /requests/:id/activities
end # Belinkr::API

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

def create_request(options={})
  post "/requests", {
    title:                "Apply to be off work",
    entity_id:            @entity.id,
    description:          "Could I be off work tomorrow please?",
    from:                 @user.id,
    to:                   @worker.id,
    approver_ids:         [@boss.id, @dean.id],
    approved_by_ids:      [],
    rejected_by_ids:      [],
    events:               []
  }.merge!(options).to_json, session
  JSON.parse(last_response.body)
end
