# encoding: utf-8
require "minitest/autorun"
require "rack/test"
require_relative "../../api"

ENV["RACK_ENV"] = "test"
Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user = Belinkr::User::Orchestrator.create(
      Belinkr::User::Member.new(first: "User", last: "111", entity_id: 1))
  end

  describe "POST /scrapbooks" do
    it "creates a new scrapbook if sucessful" do
      post "/scrapbooks", {name: 'Scrapbook 1'}.to_json, session 

      last_response.status.must_equal 201
      scrapbook = JSON.parse (last_response.body)
      scrapbook["created_at"]  .wont_be_nil
      scrapbook["id"]          .wont_be_nil
    end
  end # POST /scrapbooks


  describe "GET /scrapooks/:id" do
    it "returns the scrapbook" do
      created_scrapbook = create_scrapbook

      get "/scrapbooks/#{created_scrapbook["id"]}", {}, session
      scrapbook = JSON.parse(last_response.body)

      last_response.status  .must_equal 200
      scrapbook["id"]   .wont_be_nil
      scrapbook["id"]   .must_equal created_scrapbook["id"]
    end

    it "returns 404 if the resource doesn't exist" do
      get "/scrapbooks/999", {}, session

      last_response.status  .must_equal 404
      last_response.body    .must_be_empty
    end
  end # GET /scrapbooks/:id

  describe "GET /scrapbooks" do
    it "returns an array of the 20 most recent scrapbooks from owner,
         sorted by creation time" do

      20.times {|i| create_scrapbook(name: "my scrapbook #{i}")}

      get "/scrapbooks", {}, session
      
      last_response.status    .must_equal 200
      scrapbooks = JSON.parse(last_response.body)
      scrapbooks.length       .must_equal 20
      scrapbooks[0]["name"]   .must_equal "my scrapbook 19"
      scrapbooks[9]["name"]   .must_equal "my scrapbook 10"
      scrapbooks[19]["name"]  .must_equal "my scrapbook 0"
    end

    describe "GET /scrapbooks?page=1" do
      it "returns an array of 20 scrapbooks according to the requested page
      of the paginated results" do
        50.times { |i| create_scrapbook name: "my scrapbook #{i}"}

        get "/scrapbooks?page=1", {}, session

        scrapbooks = JSON.parse(last_response.body)
        last_response.status    .must_equal 200
        scrapbooks.length       .must_equal 20
        scrapbooks[0]["name"]   .must_equal "my scrapbook 29"
        scrapbooks[19]["name"]  .must_equal "my scrapbook 10"
      end
    end
  end # GET /scrapbooks

  describe "PUT /scrapbooks/:id" do
    it "replaces the scrapbook if it already exists" do
      scrapbook           = create_scrapbook
      scrapbook["name"]   = "my updated name"

      put "/scrapbooks/#{scrapbook["id"]}", scrapbook.to_json, session

      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)
      updated["id"]   .must_equal scrapbook["id"]
      updated["name"] .must_equal "my updated name"
    end

    it "presents an errors hash if resource invalid" do
      scrapbook          = create_scrapbook
      scrapbook["name"]  = nil

      put "/scrapbooks/#{scrapbook["id"]}", scrapbook.to_json, session

      last_response.status.must_equal 400
      scrapbook = JSON.parse(last_response.body)
      scrapbook["errors"].wont_be_empty
    end
  end # PUT /scrapbooks/:scrapbook_id
  
  describe "DELETE /scrapbooks/:id" do
    it "delete a scapbook " do
      scrapbook = create_scrapbook

      delete "/scrapbooks/#{scrapbook['id']}", {}, session
      
      last_response.body.must_be_empty
      last_response.status.must_equal 204
    end
  end # DELETE /scrapbooks/:id

  describe "GET /scrapbooks/:scrapbook_id/scraps/:id" do 
    it "get a scrap and status code 200" do
      scrapbook = create_scrapbook
      scrap     = create_scrap(scrapbook_id: scrapbook['id']) 

      get "/scrapbooks/#{scrapbook['id']}/scraps/#{scrap['id']}", {}, session

      persisted = JSON.parse(last_response.body)
      last_response.status  .must_equal 200 
      persisted             .must_equal scrap
    end
  end # GET /scrapbooks/:scrapbook_id/scraps/:id

  describe "GET scrapbooks/:scrapbook_id/scraps" do
    it "gets all scraps of the scrapbook if sucessful" do
      scrapbook = create_scrapbook

      texts = ["http://g.cn", "http://baidu.com"]
      texts.each do |text|
        create_scrap(text: text, scrapbook_id: scrapbook["id"])
      end

      get "/scrapbooks/#{scrapbook["id"]}/scraps", {}, session

      scraps = JSON.parse(last_response.body)
      last_response.status.must_equal 200
      scraps.length     .must_equal texts.count
      scraps[0]["text"] .must_equal texts[1]
      scraps[1]["text"] .must_equal texts[0]
    end
  end # GET /scrapbooks/:scrapbook_id/scraps

  describe "POST /scrapbooks/:scrapbook_id/scraps" do
    it "creates a new scrap if sucessful" do
      scrapbook = create_scrapbook

      post "/scrapbooks/#{scrapbook["id"]}/scraps", {
        text:         "http://www.google.com",
        scrapbook_id: scrapbook["id"],
        user_id:      @user.id
      }.to_json, session

      last_response.status.must_equal 201
      scrap = JSON.parse(last_response.body)
      scrap["id"]          .wont_be_nil
      scrap["text"]        .wont_be_nil
      scrap["created_at"]  .wont_be_nil
    end

  end # POST /scrapbooks/:scrapbook_id/scraps

  describe "PUT /scrapbooks/:scrapbook_id/scraps/:scrap_id" do
    it "replaces the scrap if it already exists" do
      scrapbook = create_scrapbook
      scrap     = create_scrap(scrapbook_id: scrapbook["id"])
      updated_text = "my updated text"
      scrap["text"]  = updated_text

      put "/scrapbooks/#{scrapbook['id']}/scraps/#{scrap['id']}", 
          scrap.to_json, session

      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)
      updated["id"]        .must_equal scrap["id"]
      updated["text"]      .must_equal updated_text
    end

    it "presents an errors hash if resource invalid" do
      scrapbook      = create_scrapbook
      scrap          = create_scrap(scrapbook_id: scrapbook["id"])
      updated_text   = nil
      scrap["text"]  = updated_text

      put "/scrapbooks/#{scrapbook['id']}/scraps/#{scrap['id']}", 
          scrap.to_json, session

      last_response.status.must_equal 400
      scrap = JSON.parse(last_response.body)
      scrap["errors"].wont_be_empty
    end
  end # PUT /scrapbooks/:scrapbook_id/scraps/:scrap_id
  
  describe "DELETE /scrapbooks/:scrapbook_id/scraps/:scrap_id" do
    it "delete a scrap from scrapbooks" do
      scrapbook = create_scrapbook
      scrap     = create_scrap(scrapbook_id: scrapbook["id"])

      delete "/scrapbooks/#{scrapbook['id']}/scraps/#{scrap['id']}",
             {}, session

      last_response.status.must_equal 204
      last_response.body.must_be_empty
    end
  end # DELETE /scrapbooks/:scrapbook_id/scraps/:scrap_id

  def session
    { "rack.session" => { user_id: @user.id, entity_id: @user.entity_id } }
  end

  def create_scrapbook(options={})
    post "/scrapbooks", {
      name:                 options[:name] || "my scrapbook",
      user_id:              @user.id
    }.merge!(options).to_json, session
    JSON.parse(last_response.body)
  end

  def create_scrap(options={})
    post "/scrapbooks/#{options[:scrapbook_id]}/scraps", {
      text:         options[:text] || "http://www.qq.com",
      scrapbook_id: options[:scrapbook_id],
      user_id:      @user.id
    }.to_json, session
    JSON.parse(last_response.body)
  end
end # Belinkr::API
