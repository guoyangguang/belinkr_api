# encoding: utf-8
require "minitest/autorun"
require "rack/test"
require "json"
require_relative "../../api"
require_relative "../../api/sessions"
require_relative "../../credential/orchestrator"
require_relative "../../credential/member"
require_relative "../../user/member"

ENV["RACK_ENV"] = "test"
include Belinkr

def app; API.new; end
$redis = Redis.new

describe API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @credential, @user  = 
      Credential::Orchestrator.create(
        Credential::Member.new(email: "foo@foo.com", password: "changeme"),
        User::Member.new(first: "User", last: "111", entity_id: 1)
      )
  end

  describe "POST /sessions" do
    it "authenticates the user and creates a new session" do
      credential_data = { email: @credential.email, password: "changeme" }
      post "/sessions", credential_data.to_json
      last_response.status.must_equal 201

      get "/statuses"
      last_response.status.must_equal 200
    end
  end # POST /sessions

  describe "DELETE /sessions/:id" do
    it "clears the session" do
      credential_data = { email: @credential.email, password: "changeme" }
      post "/sessions", credential_data.to_json

      delete "/sessions/1"
      last_response.status.must_equal 204

      get "/statuses"
      last_response.status.must_equal 401
    end
  end # DELETE /sessions/:id
end # API
