# encoding: utf-8
require "minitest/autorun"
require "rack/test"
require_relative "../../api"

ENV["RACK_ENV"] = "test"
Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

Belinkr::AttachmentUploader.class_eval do
  def store_dir
    "/tmp/files/#{model.id}"
  end
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do 
    $redis.flushdb 
    @user = Belinkr::User::Member
      .new(first: "User", last: "111", entity_id: 1).save
  end

  describe "unauthenticated request" do
    it "it returns 401 Unauthorized" do
      get "/statuses"
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      get "/statuses/1"
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      post "/statuses"
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      put "/statuses/1"
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      delete "/statuses/1"
      last_response.status.must_equal 401
      last_response.body.must_be_empty
    end
  end

  describe "POST /statuses" do
    before do
      @store_dir = "/tmp/files"
    end

    it "creates a new status if sucessful" do
      status = create_status(text: "my created status")
      last_response.status.must_equal 201

      status["created_at"]  .wont_be_nil
      status["id"]          .wont_be_nil
      status["text"]        .must_equal "my created status"

      check_links_for status
      get "/statuses", {}, session
      statuses = JSON.parse(last_response.body)
      last_response.status    .must_equal 200
      statuses.length         .must_equal 1
    end

    it "creates a new status with attachments" do
      text_file = {
          file: File.expand_path(
              "../uploaders/files/test.txt", File.dirname(__FILE__)
          ),
          content_type: "text/plain"
      }
      status = create_status_with_attachments({}, [text_file])
      status["id"].wont_be_nil
      status["text"].must_equal "my status"
      status["files"].must_be_kind_of Array
      status["files"].any?.must_equal true
      status["files"].size.must_equal 1
      file = status["files"].first
      file.must_be_kind_of Hash
      file["original"].wont_be_nil
      file["original"].must_match %r(^#{@store_dir})
    end

  end # POST /statuses

  describe "GET /statuses" do
    it "returns an array of the 20 most recent statuses from the people
    the user is following, sorted by creation time" do
      11.times {|i| create_status text: "user 2 status #{10 - i}"}
      11.times {|i| create_status text: "user 3 status #{10 - i}"}
      11.times {|i| create_status text: "user 4 status #{10 - i}"}

      get "/statuses", {}, session
      statuses = JSON.parse(last_response.body)

      last_response.status    .must_equal 200
      statuses.length         .must_equal 20
      statuses[0]["text"]     .must_equal "user 4 status 0"
      statuses[10]["text"]    .must_equal "user 4 status 10"
      statuses[11]["text"]    .must_equal "user 3 status 0"
      statuses[19]["text"]    .must_equal "user 3 status 8"
    end

    describe "GET /statuses?page=1" do
      it "returns an array of 20 statuses according to the requested page
      of the paginated results" do
        50.times { |i| create_status text: "user 2 status #{49 - i}" }

        get "/statuses?page=1", {}, session
        statuses = JSON.parse(last_response.body)

        last_response.status    .must_equal 200
        statuses.length         .must_equal 20
        statuses[0]["text"]     .must_equal "user 2 status 20"
        statuses[19]["text"]    .must_equal "user 2 status 39"
      end
    end
  end # GET /statuses

  describe "GET /statuses/:id" do
    it "returns the status" do
      status = create_status

      get "/statuses/#{status["id"]}", {}, session
      status = JSON.parse(last_response.body)

      last_response.status  .must_equal 200
      status["text"]        .must_equal "my status"

      check_links_for status
    end

    it "returns 404 if the resource doesn't exist" do
      get "/statuses/9999", {}, session
      last_response.status.must_equal 404
      last_response.body.must_be_empty
    end

    describe "replies" do
      it "shows an array of replies" do
        status = create_status

        post "/statuses/#{status["id"]}/replies",
          { text: "my reply" }.to_json, session


        get "/statuses/#{status["id"]}", {}, session
        last_response.status.must_equal 200
        status = JSON.parse(last_response.body)

        status["replies"].length.must_equal 1
        status["replies"][0]["text"].must_equal "my reply"
        status["replies"][0]["status_id"].must_equal status["id"]
      end

      it "is empty if no replies posted" do
        status = create_status

        get "/statuses/#{status["id"]}", {}, session
        status = JSON.parse(last_response.body)

        status["replies"].must_be_empty
      end
    end
  end

  describe "PUT /statuses/:id" do
    it "replaces the status if it already exists" do
      status = create_status
      status["text"] = "my updated status"

      put "/statuses/#{status["id"]}", status.to_json, session
      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)

      updated["id"]         .must_equal status["id"]
      updated["text"]       .must_equal "my updated status"
      updated["created_at"] .must_equal status["created_at"]

      check_links_for status
    end

    it "presents an errors hash if resource invalid" do
      status = create_status

      put "/statuses/#{status["id"]}", { id: status["id"], text: "" }.to_json,
          session
      last_response.status.must_equal 400
      status = JSON.parse(last_response.body)
      status["errors"].wont_be_empty
    end

    it "localizes errors when an Accept-Language header is passed" do
      header "Accept-Language", "es_ES"
      status = create_status

      put "/statuses/#{status["id"]}", { id: status["id"], text: "" }.to_json,
          session

      status = JSON.parse(last_response.body)
      last_response.status.must_equal 400
      status["errors"]["text"].must_include "texto no debe quedar vacío"
    end

    it "update status with files" do
      status = create_status
      status["text"] = "my updated status"
      status["files"].must_be_empty
      text_file = {
          file: File.expand_path(
              "../uploaders/files/test.txt", File.dirname(__FILE__)
          ),
          content_type: "text/plain"
      }

      put "/statuses/#{status["id"]}", status.merge(
          "file_update" => {
              0 => Rack::Test::UploadedFile.new(
                  text_file[:file], text_file[:content_type]
              )
          }
      ), session
      last_response.status.must_equal 200
      status = JSON.parse(last_response.body)
      status["text"].must_equal "my updated status"
      status["files"].must_be_kind_of Array
      status["files"].wont_be_empty
      file = status["files"].first
      file.must_be_kind_of Hash
      file["original"].wont_be_nil
      file["original"].must_match %r(^#{@store_dir})
    end

    it "append a attachment files to a status which already has attachments" do
      image_file = {
          file: File.expand_path(
              "../uploaders/files/test.jpg", File.dirname(__FILE__)
          ),
          content_type: "image/jpeg"
      }

      text_file = {
          file: File.expand_path(
              "../uploaders/files/test.txt", File.dirname(__FILE__)
          ),
          content_type: "text/plain"
      }
      status = create_status_with_attachments({}, [text_file])
      status["files"].must_be_kind_of Array
      status["files"].size.must_equal 1
      old_status = status.dup

      status["text"] = "my updated status"

      put "/statuses/#{status["id"]}", status.merge(
          "file_update" => {
              0 => Rack::Test::UploadedFile.new(
                  image_file[:file], image_file[:content_type]
              )
          }
      ), session

      last_response.status.must_equal 200
      status = JSON.parse(last_response.body)
      status["files"].must_be_kind_of Array
      status["files"].size.must_equal 2

      status["files"].must_include old_status["files"].first
    end
  end

  describe "DELETE /statuses/:id" do
    it "deletes a status" do
      status = create_status

      delete "/statuses/#{status["id"]}", {}, session
      last_response.status.must_equal 204
      last_response.body.must_be_empty
    end
  end
end # Belinkr::API

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

def create_status(options={})
  post "/statuses", {
    text:     options[:text]    || "my status"
  }.to_json, session

  JSON.parse(last_response.body)
end

def create_status_with_attachments(options={}, attachments=[])
  params = { text: options[:text] || "my status", files: {} }
  attachments.each_with_index do |attachment, index|
    file, content_type = attachment[:file], attachment[:content_type]
    params[:files][index] = Rack::Test::UploadedFile.new(file, content_type)
  end
  post "/statuses", params, session
  JSON.parse(last_response.body)
end

def check_links_for(status)
  links = status["links"]

  links             .wont_be_empty
  links["user"]     .must_equal "/users/#{status["user_id"]}"
  links["self"]     .must_equal "/statuses/#{status["id"]}"
  links["replies"]  .must_equal "/statuses/#{status["id"]}/replies"
end
