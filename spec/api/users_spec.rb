# encoding: utf-8
require "minitest/autorun"
require "rack/test"
require_relative "../../follow/orchestrator"
require_relative "../../api"

ENV["RACK_ENV"] = "test"
Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user   = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "111", entity_id: 1
                  )
    @follower1  = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "112", entity_id: 1
                  )
    @follower2  = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "113", entity_id: 1
                  )
    @followed = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member.new(
                     first: 'User', last: "114", entity_id: 1
                     ))
  end
  
  describe "GET /following" do
    it "gets following for current user" do
      Belinkr::Follow::Orchestrator.create(
        Belinkr::Follow::Member
          .new(follower_id: @user.id, followed_id: @follower1.id), @user
      )
      Belinkr::Follow::Orchestrator.create(
        Belinkr::Follow::Member
          .new(follower_id: @user.id, followed_id: @follower2.id), @user
      )

      get "/followings", {}, session

      last_response.status.must_equal 200
      followings = JSON.parse(last_response.body)
      followings.length        .must_equal 2
      followings[0]["last"]    .must_equal @follower2.last
      followings[0]["first"]   .must_equal @follower2.first
      followings[1]["last"]    .must_equal @follower1.last
      followings[1]["first"]   .must_equal @follower1.first
    end
  end # GET /following

  describe "GET /followers" do
    it "gets followers for current user" do
      Belinkr::Follow::Orchestrator.create(
        Belinkr::Follow::Member
          .new(follower_id: @follower1.id, followed_id: @user.id), @follower1
      )
      Belinkr::Follow::Orchestrator.create(
        Belinkr::Follow::Member
          .new(follower_id: @follower2.id, followed_id: @user.id), @follower2
      )

      get "/followers", {}, session

      last_response.status.must_equal 200
      followers = JSON.parse(last_response.body)
      followers.length        .must_equal 2
      followers[0]["last"]    .must_equal @follower2.last
      followers[0]["first"]   .must_equal @follower2.first
      followers[1]["last"]    .must_equal @follower1.last
      followers[1]["first"]   .must_equal @follower1.first
    end
  end # GET /followers"



  describe "post /users/:followed_id/followers" do
    it "allows current_user to follow another user" do
      post "/users/#{@followed.id}/followers", {}, session

      get_user = JSON.parse(last_response.body)

      last_response.status.must_equal 201
      get_user["id"]     .must_equal @user.id
      get_user["last"]   .must_equal @user.last

      get "/users/#{@followed.id}/followers", {}, session

      last_response.status.must_equal 200
      get_followers = JSON.parse(last_response.body)
      get_followers.length        .must_equal 1
      get_followers[0]["last"]    .must_equal @user.last
      get_followers[0]["first"]   .must_equal @user.first
      
    end
  end # post /users/:followed_id/followers
  
  describe "DELETE /users/:followed_id/followers/:id" do
    it "returns 204 if successful" do
      post "/users/#{@followed.id}/followers", {}, session
      follows = Belinkr::Follow::Collection.new(entity_id: @followed.entity_id,
        user_id: @followed.id, kind: 'followers').all.select do |follow|
          follow.follower_id == @user.id 
        end
      follow = follows[0]
      delete "/users/#{@followed.id}/followers/#{follow.id}", {}, session

      last_response.status.must_equal 204
      last_response.body.must_be_empty
    end
  end # delete /users/:followed_id/followers/:id

  def session
    { "rack.session" => { user_id: @user.id, entity_id: 1 } }
  end

end # Belinkr::API
