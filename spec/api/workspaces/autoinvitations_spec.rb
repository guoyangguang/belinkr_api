# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user       = Belinkr::User::Member
                    .new(first: "User", last: "111", entity_id: 1).save
    @user1      = Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1).save
    @user2      = Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1).save
    @user3      = Belinkr::User::Member
                    .new(first: "User3", last: "333", entity_id: 1).save
    @workspace  = create_workspaces
  end


  describe "POST /workspaces/:workspace_id/autoinvitations" do
    it "a user that not belong to a workspaces can join the autoinvitation" do
      user1_session = {
                "rack.session" => { user_id: @user1.id, entity_id: 1 }
      }
      post "/workspaces/#{@workspace['id']}/autoinvitations", {
          requesting_id: @user1.id,
          state: 'requested'
      }.to_json, user1_session

      last_response.status.must_equal 201
      autoinvitations = JSON.parse(last_response.body)
      autoinvitations['id'].wont_be_nil
      autoinvitations['state'].must_equal 'requested'
    end

    it "a workspaces's user can not access autoinvitation" do
      join_workspace @user1

      user1_session = {
          "rack.session" => { user_id: @user1.id, entity_id: 1 }
      }
      post "/workspaces/#{@workspace["id"]}/autoinvitations", {
          requesting_id: @user1.id,
          state:         'requested'
      }.to_json, user1_session

      last_response.status.must_equal 404
    end

    it "workspaces's administrator can not access autoinvitation" do
      join_workspace @user1
      promote_administrator @user1

      user1_session = {
          "rack.session" => { user_id: @user1.id, entity_id: 1 }
      }
      post "/workspaces/#{@workspace["id"]}/autoinvitations", {
          requesting_id: @user1.id,
          state: 'requested'
      }.to_json, user1_session

      last_response.status.must_equal 404
    end
  end # POST /workspaces/:workspace_id/autoinvitations

  describe "POST /workspaces/:workspace_id/autoinvitations/allowed" do
    it "returned an allowed autoinvitation and 201" do
      non_wk_user_session = {
                "rack.session" => { user_id: @user1.id, entity_id: 1 }
            }

      post "/workspaces/#{@workspace['id']}/autoinvitations", {
          requesting_id: @user1.id,
          state: 'requested'
      }.to_json, non_wk_user_session

      autoinvitation = JSON.parse(last_response.body)

      post "/workspaces/#{@workspace["id"]}/autoinvitations/allowed",
        {autoinvitation_id: autoinvitation["id"]}.to_json, session

      last_response.status.must_equal 201
      allowed_autoinv = JSON.parse(last_response.body)
      allowed_autoinv['state'].must_equal 'allowed'
    end
  end # POST /workspaces/:workspace_id/autoinvitations/allowed

  describe "GET /workspaces/:workspace_id/autoinvitations" do
    it "get a workspaces all of the autoinvitations" do
      autoinvitations = []
      [@user1, @user2, @user3].each do |user|
        non_wk_user_session = {
          "rack.session" => { user_id: user.id, entity_id: 1 }
        }
        post "/workspaces/#{@workspace['id']}/autoinvitations", {
          requesting_id: user.id,
          state: 'requested'
        }.to_json, non_wk_user_session
        autoinvitations << JSON.parse(last_response.body)
      end

      get "/workspaces/#{@workspace["id"]}/autoinvitations", {}, session

      last_response.status.must_equal 200
      all_autoinvitations = JSON.parse(last_response.body)
      all_autoinvitations.length.must_equal 3

      all_autoinvitations[0]['id'].must_equal autoinvitations[2]["id"]
      all_autoinvitations[1]['id'].must_equal autoinvitations[1]["id"]
      all_autoinvitations[2]['id'].must_equal autoinvitations[0]["id"]
    end
  end # GET / workspaces/:workspace_id/autoinvitations


  def create_workspaces(opt={})
    post '/workspaces', {
        name: opt[:name] || 'workspace1'
    }.to_json, session
    JSON.parse(last_response.body)
  end

  def create_reply(opt={})
    reply_text = "workspaces reply 1"
    post "/workspaces/#{opt[:workspace_id]}/replies", {
      text:         reply_text,
      user_id:      @user.id
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def create_status(opt={})
    status_text = "workspaces status 1"
    post "/workspaces/#{opt[:workspace_id]}/statuses", {
      text: status_text
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def join_workspace(*users)
    users.each do |user|
      temp_session = {
          "rack.session" => { user_id: user.id, entity_id: 1 }
      }
      post("/workspaces/#{@workspace['id']}/invitations",
        { invited_id: user.id }.to_json, session
      )
      invitation = JSON.parse(last_response.body)
      post "/workspaces/#{@workspace['id']}/invitations/accepted", {
          invitation_id:           invitation['id'],
          workspace_id:            @workspace['id'],
          entity_id:               @workspace['entity_id'],
          inviter_id:              @user.id,
          invited_id:              user.id,
          state:                   "pending"
      }.to_json, temp_session
    end
  end

  def promote_administrator(*users)
    users.each do |user|
      post "/workspaces/#{@workspace['id']}/administrators", {
          user_id:     user.id,
          first:       user.first,
          last:        user.last
      }.to_json, session
    end
  end

end # Belinkr::API
