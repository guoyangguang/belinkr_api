# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user       = Belinkr::User::Member
                    .new(first: "User", last: "111", entity_id: 1).save
    @user1      = Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1).save
    @user2      = Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1).save
    @user3      = Belinkr::User::Member
                    .new(first: "User3", last: "333", entity_id: 1).save
    @workspace  = create_workspaces
  end

  describe "POST /workspaces/:id/invitations" do
    it "invite other people in the same company join the workspaces" do
      post "/workspaces/#{@workspace['id']}/invitations", {
          invited_id: @user1.id
      }.to_json, session

      invitaion = JSON.parse(last_response.body)
      invitaion['state']      .must_equal 'pending'
      invitaion['invited_id'] .must_equal @user1.id
      invitaion['inviter_id'] .must_equal @user.id

      get("/workspaces/#{@workspace['id']}/invitations",
          {}, session
      )
      workspace_collection = JSON.parse(last_response.body)
      workspace_collection.length    .must_equal 1
      workspace_collection[0]['id']  .must_equal invitaion['id']
    end
  end # POST /workspaces/:id/invitations

  describe "POST /workspaces/:id/invitations/accepted" do
    it "a inviter accepted the invitation" do
      post("/workspaces/#{@workspace['id']}/invitations",
        { invited_id: @user1.id }.to_json, session
      )
      invitation = JSON.parse(last_response.body)

      temp_session = { "rack.session" => { user_id: @user1.id, entity_id: 1 } }

      post "/workspaces/#{@workspace['id']}/invitations/accepted", {
          invitation_id:           invitation['id'],
          workspace_id:            @workspace['id'],
          entity_id:               @workspace['entity_id'],
          inviter_id:              @user.id,
          invited_id:              @user1.id,
          state:                   "pending"
      }.to_json, temp_session

      return_invitation = JSON.parse(last_response.body)
      return_invitation['state'].must_equal 'accepted'

      get "/workspaces/#{@workspace['id']}/collaborators", {}, session
      collaborators = JSON.parse(last_response.body)

      collaborators.length  .must_equal 1
      collaborators[0]['id'].must_equal @user1.id

    end
  end # POST /workspaces/:id/invitations/accepted

  describe "POST /workspaces/:id/invitations/rejected" do
    it "a user reject a invitation" do
      post "/workspaces/#{@workspace['id']}/invitations", {
          invited_id: @user1.id
      }.to_json, session

      invitation = JSON.parse(last_response.body)

      temp_session = { "rack.session" => { user_id: @user1.id, entity_id: 1 } }

      post "/workspaces/#{@workspace["id"]}/invitations/rejected", {
          invitation_id:           invitation["id"],
          workspace_id:            @workspace["id"],
          entity_id:               @workspace["entity_id"],
          inviter_id:              @user.id,
          invited_id:              @user1.id,
          state:                   "pending"
      }.to_json, temp_session

      return_invitation = JSON.parse(last_response.body)
      return_invitation['state'].must_equal 'rejected'

      get "/workspaces/#{@workspace['id']}/collaborators", {}, session
      collaborators = JSON.parse(last_response.body)

      collaborators.length  .must_equal 0
    end
  end # POST /workspaces/:id/invitations/rejected

  describe "GET /workspaces/:workspace_id/invitations" do
    it "return the workspaces all invitations" do
      [@user1, @user2, @user3].each do |user|
        post "/workspaces/#{@workspace['id']}/invitations", {
          invited_id: user.id
      }.to_json, session
      end

      get "/workspaces/#{@workspace["id"]}/invitations", {}, session
      invitations = JSON.parse(last_response.body)
      invitations.length               .must_equal   3
      invitations[0]["invited_id"]     .must_equal   @user3.id
      invitations[1]["invited_id"]     .must_equal   @user2.id
      invitations[2]["invited_id"]     .must_equal   @user1.id
    end
  end #GET /workspaces/:workspace_id/invitations

  def create_workspaces(opt={})
    post '/workspaces', {
        name: opt[:name] || 'workspace1'
    }.to_json, session
    JSON.parse(last_response.body)
  end

  def create_reply(opt={})
    reply_text = "workspaces reply 1"
    post "/workspaces/#{opt[:workspace_id]}/replies", {
      text:         reply_text,
      user_id:      @user.id
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def create_status(opt={})
    status_text = "workspaces status 1"
    post "/workspaces/#{opt[:workspace_id]}/statuses", {
      text: status_text
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def join_workspace(*users)
    users.each do |user|
      temp_session = {
          "rack.session" => { user_id: user.id, entity_id: 1 }
      }
      post("/workspaces/#{@workspace['id']}/invitations",
        { invited_id: user.id }.to_json, session
      )
      invitation = JSON.parse(last_response.body)
      post "/workspaces/#{@workspace['id']}/invitations/accepted", {
          invitation_id:           invitation['id'],
          workspace_id:            @workspace['id'],
          entity_id:               @workspace['entity_id'],
          inviter_id:              @user.id,
          invited_id:              user.id,
          state:                   "pending"
      }.to_json, temp_session
    end
  end

  def promote_administrator(*users)
    users.each do |user|
      post "/workspaces/#{@workspace['id']}/administrators", {
          user_id:     user.id,
          first:       user.first,
          last:        user.last
      }.to_json, session
    end
  end

end # Belinkr::API
