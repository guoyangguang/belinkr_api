# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user       = Belinkr::User::Member
                    .new(first: "User", last: "111", entity_id: 1).save
    @user1      = Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1).save
    @user2      = Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1).save
    @user3      = Belinkr::User::Member
                    .new(first: "User3", last: "333", entity_id: 1).save
    @workspace  = create_workspaces
    @status     = create_status workspace_id: @workspace['id']
  end


  describe "POST /workspaces/:workspace_id/statuses/:status_id/replies" do
    it "creates a reply related to the workspaces" do
      reply_text = "workspaces reply 1"
      post "/workspaces/#{@workspace['id']}/statuses/#{
            @status['id']}/replies", {
        text:           reply_text,
        user_id:        @user.id,
        workspace_id:   @workspace["id"]
      }.to_json, session

      reply = JSON.parse(last_response.body)
      last_response.status.must_equal 201
      reply["id"]              .wont_be_nil
      reply["created_at"]      .wont_be_nil
      reply["text"]            .must_equal reply_text
    end
  end # POST /workspaces/:workspace_id/statuses/:status_id/replies

  describe "PUT /workspaces/:workspace_id/statuses/:status_id/
          replies/:reply_id" do
    it "creates a reply about the workspaces" do
      updated_reply_text = "updated workspaces reply 1"
      reply             = create_reply(
                           workspace_id: @workspace["id"],
                           status_id:    @status["id"]
                          )
      reply["id"].wont_be_nil

      put "/workspaces/#{@workspace["id"]}/statuses/#{
          @status["id"]}/replies/#{reply["id"]}", {
        text:           updated_reply_text,
        user_id:        @user.id,
        workspace_id:   @workspace["id"]
      }.to_json, session
      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)
      updated["created_at"]      .wont_be_nil
      updated["text"]            .must_equal updated_reply_text
    end
  end # PUT /workspaces/:workspace_id/replies/:reply_id

  describe "DELETE /workspaces/:workspace_id/statuses/:status_id/
           replies/:reply_id" do
    it "deletes a workspaces reply" do
      reply = create_reply(
                workspace_id: @workspace["id"],
                status_id:    @status["id"]
              )
      reply["id"].wont_be_nil

      delete "/workspaces/#{@workspace["id"]}/statuses/#{
             @status["id"]}/replies/#{reply["id"]}", {}, session

      last_response.status.must_equal 204
    end
  end # DELETE /workspaces/:workspace_id/statuses/:status_id/replies/:reply_id

  describe "GET /workspaces/:workspace_id/statuses/:status_id/replies" do
    it "creates a reply related to the workspaces" do
      25.times {|i| create_reply(
        workspace_id: @workspace["id"],
        status_id:    @status["id"],
        text:         "text #{i} placeholder"
      )}

      get "/workspaces/#{@workspace["id"]}/statuses/#{
          @status["id"]}/replies", {}, session

      last_response.status.must_equal 200
      replies = JSON.parse(last_response.body)
      replies.length      .must_equal 25
      replies[0]["text"]  .must_equal "text 0 placeholder"
      replies.last["text"].must_equal "text 24 placeholder"
    end
  end # GET /workspaces/:workspace_id/statuses/:status_id/replies

  def create_workspaces(opt={})
    post '/workspaces', {
        name: opt[:name] || 'workspace1'
    }.to_json, session
    JSON.parse(last_response.body)
  end

  def create_reply(opt={})
    reply_text = "workspaces reply 1"
    post "/workspaces/#{opt[:workspace_id]}/statuses/#{
          opt[:status_id]}/replies", {
      text:         reply_text,
      user_id:      @user.id
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def create_status(opt={})
    status_text = "workspaces status 1"
    post "/workspaces/#{opt[:workspace_id]}/statuses", {
      text: status_text
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end
end # Belinkr::API