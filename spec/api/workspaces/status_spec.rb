# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user       = Belinkr::User::Member
                    .new(first: "User", last: "111", entity_id: 1).save
    @user1      = Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1).save
    @user2      = Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1).save
    @user3      = Belinkr::User::Member
                    .new(first: "User3", last: "333", entity_id: 1).save
    @workspace  = create_workspaces
  end

  describe "POST /workspaces/:workspace_id/statuses" do
    it "creates a 'workspace' status" do
      status_text = "workspaces status 1"
      post "/workspaces/#{@workspace["id"]}/statuses", {
        text: status_text
      }.to_json, session

      status = JSON.parse(last_response.body)
      last_response.status.must_equal 201
      status["id"]              .wont_be_nil
      status["created_at"]      .wont_be_nil
      status["text"]            .must_equal status_text
    end
  end # POST /workspaces/:workspace_id/statuses

  describe "GET /workspaces/:workspace_id/statuses/:status_id" do
    it "gets a status about the workspaces" do
      status = create_status(workspace_id: @workspace['id'])

      get "workspaces/#{@workspace['id']}/statuses/#{status["id"]}",
          {}, session
      get_status = JSON.parse(last_response.body)
      last_response.status.must_equal 200
      get_status["id"]              .wont_be_nil
      get_status["created_at"]      .wont_be_nil
      get_status["text"]            .must_equal status["text"]
    end
  end #GET /workspaces/:workspace_id/statuses/:status_id

  describe "GET /workspaces/:workspace_id/statuses" do
    it "gets all statuses about the workspaces" do
      25.times do |i|
        create_status(
          workspace_id: @workspace['id'],
          text:         "workspaces status #{i}"
        )
      end

      get "workspaces/#{@workspace['id']}/statuses", {}, session
      statuses = JSON.parse(last_response.body)
      last_response.status.must_equal 200
      statuses.length        .must_equal 20
      statuses[0]["text"]    .must_equal "workspaces status 24"
      statuses.last["text"]  .must_equal "workspaces status 5"
    end
  end # GET /workspaces/:workspace_id/statuses

  describe "PUT /workspaces/:workspace_id/statuses/:status_id" do
    it "creates a status about the workspaces" do
      updated_status_text = "updated workspaces status 1"
      status              = create_status(
                              workspace_id: @workspace["id"]
                             )
      status["id"].wont_be_nil

      put "/workspaces/#{@workspace["id"]}/statuses/#{status["id"]}", {
        text: updated_status_text
      }.to_json, session
      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)
      updated["created_at"]      .wont_be_nil
      updated["text"]            .must_equal updated_status_text
    end
  end #PUT /workspaces/:workspace_id/statuses/:status_id

  describe "DELETE /workspaces/:workspace_id/statuses/:status_id" do
    it "deletes a workspaces status" do
      status = create_status(
               workspace_id: @workspace["id"]
              )
      status["id"].wont_be_nil

      delete "/workspaces/#{@workspace["id"]}/statuses/#{status["id"]}",
             {}, session
      last_response.status.must_equal 204
      last_response.body  .must_be_empty
    end
  end # DELETE /workspaces/:workspace_id/statuses/:status_id
 
  describe "POST /workspaces/:workspace_id/councils" do
    it "creates a 'council' status" do
      status_text = "workspace council status 1"

      post "/workspaces/#{@workspace["id"]}/councils",{
        status: {text: status_text}}.to_json, session
      
      last_response.status.must_equal 201
      status = JSON.parse(last_response.body)
      status["id"].wont_be_nil 
      status["created_at"].wont_be_nil
      status["text"].must_equal status_text
    end
  end


  def create_workspaces(opt={})
    post '/workspaces', {
        name: opt[:name] || 'workspace1'
    }.to_json, session
    JSON.parse(last_response.body)
  end

  def create_status(opt={})
    status_text = "workspaces status 1"
    post "/workspaces/#{opt[:workspace_id]}/statuses", {
      text: status_text
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def create_reply(opt={})
    reply_text = "workspaces reply 1"
    post "/workspaces/#{opt[:workspace_id]}/replies", {
      text:         reply_text,
      user_id:      @user.id
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

end # Belinkr::API
