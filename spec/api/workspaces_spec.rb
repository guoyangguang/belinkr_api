# encoding: utf-8
require 'minitest/autorun'
require 'rack/test'
require_relative '../../api'

ENV['RACK_ENV'] = 'test'

Belinkr::API.send(:set, :sessions, false)

def app; Belinkr::API.new; end

def session
  { "rack.session" => { user_id: @user.id, entity_id: 1 } }
end

describe Belinkr::API do
  include Rack::Test::Methods

  before do
    $redis.flushdb
    @user       = Belinkr::User::Member
                    .new(first: "User", last: "111", entity_id: 1).save
    @user1      = Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1).save
    @user2      = Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1).save
    @user3      = Belinkr::User::Member
                    .new(first: "User3", last: "333", entity_id: 1).save
    @workspace  = create_workspaces
  end

  describe "unauthenticated request" do
    it "it will return 401" do
      get '/workspaces'
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      get '/workspaces/1'
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      post '/workspaces'
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      put '/workspaces/1'
      last_response.status.must_equal 401
      last_response.body.must_be_empty

      delete '/workspaces/1'
      last_response.status.must_equal 401
      last_response.body.must_be_empty
    end
  end

  describe "POST /workspaces" do
    it "create a new workspaces" do
      last_response.status.must_equal 201
      @workspace['created_at']        .wont_be_nil
      @workspace['id']                .wont_be_nil
      @workspace['name']              .must_equal 'workspace1'
    end
  end # POST /workspaces

  describe "GET /workspaces/:id" do
    it "it return a workspaces" do
      get "/workspaces/#{@workspace['id']}", {}, session
      workspace = JSON.parse(last_response.body)
      last_response.status      .must_equal 200
      workspace['name']         .must_equal 'workspace1'
    end

    it "returns 404 if the resource doesn't exist" do
      get "/workspaces/9999", {}, session
      last_response.status.must_equal 404
      last_response.body  .must_be_empty
    end
  end # GET /workspaces/:id

  describe "GET /workspaces" do
    it "it return all of the company's workspaces'" do
      5.times do |i|
        create_workspaces(name: "workspaces#{i}")
      end

      get '/workspaces', {}, session
      last_response.status.must_equal 200

      workspaces = JSON.parse(last_response.body)
      workspaces[0]['name'].must_equal 'workspaces4'
      workspaces[4]['name'].must_equal 'workspaces0'
    end

    describe "GET /workspaces?page=:number" do
      it "return part of result" do
        50.times do |i|
          create_workspaces(name: "workspaces#{i}")
        end

        get '/workspaces', {}, session
        last_response.status.must_equal 200

        workspaces = JSON.parse(last_response.body)
        workspaces[0]['name']  .must_equal 'workspaces49'
        workspaces[19]['name'] .must_equal 'workspaces30'
        workspaces[20]         .must_be_nil
      end
    end
  end # GET /workspaces

  describe "DELETE /workspaces/:id" do
    it "marks the workspaces as deleted and return 204, if successfully" do
      delete "/workspaces/#{@workspace["id"]}", {}, session

      last_response.status.must_equal 204
      last_response.body  .must_be_empty
    end
  end

  describe "PUT /workspaces/:id" do
    it "replaces the workspaces if it already exists" do
      updated_name        = "my updated name"
      @workspace["name"]   = updated_name

      put "/workspaces/#{@workspace["id"]}", @workspace.to_json, session
      last_response.status.must_equal 200
      updated = JSON.parse(last_response.body)

      updated["id"]         .must_equal @workspace["id"]
      updated["name"]       .must_equal updated_name
    end

    it "presents an errors hash if resource invalid" do
      @workspace["name"] = nil
      put "/workspaces/#{@workspace["id"]}", @workspace.to_json, session

      last_response.status.must_equal 400
      workspace = JSON.parse(last_response.body)
      workspace["errors"].wont_be_empty
    end
  end # PUT /workspaces/:id

  describe "POST /workspaces/:workspace_id/administrators" do
    it "make a collaborator promote a administrator" do
      join_workspace @user1, @user2
      promote_administrator @user2

      last_response.status.must_equal 201
      promoter = JSON.parse(last_response.body)
      promoter['id'].must_equal @user2.id

      get "/workspaces/#{@workspace['id']}/collaborators", {}, session
      collaborators = JSON.parse(last_response.body)

      get "/workspaces/#{@workspace['id']}/administrators", {}, session
      administrators = JSON.parse(last_response.body)

      collaborators.length  .must_equal 1
      administrators.length .must_equal 2

      collaborators[0]['id'] .must_equal @user1.id
      administrators[0]['id'].must_equal @user2.id
    end
  end # POST /workspaces/:workspace_id/administrators

  describe "DELETE /workspaces/:workspace_id/users/:user_id" do
    it "user leave the workspaces if he is in the workspaces" do
      post "/workspaces/#{@workspace['id']}/invitations", {
          invited_id: @user1.id
      }.to_json, session

      invitation = JSON.parse(last_response.body)

      temp_session = { "rack.session" => { user_id: @user1.id, entity_id: 1 } }

      post "/workspaces/#{@workspace["id"]}/invitations/accepted", {
          invitation_id:           invitation["id"],
          workspace_id:            @workspace["id"],
          entity_id:               @workspace["entity_id"],
          inviter_id:              @user.id,
          invited_id:              @user1.id,
          state:                   "pending"
      }.to_json, temp_session

      delete "/workspaces/#{@workspace["id"]}/users/#{@user1.id}",
             {}, temp_session

      last_response.status.must_equal 204
      last_response.body  .must_be_empty
    end
  end # DELETE /workspaces/:workspace_id/users/:user_id

  describe "DELETE /workspaces/:workspace_id/collaborators/:collaborator_id" do
    it "remove a user from the collaborators of that workspaces" do
      join_workspace @user1, @user2, @user3

      get "/workspaces/#{@workspace['id']}/collaborators", {}, session
      collaborators = JSON.parse(last_response.body)
      collaborators.length.must_equal 3

      delete "workspaces/#{@workspace['id']}/collaborators/#{@user2.id}",
        {}, session

      last_response.status.must_equal   204
    end
  end # DELETE /workspaces/:workspace_id/collaborators/:collaborator_id


  describe "DELETE /workspaces/:workspace_id/administrators/:id" do
    it "demotes an admin of the workspaces and return 204, if successfully" do
      join_workspace @user1, @user2, @user3
      promote_administrator @user1, @user2


      delete "/workspaces/#{@workspace['id']}/administrators/#{@user1.id}",
            {}, session

      last_response.status.must_equal 204
      last_response.body.must_be_empty

      get "/workspaces/#{@workspace['id']}/administrators", {}, session
      administrators = JSON.parse(last_response.body)
      administrators.length.must_equal 2
    end
  end

  describe "GET collaborator or administrators" do
    it "gets all collaborators of this workspaces" do
      join_workspace @user1, @user2

      get "/workspaces/#{@workspace["id"]}/collaborators", {}, session

      get_collabotaros = JSON.parse(last_response.body)
      last_response.status        .must_equal 200
      get_collabotaros.size       .must_equal 2
      get_collabotaros[0]["id"]   .must_equal @user2["id"]
      get_collabotaros[0]["first"].must_equal @user2.first
      get_collabotaros[0]["last"] .must_equal @user2.last
    end

    it "gets all administrators of this workspaces" do
      join_workspace @user1, @user2, @user3
      promote_administrator @user1, @user2

      get "/workspaces/#{@workspace["id"]}/administrators", {}, session

      get_administrators = JSON.parse(last_response.body)
      last_response.status          .must_equal 200
      get_administrators.size       .must_equal 3
      get_administrators.last["id"] .must_equal @user.id
      get_administrators[0]["id"]   .must_equal @user2["id"]
      get_administrators[0]["first"].must_equal @user2.first
      get_administrators[0]["last"] .must_equal @user2.last
    end
  end

  def create_workspaces(opt={})
    post '/workspaces', {
        name: opt[:name] || 'workspace1'
    }.to_json, session
    JSON.parse(last_response.body)
  end

  def create_reply(opt={})
    reply_text = "workspaces reply 1"
    post "/workspaces/#{opt[:workspace_id]}/replies", {
      text:         reply_text,
      user_id:      @user.id
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def create_status(opt={})
    status_text = "workspaces status 1"
    post "/workspaces/#{opt[:workspace_id]}/statuses", {
      text: status_text
    }.merge!(opt).to_json, session
    JSON.parse(last_response.body)
  end

  def join_workspace(*users)
    users.each do |user|
      temp_session = {
          "rack.session" => { user_id: user.id, entity_id: 1 }
      }
      post("/workspaces/#{@workspace['id']}/invitations",
        { invited_id: user.id }.to_json, session
      )
      invitation = JSON.parse(last_response.body)
      post "/workspaces/#{@workspace['id']}/invitations/accepted", {
          invitation_id:           invitation['id'],
          workspace_id:            @workspace['id'],
          entity_id:               @workspace['entity_id'],
          inviter_id:              @user.id,
          invited_id:              user.id,
          state:                   "pending"
      }.to_json, temp_session
    end
  end

  def promote_administrator(*users)
    users.each do |user|
      post "/workspaces/#{@workspace['id']}/administrators", {
          user_id:     user.id,
          first:       user.first,
          last:        user.last
      }.to_json, session
    end
  end

end # Belinkr::API
