#encoding: utf-8
require 'minitest/autorun'
require_relative '../../init'
require_relative '../../appointment/member'


describe Belinkr::Appointment::Member do
  describe "validations" do
    describe "user_id" do
      it "must be present" do
        appointment = Belinkr::Appointment::Member.new
        appointment.valid?.must_equal false
        appointment.errors[:user_id].must_include "user must not be blank"
      end

      it "is a integer" do
        appointment = Belinkr::Appointment::Member.new(user_id: 'aa')
        appointment.valid?.must_equal false
        appointment.errors[:user_id].must_include "user must be a number"
      end
    end # user_id

    describe "entity_id" do
      it "must be present" do
        appointment = Belinkr::Appointment::Member.new
        appointment.valid?.must_equal false
        appointment.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        appointment = Belinkr::Appointment::Member.new(entity_id: 'aa')
        appointment.valid?.must_equal false
        appointment.errors[:entity_id].must_include "entity must be a number"
      end
    end # entity_id

    describe "#name" do
      it "must be present" do
        appointment = Belinkr::Appointment::Member.new
        appointment.valid?.must_equal false
        appointment.errors[:name].must_include "name must not be blank"
      end

      it "has minimum length of 3 characters" do
        appointment = Belinkr::Appointment::Member.new(name: "ab")
        appointment.valid?.must_equal false
        appointment.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end

      it "has maximum length of 250 characters" do
        appointment = Belinkr::Appointment::Member.new(name: "a" * 251)
        appointment.valid?.must_equal false
        appointment.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end
    end # name

    describe "#description" do
      it "must be present" do
        appointment = Belinkr::Appointment::Member.new
        appointment.valid?.must_equal false
        appointment.errors[:description]
                  .must_include "description must not be blank"
      end

      it "has minimum length of 3 characters" do
        appointment = Belinkr::Appointment::Member.new(description: "ab")
        appointment.valid?.must_equal false
        appointment.errors[:description]
          .must_include "description must be between 3 and 250 characters long"
      end

      it "has maximum length of 250 characters" do
        appointment = Belinkr::Appointment::Member.new(description: "a" * 251)
        appointment.valid?.must_equal false
        appointment.errors[:description]
          .must_include "description must be between 3 and 250 characters long"
      end
    end # description

  end # validations
end # Belinkr::Appointment::Member