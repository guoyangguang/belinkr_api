#encoding: utf-8
require 'minitest/autorun'
require_relative '../../init'
require_relative '../../appointment/orchestrator'
require_relative '../../user/orchestrator'

$redis ||= Redis.new
describe Belinkr::Appointment::Orchestrator do
  before do
    @user      = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User", last: "123", entity_id: 1)
                 )
    @appointment = Belinkr::Appointment::Member.new(
                     entity_id:   @user.entity_id,
                     user_id:     @user.id,
                     name:        'appointment',
                     description: 'appointment'
                    )
  end


  describe ".create" do
    it "create a appointment" do
      appointment = Belinkr::Appointment::Orchestrator
                      .create(@appointment, @user)
      appointment.id.wont_be_nil
      appointment.name.must_equal 'appointment'

      Belinkr::Appointment::Member
        .new(id: appointment.id, entity_id: appointment.entity_id)
        .attributes[:name].must_equal 'appointment'

      Belinkr::Appointment::Collection
        .new(entity_id: appointment.entity_id)
        .must_include appointment
    end
  end # .create

  describe ".update" do
    it "appointment's name is changed" do
      appointment = Belinkr::Appointment::Orchestrator
                      .create(@appointment, @user)
      update_appointment = appointment.dup
      update_appointment.name = 'appointment name update'

      Belinkr::Appointment::Orchestrator
        .update(update_appointment, @user)
        .name.must_equal 'appointment name update'
    end

    it "appointment's description changed" do
      appointment = Belinkr::Appointment::Orchestrator
                      .create(@appointment, @user)
      update_appointment = appointment.dup
      update_appointment.description = 'appointment description update'

      Belinkr::Appointment::Orchestrator
        .update(update_appointment, @user)
        .description.must_equal 'appointment description update'
    end
  end # .update

  describe ".read" do
    it "access appointment content" do
      appointment = Belinkr::Appointment::Orchestrator
                      .create(@appointment, @user)

      read_appointment = Belinkr::Appointment::Member.new(
                          id: appointment.id,
                          entity_id: appointment.entity_id
                        )
      Belinkr::Appointment::Orchestrator.read(read_appointment, @user)
        .id.wont_be_nil
    end
  end # .read

  describe ".delete" do
    it "delete a appointment" do
      appointment = Belinkr::Appointment::Orchestrator
                      .create(@appointment, @user)
      appointment.deleted_at.must_be_nil

      delete_appointment = Belinkr::Appointment::Orchestrator
                            .delete(appointment, @user)
      delete_appointment.deleted_at.wont_be_nil
      delete_appointment.id.must_equal appointment.id
    end
  end # .delete

end # Belinkr::Appointment::Orchestrator