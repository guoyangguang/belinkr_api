# encoding: utf-8
require "minitest/autorun"
require "bcrypt"
require_relative "../../init"
require_relative "../../credential/member"
require_relative "../../entity/orchestrator"
require_relative "../../entity/member"

$redis = Redis.new

include Belinkr

describe Credential::Member do
  before { $redis.flushdb }

  describe "validations" do
    describe "#email" do
      it "must be present" do
        credential = Credential::Member.new
        credential.valid?.must_equal false
        credential.errors[:email].must_include "e-mail must not be blank"
      end

      it "has the appropriate format" do  
        credential = Credential::Member.new(email: "foo")
        credential.valid?.must_equal false
        credential.errors[:email].must_include "e-mail has an invalid format"
      end
    end #email

    describe "#password" do
      it "has more than 3 characters" do
        credential = Credential::Member.new(password: "ab")
        credential.valid?.must_equal false
        credential.errors[:password]
          .must_include "password must be between 8 and 150 characters long"
      end

      it "has less than 150 characters" do
        credential = Credential::Member.new(password: "a" * 151)
        credential.valid?.must_equal false
        credential.errors[:password]
          .must_include "password must be between 8 and 150 characters long"
      end
    end #password
  end # validations

  describe "#encrypted?" do
    it "returns true if password is encrypted with bcrypt" do
      credential  = Credential::Member.new(
                      first:    "User",
                      last:     "111",
                      email:    "user1@foo.com",
                      password: "changeme"
                    )
      credential.encrypted?.must_equal false
      credential.save
      credential.encrypted?.must_equal true
    end
  end #encrypted?

  describe "#save" do
    it "encrypts with BCrypt on save" do
      credential  = Credential::Member.new(
                      email:    "user1@foo.com",
                      password: "changeme"
                    ).save
      credential.password.wont_equal "changeme"
      encrypted = BCrypt::Password.new(credential.password)
      encrypted.is_password?("changeme").must_equal true
    end
  end # save
end # Credential::Member 
