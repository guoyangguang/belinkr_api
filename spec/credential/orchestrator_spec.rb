# encoding: utf-8
require "minitest/autorun"
require "redis"
require_relative "../../credential/orchestrator"
require_relative "../../credential/member"
require_relative "../../user/member"
require_relative "../../user/collection"

$redis = Redis.new

include Belinkr

describe Credential::Orchestrator do
  before do
    $redis.flushdb
    @credential = Credential::Member.new(
                    email:    "foo@foo.com", 
                    password: "changeme"
                  )
    @first_user = User::Member.new(first: "User", last: "111", entity_id: 1)
  end

  describe ".create" do
    it "creates a new credential and a new user" do
      Credential::Orchestrator.create(@credential, @first_user)

      @credential.id          .wont_be_nil
      @credential.created_at  .wont_be_nil
      @credential.updated_at  .wont_be_nil
      @credential.encrypted?  .must_equal true

      @first_user.id          .wont_be_nil
      @first_user.created_at  .wont_be_nil
      @first_user.updated_at  .wont_be_nil

      Credential::Collection.new.must_include @credential
      User::Collection.new(entity_id: 1).must_include @first_user
    end

    it "links the user to this credential" do
      Credential::Orchestrator.create(@credential, @first_user)
      @credential.user_ids    .must_include @first_user.id
      @credential.entity_ids  .must_include @first_user.entity_id
    end

    it "adds the email => credential_id tuple to the Authenticator" do
      Authenticator.exists?(@credential.email).must_equal false
      Credential::Orchestrator.create(@credential, @first_user)
      Authenticator.exists?(@credential.email).must_equal true

      credential = Credential::Member.new(id: @credential.id)
      Authenticator.authenticate(credential.email, "changeme")
        .must_equal credential
    end
  end # .create

  describe ".delete" do
  end # .delete

  describe ".destroy" do
  end # .destroy
end # Credential::Orchestrator
