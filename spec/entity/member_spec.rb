# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../entity/member"

describe Belinkr::Entity::Member do
  describe "validations" do
    describe "name" do
      it "must be present" do
        entity = Belinkr::Entity::Member.new
        entity.valid?.must_equal false
        entity.errors[:name].must_include "name must not be blank"
      end

      it "must be unique" do
        skip
      end

      it "has at least 2 characters" do
        entity = Belinkr::Entity::Member.new(name: "a")
        entity.valid?.must_equal false
        entity.errors[:name]
          .must_include "name must be between 2 and 150 characters long"
      end

      it "has at most 150 characters" do
        entity = Belinkr::Entity::Member.new(name: "a" * 151)
        entity.valid?.must_equal false
        entity.errors[:name]
          .must_include "name must be between 2 and 150 characters long"
      end
    end

    describe "address" do
      it "must be present" do
        entity = Belinkr::Entity::Member.new
        entity.valid?.must_equal false
        entity.errors[:address].must_include "address must not be blank"
      end

      it "has at least 9 characters" do
        entity = Belinkr::Entity::Member.new(address: "a" * 9)
        entity.valid?.must_equal false
        entity.errors[:address]
          .must_include "address must be between 10 and 300 characters long"
      end

      it "has at most 301 characters" do
        entity = Belinkr::Entity::Member.new(address: "a" * 301)
        entity.valid?.must_equal false
        entity.errors[:address]
          .must_include "address must be between 10 and 300 characters long"
      end
    end

    describe "phone" do
      it "must be present" do
        entity = Belinkr::Entity::Member.new
        entity.valid?.must_equal false
        entity.errors[:phone].must_include "phone must not be blank"
      end

      it "has at least 5 characters" do
        entity = Belinkr::Entity::Member.new(phone: "a" * 4)
        entity.valid?.must_equal false
        entity.errors[:phone]
          .must_include "phone must be between 5 and 20 characters long"
      end

      it "has at most 21 characters" do
        entity = Belinkr::Entity::Member.new(phone: "a" * 21)
        entity.valid?.must_equal false
        entity.errors[:phone]
          .must_include "phone must be between 5 and 20 characters long"
      end
    end
  end
end
