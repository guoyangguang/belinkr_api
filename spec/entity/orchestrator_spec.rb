# encoding: utf-8
require "minitest/autorun"
require_relative "../../entity/orchestrator"
require_relative "../../tinto/exceptions"

$redis ||= Redis.new

describe Belinkr::Entity::Orchestrator do
  before { $redis.flushall }

  describe ".create" do
    it "creates an entity and a user" do
      entity = Belinkr::Entity::Member.new(
        name:     "entity 1",
        address:  "100 AnXi Lu",
        phone:    "+86 123456789",
        vat:      "A-123456789" # optional
      )
      first_credential = Belinkr::Credential::Member
        .new(email: "user1@foo.com", password: 'changeme')
      first_user = Belinkr::User::Member.new(first: "User", last: "111")

      persisted_entity, persisted_credential, persisted_user = 
        Belinkr::Entity::Orchestrator.create(entity, first_credential, first_user)

      persisted_entity.id   .wont_be_nil
      persisted_entity.name .must_equal "entity 1"
      Belinkr::Entity::Collection.new.must_include persisted_entity
      
      persisted_credential.id.wont_be_nil
      Belinkr::Credential::Collection.new.must_include persisted_credential

      persisted_user.id.wont_be_nil
      Belinkr::User::Collection.new(entity_id: persisted_entity.id)
        .must_include persisted_user

    end

    describe "when the entity is invalid" do
      it "raises a Tinto::Exception::InvalidResource" do
        invalid_entity = Belinkr::Entity::Member.new(
          name:     nil,
          address:  "100 AnXi Lu",
          phone:    "+86 123456789",
          vat:      "A-123456789" # optional
        )
        first_credential = Belinkr::Credential::Member
          .new(email: "user1@foo.com", password: 'changeme')
        first_user = Belinkr::User::Member.new(first: "User", last: "111")

        lambda { 
          Belinkr::Entity::Orchestrator
            .create(invalid_entity, first_credential, first_user)
        }.must_raise Tinto::Exceptions::InvalidResource
      end

      it "won't persist the entity nor the user" do
        invalid_entity = Belinkr::Entity::Member.new(
          name:     nil,
          address:  "100 AnXi Lu",
          phone:    "+86 123456789",
          vat:      "A-123456789" # optional
        )
        first_credential = Belinkr::Credential::Member
          .new(email: "user1@foo.com", password: 'changeme')
        first_user = Belinkr::User::Member.new(first: "User", last: "111")

        lambda { 
          Belinkr::Entity::Orchestrator
            .create(invalid_entity, first_credential, first_user)
        }.must_raise Tinto::Exceptions::InvalidResource

        invalid_entity.valid? .must_equal false
        invalid_entity.errors .wont_be_empty
        invalid_entity.id     .must_be_nil
        first_user.id         .must_be_nil
      end
    end # when entity is invalid

    describe "when the user is invalid" do
      it "raises a Tinto::Exception::InvalidResource" do
        entity = Belinkr::Entity::Member.new(
          name:     "entity 1",
          address:  "100 AnXi Lu",
          phone:    "+86 123456789",
          vat:      "A-123456789" # optional
        )
        first_credential = Belinkr::Credential::Member
          .new(email: "user1@foo.com", password: 'changeme')
        invalid_first_user = Belinkr::User::Member.new(last: "112")

        lambda {
          Belinkr::Entity::Orchestrator
            .create(entity, first_credential, invalid_first_user)
        }.must_raise Tinto::Exceptions::InvalidResource
      end

      it "won't persist the entity nor the resource" do
        entity = Belinkr::Entity::Member.new(
          name:     "entity 1",
          address:  "100 AnXi Lu",
          phone:    "+86 123456789",
          vat:      "A-123456789" # optional
        )
        first_credential = Belinkr::Credential::Member
          .new(email: "user1@foo.com", password: 'changeme')
        invalid_first_user = Belinkr::User::Member.new(last: "112")

        lambda {Belinkr::Entity::Orchestrator
          .create(entity, first_credential, invalid_first_user)
        }.must_raise Tinto::Exceptions::InvalidResource
        
        entity.id             .must_be_nil
        invalid_first_user.id .must_be_nil
      end
    end # when user is invalid
  end # .create

  describe ".update" do
    it "updates an entity" do
      entity = Belinkr::Entity::Member.new(
        name:     "entity 1",
        address:  "100 AnXi Lu",
        phone:    "+86 123456789",
        vat:      "A-123456789" #optional
      )
      first_credential = Belinkr::Credential::Member
        .new(email: "user1@foo.com", password: 'changeme')
      first_user = Belinkr::User::Member
                    .new(first: "User", last: "111")
      
      persisted_entity, persisted_credential, persisted_user = 
        Belinkr::Entity::Orchestrator.create(entity, first_credential, first_user)
      
      updated_entity = entity.dup
      updated_entity.name = "updated entity 1"

      updated_entity = Belinkr::Entity::Orchestrator
                        .update(persisted_entity, updated_entity)

      updated_entity.id       .must_equal persisted_entity.id
      updated_entity.name     .must_equal "updated entity 1"
    end
  end

  describe ".entity" do
    it "marks the entity as deleted" do
      entity = Belinkr::Entity::Member.new(
        name:     "entity 1",
        address:  "100 AnXi Lu",
        phone:    "+86 123456789",
        vat:      "A-123456789" #optional
      )
      first_credential = Belinkr::Credential::Member
        .new(email: "user1@foo.com", password: 'changeme')
      first_user = Belinkr::User::Member
                    .new(first: "User", last: "111")
      
      persisted_entity, persisted_credential, persisted_user = 
        Belinkr::Entity::Orchestrator.create(entity, first_credential,  first_user)

      persisted_entity.deleted_at.must_be_nil

      deleted = persisted_entity.delete
      deleted.deleted_at  .wont_be_nil
      deleted.id          .must_equal entity.id
      deleted.name        .must_equal entity.name
    end
  end
end # Belinkr::Entity::Orchestrator
