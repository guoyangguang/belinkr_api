# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../follow/collection"

describe Belinkr::Follow::Collection do
  describe "validations" do
    describe "user_id" do
      it "must be present" do
        follow = Belinkr::Follow::Collection.new
        follow.valid?.must_equal false
        follow.errors[:user_id].must_include "user must not be blank"
      end

      it "must be a number" do
        follow = Belinkr::Follow::Collection.new
        follow.valid?.must_equal false
        follow.errors[:user_id].must_include "user must be a number"
      end
    end

    describe "entity_id" do
      it "must be present" do
        follow = Belinkr::Follow::Collection.new
        follow.valid?.must_equal false
        follow.errors[:entity_id].must_include "entity must not be blank"
      end

      it "must be a number" do
        follow = Belinkr::Follow::Collection.new
        follow.valid?.must_equal false
        follow.errors[:entity_id].must_include "entity must be a number"
      end
    end

    describe "kind" do
      it "must be present" do
        follow = Belinkr::Follow::Collection.new
        follow.valid?.must_equal false
        follow.errors[:kind].must_include "kind must not be blank"
      end

      it "must be 'follower' or 'following'" do
        follow = Belinkr::Follow::Collection.new
        follow.valid?.must_equal false
        follow.errors[:kind]
          .must_include "kind must be one of followers, following"
      end
    end
  end
end # Belinkr::Follow::Collection
