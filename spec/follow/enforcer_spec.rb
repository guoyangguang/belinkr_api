# encoding: utf-8
require "ostruct"
require "minitest/autorun"
require_relative "../../follow/enforcer"
require_relative "../../tinto/exceptions"

describe Belinkr::Follow::Enforcer do
  describe "create_by?" do
    it "raises NotAllowed if user not present" do
      user   = OpenStruct.new
      follow = OpenStruct.new(follower_id: 2, followed_id: 4)

      lambda { Belinkr::Follow::Enforcer.authorize user, :create, follow }
        .must_raise Tinto::Exceptions::NotAllowed
    end

    it "raises NotAllowed if entity_id doesn't match" do
      user   = OpenStruct.new(id: 1, entity_id: 1)
      follow = OpenStruct.new(follower_id: 1, followed_id: 4, entity_id: 2)

      lambda { Belinkr::Follow::Enforcer.authorize user, :create, follow }
        .must_raise Tinto::Exceptions::NotAllowed
    end

    it "raises NotAllowed unless user is the follower" do
      user   = OpenStruct.new(id: 1, entity_id: 1)
      follow = OpenStruct.new(follower_id: 2, followed_id: 4, entity_id: 1)

      lambda { Belinkr::Follow::Enforcer.authorize user, :create, follow }
        .must_raise Tinto::Exceptions::NotAllowed
    end
  end #create_by?

  describe "read_by?" do
    it "raises NotAllowed if user not present" do
      user   = OpenStruct.new
      follow = OpenStruct.new(follower_id: 2, followed_id: 4)

      lambda { Belinkr::Follow::Enforcer.authorize user, :read, follow }
        .must_raise Tinto::Exceptions::NotAllowed
    end
  end #read_by?

  describe "delete_by?" do
    it "raises NotAllowed unless user is the follower" do
      user   = OpenStruct.new(id: 1, entity_id: 1)
      follow = OpenStruct.new(follower_id: 2, followed_id: 4, entity_id: 1)

      lambda { Belinkr::Follow::Enforcer.authorize user, :delete, follow }
        .must_raise Tinto::Exceptions::NotAllowed
    end
  end #delete_by?
end # Belinkr::Follow::Enforcer
