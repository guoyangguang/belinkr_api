# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../follow/member"

describe Belinkr::Follow::Member do
  describe "#validations" do
    describe "follower_id" do
      it "must be present" do
        follow = Belinkr::Follow::Member.new
        follow.valid?.must_equal false
        follow.errors[:follower_id].must_include "follower must not be blank"
      end

      it "is a integer" do
        follow = Belinkr::Follow::Member.new(text: "test", follower_id: "ad")
        follow.valid?.must_equal false
        follow.errors[:follower_id].must_include "follower must be a number"
      end
    end

    describe "followed_id" do
      it "must be present" do
        follow = Belinkr::Follow::Member.new
        follow.valid?.must_equal false
        follow.errors[:followed_id].must_include "followed must not be blank"
      end

      it "is a integer" do
        follow = Belinkr::Follow::Member.new(text: "test", followed_id: "ad")
        follow.valid?.must_equal false
        follow.errors[:followed_id].must_include "followed must be a number"
      end
    end

    describe "entity_id" do
      it "must be present" do
        follow = Belinkr::Follow::Member.new
        follow.valid?.must_equal false
        follow.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        follow = Belinkr::Follow::Member.new(text: "test", entity_id: "ad")
        follow.valid?.must_equal false
        follow.errors[:entity_id].must_include "entity must be a number"
      end
    end
  end
end
