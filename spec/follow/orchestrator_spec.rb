# encoding: utf-8
require "minitest/autorun"
require_relative "../../follow/orchestrator"
require_relative "../../status/orchestrator"
require_relative "../../user/orchestrator"
require_relative "../../user/member"
require_relative "../../activity/collection"

$redis = Redis.new

describe Belinkr::Follow::Orchestrator do
  before do
    @follower = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "user", last: "111", entity_id: 1)
                )
    @followed = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "user", last: "112", entity_id: 1)
                )
    @follow   = Belinkr::Follow::Member.new(
                      follower_id: @follower.id, 
                      followed_id: @followed.id
                )
    @follows  = Belinkr::Follow::Collection.new(
                  user_id:    @followed.id,
                  entity_id:  @followed.entity_id,
                  kind:       "followers"
                )
  end

  describe ".create" do
    it "persists a new follow" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      @follow.id.wont_be_nil
      @follows.size.must_equal 1
    end

    it "returns the follower" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
        .must_equal @follower
    end

    it "adds the follower to the followers collection of the followed user" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      @follows.must_include @follow
    end

    it "adds the 20 latest tweets from the followed user
    to the follower's general timeline" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      5.times do |i|
        Belinkr::Status::Orchestrator.create(
          Belinkr::Status::Member.new(text: "status #{i}"), @followed
        )
      end

      Belinkr::Status::Collection.new(
        user_id: @follower.id, entity_id: @follower.entity_id, kind: "general"
      ).size.must_equal 5

      Belinkr::Status::Collection.new(
        user_id: @follower.id, entity_id: @follower.entity_id, kind: "general"
      ).all.to_a.first.text.must_equal "status 4"
    end

    it "returns the followed user" do
    end

    it "registers an activity" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      activity = Belinkr::Activity::Collection.new.all.to_a.first
      activity.actor.resource.id  .must_equal @follower.id
      activity.action             .must_equal "follow"
      activity.object.resource.id .must_equal @followed.id
    end
  end # .create

  describe ".delete" do
    it "fake deletes an existing follow" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      @follow.deleted_at.must_be_nil
      Belinkr::Follow::Orchestrator.delete(@follow, @follower)
      @follow.deleted_at.wont_be_nil
    end

    it "returns the follower" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      Belinkr::Follow::Orchestrator.delete(@follow, @follower)
        .must_equal @follower
    end

    it "removes the follower from the followers collection of the followed" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      Belinkr::Follow::Orchestrator.delete(@follow, @follower)
      Belinkr::Follow::Collection.new(
        user_id:    @followed.id,
        entity_id:  @followed.entity_id,
        kind:       "followers"
      ).wont_include @follow
    end

    it "removes the followed from the following collection of the follower" do
      Belinkr::Follow::Orchestrator.create(@follow, @follower)
      Belinkr::Follow::Orchestrator.delete(@follow, @follower)
      Belinkr::Follow::Collection.new(
        user_id:    @followed.id,
        entity_id:  @followed.entity_id,
        kind:       "following"
      ).wont_include @follower
    end
  end # .delete
end # Belinkr::Follow::Orchestrator
