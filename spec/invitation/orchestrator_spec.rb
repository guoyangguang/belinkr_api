# encoding: utf-8
require "minitest/autorun"
require "redis"
require_relative "../../init"
require_relative "../../invitation/orchestrator"
require_relative "../../invitation/member"
require_relative "../../user/orchestrator"
require_relative "../../user/member"
require_relative "../../activity/collection"

$redis = Redis.new

include Belinkr

describe Invitation::Orchestrator do
  before do
    $redis.flushdb
    @inviter    = User::Orchestrator.create(
                    User::Member
                      .new(first: "Inviter", last: "111", entity_id: 1)
                  )
    @invited    = User::Member
                    .new(first: "Invited", last: "111", entity_id: 1)
    @invitation = Invitation::Member.new(
                    entity_id:      1,
                    inviter_id:     1,
                    invited_name:   "#{@invited.first} #{@invited.last}",
                    invited_email:  "lp@belinkr.com",
                    locale:         "en"
                  )
  end

  describe ".create" do
    it "creates a new invitation" do
      Invitation::Orchestrator.create(@invitation, @inviter)
      @invitation.id          .wont_be_nil
      @invitation.token       .wont_be_nil
      @invitation.created_at  .wont_be_nil

      Invitation::Collection.new(entity_id: 1).must_include @invitation
    end

    it "raises if the email is already registered in this entity" do
      entity = Entity::Member.new(
                 name:    "entity 1",
                 phone:   "12345678",
                 address: "1, Infinite Loop"
               ).save
      Authenticator.add Credential::Member.new(
                 email:       "lp@belinkr.com",
                 password:    "changeme",
                 entity_ids:  [1]
               ).save

      lambda { Invitation::Orchestrator.create(@invitation, @inviter) }
        .must_raise Tinto::Exceptions::InvalidResource

      @invitation.errors[:invited_email].must_include(
        "#{@invitation.invited_email} " + 
        "is already registered in this company"
      )
    end

    it "registers an activity" do
      Invitation::Orchestrator.create(@invitation, @inviter)
      activity = Belinkr::Activity::Collection.new.all.to_a.first

      activity.actor.resource.id  .must_equal @invitation.inviter_id
      activity.action             .must_equal "invite"
      activity.object.resource    .must_equal @invitation.invited_name
    end
  end # .create

  describe ".accept" do
    it "accepts a new invitation" do
      Invitation::Orchestrator.create(@invitation, @inviter)
      Invitation::Orchestrator.accept(@invitation, @invited)

      @invitation.accepted? .must_equal true
      @invitation.id        .wont_be_nil

      Invitation::Member.new(id: @invitation.id, entity_id: @invitation.entity_id)
        .accepted?.must_equal true
    end

    it "registers an activity" do
      Invitation::Orchestrator.create(@invitation, @inviter)
      Invitation::Orchestrator.accept(@invitation, @invited)
      activity = Belinkr::Activity::Collection.new.all.to_a.first

      activity.actor.resource       .must_be_instance_of User::Member
      activity.action               .must_equal "accept"
      activity.object.resource.id   .must_equal @invitation.id
    end
  end # .accept

  describe ".delete" do
    it "marks the invitation as deleted" do
      Invitation::Orchestrator.create(@invitation, @inviter)

      @invitation.deleted_at.must_be_nil
      Invitation::Orchestrator.delete(@invitation, @inviter)
      @invitation.deleted_at.wont_be_nil

      Invitation::Collection.new(entity_id: 1).wont_include @invitation
    end
  end # .delete

  describe ".undelete" do
    it "marks the invitation as deleted" do
      Invitation::Orchestrator.create(@invitation, @inviter)
      Invitation::Orchestrator.delete(@invitation, @inviter)

      @invitation.deleted_at.wont_be_nil
      Invitation::Orchestrator.undelete(@invitation, @inviter)
      @invitation.deleted_at.must_be_nil

      Invitation::Collection.new(entity_id: 1).must_include @invitation
    end
  end # .undelete

  describe ".destroy" do
    it "destroys the invitation from the database" do
      Invitation::Orchestrator.create(@invitation, @inviter)
      id = @invitation.id

      Invitation::Orchestrator.destroy(@invitation, @inviter)
      @invitation.id.must_be_nil

      lambda { Invitation::Member.new id: id, entity_id: @invitation.entity_id }
        .must_raise Tinto::Exceptions::NotFound
    end
  end # .destroy
end # Invitation::Orchestrator
