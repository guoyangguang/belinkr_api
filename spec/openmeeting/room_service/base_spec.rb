# encoding: utf-8
require "minitest/autorun"
require_relative "../../../openmeeting/user_service/base"
require_relative "../../../openmeeting/room_service/base"

describe Belinkr::RoomService::Base do
  before do
    @sid = Belinkr::UserService::Base.get_session
             .body.split("session_id")[1].split(/[<,>]/)[1]

    Belinkr::UserService::Base.login_user(@sid, "evan", "223344")
    room    = Belinkr::RoomService::Base.addRoomWithModeration(@sid, "my_meeting_room", 2, "welcome", 30, true, false, false, 300, true)
    @room_id = room.body.split("return")[1].split(/[<,>]/)[1]
  end

  describe '.addRoomWithModeration' do

    it "Create a conference room" do
      created_room = Belinkr::RoomService::Base.getRoomById(@sid, @room_id)
      created_room.headers.must_include "200 OK"
      created_room.body.split("name")[1].split(/[<,>]/)[1].must_equal "my_meeting_room"
    end
  end # .addRoomWithModeration

  describe ".sendInvitationHash" do
    it "create a Invitation hash and optionally send it by mail the From to
       Date is as String as some SOAP libraries do not accept Date Objects
       in SOAP Calls Date is parsed as dd.mm.yyyy, time as hh:mm (don't forget
       the leading zero's)" do
      invite = Belinkr::RoomService::Base.sendInvitationHash(
                 @sid, "Rachel", "", "http://localhost:5080/openmeetings/",
                 "evan__zhang@yeah.net","Invitation to OpenMeetings", @room_id,
                 "", false, "", 1, "", "", "", "", 1, true)
    end
  end # .sendInvitationHash

end # Belinkr::RoomService::Base
