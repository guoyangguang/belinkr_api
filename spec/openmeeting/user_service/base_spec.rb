# encoding: utf-8
require "minitest/autorun"
require_relative "../../../openmeeting/user_service/base"

describe Belinkr::UserService::Base do
  before do
    @sid = Belinkr::UserService::Base.get_session
             .body.split("session_id")[1].split(/[<,>]/)[1]
  end

  describe '.loginUser' do

    it "the block return session id" do
      status = Belinkr::UserService::Base.login_user(@sid, "evan", "223344")
                 .body.split("return")[1].split(/[<,>]/)[1]
      status.must_equal "1"
    end
  end # .configure

end # Belinkr::Worker::Config::FilePorter
