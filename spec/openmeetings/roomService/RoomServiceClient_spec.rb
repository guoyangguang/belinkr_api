# encoding: utf-8
require "minitest/autorun"
require_relative "../../../tinto/exceptions"
require_relative "../../../openmeetings/roomService/defaultDriver"
require_relative "../../../openmeetings/userService/defaultDriver"

describe RoomServicePortType do
  before do
    user_obj  = UserServicePortType.new
    @sid      = user_obj.getSession.m_return.session_id
    user      = LoginUser.new(@sid, "evan", "223344")

    user_obj.loginUser(user)

    @room_obj     = RoomServicePortType.new
    room         = AddRoomWithModeration.new(@sid, "meeting room", 2, "welcome", 30, true, false, false, 300, true)
    @room_id     = @room_obj.addRoomWithModeration(room).m_return
  end
  describe ".addRoomWithModeration" do
    it "create a room for openmeetings" do
      room         = GetRoomById.new(@sid, @room_id)
      created_room = @room_obj.getRoomById(room).m_return

      created_room.name                 .must_equal "meeting room"
      created_room.numberOfPartizipants .must_equal "30"
    end # create a room for openmeetings
  end # .addRoomWithModeration

  describe ".invite another user to join the meeting room" do
    it "create a Invitation hash and optionally send
       it by mail, the user click invite and join the room" do
      invite = SendInvitationHash.new(
                 @sid, "Rachel", "", "http://localhost:5080/openmeetings/",
                 "evan__zhang@yeah.net","Invitation to OpenMeetings", @room_id,
                 "", false, "", 1, "", "", "", "", 1, true)
      @room_obj.sendInvitationHash(invite)
    end
  end # .sendInvitationHashWithDateObject
end # RoomServicePortType
