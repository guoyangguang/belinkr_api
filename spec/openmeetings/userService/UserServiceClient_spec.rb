# encoding: utf-8
require "minitest/autorun"
require_relative "../../../tinto/exceptions"
require_relative "../../../openmeetings/userService/defaultDriver"

describe UserServicePortType do
  describe ".loginUser" do
    it "user must be login in" do
      obj  = UserServicePortType.new
      sid  = obj.getSession.m_return.session_id
      user = LoginUser.new(sid, "evan", "223344")
      
      obj.loginUser(user).v_return.must_equal 1
    end # user must be login in
  end # .loginUser
end # UserServicePortType
