# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../reply/collection"
require_relative "../../reply/member"
require_relative "../../tinto/exceptions"

describe Belinkr::Reply::Collection do
  before do
    @reply_hash1 = { 
      id: 0, text: "reply 1", status_id: 1, user_id: 1, entity_id: 1 }
    @reply_hash2 = { 
      id: 1, text: "reply 2", status_id: 1, user_id: 1, entity_id: 1 
    }
  end

  describe "#initialize" do
    it "coerces all members" do
      collection = Belinkr::Reply::Collection.new([@reply_hash1])
      collection.each { |i| i.must_be_instance_of Belinkr::Reply::Member }
    end
  end #initialize

  describe "#each" do
    it "yields Member instances included in the collection" do
      collection = Belinkr::Reply::Collection.new([@reply_hash1])
      collection.each { |i| i.must_be_instance_of Belinkr::Reply::Member }
    end

    it "raises InvalidResource if members aren't valid" do
      collection = Belinkr::Reply::Collection.new([{"text" => "reply 1" }])

      lambda { collection.each { |i| i } }
        .must_raise Tinto::Exceptions::InvalidResource
    end
  end #each

  describe "#to_a" do
    it "returns the members of the collection" do 
      collection = Belinkr::Reply::Collection.new([@reply_hash1])

      collection.to_a.must_be_instance_of Array
      collection.to_a.first.text.must_equal "reply 1"
    end
  end #to_a

  describe "#add" do
    it "adds a new member to the end of the collection" do
      collection = Belinkr::Reply::Collection.new([@reply_hash1])
      reply      = Belinkr::Reply::Member.new(@reply_hash2)

      collection.add reply
      collection.count.must_equal 2
    end

    it "raises an exception if reply is invalid" do
      collection = Belinkr::Reply::Collection.new([@reply_hash1])
      reply      = Belinkr::Reply::Member.new(text: "reply 2")

      lambda { collection.add reply }
        .must_raise Tinto::Exceptions::InvalidResource
    end
  end #add

  describe "#update" do
    it "replaces an index contents with the passed reply" do
      collection = Belinkr::Reply::Collection.new([@reply_hash1])
      reply      = Belinkr::Reply::Member.new(@reply_hash1)
      reply.text = "updated text"

      collection.update(reply)
      collection.first.text.must_equal "updated text"
    end

    it "raises an exception if reply is invalid" do
      collection = Belinkr::Reply::Collection.new([@reply_hash1])
      reply      = Belinkr::Reply::Member.new(id: 0, text: "reply 2")

      lambda { collection.update reply }
        .must_raise Tinto::Exceptions::InvalidResource
    end
  end #update

  describe "#delete" do
    it "marks the reply as deleted" do
      reply       = Belinkr::Reply::Member.new(@reply_hash1)
      collection  = Belinkr::Reply::Collection.new([reply])

      collection.delete(reply)
      reply.deleted_at.wont_be_nil

      collection.count.must_equal 1
    end
  end #delete

  describe "#undelete" do
    it "marks the reply as not deleted" do
      reply       = Belinkr::Reply::Member.new(@reply_hash1)
      collection  = Belinkr::Reply::Collection.new([reply])

      collection.delete(reply)
      reply.deleted_at.wont_be_nil

      collection.undelete(reply)
      reply.deleted_at.must_be_nil
    end
  end #undelete

  describe "#destroy" do
    it "nilifies the index that matches the id of the passed reply" do
      reply1 = Belinkr::Reply::Member.new(@reply_hash1)
      reply2 = Belinkr::Reply::Member.new(@reply_hash2)

      collection = Belinkr::Reply::Collection.new([reply1, reply2])
      collection.count.must_equal 2

      collection.delete(reply1)
      collection.destroy(reply1)
      collection.count.must_equal 1
    end
  end #destroy

  describe "#to_json" do
    it "returns an array of members serialized to JSON" do
      reply1 = Belinkr::Reply::Member.new(@reply_hash1)
      reply2 = Belinkr::Reply::Member.new(@reply_hash2)

      collection = Belinkr::Reply::Collection.new([reply1, reply2])
      collection.to_json.must_equal "[#{reply1.to_json}, #{reply2.to_json}]"
    end
  end #to_json
end # Belinkr::Reply::Collection
