# encoding: utf-8
require "ostruct"
require "minitest/autorun"
require_relative "../../reply/enforcer"
require_relative "../../tinto/exceptions"

$redis = Redis.new

describe Belinkr::Reply::Enforcer do
  describe "all actions" do
    it "raises NotAllowed if user not present" do
      user    = OpenStruct.new
      status  = OpenStruct
                  .new(id: 1, text: "my status", user_id: 1, entity_id: 1)
      reply   = OpenStruct
                  .new(id: 1, text: "my reply", user_id: 2, entity_id: 1)

      [:create, :read, :update]. each do |action|
        lambda { 
          Belinkr::Reply::Enforcer.authorize user, action, status, reply
        }.must_raise Tinto::Exceptions::NotAllowed
      end
    end

    it "raises NotAllowed if the entity of status and user don't match" do
      user    = OpenStruct.new(id: 1, first: "User", last: "111", entity_id: 1)
      status  = OpenStruct
                  .new(id: 1, text: "my status", user_id: 1, entity_id: 2)
      reply   = OpenStruct
                  .new(id: 1, text: "my reply", user_id: 2, entity_id: 1)

      [:create, :read, :update]. each do |action|
        lambda { 
          Belinkr::Reply::Enforcer.authorize user, action, status, reply
        }.must_raise Tinto::Exceptions::NotAllowed
      end
    end
  end # all actions

  describe "update_by?" do
    it "raises NotAllowed if the user isn't the author of the reply" do
      user1  = OpenStruct.new(id: 1, first: "User", last: "111", entity_id: 1)
      user2  = OpenStruct.new(id: 2, first: "User", last: "111", entity_id: 1)
      user3  = OpenStruct.new(id: 3, first: "User", last: "111", entity_id: 1)
      status = OpenStruct
                .new(id: 1, text: "my status", user_id: user1.id, entity_id: 1)
      reply  = OpenStruct
                .new(id: 1, text: "my reply", user_id: user2.id, entity_id: 1)

      lambda { 
        Belinkr::Reply::Enforcer.authorize user3, :update, status, reply
      }.must_raise Tinto::Exceptions::NotAllowed
    end
  end #update_by?
end # Belinkr::Reply::Enforcer
