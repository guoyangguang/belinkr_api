# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../reply/member"

describe Belinkr::Reply::Member do
  describe "#validations" do
    describe "id" do
      it "must be present" do
        reply = Belinkr::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:id].must_include "id must not be blank"
      end

      it "is a integer" do
        reply = Belinkr::Reply::Member.new(text: "test", id: "ad")
        reply.valid?.must_equal false
        reply.errors[:id].must_include "id must be a number"
      end
    end

    describe "text" do
      it "has more than 3 characters" do
        reply = Belinkr::Reply::Member.new(text: "ab")
        reply.valid?.must_equal false
        reply.errors[:text]
          .must_include "text must be between 3 and 5000 characters long"
      end

      it "has less than 5000 characters" do
        reply = Belinkr::Reply::Member.new(text: "a" * 5001)
        reply.valid?.must_equal false
        reply.errors[:text]
          .must_include "text must be between 3 and 5000 characters long"
      end
    end

    describe "status_id" do
      it "must be present" do
        reply = Belinkr::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:status_id].must_include "status must not be blank"
      end

      it "is a integer" do
        reply = Belinkr::Reply::Member.new(text: "test", status_id: "ad")
        reply.valid?.must_equal false
        reply.errors[:status_id].must_include "status must be a number"
      end
    end

    describe "user_id" do
      it "must be present" do
        reply = Belinkr::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:user_id].must_include "user must not be blank"
      end

      it "is a integer" do
        reply = Belinkr::Reply::Member.new(text: "test", user_id: "ad")
        reply.valid?.must_equal false
        reply.errors[:user_id].must_include "user must be a number"
      end
    end

    describe "entity_id" do
      it "must be present" do
        reply = Belinkr::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        reply = Belinkr::Reply::Member.new(text: "test", entity_id: "ad")
        reply.valid?.must_equal false
        reply.errors[:entity_id].must_include "entity must be a number"
      end
    end
  end

  describe "#==" do
    it "compares replies based on their attributes" do
      reply1 = Belinkr::Reply::Member.new(text: "reply 1", user_id: 1)
      reply2 = Belinkr::Reply::Member.new(text: "reply 1", user_id: 2)
      reply1.wont_equal reply2

      reply2.user_id = 1
      reply2.must_equal reply1
    end
  end

  describe "#to_json" do
    it "serializes attributes to JSON" do
      reply = Belinkr::Reply::Member.new(text: "reply 1", user_id: 1)
      reply.to_json.must_equal reply.attributes.to_json
    end
  end
end
