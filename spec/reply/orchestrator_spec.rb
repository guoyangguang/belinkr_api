# encoding: utf-8
require "minitest/autorun"
require_relative "../../reply/orchestrator"
require_relative "../../status/orchestrator"
require_relative "../../user/orchestrator"
require_relative "../../follow/orchestrator"

$redis = Redis.new

describe Belinkr::Reply::Orchestrator do
  before do
    @user    = Belinkr::User::Orchestrator.create(
                Belinkr::User::Member
                  .new(first: "User", last: "111", entity_id: 1)
              )
    @replier = Belinkr::User::Orchestrator.create(
                Belinkr::User::Member
                  .new(first: "User", last: "112", entity_id: 1)
              )

    Belinkr::Follow::Orchestrator.create(
      Belinkr::Follow::Member
        .new(followed_id: @user.id, follower_id: @replier.id),
      @replier
    )

    @status  = Belinkr::Status::Orchestrator.create(
                Belinkr::Status::Member
                  .new(text: "my status", user_id: @user.id),
                @user
              )
    @reply   = Belinkr::Reply::Member.new(text: "my reply")
  end

  describe ".create" do
    it "updates the status with a new reply" do
      reply = Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)
      @status.replies.must_include @reply
      reply.id.wont_be_nil
      @reply.id.wont_be_nil
      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: @status.user_id)
        .must_include @status
    end

    it "Status shouldn't be added to this the 'replies' kind of collection
       if the user that replies is the same that the user
       who created the statuses" do
      reply1 = Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)
      storage_key = "entities:#{@status
                    .entity_id}:timelines:#{@status.user_id}:replies"
      r1_score = $redis.zscore(storage_key, @status.id)

      #sleep 5
      reply2 = Belinkr::Reply::Orchestrator.create(@status, @reply, @user)
      r2_score = $redis.zscore(storage_key, @status.id)

      reply3 = Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)
      r3_score = $redis.zscore(storage_key, @status.id)

      r1_score.must_equal r2_score
      r1_score.wont_equal r3_score
    end

    it "Status should be added to the 'replies' kind of Status::Collection
      for all the users that previously replied to the status" do
      user1    = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1)
                )
      user2    = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1)
                )
      reply1   = Belinkr::Reply::Member.new(text: "my reply")
      reply2   = Belinkr::Reply::Member.new(text: "my reply")
      Belinkr::Reply::Orchestrator.create(@status, reply1, user1)
      Belinkr::Reply::Orchestrator.create(@status, reply2, user2)

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: user1.id)
        .must_include @status

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: user2.id)
        .must_include @status
    end

    it "status replies collection always excluding the user
       who is replying now,if it happens that also replied previously" do
      user1    = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1)
                )
      user2    = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1)
                )

      Belinkr::Reply::Orchestrator.create(@status, @reply, user1)
      Belinkr::Reply::Orchestrator.create(@status, @reply, user2)
      Belinkr::Reply::Orchestrator.create(@status, @reply, user1)
      Belinkr::Reply::Orchestrator.create(@status, @reply, user1)

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: user1.id)
        .size.must_equal 1

    end
  end

  describe ".update" do
    it "updates the existing reply with new text" do
      @status.embedded_replies = Belinkr::Reply::Collection.new
      @status.save

      Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)

      @reply.text  = "my updated reply"
      reply = Belinkr::Reply::Orchestrator.update @status, @reply, @replier
      reply.id.wont_be_nil

      status = Belinkr::Status::Member
                .new(id: @status.id, entity_id: @reply.entity_id)
      status.replies.first.text.must_equal "my updated reply"
      status.replies.first.id.wont_be_nil
    end
  end

  describe ".delete" do
    it "deletes an existing reply" do
      @status.embedded_replies = Belinkr::Reply::Collection.new
      @status.save

      Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)

      @reply.deleted_at      .must_be_nil
      @status.replies        .must_include @reply
      @status.replies.count  .must_equal 1

      Belinkr::Reply::Orchestrator.delete(@status, @reply, @replier)
      @status.replies.count.must_equal 1
      @status.replies.select { |r| r.deleted_at }.size.must_equal 1
    end

    it "if the status only one reply" do
      Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)
      Belinkr::Reply::Orchestrator.delete(@status, @reply, @replier)

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: @status.user_id)
        .wont_include @status

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: @replier.id)
        .wont_include @status
    end

    it "the status has many replies" do
      user1    = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User1", last: "111", entity_id: 1)
                )
      user2    = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User2", last: "222", entity_id: 1)
                )
      reply1   = Belinkr::Reply::Member.new(text: "my reply")
      reply2   = Belinkr::Reply::Member.new(text: "my reply")

      Belinkr::Reply::Orchestrator.create(@status, reply1, user1)
      Belinkr::Reply::Orchestrator.create(@status, reply2, user2)
      Belinkr::Reply::Orchestrator.delete(@status, reply1, user1)

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: @status.user_id)
        .must_include @status

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: user1.id)
        .wont_include @status

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: user2.id)
        .must_include @status

    end
  end # .delete

  describe ".undelete" do
    it "recover a delete status reply" do
      @status.embedded_replies = Belinkr::Reply::Collection.new
      @status.save

      Belinkr::Reply::Orchestrator.create(@status, @reply, @replier)
      Belinkr::Reply::Orchestrator.delete(@status, @reply, @replier)
      Belinkr::Reply::Orchestrator.undelete(@status, @reply, @replier)

      @status.replies.count             .must_equal 1
      @status.replies.first.created_at  .wont_be_nil
      @status.replies.first.updated_at  .wont_be_nil
      @status.replies.first.deleted_at  .must_be_nil

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: @status.user_id)
        .must_include @status

      Belinkr::Status::Collection.new(kind: "replies",
                                      entity_id: @status.entity_id,
                                      user_id: @replier.id)
        .must_include @status
    end
  end # .undelete
end # Belinkr::Reply::Orchestrator
