# encoding: utf-8
require "minitest/autorun"
require "redis"
require_relative "../../../init"
require_relative "../../../request/approver/collection"

$redis ||= Redis.new

describe Belinkr::Request::Approver::Collection do
  describe "validations" do
    describe "#request_id" do
      it "must be present" do
        collection = Belinkr::Request::Approver::Collection.new
        collection.valid?.must_equal false
        collection.errors[:request_id]
          .must_include "request must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Request::Approver::Collection.new(
          request_id: "ad"
        )
        collection.valid?.must_equal false

        collection.errors[:request_id]
          .must_include "request must be a number"
      end
    end

    describe "#entity_id" do
      it "must be present" do
        collection = Belinkr::Request::Approver::Collection.new
        collection.valid?.must_equal false

        collection.errors[:entity_id]
          .must_include "entity must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Request::Approver::Collection.new(
          entity_id: "ad"
        )
        collection.valid?.must_equal false

        collection.errors[:entity_id]
          .must_include "entity must be a number"
      end
    end #entity_id

    describe "#add" do
      it "add approver to request's approver collection" do
        request  = OpenStruct.new(entity_id: 1, id: 2)
        approver = OpenStruct.new(id: 2)
        Belinkr::Request::Approver::Collection.new(
          entity_id:  request.entity_id,
          request_id: request.id
        ).add approver
        Belinkr::Request::Approver::Collection.new(
          entity_id:  request.entity_id,
          request_id: request.id
        ).must_include approver
      end
    end #add
  end # validations
end # Request::Approver::Collection
