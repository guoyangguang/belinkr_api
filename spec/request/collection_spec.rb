# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../request/collection"

describe Belinkr::Request::Collection do
  describe "validations" do
    describe "user_id" do
      it "must be present" do
        collection = Belinkr::Request::Collection.new
        collection.valid?.must_equal false
        collection.errors[:user_id].must_include "user must not be blank"
      end
      it "must be number" do
        collection = Belinkr::Request::Collection.new
        collection.valid?.must_equal false
        collection.errors[:user_id].must_include "user must be a number"
      end
    end #user_id
    describe "kind" do
      it "must be present" do
        collection = Belinkr::Request::Collection.new
        collection.valid?.must_equal false
        collection.errors[:kind].must_include "kind must not be blank"
      end
      it "must be in set KINDS" do
        request = Belinkr::Request::Collection.new
        request.valid?.must_equal false
        request.errors[:kind]
          .must_include "kind must be one of #{Belinkr::Request::Collection::KINDS.join(", ")}"
      end
    end #kind
    describe "entity_id" do
      it "must be present" do
        collection = Belinkr::Request::Collection.new
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        collection = Belinkr::Request::Collection.new(entity_id: "ad")
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id
  end # validations
end # Belinkr::Request::Collection
