# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../request/member"

describe Belinkr::Request::Member do
  before do
    @request = Belinkr::Request::Member.new
  end
  describe "#validations" do
    describe "title" do
      it "must be present" do
        @request.valid?.must_equal false
        @request.errors[:title].must_include "title must not be blank"
      end
      it "has less than 5000 characters" do
        @request.title = "a" * 151
        @request.valid?.must_equal false
        @request.errors[:title]
          .must_include "title must be between 3 and 150 characters long"
      end
      it "must be at least 3 charactors long " do
        @request.title = 'ab'
        @request.valid?.must_equal false
        @request.errors[:title].must_include "title must be between 3 and 150 characters long"
      end
    end #title

    describe "from" do
      it "must be present" do
        @request.valid?.must_equal false
        @request.errors[:from].must_include "from must not be blank"
      end
      it "is a integer that refers to the id of user who creates the request" do
        @request.valid?.must_equal false
        @request.errors[:from].must_include "from must be a number"
      end
    end #from

    describe "to" do
      it "must be present" do
        @request.valid?.must_equal false
        @request.errors[:to].must_include "to must not be blank"
      end

      it "is a integer that refers to the id of user who should be operator of the request" do
        @request.valid?.must_equal false
        @request.errors[:to].must_include "to must be a number"
      end
    end #to

    describe "state" do
      it "must be present" do
        @request.state.must_equal "pending"
      end
    end #state

    describe "entity_id" do
      it "must be present" do
        @request.valid?.must_equal false
        @request.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        @request.entity_id = "ad"
        @request.valid?.must_equal false
        @request.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id

    describe "#accepted?" do
      it "must be true " do
        @request.state.must_equal "pending"
        @request.accept
        @request.valid?.must_equal false
        @request.accepted?.must_equal true
      end
    end #accepted

    describe "#rejected?" do
      it "must be true " do
        @request.state.must_equal "pending"
        @request.reject
        @request.valid?.must_equal false
        @request.rejected?.must_equal true
      end
    end #accepted

  end #validations
end
