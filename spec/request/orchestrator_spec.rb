require "minitest/autorun"
require "redis"
require_relative "../../request/orchestrator"
require_relative "../../request/collection"
require_relative "../../user/member"
require_relative "../../user/orchestrator"
require_relative "../../entity/member"
require_relative "../../entity/orchestrator"
require_relative "../../entity/collection"

$redis = Redis.new host: "localhost", port: 6379

describe Belinkr::Request::Orchestrator do

  before do
    $redis.flushdb
    entity = Belinkr::Entity::Member.new(
        name:     "entity 1",
        address:  "100 AnXi Lu",
        phone:    "+86 123456789",
        vat:      "A-123456789" # optional
      )
    first_credential = Belinkr::Credential::Member
        .new(email: "user1@foo.com", password: 'changeme')
    first_user = Belinkr::User::Member.new(first: "User", last: "111")
    @entity, @credential, @user = Belinkr::Entity::Orchestrator
      .create(entity, first_credential, first_user)
    @boss          = Belinkr::User::Orchestrator.create(
                      Belinkr::User::Member
                        .new(first: "User", last: "112", entity_id: @entity.id)
                    )
    @worker       = Belinkr::User::Orchestrator.create(
                      Belinkr::User::Member
                      .new(first: "User", last: "113", entity_id: @entity.id)
                  )
    @dean        = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new(first: "User", last: "114", entity_id: @entity.id)
                  )
    @request    = Belinkr::Request::Orchestrator.create(
                    Belinkr::Request::Member.new(
                      title:                "Apply to be off work",
                      entity_id:            @entity.id,
                      description:          "for some personal reason, could I be off work tomorrow please?",
                      from:                 @user.id,
                      to:                   @worker.id,
                      approver_ids:         [@boss.id, @dean.id],
                      approved_by_ids:      [],
                      rejected_by_ids:      [],
                      events:               []
                    ), @user
                  )
  end

  describe ".create" do
    it "adds the request to the Request::Collection" do
      Belinkr::Request::Collection::KINDS.each do |kind| 
        collection = Belinkr::Request::Collection.new(
          entity_id: @request.entity_id,
          kind:      kind,
          user_id:   @request[kind]
        )
        if ["approver_ids", "approved_by_ids", "rejected_by_ids"].include? collection.kind
          @request[kind].each do |user_id|
            Belinkr::Request::Collection.new(
              entity_id: @request.entity_id,
              kind:      kind,
              user_id:   user_id).must_include @request
          end
        else
          collection.must_include @request
        end
      end
    end
  end #describe .create

  describe ".update" do
    it "update the present request" do
      update_request       = @request.dup
      update_request.title = "update request title"
      updated_request      = Belinkr::Request::Orchestrator.update(@request, update_request, @user)
      updated_request.title.must_equal update_request.title
    end
  end #describe .update

  describe ".delete" do
    it "marks the request as deleted" do
      @request.created_at.wont_be_nil
      @request.deleted_at.must_be_nil

      Belinkr::Request::Orchestrator.delete(@request, @user)

      @request.deleted_at .wont_be_nil
      Belinkr::Request::Collection::KINDS.each do |kind|
        collection = Belinkr::Request::Collection.new(
          entity_id: @request.entity_id,
          kind:      kind,
          user_id:   @request[kind]
        )
        if ["approver_ids", "approved_by_ids", "rejected_by_ids"].include? collection.kind
          @request[kind].each do |user_id|
            Belinkr::Request::Collection.new(
              entity_id: @request.entity_id,
              kind:      kind,
              user_id:   user_id).wont_include @request
          end
        else
          collection.wont_include @request
        end
      end
    end
  end # .delete

  describe ".undelete" do
    it "unmarks a previously deleted request as not deleted" do
      Belinkr::Request::Orchestrator.delete(@request, @user)

      @request.deleted_at.wont_be_nil
      Belinkr::Request::Collection::KINDS.each do |kind|
        collection = Belinkr::Request::Collection.new(
          entity_id: @request.entity_id,
          kind:      kind,
          user_id:   @request[kind]
        )
        if ["approver_ids", "approved_by_ids", "rejected_by_ids"].include? collection.kind
          @request[kind].each do |user_id|
            Belinkr::Request::Collection.new(
              entity_id: @request.entity_id,
              kind:      kind,
              user_id:   user_id).wont_include @request
          end
        else
          collection.wont_include @request
        end
      end

      Belinkr::Request::Orchestrator.undelete(@request, @user)

      @request.deleted_at.must_be_nil
      Belinkr::Request::Collection::KINDS.each do |kind|
        collection = Belinkr::Request::Collection.new(
          entity_id: @request.entity_id,
          kind:      kind,
          user_id:   @request[kind])
        if ["approver_ids", "approved_by_ids", "rejected_by_ids"].include? collection.kind
          @request[kind].each do |user_id|
            Belinkr::Request::Collection.new(
              entity_id: @request.entity_id,
              kind:      kind,
              user_id:   user_id).must_include @request
          end
        else
          collection.must_include @request
        end
      end
    end
  end #.undelete

  describe ".destroy" do
    it "destroy the persent request" do
      saved_id  = @request.id
      Belinkr::Request::Orchestrator.destroy(@request, @user)
      Belinkr::Request::Collection::KINDS.each do |kind|
        collection = Belinkr::Request::Collection.new(
          entity_id: @request.entity_id,
          kind:      kind,
          user_id:   @request[kind])
        if ["approver_ids", "approved_by_ids", "rejected_by_ids"].include? collection.kind
          @request[kind].each do |user_id|
            Belinkr::Request::Collection.new(
              entity_id: @request.entity_id,
              kind:      kind,
              user_id:   user_id).wont_include @request
          end
        else
          collection.wont_include @request
        end
      end
      lambda { Belinkr::Request::Member.new(id: saved_id) }
        .must_raise Tinto::Exceptions::NotFound
    end
  end #describe .destroy

  describe ".approve" do
    it "approve the persent request by one of approvers" do
      request = Belinkr::Request::Orchestrator.approve(
        @request, @boss
      )
      request.approved_by_ids.must_include @boss.id
      request.approver_ids.wont_include @boss.id
      Belinkr::Request::Collection.new(
        entity_id: @request.entity_id,
        kind:      "approver_ids",
        user_id:   @boss.id).wont_include @request
      Belinkr::Request::Collection.new(
        entity_id: @request.entity_id,
        kind:      "approved_by_ids",
        user_id:   @boss.id).must_include @request
      request.state.must_equal "pending"
    end

    it "registers an activity" do
      description  = "ok, you are granted to go ahead"
      request      = Belinkr::Request::Orchestrator.approve(
        @request, @boss, description
      )
      activities = Belinkr::Request::Activity::Collection.new(
        entity_id: @request.entity_id, request_id: @request.id
      ).all
      activity = activities.to_a.first
      activity.actor.resource.id      .must_equal @boss.id
      activity.action                 .must_equal Belinkr::Request::Activity::Member::ACTIONS[0]
      activity.object.resource.id     .must_equal @request.id
      activity.description.must_equal description
    end

    it "approve the persent request by all approvers, 
                  then its statue should be update to 'approved'" do
      request = Belinkr::Request::Orchestrator.approve(
        @request, @boss ).tap do |request|
        Belinkr::Request::Orchestrator.approve(request, @dean)
      end
      request.state.must_equal "accepted"
      activities = Belinkr::Request::Activity::Collection.new(
        entity_id: @request.entity_id, request_id: @request.id
      )
      activities.size.must_equal 2
    end
  end #describe .approve
  
  describe ".reject" do
    it " reject the persent request" do
     request = Belinkr::Request::Orchestrator.reject(@request, @boss)
     request.rejected_by_ids.must_include @boss.id
     request.approver_ids.wont_include @boss.id
     Belinkr::Request::Collection.new(
       entity_id: @request.entity_id,
       kind:      "approver_ids",
       user_id:   @boss.id
     ).wont_include @request
     Belinkr::Request::Collection.new(
       entity_id: @request.entity_id,
       kind:      "rejected_by_ids",
       user_id:   @boss.id
     ).must_include @request
     request.state.must_equal "rejected"
    end

    it "registers an activity" do
      description  = "Hold on please, we'll discuss this request laster"
      request      = Belinkr::Request::Orchestrator.reject(
        @request, @boss, description
      )
      activities   = Belinkr::Request::Activity::Collection.new(
        entity_id: @request.entity_id, request_id: @request.id
      ).all
      activity = activities.to_a.first
      activity.actor.resource.id      .must_equal @boss.id
      activity.action                 .must_equal Belinkr::Request::Activity::Member::ACTIONS[1]
      activity.object.resource.id     .must_equal @request.id
      activity.description.must_equal description
    end
  end #describe .reject
end
