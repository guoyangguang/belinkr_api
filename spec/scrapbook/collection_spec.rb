# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../scrapbook/collection"

describe Belinkr::Scrapbook::Collection do
  describe "validations" do
    describe "user_id" do
      it "must be present" do
        scrapbook = Belinkr::Scrapbook::Collection.new
        scrapbook.valid?.must_equal false
        scrapbook.errors[:user_id].must_include "user must not be blank"
      end

      it "must be a number" do
        scrapbook = Belinkr::Scrapbook::Collection.new
        scrapbook.valid?.must_equal false
        scrapbook.errors[:user_id].must_include "user must be a number"
      end
    end

    describe "kind" do
      it "must be present" do
        collection = Belinkr::Scrapbook::Collection.new
        collection.valid?.must_equal false
        collection.errors[:kind].must_include "kind must not be blank"
      end

      it "must be 'own' or 'other'" do
        collection = Belinkr::Scrapbook::Collection.new
        collection.valid?.must_equal false
        collection.errors[:kind]
          .must_include "kind must be one of own, others"
      end
    end #kind
  end # validations
end # Belinkr::Scrapbook::Collection
