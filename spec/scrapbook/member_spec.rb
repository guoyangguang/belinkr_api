# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../scrapbook/member"

describe Belinkr::Scrapbook::Member do
  describe "validations" do
    describe "#name" do
      it "must be present" do
        scrapbook = Belinkr::Scrapbook::Member.new
        scrapbook.valid?.must_equal false
        scrapbook.errors[:name].must_include "name must not be blank"
      end

      it "has minimum length of 3 characters" do
        scrapbook = Belinkr::Scrapbook::Member.new(name: "ab")
        scrapbook.valid?.must_equal false
        scrapbook.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end

      it "has maximum length of 250 characters" do
        scrapbook = Belinkr::Scrapbook::Member.new(name: "a" * 251)
        scrapbook.valid?.must_equal false
        scrapbook.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end
    end #name
      
    describe "#user_id" do
      it "must be present" do
        scrapbook = Belinkr::Scrapbook::Member.new
        scrapbook.valid?.must_equal false
        scrapbook.errors[:user_id].must_include "user must be a number"
      end

      it "must be a number" do
        scrapbook = Belinkr::Scrapbook::Member.new(scrapbook_id: "ab")
        scrapbook.valid?.must_equal false
        scrapbook.errors[:user_id].must_include "user must be a number"
      end
    end #user_id
  end # validations
end # Belinkr::Scrapbook::Member
