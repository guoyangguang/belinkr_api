# encoding: utf-8
require "minitest/autorun"
require_relative "../../user/member"
require_relative "../../user/orchestrator"
require_relative "../../user/collection"
require_relative "../../scrapbook/collection"
require_relative "../../scrapbook/orchestrator"
require_relative "../../user/member"
require_relative "../../user/orchestrator"


$redis ||= Redis.new

describe Belinkr::Scrapbook::Orchestrator do
  before do
    $redis.flushdb
    @user       = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new(first: "User", last: "111", entity_id: 1)
                  )
    @scrapbook  = Belinkr::Scrapbook::Orchestrator.create(
                    Belinkr::Scrapbook::Member
                      .new(name: "scrapbook 1", user_id: @user.id),
                      @user
                  )
  end

  describe ".create" do
    it "creates @scrapbook" do
      @scrapbook.valid?      .must_equal true
      @scrapbook.id          .wont_be_nil
      @scrapbook.name        .must_equal "scrapbook 1"
      @scrapbook.created_at  .wont_be_nil

      Belinkr::Scrapbook::Collection.new(user_id: @user.id, kind: "own")
        .must_include @scrapbook
    end
  end # .create

  describe ".update" do
    it "update a scrapbook" do
      updated       = @scrapbook.dup
      updated.name  = "updated name"

      Belinkr::Scrapbook::Orchestrator.update(@scrapbook, updated, @user)
      @scrapbook.name.must_equal "updated name"
    end
  end # .update

  describe ".delete" do
    it "marks the scrapbook as deleted" do
      @scrapbook.created_at.wont_be_nil
      @scrapbook.deleted_at.must_be_nil

      Belinkr::Scrapbook::Orchestrator.delete(@scrapbook, @user)

      @scrapbook.deleted_at .wont_be_nil
      Belinkr::Scrapbook::Collection.new(user_id: @user.id, kind: "own")
        .wont_include @scrapbook
    end
  end # .delete

  describe ".undelete" do
    it "unmarks a previously deleted scrapbook as not deleted" do
      Belinkr::Scrapbook::Orchestrator.delete(@scrapbook, @user)

      @scrapbook.deleted_at.wont_be_nil
      Belinkr::Scrapbook::Collection.new(user_id: @user.id, kind: "own")
        .wont_include @scrapbook
      
      Belinkr::Scrapbook::Orchestrator.undelete(@scrapbook, @user)

      @scrapbook.deleted_at.must_be_nil 
      Belinkr::Scrapbook::Collection.new(user_id: @user.id, kind: "own")
        .must_include @scrapbook
    end
  end #.undelete

  describe ".destroy" do
    it "destroys a scrapbook from the database" do
      saved_id  = @scrapbook.id
      Belinkr::Scrapbook::Orchestrator.destroy(@scrapbook, @user)

      @scrapbook.id          .must_be_nil 
      @scrapbook.deleted_at  .wont_be_nil

      Belinkr::Scrapbook::Collection.new(user_id: @user.id, kind: "own")
        .wont_include @scrapbook

      lambda { Belinkr::Scrapbook::Member.new(id: saved_id) }
        .must_raise Tinto::Exceptions::NotFound
    end
  end # .destroy
end # Belinkr::Scrapbook::Orchestrator
