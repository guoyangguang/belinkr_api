# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../scrapbook/scrap/collection"

describe Belinkr::Scrapbook::Scrap::Collection do
  describe "validations" do
    describe "scrapbook_id" do
      it "must be present" do
        collection = Belinkr::Scrapbook::Scrap::Collection.new
        collection.valid?.must_equal false
        collection.errors[:scrapbook_id]
          .must_include "scrapbook must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Scrapbook::Scrap::Collection.new
        collection.valid?.must_equal false
        collection.errors[:scrapbook_id]
          .must_include "scrapbook must be a number"
      end
    end

    describe "user_id" do
      it "must be present" do
        collection = Belinkr::Scrapbook::Scrap::Collection.new
        collection.valid?.must_equal false
        collection.errors[:user_id]
          .must_include "user must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Scrapbook::Scrap::Collection.new
        collection.valid?.must_equal false
        collection.errors[:user_id]
          .must_include "user must be a number"
      end
    end
  end
end # Belinkr::Scrapbook::Scrap::Collection
