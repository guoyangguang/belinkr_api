# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../scrapbook/scrap/member"

describe Belinkr::Scrapbook::Scrap::Member do
  describe "validations" do
    describe "#text" do
      it "must be present" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new
        scrap.valid?.must_equal false
        scrap.errors[:text].must_include "text must not be blank"
      end

      it "has minimum length of 3 characters" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new(text: "ab")
        scrap.valid?.must_equal false
        scrap.errors[:text]
          .must_include "text must be between 3 and 5000 characters long"
      end

      it "has maximum length of 5000 characters" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new(text: "a" * 5001)
        scrap.valid?.must_equal false
        scrap.errors[:text]
          .must_include "text must be between 3 and 5000 characters long"
      end
    end

    describe "#scrapbook_id" do
      it "must be present" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new
        scrap.valid?.must_equal false
        scrap.errors[:scrapbook_id].must_include "scrapbook must not be blank"
      end

      it "must be a number" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new(scrapbook_id: "ab")
        scrap.valid?.must_equal false
        scrap.errors[:scrapbook_id].must_include "scrapbook must be a number"
      end
    end

    describe "#user_id" do
      it "must be present" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new
        scrap.valid?.must_equal false
        scrap.errors[:user_id].must_include "user must not be blank"
      end

      it "must be a number" do
        scrap = Belinkr::Scrapbook::Scrap::Member.new(scrap_id: "ab")
        scrap.valid?.must_equal false
        scrap.errors[:user_id].must_include "user must be a number"
      end
    end
  end # validations
end # Belinkr::Scrapbook::Scrap::Member
