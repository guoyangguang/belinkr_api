# encoding: utf-8
require "minitest/autorun"
require_relative "../../../user/member"
require_relative '../../../user/orchestrator'
require_relative "../../../scrapbook/collection"
require_relative "../../../scrapbook/member"
require_relative "../../../scrapbook/orchestrator"
require_relative "../../../scrapbook/scrap/collection"
require_relative "../../../scrapbook/scrap/member"
require_relative "../../../scrapbook/scrap/orchestrator"

$redis ||= Redis.new

describe Belinkr::Scrapbook::Orchestrator do
  before do
    @user       = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "111", entity_id: 1
                  )
    @scrapbook  = Belinkr::Scrapbook::Orchestrator.create(
                    Belinkr::Scrapbook::Member
                      .new(name: "scrapbook 1", user_id: @user.id),
                    @user
                  )
    @scrap      = Belinkr::Scrapbook::Scrap::Orchestrator.create(
                    @scrapbook,
                    Belinkr::Scrapbook::Scrap::Member.new(
                      text:          "http://www.google.com",
                      scrapbook_id:  @scrapbook.id,
                      user_id:       @user.id,
                    ),
                    @user
                  )
  end

  describe ".create" do
    it "creates a scrap" do
      @scrap.id         .wont_be_nil
      @scrap.text       .must_equal "http://www.google.com"
      @scrap.created_at .wont_be_nil

      Belinkr::Scrapbook::Scrap::Collection
        .new(scrapbook_id: @scrap.scrapbook_id, user_id: @scrap.user_id)
        .must_include @scrap
    end
  end #.create

  describe ".update" do
    it "updates scrap" do
      updated       = @scrap.dup       
      updated.text  = "http://www.baidu.cn"
      Belinkr::Scrapbook::Scrap::Orchestrator
        .update(@scrapbook, @scrap, updated, @user)

      @scrap.text.must_equal "http://www.baidu.cn"
    end
  end #.update

  describe ".delete" do
    it "marks a scrap as deleted" do
      @scrap.deleted_at.must_be_nil
      Belinkr::Scrapbook::Scrap::Collection
        .new(scrapbook_id: @scrap.scrapbook_id, user_id: @scrap.user_id)
        .must_include @scrap

      Belinkr::Scrapbook::Scrap::Orchestrator.delete(@scrapbook, @scrap, @user)

      @scrap.deleted_at.wont_be_nil
      Belinkr::Scrapbook::Scrap::Collection
        .new(scrapbook_id: @scrap.scrapbook_id, user_id: @scrap.user_id)
        .wont_include @scrap
    end
  end #.delete

  describe ".undelete" do
    it "marks a previously deleted scrap as not deleted" do
      Belinkr::Scrapbook::Scrap::Orchestrator.delete(@scrapbook, @scrap, @user)
      Belinkr::Scrapbook::Scrap::Orchestrator
        .undelete(@scrapbook, @scrap, @user)

      @scrap.created_at.wont_be_nil
      @scrap.updated_at.wont_be_nil
      @scrap.deleted_at.must_be_nil

      Belinkr::Scrapbook::Scrap::Collection
        .new(scrapbook_id: @scrap.scrapbook_id, user_id: @scrap.user_id)
        .must_include @scrap
    end
  end # .undelete

  describe ".destroy" do
    it "removes a previously deleted scrap from database" do
      saved_id = @scrap.id

      Belinkr::Scrapbook::Scrap::Orchestrator.delete(@scrapbook, @scrap, @user)
      @scrap.deleted_at.wont_be_nil
      Belinkr::Scrapbook::Scrap::Orchestrator
        .destroy(@scrapbook, @scrap, @user)
      
      lambda { 
        Belinkr::Scrapbook::Scrap::Member.new(
          id:           saved_id,
          user_id:      @scrap.user_id,
          scrapbook_id: @scrap.scrapbook_id
        )
      }
        .must_raise Tinto::Exceptions::NotFound
    end
  end # .destroy

  describe ".read" do
    it "return a scrap if authorized successfully" do
      scrap = Belinkr::Scrapbook::Scrap::Orchestrator
        .read(@scrapbook, @scrap, @user) 
      scrap.must_equal @scrap
    end
  end
end # Belinkr::Scrapbook::Scrap::Orchestrator
