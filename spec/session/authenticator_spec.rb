# encoding: utf-8
require "redis"
require "minitest/autorun"
require_relative "../../session/authenticator"
require_relative "../../credential/member"
require_relative "../../tinto/exceptions"

$redis = Redis.new

include Belinkr
describe Authenticator do
  before do 
    $redis.flushdb
    @credential = Credential::Member.new(
                    email:    "dummy@belinkr.com", 
                    password: "changeme"
                  )
  end

  describe ".add" do
    it "adds a email => credential_id pair 
    to the authenticator hash" do
      Authenticator.exists?(@credential.email).must_equal false
      Authenticator.add(@credential)
      Authenticator.exists?(@credential.email).must_equal true
    end

    it "raises InvalidResource if the key already exists" do
      Authenticator.add(@credential)

      lambda { Authenticator.add(@credential) }
        .must_raise Tinto::Exceptions::InvalidResource
    end

    it "ensures password is encrypted with BCrypt" do
      Authenticator.add(@credential)
      Tinto::Utils.bcrypted?(@credential.password).must_equal true
    end
  end #.add

  describe ".remove" do
    it "removes a email => password pair from the authenticator hash" do
      Authenticator.add(@credential)
      Authenticator.exists?(@credential.email).must_equal true
      Authenticator.remove(@credential)
      Authenticator.exists?(@credential.email).must_equal false
    end
  end #.remove

  describe "exists?" do
    it "returns true if the passed email already exists as a key" do
      Authenticator.add(@credential)
      Authenticator.exists?(@credential.email).must_equal true
      Authenticator.exists?("foo@belinkr.com").must_equal false
    end
  end #.exists?

  describe ".keys" do
    it "gets all the emails in the authenticator hash" do
      Authenticator.keys.must_be_empty
      Authenticator.add(@credential)
      Authenticator.keys.must_equal [@credential.email]
    end
  end #.keys

  describe ".authenticate" do
    it "returns the credential if the email and password match" do
      Authenticator.add(@credential)

      Authenticator.authenticate("dummy@belinkr.com", "incorrect")
        .must_equal false
      Authenticator.authenticate("incorrect@belinkr.com", "changeme")
        .must_equal false
      Authenticator.authenticate("dummy@belinkr.com", "changeme")
        .must_be_instance_of Credential::Member
    end
  end #.authenticate

  describe ".get" do
    it "returns the credential" do
      Authenticator.add(@credential)
      Authenticator.get(@credential.email).id.must_equal @credential.id
    end
  end
end # Authenticator
