# encoding: utf-8
require "minitest/autorun"
require_relative "../../session/orchestrator"
require_relative "../../user/member"
require_relative "../../credential/member"
require_relative "../../credential/orchestrator"
require_relative "../../tinto/exceptions"

$redis = Redis.new

include Belinkr

describe Session::Orchestrator do
  before do
    $redis.flushdb
    @session = {}
    @credential, @user  = 
      Credential::Orchestrator.create(
        Credential::Member.new(email: "foo@foo.com", password: "changeme"),
        User::Member.new(first: "User", last: "111", entity_id: 1)
      )
  end

  describe ".create" do
    it "returns a user member if authentication successful" do
      Session::Orchestrator.create(@session, @credential.email, "changeme")
        .must_be_instance_of User::Member
    end

    it "raises InvalidResource if authentication unsuccessful" do
      lambda {
        Session::Orchestrator.create(@session, "wrong_email", "changeme")
      }.must_raise Tinto::Exceptions::NotAllowed
    end

    it "sets session[:user_id] and session[:entity_id] if successful" do
      user = Session::Orchestrator
              .create(@session, @credential.email, "changeme")

      @session[:user_id]    .must_equal user.id
      @session[:entity_id]  .must_equal user.entity_id
    end
  end #.create

  describe ".destroy" do
    it "clears the session" do
      Session::Orchestrator.create(@session, @credential.email, "changeme")
      Session::Orchestrator.delete(@session)
      @session[:user_id].must_equal nil
      @session[:entity_id].must_equal nil
    end
  end #.destroy
end # Session::Orchestrator
