# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../status/collection"

describe Belinkr::Status::Collection do
  describe "validations" do
    describe "user_id" do
      it "must be present" do
        collection = Belinkr::Status::Collection.new
        collection.valid?.must_equal false
        collection.errors[:user_id].must_include "user must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Status::Collection.new
        collection.valid?.must_equal false
        collection.errors[:user_id].must_include "user must be a number"
      end
    end #user_id

    describe "entity_id" do
      it "must be present" do
        collection = Belinkr::Status::Collection.new
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Status::Collection.new
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id

    describe "kind" do
      it "must be present" do
        collection = Belinkr::Status::Collection.new
        collection.valid?.must_equal false
        collection.errors[:kind].must_include "kind must not be blank"
      end

      it "must be 'own' or 'general'" do
        collection = Belinkr::Status::Collection.new
        collection.valid?.must_equal false
        collection.errors[:kind]
          .must_include "kind must be one of own, general, replies"
      end
    end #kind
  end # validations
end # Belinkr::Status::Collection
