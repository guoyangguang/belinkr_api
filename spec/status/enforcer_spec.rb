# encoding: utf-8
require "ostruct"
require "minitest/autorun"
require_relative "../../status/enforcer"
require_relative "../../user/orchestrator"
require_relative "../../follow/collection"
require_relative "../../tinto/exceptions"

$redis = Redis.new

describe Belinkr::Status::Enforcer do
  before do
    $redis.flushdb
    @user   = Belinkr::User::Orchestrator.create(
                Belinkr::User::Member
                  .new(first: "User", last: "111", entity_id: 1)
              )
    @status = Belinkr::Status::Member
                .new(text: "my status", user_id: 2, entity_id: 1)
  end

  describe "all actions" do
    it "raises NotAllowed if user not present" do
      user = OpenStruct.new

      [:create, :read, :update].each do |action|
        lambda { 
          Belinkr::Status::Enforcer.authorize user, action, @status
        }.must_raise Tinto::Exceptions::NotAllowed
      end
    end
  end # all actions

  describe "update_by?" do
    it "raises NotAllowed unless user is the author of the status" do
      lambda { Belinkr::Status::Enforcer.authorize @user, :update, @status }
        .must_raise Tinto::Exceptions::NotAllowed
    end
  end #update_by?
end
