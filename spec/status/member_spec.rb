# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../status/member"

describe Belinkr::Status::Member do
  describe "validations" do
    describe "text" do
      it "has more than 3 characters" do
        status = Belinkr::Status::Member.new(text: "ab")
        status.valid?.must_equal false
        status.errors[:text]
          .must_include "text must be between 3 and 5000 characters long"
      end

      it "has less than 5000 characters" do
        status = Belinkr::Status::Member.new(text: "a" * 5001)
        status.valid?.must_equal false
        status.errors[:text]
          .must_include "text must be between 3 and 5000 characters long"
      end
    end #text

    describe "user_id" do
      it "must be present" do
        status = Belinkr::Status::Member.new
        status.valid?.must_equal false
        status.errors[:user_id].must_include "user must not be blank"
      end

      it "is a integer" do
        status = Belinkr::Status::Member.new(text: "test", user_id: "ad")
        status.valid?.must_equal false
        status.errors[:user_id].must_include "user must be a number"
      end
    end #user_id

    describe "entity_id" do
      it "must be present" do
        status = Belinkr::Status::Member.new
        status.valid?.must_equal false
        status.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        status = Belinkr::Status::Member.new(text: "test", entity_id: "ad")
        status.valid?.must_equal false
        status.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id

    describe "files" do
      it "is a Array" do
        status = Belinkr::Status::Member.new
        status.valid?.must_equal false
        status.files.must_be_kind_of Array
        status.files.must_be_empty
      end
    end #files
  end # validations

  describe "protected attributes" do
    it "protects #id" do
      skip
      status = Belinkr::Status::Member.new
      lambda { status.id = 20 }.must_raise NoMethodError
    end

    it "protects #created_at" do
      skip
      status = Belinkr::Status::Member.new
      lambda { status.created_at = Time.now }.must_raise NoMethodError
    end

    it "protects #updated_at" do
      skip
      status = Belinkr::Status::Member.new
      lambda { status.updated_at = Time.now }.must_raise NoMethodError
    end

    it "protects #deleted_at" do
      skip
      status = Belinkr::Status::Member.new
      lambda { status.deleted_at = Time.now }.must_raise NoMethodError
    end
  end

  describe '#replies' do
    it "returns a Reply::Collection" do
      status = Belinkr::Status::Member.new
      status.embedded_replies.class.must_equal Belinkr::Reply::Collection
      status.replies.class.must_equal Belinkr::Reply::Collection
    end
  end #replies
end # Belinkr::Status::Member
