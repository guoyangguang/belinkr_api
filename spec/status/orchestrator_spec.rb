# encoding: utf-8
require "minitest/autorun"
require_relative "../../status/orchestrator"
require_relative "../../follow/orchestrator"
require_relative "../../user/orchestrator"
require_relative "../../user/collection"

$redis = Redis.new

describe Belinkr::Status::Orchestrator do
  before do 
    $redis.flushdb

    @user       = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "111", entity_id: 1
                  )
    @status     = Belinkr::Status::Member
                    .new text: "test", user_id: @user.id

    @followed   = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "111", entity_id: 1
                  )
    @follower1  = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "112", entity_id: 1
                  )
    @follower2  = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new first: "User", last: "113", entity_id: 1
                  )
    @follow1    = Belinkr::Follow::Orchestrator.create(
                    Belinkr::Follow::Member.new(
                      follower_id:  @follower1.id, 
                      followed_id:  @followed.id,
                    ),
                    @follower1
                  )
    @follow2    = Belinkr::Follow::Orchestrator.create(
                    Belinkr::Follow::Member.new(
                      follower_id:  @follower2.id, 
                      followed_id:  @followed.id,
                    ),
                    @follower2
                  )
    @status1    = Belinkr::Status::Member.new text: "test 1"
    @status2    = Belinkr::Status::Member.new text: "test 2"
  end

  describe ".create" do
    it "persists a new status" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      status.id.wont_be_nil
    end

    it "adds it to the author's general timeline" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      general_timeline_for(@user).must_include status
    end

    it "adds it to the author's own timeline" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      own_timeline_for(@user).must_include status
    end

    it "adds it to the general timeline of all author's followers" do
      status = Belinkr::Status::Orchestrator.create(
                 Belinkr::Status::Member.new(text: "my status"), @followed
               )

      general_timeline_for(@follower1).must_include status
      general_timeline_for(@follower2).must_include status
    end

    it "take a block yield the saved status" do
      Belinkr::Status::Orchestrator.create(@status, @user) do |resource|
        resource.id.wont_be_nil
        resource.must_be_kind_of Belinkr::Status::Member
      end
    end

  end # .create

  describe ".update" do
    it "updates an existing status" do
      status  = Belinkr::Status::Orchestrator.create(@status, @user)
      updated = status.dup
      updated.text = "updated text"
      
      updated = Belinkr::Status::Orchestrator.update(status, updated, @user)

      status.id       .must_equal updated.id
      status.user_id  .must_equal updated.user_id
      updated.text    .must_equal "updated text"
    end

    it "take a block yield the original status and updated status" do
      status  = Belinkr::Status::Orchestrator.create(@status, @user)
      updated = status.dup
      updated.text = "updated text"

      updated = Belinkr::Status::Orchestrator
                  .update(status, updated, @user) do |old_status, new_status|
        old_status.text.must_equal "test"
        new_status.text.must_equal "updated text"
      end

      status.id       .must_equal updated.id
      status.user_id  .must_equal updated.user_id
      updated.text    .must_equal "updated text"
    end

    it "updates the score of the status to a higher value" do
      status        = Belinkr::Status::Orchestrator.create(@status, @user)
      updated       = status.clone
      updated.text  = "updated text"
      
      score   = status.score
      updated = Belinkr::Status::Orchestrator.update(status, updated, @user)

      (updated.score > score).must_equal true
    end

    it "moves the status to the top of the author's own timeline" do
      status1   = Belinkr::Status::Orchestrator.create(@status1, @followed)
      status2   = Belinkr::Status::Orchestrator.create(@status2, @followed)

      own_timeline_for(@followed).all.first.text.must_equal status2.text
      
      updated       = status1.dup
      updated.text  = "updated status 1"

      Belinkr::Status::Orchestrator.update(status1, updated, @followed)
      own_timeline_for(@followed).all.first.text
        .must_equal "updated status 1"
    end

    it "moves the status to the top of the author's general timeline" do
      status1   = Belinkr::Status::Orchestrator.create(@status1, @followed)
      status2   = Belinkr::Status::Orchestrator.create(@status2, @followed)

      general_timeline_for(@followed).all.first.text.must_equal status2.text
      
      updated       = status1.dup
      updated.text  = "updated status 1"

      Belinkr::Status::Orchestrator.update(status1, updated, @followed)
      general_timeline_for(@followed).all.first.text
        .must_equal "updated status 1"
    end

    it "moves the status to the top of the genernal timeline of all
    followers of this author" do
      status1   = Belinkr::Status::Orchestrator.create(@status1, @followed)
      status2   = Belinkr::Status::Orchestrator.create(@status2, @followed)

      updated       = status1.dup
      updated.text  = "updated status 1"

      Belinkr::Status::Orchestrator.update(status1, updated, @followed)
      general_timeline_for(@follower1).all.first.text
        .must_equal "updated status 1"
      general_timeline_for(@follower2).all.first.text
        .must_equal "updated status 1"
    end
  end

  describe ".delete" do
    it "deletes an existing status" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      status.deleted_at.must_be_nil
      status.created_at.wont_be_nil

      deleted = Belinkr::Status::Orchestrator.delete(status, @user)

      deleted.deleted_at .wont_be_nil
      deleted.id         .must_equal status.id
      deleted.text       .must_equal status.text
    end

    it "removes the status from the author's own timeline" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)

      own_timeline_for(@user).must_include(status)
      Belinkr::Status::Orchestrator.delete(status, @user)
      own_timeline_for(@user).wont_include(status)
    end

    it "removes the status from the author's general timeline" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)

      general_timeline_for(@user).must_include(status)
      Belinkr::Status::Orchestrator.delete(status, @user)
      general_timeline_for(@user).wont_include(status)
    end

    it "removes the status from the general timeline of all followers of
    this author" do
      status = Belinkr::Status::Orchestrator.create(
                 Belinkr::Status::Member.new(text: "my status"), @followed
               )

      general_timeline_for(@follower1).must_include status
      general_timeline_for(@follower2).must_include status

      Belinkr::Status::Orchestrator.delete(status, @followed)
      general_timeline_for(@follower1).wont_include status
      general_timeline_for(@follower2).wont_include status
    end
  end

  describe ".undelete" do
    it "restores a previously deleted status" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      Belinkr::Status::Orchestrator.delete(status, @user)
      status.deleted_at.wont_be_nil

      Belinkr::Status::Orchestrator.undelete(status, @user)
      status.deleted_at.must_be_nil
    end

    it "adds the status to the author's own timeline" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      status = Belinkr::Status::Orchestrator.delete(status, @user)
      own_timeline_for(@user).wont_include status

      status = Belinkr::Status::Orchestrator.undelete(status, @user)
      own_timeline_for(@user).must_include status
    end

    it "adds the status to the author's general timeline" do
      status = Belinkr::Status::Orchestrator.create(@status, @user)
      status = Belinkr::Status::Orchestrator.delete(status, @user)
      general_timeline_for(@user).wont_include status

      status = Belinkr::Status::Orchestrator.undelete(status, @user)
      general_timeline_for(@user).must_include status
    end

    it "adds the status to the general timeline of all the
    followers of the author" do
      status = Belinkr::Status::Orchestrator.create(
                 Belinkr::Status::Member.new(text: "my status"), @followed
               )

      general_timeline_for(@follower1).must_include status
      general_timeline_for(@follower2).must_include status

      Belinkr::Status::Orchestrator.delete(status, @followed)
      general_timeline_for(@follower1).wont_include status
      general_timeline_for(@follower2).wont_include status

      status = Belinkr::Status::Orchestrator.undelete(status, @followed)
      general_timeline_for(@follower1).must_include status
      general_timeline_for(@follower2).must_include status
    end
  end
end

def general_timeline_for(user)
  Belinkr::Status::Collection
    .new(user_id: user.id, entity_id: user.entity_id, kind: "general")
end

def own_timeline_for(user)
  Belinkr::Status::Collection
    .new(user_id: user.id, entity_id: user.entity_id, kind: "own")
end
