# encoding: utf-8
require "minitest/autorun"
require "mocha"
require "ostruct"

require_relative '../../api/helpers/file_mover'
require_relative '../../user/member'
require_relative '../../status/orchestrator'

$redis = Redis.new
include Belinkr::ApiHelpers

describe Tinto::FileScheduler do

  describe "Constant QUEUE_KEY" do
    it "the queue key must be present" do
      Tinto::FileScheduler::QUEUE_KEY.wont_be_nil
    end
  end

  describe "responds" do
    it "should responds to specify methods" do
      prefix = "enqueue_for_"
      enqueue_for = %w(create update delete destroy)
      enqueue_for.each do |action|
        method_name = "#{prefix}#{action}".to_sym
        Tinto::FileScheduler.respond_to?(method_name).must_equal true
        Tinto::FileScheduler.method(method_name).arity.must_equal -1
      end

      Tinto::FileScheduler.respond_to?(:enqueue).must_equal true
    end
  end

  describe "enqueue" do
    before do
      $redis.flushdb
    end

    describe ".enqueue" do
      it "enqueue the worker job" do
        job = { action: 'create',
                resource: { type: 'Status', id: 1 },
                files: [{ id: "string"}]
              }.to_json
        Tinto::FileScheduler.enqueue(job)
        $redis.llen(Tinto::FileScheduler::QUEUE_KEY).must_equal 1
        # consume
        queued_job = $redis.blpop(Tinto::FileScheduler::QUEUE_KEY, 0)
        queued_job.must_be_kind_of Array
        key, value = *queued_job
        key.must_equal Tinto::FileScheduler::QUEUE_KEY
        parsed_job = JSON.parse(value)
        parsed_job.must_be_kind_of Hash
        parsed_job["action"].must_equal 'create'
        parsed_job['resource'].wont_be_nil
        parsed_job['files'].must_be_kind_of Array
        parsed_job['files'].size.must_equal 1
        parsed_job['files'].first.must_equal "id" => "string"
        $redis.llen(Tinto::FileScheduler::QUEUE_KEY).must_equal 0
      end
    end

    describe ".resource_valid?" do
      it "return false if resource invalid" do
        resource = "resource"
        Tinto::FileScheduler.resource_valid?(resource).must_equal false

        resource.stubs(:valid?).returns(false)
        Tinto::FileScheduler.resource_valid?(resource).must_equal false
      end

      it "return false if resource id is nil" do
        resource = "resource"
        resource.stubs(:valid?).returns(true)
        resource.stubs(:id).returns(nil)
        Tinto::FileScheduler.resource_valid?(resource).must_equal false
      end

      it "return false if resource has no files" do
        resource = "resource"
        resource.stubs(:valid?).returns(true)
        resource.stubs(:id).returns(1)
        Tinto::FileScheduler.resource_valid?(resource).must_equal false
        resource.stubs(:files).returns("string")
        Tinto::FileScheduler.resource_valid?(resource).must_equal false
      end

      it "return true" do
        resource = "resource"
        resource.stubs(:valid?).returns(true)
        resource.stubs(:id).returns(1)
        resource.stubs(:files).returns([])
        Tinto::FileScheduler.resource_valid?(resource).must_equal true
      end
    end # .resource_valid?

    describe ".create" do
      it "return false if no args passed" do
        Tinto::FileScheduler.enqueue_for_create.must_equal false
      end

      it "return false if the files is empty" do
        resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: []
        )
        Tinto::FileScheduler.enqueue_for_create(resource).must_equal false
      end

      it "enqueue the job" do
        expect_data = {
          action: "create",
          resource: { type: "OpenStruct", id: 1},
          files: ["file1", "file2"]
        }

        Tinto::FileScheduler.expects(:enqueue).with(expect_data.to_json).once
        resource = OpenStruct.new(
          id: 1,
          valid?: true,
          files: [{ "original" => '/path/to/file1' },
                  { "original" => '/path/to/file2' }]
        )
        Tinto::FileScheduler.enqueue_for_create(resource).must_equal true
      end

      describe "with real resource" do
        before do
          @user = Belinkr::User::Member.new(
              first: 'a234', last: 'b678', entity_id: 1
          ).save
        end

        it "create status without files should return false" do
          Tinto::FileScheduler.expects(:enqueue_for_create).returns(false).once
          status = Belinkr::Status::Member.new(text: "my status", entity_id: 1)
          Belinkr::Status::Orchestrator.create(status, @user, &file_mover)
        end

        it "create status with files should return true" do
          expect_data = {
              action: "create",
              resource: { type: "Belinkr::Status::Member", id: 1},
              files: ["file1", "file2"]
          }
          Tinto::FileScheduler.expects(:enqueue).with(expect_data.to_json).once
          status = Belinkr::Status::Member.new(
            text: "my status", entity_id: 1,
            files: [ { "id" => "long1", "original" => '/path/to/file1' },
                     { "id" => "long2", "original" => '/path/to/file2' }]
          )
          Belinkr::Status::Orchestrator.create(status, @user, &file_mover)
        end
      end # with real resource

    end # .create

    describe ".update" do
      it "return false if no args passed" do
        Tinto::FileScheduler.enqueue_for_update.must_equal false
      end

      it "return false if old_resource invalid" do
        old_resource = "resource_old"
        new_resource = "resource_new"
        Tinto::FileScheduler.enqueue_for_update(old_resource, new_resource)
          .must_equal false
      end

      it "return false if new_resource invalid" do
        old_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: [{ "original" => '/path/to/file1' },
                    { "original" => '/path/to/file2' }]
        )
        new_resource = "resource_new"
        Tinto::FileScheduler.enqueue_for_update(old_resource, new_resource)
          .must_equal false
      end

      it "return false if resources id not match" do
        old_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: []
        )
        new_resource = OpenStruct.new(
            id: 2,
            valid?: true,
            files: []
        )
        Tinto::FileScheduler.enqueue_for_update(old_resource, new_resource)
          .must_equal false
      end

      it "return false if new resource files is empty" do
        old_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: []
        )
        new_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: []
        )
        Tinto::FileScheduler.enqueue_for_update(old_resource, new_resource)
          .must_equal false
      end

      it "return false if got no incremental files with update" do
        old_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: [{ "original" => '/path/to/file1' }]
        )
        new_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: [{ "original" => '/path/to/file1' }]
        )
        Tinto::FileScheduler.enqueue_for_update(old_resource, new_resource)
          .must_equal false
      end

      it "enqueue the job" do
        expect_data = {
            action: "create",
            resource: { type: "OpenStruct", id: 1},
            files: ["file2"]
        }

        old_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: [{ "original" => '/path/to/file1' }]
        )
        new_resource = OpenStruct.new(
            id: 1,
            valid?: true,
            files: [{ "original" => '/path/to/file1' },
                    { "original" => '/path/to/file2' }]
        )
        Tinto::FileScheduler.expects(:enqueue).with(expect_data.to_json).once
        Tinto::FileScheduler.enqueue_for_update(old_resource, new_resource)
          .must_equal true
      end

    end # .update
  end # enqueue
end # Tinto::FileScheduler