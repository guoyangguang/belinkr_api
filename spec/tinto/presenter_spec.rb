# encoding: utf-8
require "minitest/autorun"

require_relative "../../tinto/presenter"

describe Tinto::Presenter do

  before do
    @host_klass = Object.const_set("Host#{('A'..'Z').to_a.sample(6).join}", Class.new)
    Object.instance_eval { remove_const :Presenter } if Object.const_defined?(:Presenter)
  end

  describe ".determine_for" do

    it "raise ArgumentError if the argument of the method is not a Class" do
      lambda { Tinto::Presenter.determine_for(1) }.must_raise ArgumentError
    end

    it "raise NameError if presenter class not exist" do
      lambda { Tinto::Presenter.determine_for(@host_klass) }
      .must_raise NameError
    end

    it "return the right presenter class" do
      module_presenter = Object.const_set("Presenter", Module.new)
      presenter_class = module_presenter.const_set(@host_klass.name, Class.new)
      Tinto::Presenter.determine_for(@host_klass).must_equal presenter_class
    end

    it "raise RuntimeError if the presenter CONST is not a Class type" do
      module_presenter = Object.const_set("Presenter", Module.new)
      module_presenter.const_set(@host_klass.name, "string")
      lambda { Tinto::Presenter.determine_for(@host_klass) }.must_raise RuntimeError
    end

  end # .determine_for

end # Tinto::Presenter