# encoding: utf-8
require "minitest/autorun"
require 'redis'
require_relative "../../tinto/utils"

$redis = Redis.new

module TestSuite
  class Dummy
    def self.redis_key_oo;"dummy:multi:oo";end
    def self.redis_key_yn;"dummy:multi:yn";end

    def self.with_multi_exec(&block); $redis.multi { block.call } ;end
    def self.without_multi_exec(&block); block.call ;end

    def self.operate_on_off!
      value = Tinto::Utils.in_multi? ? "on" : "off"
      $redis.set redis_key_oo, value
    end

    def self.operate_yes_no!
      value = Tinto::Utils.in_multi? ? "yes" : "no"
      $redis.set redis_key_yn, value
    end
  end # Dummy
end # TestSuite

describe Tinto::Utils do
  describe ".bcrypted?" do
    it "returns true if password is a BCrypt hash" do
      plaintext = "changeme"
      password  = BCrypt::Password.create(plaintext)

      Tinto::Utils.bcrypted?(password).must_equal true
      Tinto::Utils.bcrypted?("plaintext").must_equal false
    end
  end # bcrypted?

  describe ".local_js_time_for" do
    it "convert ruby time object to string that js could understand" do
      offset_seconds = 60 * 60
      timezone       = "Europe/Madrid"
      utc_time       = Time.utc(2000,"jan",1,20,15,1)
      js_time        = Tinto::Utils.local_js_time_for(utc_time, timezone) 
      milliseconds   = ExecJS.eval "(new Date(\"#{js_time}\")).getTime()"
      milliseconds.to_i.must_equal (utc_time.to_i + offset_seconds) * 1000
    end
  end # .local_js_time_for

  describe ".in_multi?" do
    before do
      $redis.flushdb
    end

    describe "single job" do
      it "will get 'on' if running inside the redis multi" do
        TestSuite::Dummy.with_multi_exec { TestSuite::Dummy.operate_on_off! }
        value = $redis.get TestSuite::Dummy.redis_key_oo
        value.must_equal 'on'
      end

      it "will get 'off' if running without the redis multi" do
        TestSuite::Dummy.without_multi_exec { TestSuite::Dummy.operate_on_off! }
        value = $redis.get TestSuite::Dummy.redis_key_oo
        value.must_equal 'off'
      end
    end

    describe "multiple jobs" do
      it "will get 'on' and 'yes'" do
        TestSuite::Dummy.with_multi_exec {
          TestSuite::Dummy.operate_on_off!
          TestSuite::Dummy.operate_yes_no!
        }
        value = $redis.get TestSuite::Dummy.redis_key_oo
        value.must_equal 'on'
        value = $redis.get TestSuite::Dummy.redis_key_yn
        value.must_equal 'yes'
      end

      it "will get 'off' and 'no'" do
        TestSuite::Dummy.without_multi_exec {
          TestSuite::Dummy.operate_on_off!
          TestSuite::Dummy.operate_yes_no!
        }
        value = $redis.get TestSuite::Dummy.redis_key_oo
        value.must_equal 'off'
        value = $redis.get TestSuite::Dummy.redis_key_yn
        value.must_equal 'no'
      end
    end # multiple jobs
  end # .in_multi?

  describe '.add_to_sorted_set' do
    before do
      $redis.flushdb
      @sorted_set_key = "dummy:sorted_set_key"
      @method         = :add_to_sorted_set
    end

    it "raise ArgumentError if key is missing" do
      lambda { Tinto::Utils.send(@method) }.must_raise(ArgumentError)
    end

    it "do nothing if members is a empty Array" do
      count = $redis.zcard(@sorted_set_key)
      count.must_equal 0
      Tinto::Utils.send(@method, @sorted_set_key)
      count = $redis.zcard(@sorted_set_key)
      count.must_equal 0
    end

    describe 'members already contains score' do

      before do
        @valid_members = [
            { member: '123', score: 1 },
            { 'member' => '456', 'score' => 2 }
        ]
        @invalid_members = @valid_members.dup.concat [
            { member: 'invalid score', score: 'invalid score' },
            { score: 4 }
        ]
      end

      it "with valid members" do
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 0
        Tinto::Utils.send(@method, @sorted_set_key, @valid_members, true)
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 2

        @valid_members.each do |pair|
          member = pair[:member] || pair['member']
          score  = pair[:score] || pair['score']
          $redis.zscore(@sorted_set_key, member).must_equal score.to_s
        end
      end # all valid members

      it "with invalid members" do
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 0
        Tinto::Utils.send(@method, @sorted_set_key, @invalid_members, true)
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 2

        @valid_members.each do |pair|
          member = pair[:member] || pair['member']
          score  = pair[:score] || pair['score']
          $redis.zscore(@sorted_set_key, member).must_equal score.to_s
        end
      end
    end # members already contains scores

    describe "members without scores" do
      before do
        @valid_members = ['123', '456']
        @invalid_members = @valid_members.dup.concat [nil, '789']
      end

      it 'with valid members' do
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 0
        Tinto::Utils.send(@method, @sorted_set_key, @valid_members)
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 2

        # score high to low
        members = $redis.zrevrange @sorted_set_key, 0, count - 1
        members.must_equal @valid_members.reverse
      end

      it 'with invalid members' do
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 0
        Tinto::Utils.send(@method, @sorted_set_key, @invalid_members)
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 3

        # score high to low
        members = $redis.zrevrange @sorted_set_key, 0, count - 1
        members.must_equal (@invalid_members).compact.reverse
      end
    end # members without scores
  end # .add_to_sorted_set

  describe '.remove_from_sorted_set' do
    before do
      $redis.flushdb
      @sorted_set_key = "dummy:sorted_set_key"
      @method         = :remove_from_sorted_set

      @members = ['123', '456', '789']

      @members.each do |member|
        $redis.zadd @sorted_set_key, Time.now.to_f, member
      end
    end

    it "raise ArgumentError if key is missing" do
      lambda { Tinto::Utils.send(@method) }.must_raise(ArgumentError)
    end

    it "do nothing if members is a empty Array" do
      @members.wont_be_empty
      count = $redis.zcard(@sorted_set_key)
      count.must_equal @members.size
      Tinto::Utils.send(@method, @sorted_set_key)
      @members.wont_be_empty
      count = $redis.zcard(@sorted_set_key)
      count.must_equal @members.size
    end

    describe "removes all the given members in the sorted set" do
      it "remove all members" do
        @members.wont_be_empty
        count = $redis.zcard(@sorted_set_key)
        count.must_equal @members.size

        Tinto::Utils.send(@method, @sorted_set_key, @members)
        count = $redis.zcard(@sorted_set_key)
        count.must_equal 0
      end

      it "remove a member from the sorted_set" do
        @members.wont_be_empty
        count = $redis.zcard(@sorted_set_key)
        count.must_equal @members.size

        member_to_remove = @members.sample
        Tinto::Utils.send(@method, @sorted_set_key, member_to_remove)
        count = $redis.zcard(@sorted_set_key)
        count.must_equal @members.size - 1

        result = $redis.zrevrank @sorted_set_key, member_to_remove
        result.must_be_nil
      end
    end #
  end # .remove_from_sorted_set
end # Tinto::Utils
