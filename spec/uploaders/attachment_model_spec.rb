# encoding: utf-8

require 'minitest/autorun'
require 'timecop'
require 'mocha'

require_relative "../../uploaders/attachment_model"

describe Belinkr::AttachmentModel do

  describe "#intialize" do
    it "identity not nil" do
      t = Time.now
      Timecop.freeze(t) do
        model = Belinkr::AttachmentModel.new("md5" * 10)
        model.instance_variable_get(:@identity).must_equal "md5" * 10
        model.instance_variable_get(:@timestamp).must_equal t
      end
    end

    it "identity is nil" do
      t = Time.now
      Timecop.freeze(t) do
        model = Belinkr::AttachmentModel.new
        model.instance_variable_get(:@identity).must_be_nil
        model.instance_variable_get(:@timestamp).must_equal t
      end
    end
  end #initialize

  describe "#random_seed" do
    it "is a float" do
      model = Belinkr::AttachmentModel.new
      model.random_seed.must_be_kind_of Float
    end
  end #random_seed


  describe "#identity" do
    describe "initialize with a identity" do
      before do
        Digest::MD5.expects(:hexdigest).never
      end

      it "will return the initialized identity" do
        model = Belinkr::AttachmentModel.new("md5" * 10)
        model.instance_variable_get(:@identity).must_equal "md5" * 10
        model.identity.must_equal "md5" * 10
      end
    end # initialize with a identity

    describe "initialize without identity" do
      before do
        Belinkr::AttachmentModel.any_instance
          .stubs(:random_seed).returns(0.9876543210123456)
      end

      it "return a new identity" do
        model_ = Belinkr::AttachmentModel.new
        no_freeze_identity = model_.identity

        t = Time.now
        Timecop.freeze(t) do
          model = Belinkr::AttachmentModel.new
          model.instance_variable_get(:@identity).must_be_nil
          model.instance_variable_get(:@timestamp).must_equal t
          seed_md5 = Digest::MD5.hexdigest(
               model_.random_seed.to_s + t.to_f.to_s
          )
          model.identity.must_equal seed_md5
          model.instance_variable_get(:@identity).must_equal seed_md5
          no_freeze_identity.wont_equal seed_md5
        end
      end
    end # initialize without identity
  end #identity

  describe ".for_identity" do
    it "if identity missing should raise a ArgumentError" do
      lambda {
        Belinkr::AttachmentModel.for_identity(nil)
      }.must_raise ArgumentError
    end

    it "instantiated" do
      model = Belinkr::AttachmentModel.for_identity("1234567890")
      model.must_be_kind_of Belinkr::AttachmentModel
      model.identity.must_equal "1234567890"
    end
  end #.for_identity

end #Belinkr::AttachmentModel