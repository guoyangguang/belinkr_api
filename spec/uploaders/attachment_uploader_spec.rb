# encoding: utf-8
require 'minitest/autorun'
require 'ostruct'

require 'timecop'
require 'mocha'
require 'fileutils'

require_relative "../../uploaders/attachment_uploader"

Belinkr::AttachmentUploader.class_eval do
  def store_dir
    "/tmp/files/#{model.id}"
  end
end

describe Belinkr::AttachmentUploader do

  before do
    @store_dir = "/tmp/files"
  end

  describe ".for_identity" do
    it "instantiated for a identity" do
      uploader = Belinkr::AttachmentUploader.for_identity((0..9).to_a.join)
      uploader.model.wont_be_nil
      uploader.model.identity.must_equal "0123456789"
    end
  end #.for_identity

  describe ".for" do
    it "instantiated for a model" do
      model = Belinkr::AttachmentModel.new("123")
      uploader = Belinkr::AttachmentUploader.for(model)
      uploader.model.wont_be_nil
      uploader.model.identity.must_equal "123"
    end
  end #.for_model

  describe "#model_validate!" do
    it "raise RuntimeError if model is not present" do
      uploader = Belinkr::AttachmentUploader.new
      uploader.model.must_be_nil
      lambda { uploader.send(:model_validate!) }.must_raise RuntimeError
    end

    it "raise RuntimeError if model has no spec behaviors" do
      model = OpenStruct.new(just: "one")
      uploader = Belinkr::AttachmentUploader.new(model)
      lambda { uploader.send(:model_validate!) }.must_raise RuntimeError
    end
  end #model_validate!

  describe "#storage_identity" do
    it "instantiated for identity" do
      uploader = Belinkr::AttachmentUploader.for_identity((0..9).to_a.join)
      uploader.storage_identity.must_equal "0123456789"
    end

    it "instantiated a new model" do
      Belinkr::AttachmentModel.any_instance
      .stubs(:random_seed).returns(0.12345)
      t = Time.now
      Timecop.freeze(t) do
        model = Belinkr::AttachmentModel.new
        uploader = Belinkr::AttachmentUploader.new(model)
        uploader.storage_identity
        .must_equal Digest::MD5.hexdigest("0.12345" + t.to_f.to_s)
      end
    end
  end #storage_identity

  describe "#store_dir" do
    it "base on a existed model identity" do
      uploader = Belinkr::AttachmentUploader.for_identity((0..9).to_a.join)
      uploader.store_dir.must_equal "/tmp/files/0123456789"
    end

    it "base on a model from identity" do
      uploader = Belinkr::AttachmentUploader.for(
          Belinkr::AttachmentModel.for_identity("1234")
      )
      uploader.store_dir.must_equal "/tmp/files/1234"
    end

    it "instantiated a new model" do
      Belinkr::AttachmentModel.any_instance
      .stubs(:random_seed).returns(0.12345)
      t = Time.now
      Timecop.freeze(t) do
        model = Belinkr::AttachmentModel.new
        uploader = Belinkr::AttachmentUploader.new(model)
        uploader.store_dir.must_equal "/tmp/files/#{Digest::MD5.hexdigest(
            "0.12345" + t.to_f.to_s
        )}"
      end
    end
  end #store_dir

  # same as storage_identity
  describe "#filename" do;end

  # dont stub anything, just real behaviors
  describe "#image?" do
    it "return false if upload a empty file" do
      file = File.open(
          File.expand_path("files/empty", File.dirname(__FILE__))
      )
      uploader = Belinkr::AttachmentUploader.get_instance
      uploader.store!(file)
      uploader.image?.must_equal false
    end

    it "return false if upload a txt file" do
      file = File.open(
          File.expand_path("files/test.txt", File.dirname(__FILE__))
      )
      uploader = Belinkr::AttachmentUploader.get_instance
      uploader.store!(file)
      uploader.image?.must_equal false
    end

    it "return true if upload a JPEG file" do
      file = File.open(
          File.expand_path("files/test.jpg", File.dirname(__FILE__))
      )
      uploader = Belinkr::AttachmentUploader.get_instance
      uploader.store!(file)
      uploader.image?.must_equal true
    end

  end #image?

  describe "image versions" do
    before do
      Belinkr::AttachmentModel.any_instance
      .stubs(:random_seed).returns(0.123456)
    end

    it "should test with all versions with the upload file" do
      file = File.open(
          File.expand_path("files/test.jpg", File.dirname(__FILE__))
      )
      t = Time.now
      Timecop.freeze(t) do
        uploader = Belinkr::AttachmentUploader.get_instance
        uploader.store!(file)

        md5 = Digest::MD5.hexdigest("0.123456" + t.to_f.to_s)
        original_file = md5

        uploader.store_path(original_file)
        .must_equal "#{@store_dir}/#{md5}/#{md5}"
        File.exist?(File.join(@store_dir, original_file, md5)).must_equal true
        versions = Belinkr::AttachmentUploader.versions.keys
        versions.each do |version|
          uploader.store_path("#{version}_#{md5}")
          .must_equal "#{@store_dir}/#{md5}/#{version}_#{md5}"

          File.exist?(File.join(@store_dir, md5, "#{version}_#{md5}"))
          .must_equal true
        end
      end
    end

  end

end # Belinkr::AttachementUploader