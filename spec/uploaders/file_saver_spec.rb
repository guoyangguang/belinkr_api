# encoding: utf-8
require 'minitest/autorun'
require 'tempfile'
require 'fileutils'
require 'mocha'
require 'timecop'

require 'mini_magick'
require 'mime/types'
require 'carrierwave'

require_relative "../../uploaders/file_saver"
#"libreoffice --invisible --convert-to pdf -outdir #{File.dirname(path)} #{path}"
#%w(msword vnd.ms-office vnd.ms-powerpoint vnd.ms-excel)

Belinkr::AttachmentUploader.class_eval do
  def store_dir
    "/tmp/files/#{model.id}"
  end
end

describe Belinkr::FileSaver do
  describe ".chew!" do
    describe "basic" do
      # The FileSaver is used for middleware Rack::AyeAye
      # so it must have chew! singleton method
      it "must respond to .chew! method" do
        Belinkr::FileSaver.respond_to?(:chew!).must_equal true
      end

      it "raise ArgumentError if the argument passed is not a Array" do
        lambda {
          Belinkr::FileSaver.chew!("string")
        }.must_raise ArgumentError
      end

      it "raise ArgumentError if the argument passed is a empty Array" do
        lambda {
          Belinkr::FileSaver.chew!([])
        }.must_raise ArgumentError
      end
    end #basic

    describe "store!" do
      before do
        Belinkr::AttachmentModel.any_instance
          .stubs(:random_seed).returns(0.123456)

        @store_dir  = "/tmp/files"

        @text_file  = File.expand_path("files/test.txt", File.dirname(__FILE__))
        @image_file = File.expand_path("files/test.jpg", File.dirname(__FILE__))
        @doc_file   = File.expand_path("files/test.doc", File.dirname(__FILE__))
        @pdf_file   = File.expand_path("files/test.pdf", File.dirname(__FILE__))

        @text_to_upload     = File.new(@text_file)
        @text_base_name     = File.basename(@text_file)
        @text_content_type  = "text/plain"
        @text_to_upload.stubs(:[]).with(:filename).returns(@text_base_name)
        @text_to_upload.stubs(:[]).with(:type).returns(@text_content_type)

        @image_to_upload    = File.new(@image_file)
        @image_base_name    = File.basename(@image_file)
        @image_content_type = "image/jpeg"
        @image_to_upload.stubs(:[]).with(:filename).returns(@image_base_name)
        @image_to_upload.stubs(:[]).with(:type).returns(@image_content_type)

        @doc_to_upload    = File.new(@doc_file)
        @doc_base_name    = File.basename(@doc_file)
        @doc_content_type = "application/vnd.ms-office"
        @doc_to_upload.stubs(:[]).with(:filename).returns(@doc_base_name)
        @doc_to_upload.stubs(:[]).with(:type).returns(@doc_content_type)

        @pdf_to_upload    = File.new(@pdf_file)
        @pdf_base_name    = File.basename(@pdf_file)
        @pdf_content_type = "application/pdf"
        @pdf_to_upload.stubs(:[]).with(:filename).returns(@pdf_base_name)
        @pdf_to_upload.stubs(:[]).with(:type).returns(@pdf_content_type)
      end

      describe "single file" do
        # NON-IMAGE file: use text file as example
        describe "text file" do
          describe "successful" do
            it "return the json" do
              t = Time.now
              Timecop.freeze(t) do
                result = Belinkr::FileSaver.chew!([@text_to_upload])
                identity = Digest::MD5.hexdigest("0.123456" + t.to_f.to_s)

                File.exist?(File.join(@store_dir, identity, identity))
                  .must_equal true

                file_hash = {
                    storage:            "local",
                    id:                 identity,
                    original_filename:  @text_base_name,
                    size:               File.read(@text_file).size,
                    content_type:       @text_content_type,
                    original:           File.join(@store_dir,identity,identity)
                }
                # in case(when test error)
                # below assertions not our expects
                JSON.parse(result).first.each do |k, v|
                  file_hash[k.to_sym].must_equal v
                end
              end
            end # return the json
          end # successful

          describe "fail" do
            it "error before file save" do
              Belinkr::AttachmentUploader.stubs(:get_instance)
                .raises(RuntimeError.new("error raise"))
              result = Belinkr::FileSaver.chew!([@text_to_upload])
              result.must_be_kind_of String
              result = JSON.parse(result)
              result.must_be_kind_of Hash
              result.has_key?("error").must_equal true
              result["error"].wont_be_nil
              #result["error"]
              #  .must_equal "#{@text_base_name} upload error: error raise"
            end

            it "error after file saved" do
              Belinkr::FileSaver.stubs(:do_nothing!)
                .raises(RuntimeError.new("error raise"))
              Belinkr::FileSaver.expects(:remove_dir).once
              t = Time.now
              Timecop.freeze(t) do
                result = Belinkr::FileSaver.chew!([@text_to_upload])
                result.must_be_kind_of String
                result = JSON.parse(result)
                result.must_be_kind_of Hash
                result.has_key?("error").must_equal true
                result["error"].wont_be_nil

                identity = Digest::MD5.hexdigest("0.123456" + t.to_f.to_s)

                Dir.exist?(File.join(@store_dir, identity)).must_equal true

                File.exist?(File.join(@store_dir, identity, identity))
                  .must_equal true
              end # Timecop
            end # error after file saved
          end # fail
        end # text file

        describe "image file" do
          describe "successful" do
            it "return the json" do
              t = Time.now
              Timecop.freeze(t) do
                result = Belinkr::FileSaver.chew!([@image_to_upload])
                identity = Digest::MD5.hexdigest("0.123456" + t.to_f.to_s)

                File.exist?(File.join(@store_dir, identity, identity))
                .must_equal true

                file_hash = {
                    storage:            "local",
                    id:                 identity,
                    original_filename:  @image_base_name,
                    size:               File.read(@image_file).size,
                    content_type:       @image_content_type,
                    original:           File.join(@store_dir,identity,identity)
                }
                parsed_result = JSON.parse(result)

                size = parsed_result.first.delete("size")
                (size > 0).must_equal true

                parsed_result.first.has_key?("versions").must_equal true
                parsed_result_versions = parsed_result.first.delete("versions")

                parsed_result.first.each do |k, v|
                  file_hash[k.to_sym].must_equal v
                end

                parsed_result_versions.must_be_kind_of Array
                version_names = Belinkr::AttachmentUploader.versions
                                  .keys.map(&:to_s)
                # versions specs
                parsed_result_versions.each do |v_hash|
                  version_names.include?(v_hash["name"]).must_equal true
                  version_file = File.join(
                      @store_dir, identity, "#{v_hash["name"]}_#{identity}"
                  )
                  v_hash["original"].must_equal version_file
                  (v_hash["size"] > 0).must_equal true
                end
              end # Timecop
            end # return the json
          end # successful

          describe "fail" do
            it "error before file save" do
              # SKIP same as text file spec
              (1 == 1).must_equal true
            end

            it "error after file saved" do
              Belinkr::FileSaver.stubs(:do_nothing!)
              .raises(RuntimeError.new("error raise"))
              Belinkr::FileSaver.expects(:remove_dir).once
              t = Time.now
              Timecop.freeze(t) do
                result = Belinkr::FileSaver.chew!([@image_to_upload])
                result.must_be_kind_of String
                result = JSON.parse(result)
                result.must_be_kind_of Hash
                result.has_key?("error").must_equal true
                result["error"].wont_be_nil

                identity = Digest::MD5.hexdigest("0.123456" + t.to_f.to_s)

                Dir.exist?(File.join(@store_dir, identity)).must_equal true

                File.exist?(File.join(@store_dir, identity, identity))
                  .must_equal true

                Belinkr::AttachmentUploader.versions.keys.each do |version|
                  File.exist?(
                    File.join(@store_dir, identity, "#{version}_#{identity}")
                  ).must_equal true
                end
              end # Timecop
            end # error after file saved
          end # fail
        end # image file
      end # single file

      describe "multiple files" do
        before do
          @multi_files = [@image_to_upload,
                          @text_to_upload,
                          @doc_to_upload,
                          @pdf_to_upload]
        end

        describe "successful" do
          it "return all json" do
            result = Belinkr::FileSaver.chew!(@multi_files)
            result = JSON.parse(result)

            result.must_be_kind_of Array
            result.size.must_equal 4

            # just check original not nil
            result.each do |file|
              file["original"].wont_be_nil

              if file["content_type"] == @image_content_type
                file["versions"].wont_be_nil
                file["versions"].must_be_kind_of Array
                file["versions"].size
                  .must_equal Belinkr::AttachmentUploader.versions.size
              end
            end
          end # return all json
        end # successful

        describe "fail" do
          before do
            Belinkr::FileSaver.stubs(:do_nothing!)
              .raises(RuntimeError.new("error raise"))
            Belinkr::FileSaver.expects(:remove_dir).once
          end

          it "got error message" do
            result = Belinkr::FileSaver.chew!(@multi_files)
            result.must_be_kind_of String
            result = JSON.parse(result)
            result.must_be_kind_of Hash
            result.has_key?("error").must_equal true
            result["error"].wont_be_nil
          end
        end # fail

      end # multiple files

    end # store!
  end # .chew!
end # Belinkr::FileSaver