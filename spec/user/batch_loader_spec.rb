require "minitest/autorun"
require "redis"
require "csv"
require_relative "../../init"
require_relative "../../user/batch_loader"
require_relative "../../user/collection"

$redis = Redis.new

module TestSuite
  extend self
  def csv_files
    { valid: "/tmp/entity_valid.csv", invalid: "/tmp/entity_invalid.csv" }
  end

  def fill_csv(type="valid", count = 10)
    case type
      when "valid"
        File.open(csv_files[:valid], "w") { |f| f.write valid_records(count) }
      else
        File.open(csv_files[:invalid], "w") do |f|
          f.write valid_records(count) + "\n" + invalid_record
        end
    end
  end

  def valid_records(rows = 2)
    rows.times.collect {
      Array.new(2) { random_string }.join(",")
    }.join("\n")
  end

  def invalid_record
    Array.new(2) { random_string(1) }.join(",")
  end

  def random_string(count=4)
    rand(16**count).to_s(16)
  end
end

include Belinkr

describe User::BatchLoader do

  describe ".load" do
    before do
      $redis.flushdb
      TestSuite.fill_csv('valid', 5)
      TestSuite.fill_csv('invalid', 1)
      @valid_csv   = TestSuite.csv_files[:valid]
      @invalid_csv = TestSuite.csv_files[:invalid]

      File.exist?(@valid_csv).must_equal true
      File.exist?(@invalid_csv).must_equal true
    end

    it "import csv file which contains invalid record" do
      lambda {
        Belinkr::User::BatchLoader.load(@invalid_csv, 1)
      }.must_raise Tinto::Exceptions::InvalidResource
    end

    it "import user according to csv file" do
      Belinkr::User::BatchLoader.load(@valid_csv, 1)
      users = Belinkr::User::Collection.new(entity_id: 1).all.map(&:first)
      new_users = []
      CSV.open(@valid_csv).each do |line|
        new_users << line[0]
      end
      new_users.each do |user_first|
        users.must_include user_first
      end
    end
  end  # .load
end # User::Orchestrator
