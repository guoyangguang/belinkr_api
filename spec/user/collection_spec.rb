# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../user/collection"

describe Belinkr::User::Collection do
  describe "validations" do
    describe "#entity_id" do
      it "must be present" do
        collection = Belinkr::User::Collection.new
        collection.valid?.must_equal false

        collection.errors[:entity_id].must_include "entity must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::User::Collection.new
        collection.valid?.must_equal false

        collection.errors[:entity_id].must_include "entity must be a number"
      end
    end
  end
end # Belinkr::User::Collection
