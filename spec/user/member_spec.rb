# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../user/member"

describe Belinkr::User::Member do
  describe "#validations" do
    describe "first" do
      it "has more than 3 characters" do
        user = Belinkr::User::Member.new(first: "ab")
        user.valid?.must_equal false
        user.errors[:first]
          .must_include "first name must be between 3 and 150 characters long"
      end

      it "has less than 150 characters" do
        user = Belinkr::User::Member.new(first: "a" * 151)
        user.valid?.must_equal false
        user.errors[:first]
          .must_include "first name must be between 3 and 150 characters long"
      end
    end

    describe "last" do
      it "has more than 3 characters" do
        user = Belinkr::User::Member.new(last: "ab")
        user.valid?.must_equal false
        user.errors[:last]
          .must_include "last name must be between 3 and 150 characters long"
      end

      it "has less than 150 characters" do
        user = Belinkr::User::Member.new(last: "a" * 151)
        user.valid?.must_equal false
        user.errors[:last]
          .must_include "last name must be between 3 and 150 characters long"
      end
    end

    describe "entity_id" do
      it "must be present" do
        user = Belinkr::User::Member.new
        user.valid?.must_equal false
        user.errors[:entity_id].must_include "entity must not be blank"
      end

      it "must be a number" do
        user = Belinkr::User::Member.new
        user.valid?.must_equal false
        user.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id

    describe "timezone" do
      it "must be one of the value of TZInfo::Timezone.all_identifiers" do
        user = Belinkr::User::Member.new
        user.timezone = 'wrong/timezone'
        user.valid?.must_equal false
        user.errors[:timezone]
            .must_include "timezone must be one of #{Tinto::Utils
            .timezones.join(', ')}"
      end
    end # timezone
  end # validations
end # User::Member
