require "minitest/autorun"
require "redis"
require_relative "../../init"
require_relative "../../user/orchestrator"
require_relative "../../user/collection"
require_relative "../../scrapbook/orchestrator"

$redis = Redis.new

include Belinkr

describe User::Orchestrator do
  before do
    $redis.flushdb

    @user = User::Orchestrator.create(
              User::Member.new first: "user", last: "111", entity_id: 1
            )
  end

  describe ".create" do
    it "adds the user to the User::Collection" do
      User::Collection.new(entity_id: @user.entity_id).must_include @user
    end

    it "creates a general timeline when creating a user" do
      @user.id.wont_be_nil

      Status::Collection.new(kind: "general", user_id: @user.id, entity_id: 1)
        .must_be_empty
    end

    it "creates a timeline from the user himself when creating user" do
      @user.id.wont_be_nil

      Status::Collection.new(kind: "own", user_id: @user.id, entity_id: 1)
        .must_be_empty
    end

    it "create two scrapbooks from the user himself when creating user" do
      scrapbook_names = Scrapbook::Collection.new(user_id: @user.id, kind: "own")
                          .all.map(&:name)
      scrapbook_names.must_include "favorites"
      scrapbook_names.must_include "drafts"
    end
  end #.create
end # User::Orchestrator
