# encoding: utf-8
require "minitest/autorun"
require_relative "../../../workers/file_mover/config"

describe Belinkr::Config::FileMover do

  describe '.configure' do

    it "the block return Config itself" do
      Belinkr::Config::FileMover.configure do |cfg|
        cfg.must_equal Belinkr::Config::FileMover
      end
    end
  end # .configure

  describe ".use_defaults!" do

    it "return defaults not nil" do
      Belinkr::Config::FileMover.use_defaults!
      [:api_url, :request_params, :files_root, :root].each do |msg|
        Belinkr::Config::FileMover.send(msg).wont_be_nil
      end
    end
  end # .use_defaults

  describe ".suite!" do
    describe "without suite" do
      it "if no suite it will return the default configs" do
        Belinkr::Config::FileMover.suite!
        Belinkr::Config::FileMover.root.must_equal File.expand_path('.', Dir.pwd)
      end
    end

    describe "with suite" do
      before do
        @file = File.join(File.dirname(__FILE__), ".file_mover")
        code =<<CODE
Belinkr::Config::FileMover.configure do |config|
  config.api_url = "http://fileserver"
  config.request_params = {
    post: :attaches,
    delete: :a_id,
    put: { id: :a_id, file: :a_file }
  }
  config.files_root = "/tmp/files_stored"
end
CODE
        File.open(@file, "w") { |f|
          f.write code
        }
      end

      after do
        File.delete @file if File.exist? @file
      end

      it "should change the defaults config" do
        Belinkr::Config::FileMover.suite! File.dirname(__FILE__)
        Belinkr::Config::FileMover.root.must_equal File.dirname(__FILE__)
        Belinkr::Config::FileMover.request_params.must_be_kind_of Hash
        Belinkr::Config::FileMover.request_params[:post].must_equal :attaches
        Belinkr::Config::FileMover.request_params[:delete].must_equal :a_id
        Belinkr::Config::FileMover.request_params[:put].must_be_kind_of Hash
        Belinkr::Config::FileMover.request_params[:put][:id].must_equal :a_id
        Belinkr::Config::FileMover
          .request_params[:put][:file].must_equal :a_file
        Belinkr::Config::FileMover.api_url.must_equal "http://fileserver"
        Belinkr::Config::FileMover.files_root.must_equal "/tmp/files_stored"
      end
    end # with suite
  end

end # Belinkr::Worker::Config::FilePorter
