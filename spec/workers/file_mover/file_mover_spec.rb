# encoding: utf-8
require "minitest/autorun"
require 'tempfile'
require "mocha"

ENV["WORKER_ENV"] = "test"

require_relative "../../../workers/file_mover/file_mover"

describe Belinkr::FileMover do

  describe 'em reactor running' do
    it "not running" do
      EM.reactor_running?.must_equal false
    end
  end # reactor

  describe ".operate" do

    before do
      Belinkr::FileMover.stubs(:create).returns("create")
      Belinkr::FileMover.stubs(:update).returns("update")
    end

    it "respond to request" do
      pkgs = [:create, :update].collect do |req|
        { action: req, others: "others" }.to_json
      end

      pkgs.collect { |pkg|
        Belinkr::FileMover.operate(pkg)
      }.must_equal ["create", "update"]
    end

  end # .operate

  describe "check options" do

    describe ".check_options_is_hash!" do

      it "raise ArgumentError if args is not a Hash" do
        lambda {
          Belinkr::FileMover.check_options_is_hash!("string")
        }.must_raise ArgumentError
      end

    end # .check_options_is_hash!

    describe ".check_options_resource!" do

      it "raise ArgumentError if no resource key" do
        lambda {
          Belinkr::FileMover.check_options_resource!({})
        }.must_raise ArgumentError
      end

      it "raise ArgumentError if resource type not present" do
        lambda { Belinkr::FileMover.check_options_resource!(
          {
            "resource" => { "id" => 1 }
          })
        }.must_raise ArgumentError
      end

      it "raise ArgumentError if resource id not present" do
        lambda { Belinkr::FileMover.check_options_resource!(
          {
            "resource" => { "type" => 1 }
          })
        }.must_raise ArgumentError
      end

    end # .check_options_resource!

    describe ".check_options_for_create!" do

      before do
        @default_options = { "resource" => {"id" => 1, "type" => "Status"} }
      end

      it "raise ArgumentError if args has no files key" do
        lambda {
          Belinkr::FileMover.check_options_for_create!(
            {}.merge!(@default_options)
          )
        }.must_raise ArgumentError
      end

      it "raise ArgumentError if args files is not a Array" do
        lambda {
          Belinkr::FileMover.check_options_for_create!(
            {"files" => 1}.merge!(@default_options)
          )
        }.must_raise ArgumentError

      end

      it "raise ArgumentError if args files is empty" do
        lambda {
          Belinkr::FileMover.check_options_for_create!(
              {"files" => []}.merge!(@default_options)
          )
        }.must_raise ArgumentError
      end

    end # .check_options_for_create!

    describe ".check_options_for_update!" do
      it "raise ArgumentError if args has no file key" do
        lambda {
          Belinkr::FileMover.check_options_for_update!({})
        }.must_raise ArgumentError
      end
    end # .check_options_for_update!

    describe ".check_options_for_delete!" do
      it "raise ArgumentError if args has no file key" do
        lambda {
          Belinkr::FileMover.check_options_for_delete!({})
        }.must_raise ArgumentError
      end

    end # .check_options_for_delete!

    describe ".check_options_for_destroy!" do

      it "raise ArgumentError if args has no files key" do
        lambda {
          Belinkr::FileMover.check_options_for_destroy!(
              {}
          )
        }.must_raise ArgumentError
      end

      it "raise ArgumentError if args files is not a Array" do
        lambda {
          Belinkr::FileMover.check_options_for_destroy!(
              {"files" => 1}
          )
        }.must_raise ArgumentError

      end

      it "raise ArgumentError if args files is empty" do
        lambda {
          Belinkr::FileMover.check_options_for_destroy!(
              {"files" => []}
          )
        }.must_raise ArgumentError
      end
    end # check_options_for_destroy!

  end # check options

  describe '.local_storage_files' do

    before do
      File.stubs(:exist?).returns(true)
      Belinkr::Config::FileMover.stubs(:files_root).returns("/path/to/files")
      @files = Array.new(3) { rand(16**32).to_s(16) }
      @file = rand(16**32).to_s(16)
    end

    it "raise error" do
      File.stubs(:exist?).returns(false)
      lambda {
        Belinkr::FileMover.local_storage_files(@file)
      }.must_raise RuntimeError
    end

    it "return single local storage file full path" do
      result = Belinkr::FileMover.local_storage_files(@file)
      result.must_be_kind_of Array
      result.first.must_equal "/path/to/files/#{@file}/#{@file}"
    end

    it "return local storage files full path" do
      result = Belinkr::FileMover.local_storage_files(@files)
      result.must_be_kind_of Array
      @files.size.must_equal result.size
      @files.zip(result).each do |(from, to)|
        to.must_equal "/path/to/files/#{from}/#{from}"
      end
    end

  end # .local_storage_files

  describe '.request_json' do

    it "update the params headers with json" do
      Typhoeus::Request.expects(:run).with("http://localhost",{
                :headers => {
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json'
                }
            }
      ).at_least_once
      Belinkr::FileMover.request_json("http://localhost", {})
    end

  end # .request_json

  describe '.remove_local_storage_folders' do

    it "send a notifier message" do
      Belinkr::FileMover.stubs(:message_notify).returns("notify_sent")
      Belinkr::Config::FileMover.files_root = "/tmp"
      folders = Array.new(3) { "#{rand(100)}" }
      folders.each { |folder|
        Dir.stubs(:exist?).with("/tmp/" + folder).returns(true)
      }
      Belinkr::FileMover.remove_local_storage_folders(folders)
        .must_equal "notify_sent"
    end

    it "remove all the specify folders" do
      Belinkr::Config::FileMover.files_root = "/tmp"
      FileUtils.stubs(:rm_r).returns(true)
      folders = Array.new(3) { "#{rand(100)}" }
      folders.each { |folder|
        Dir.stubs(:exist?).with("/tmp/" + folder).returns(true)
      }
      Belinkr::FileMover.remove_local_storage_folders(folders).must_equal true
    end

  end # .remove_local_storage_folders

  # just the behavior
  describe ".dequeue" do
    it "dequeue all items" do
      Belinkr::FileMover.expects(:dequeue).at_most(3)
      Array.new(3) { Belinkr::FileMover.dequeue }
    end
  end # .dequeue

  describe ".next" do
    describe "without em running" do
      before do
        EM.stubs(:next_tick).returns("useless")
      end

      it "dequeue nothing" do
        Belinkr::FileMover.stubs(:dequeue).returns(nil)
        Belinkr::FileMover.expects(:operate).never
        Belinkr::FileMover.next.must_equal("useless")
      end

      it "dequeue something" do
        Belinkr::FileMover.stubs(:dequeue).returns("msg")
        Belinkr::FileMover.expects(:operate).with("msg").at_most(1)
        result = Belinkr::FileMover.next
        result.must_equal("useless")
      end
    end

    describe "full flow" do
      it "should test" do
        Belinkr::FileMover.stubs(:operate).with("one").once
        Belinkr::FileMover.stubs(:operate).with("two").once
        Belinkr::FileMover.stubs(:operate).with("three").once
        Belinkr::FileMover.stubs(:operate).with(nil).never
        Belinkr::FileMover.class_eval do
          @queue = %w(one two three)

          def self.dequeue
            info = @queue.shift
            info.nil? ? EM.stop : info
          end
        end

        EM.reactor_running?.must_equal false

        EM.run {
          Belinkr::FileMover.next
          EM.reactor_running?.must_equal true
        }
        EM.reactor_running?.must_equal false

        Belinkr::FileMover.instance_variable_get(:@queue).must_be_empty
      end
    end

  end # .next

  describe ".create" do
    before do
      Belinkr::Config::FileMover.api_url = "http://localhost:3000"
      Belinkr::Config::FileMover.request_params[:post] = :files

      @valid_options = JSON.parse({
          resource: { id: 1, type: 'Status' },
          files: Array.new(3) { rand(16**32).to_s(16) }
      }.to_json)

      File.stubs(:exist?).returns(true)
      Belinkr::Config::FileMover.stubs(:files_root).returns("/path/to/files")

      @valid_options["files"].each { |f|
        file_path = File.join(Belinkr::Config::FileMover.files_root, f, f)
        File.stubs(:new)
        .with(
            file_path
        ).returns(Tempfile.new(f))
      }
    end

    describe "request was successful" do

      describe "response no errors" do
        before do
          @body = [
            { id: rand(16**32).to_s(16), original: "path/to/file1" },
            { id: rand(16**32).to_s(16), original: "path/to/file2" }
          ]
          @success_response = Typhoeus::Response.new(
              body: @body.to_json,
              :mock => true
          )
          Typhoeus::Request.any_instance.stubs(:response)
            .returns(@success_response)
        end

        describe "update resource" do

          it "success and call remove local storage folders" do
            Belinkr::FileMover.stubs(:update_resource).returns(true)
            Belinkr::FileMover.stubs(:remove_local_storage_folders)
              .raises(RuntimeError.new("remove folder invoked"))
            Belinkr::FileMover.expects(:message_notify).with(
                "remove folder invoked").once
            Belinkr::FileMover.create(@valid_options)
          end

          it "fail" do
            Belinkr::FileMover.stubs(:update_resource)
              .raises(RuntimeError.new("update resource fail"))
            Belinkr::FileMover.expects(:message_notify).with(
                "update resource fail").once
            Belinkr::FileMover.create(@valid_options)
          end

        end
      end # response no errors

      describe "response with errors" do
        before do
          @body = {
              error: 'remote file server error'
          }
          @fail_response = Typhoeus::Response.new(
              body: @body.to_json,
              :mock => true
          )
          Typhoeus::Request.any_instance.stubs(:response)
          .returns(@fail_response)
        end

        it "notify the error messages" do
          Belinkr::FileMover.expects(:message_notify).with(
              "remote file server error").once
          Belinkr::FileMover.create(@valid_options)
        end

      end # response with errors

    end # request was successful

    describe "create fails with a exception" do

      it "catch and notify the exception message" do
        Belinkr::FileMover.stubs(:request_json)
          .raises(RuntimeError.new("raise raise raise"))
        Belinkr::FileMover.expects(:message_notify).with(
            "raise raise raise").once
        Belinkr::FileMover.create(@valid_options)
      end

    end # request fails

  end # .create

  describe ".update" do
    before do
      Belinkr::Config::FileMover.api_url = "http://localhost:3000"
      Belinkr::Config::FileMover.request_params[:put] = {
          id: :id,
          file: :file
      }

      @valid_options = JSON.parse({
          resource: { id: 1, type: 'Status' },
          file: rand(16**32).to_s(16)
      }.to_json)

      File.stubs(:exist?).returns(true)
      Belinkr::Config::FileMover.stubs(:files_root).returns("/path/to/files")

      file = @valid_options["file"]
      file_path = File.join(Belinkr::Config::FileMover.files_root, file, file)
      File.stubs(:new)
      .with(
          file_path
      ).returns(Tempfile.new(file))
    end

    describe "request was successful" do
      describe "response no errors" do
        describe "response result is not a Hash" do
          before do
            @body = ["some text"]
            @success_response = Typhoeus::Response.new(
                body: @body.to_json,
                :mock => true
            )
            Typhoeus::Request.any_instance.stubs(:response)
            .returns(@success_response)
          end

          it "raise a error" do
            Belinkr::FileMover.expects(:message_notify).with(
                "PUT request returns an error specifications").once
            Belinkr::FileMover.update(@valid_options)
          end
        end

        describe "update resource" do

          before do
            @body = { id: rand(16**32).to_s(16), original: "path/to/file1" }
            @success_response = Typhoeus::Response.new(
                body: @body.to_json,
                :mock => true
            )
            Typhoeus::Request.any_instance.stubs(:response)
            .returns(@success_response)
          end

          it "success and call remove local storage file" do
            Belinkr::FileMover.stubs(:update_resource).returns(true)
            Belinkr::FileMover.stubs(:remove_local_storage_folders)
            .raises(RuntimeError.new("remove folder invoked"))
            Belinkr::FileMover.expects(:message_notify).with(
                "remove folder invoked").once
            Belinkr::FileMover.update(@valid_options)
          end

          it "fails" do
            Belinkr::FileMover.stubs(:update_resource)
            .raises(RuntimeError.new("update resource fail"))
            Belinkr::FileMover.expects(:message_notify).with(
                "update resource fail").once
            Belinkr::FileMover.update(@valid_options)
          end
        end

      end # no errors

      describe "response with errors" do
        before do
          @body = {
              error: 'remote file server error'
          }
          @fail_response = Typhoeus::Response.new(
              body: @body.to_json,
              :mock => true
          )
          Typhoeus::Request.any_instance.stubs(:response)
          .returns(@fail_response)
        end

        it "notify the error messages" do
          Belinkr::FileMover.expects(:message_notify).with(
              "remote file server error").once
          Belinkr::FileMover.update(@valid_options)
        end

      end # response with errors

    end # successful

    describe "update fails with a exception" do

      it "catch and notify the exception message" do
        Belinkr::FileMover.stubs(:request_json)
        .raises(RuntimeError.new("raise raise raise"))
        Belinkr::FileMover.expects(:message_notify).with(
            "raise raise raise").once
        Belinkr::FileMover.update(@valid_options)
      end

    end # request fails

  end # .update

  describe ".delete" do

    before do
      Belinkr::Config::FileMover.stubs(:files_root).returns("/path/to/files")
      Belinkr::Config::FileMover.stubs(:api_url)
        .returns("http://localhost:3000")
      Belinkr::Config::FileMover.stubs(:request_params).returns(
          delete: :id
      )
      @md5 = "md5flags" * 4
      @valid_options = { 'file' => @md5 }
    end

    describe 'remove local storage file' do
      it "got the expects call" do
        Belinkr::FileMover.expects(:remove_local_storage_folders).once
        Belinkr::FileMover.stubs(:request_json)
          .raises(ArgumentError.new("to message notifier"))
        Belinkr::FileMover.expects(:message_notify)
          .with("to message notifier").once
        Belinkr::FileMover.delete(@valid_options)
      end
    end # remove local storage file

    describe "remove remote storage file" do
      before do
        @success_response = Typhoeus::Response.new(code: 204)
        @body = {
            error: 'remote file server error'
        }
        @fail_response = Typhoeus::Response.new(
            code: 200,
            body: @body.to_json,
            mock: true
        )
      end

      it "successful" do
        Typhoeus::Request.any_instance.stubs(:response)
          .returns(@success_response)
        JSON.expects(:parse).never
        Belinkr::FileMover.delete(@valid_options)
      end

      it "fail" do
        Typhoeus::Request.any_instance.stubs(:response).returns(@fail_response)
        JSON.stubs(:parse).returns({"error" => "remote file server error"})
        Belinkr::FileMover.expects(:message_notify)
          .with("remote file server error").once
        Belinkr::FileMover.delete(@valid_options)
      end
    end # remove remote storage file

  end # .delete

  describe ".destroy" do
    before do
      @options = {"files" => %w(one two three)}
    end

    it "raise a error" do
      Belinkr::FileMover.stubs(:check_options_for_destroy!)
        .raises(RuntimeError.new("destroy raise a error"))
      Belinkr::FileMover.expects(:message_notify)
        .with("destroy raise a error").once
      Belinkr::FileMover.destroy(@options)
    end

    it "delete all the files" do
      Belinkr::FileMover.stubs(:check_options_for_destroy!).returns(true)
      @options["files"].each { |file|
        Belinkr::FileMover.expects(:delete).with(file).once
      }
      Belinkr::FileMover.destroy(@options)
    end
  end # .destroy

end # Belinkr::FileMover
