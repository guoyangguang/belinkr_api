# encoding: utf-8
require "minitest/autorun"
require_relative "../../../activity/member" 
require_relative "../../../user/member" 
require_relative "../../../workers/logger/logger" 

$redis = Redis.new

describe Logger do
  it "could persist an activity in json format into cassandra database"  do
    user = Belinkr::User::Member.new(first: "User", last: "111")
    activity        = Belinkr::Activity::Member.new(
      actor:  user,
      action: "invite",
      object: user
    )
    activity.actor.kind           .must_equal "user"
    activity.actor.resource.first .must_equal "User"
    activity.save
    activity  = Belinkr::Activity::Member.new(id: activity.id)
    row_key   = Belinkr::Worker::Logger.persist(activity.to_json)

    persisted = Belinkr::Worker::Logger.client.get(
      Belinkr::Worker::Logger.key, row_key
    )
    persisted["action"].must_equal activity.action
    persisted["created_at"].must_equal activity.created_at.to_s
    persisted["updated_at"].must_equal activity.updated_at.to_s
  end
end

