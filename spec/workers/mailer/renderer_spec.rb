# encoding: utf-8
require "minitest/autorun"
require_relative "../../../config"
require_relative "../../../workers/mailer/renderer"

include Belinkr::Worker

describe "invitation templates" do
  before do
    @templates      = Belinkr::Config::MAILER_TEMPLATES.fetch(:invitation)
    @substitutions  = {
      invited_name:     "Lorenzo",
      inviter_name:     "Jorge",
      invitation_link:  "https://www.belinkr.com/join",
      entity_name:      "belinkr"
    }
  end

  describe "existence" do
    it "there is an English template" do
      Mailer::Renderer.new(@templates[:en], @substitutions).render
        .must_match /he belinkr team/
    end

    it "there is a Spanish template" do
      Mailer::Renderer.new(@templates[:es], @substitutions).render
        .must_match /l equipo de belinkr/
    end
  end # existence

  describe "rendering" do
    it "renders to English" do
      body = Mailer::Renderer.new(@templates[:en], @substitutions).render

      body.must_match %r{#{@substitutions[:invited_name]}}
      body.must_match %r{#{@substitutions[:inviter_name]}}
      body.must_match %r{#{@substitutions[:invitation_link]}}
      body.must_match %r{#{@substitutions[:entity_name]}}
      body.must_match /he belinkr team/
    end

    it "renders to Spanish" do
      body = Mailer::Renderer.new(@templates[:es], @substitutions).render

      body.must_match %r{#{@substitutions[:invited_name]}}
      body.must_match %r{#{@substitutions[:inviter_name]}}
      body.must_match %r{#{@substitutions[:invitation_link]}}
      body.must_match %r{#{@substitutions[:entity_name]}}
      body.must_match /l equipo de belinkr/
    end
  end # rendering
end # invitation templates

describe Mailer::Renderer do
  before do
    @template       = "join the party!: {{invitation_link}}"
    @substitutions  = { invitation_link: "https://www.belinkr.com/join" }
  end

  describe "#check_substitutions" do
    it "raises if the variable isn't used in the template" do
      substitutions = 
        @substitutions.merge(party_link: "https://party.belinkr.com")

      lambda { Mailer::Renderer.new(@template, substitutions).render }
        .must_raise ArgumentError, "party_link is not used in the template"
    end

    it "raises if some needed variable isn't included in the substitutions" do
      lambda { Mailer::Renderer.new(@template, {}).render }
        .must_raise ArgumentError, 
                    "a substitution for invitation_link is required"
    end
  end

  describe "render" do
    it "subtitutes variables marked as {{variable}} in template" do
      body = Mailer::Renderer.new(@template, @substitutions).render
      body.must_match %r{#{@substitutions[:invitation_link]}}
    end

    it "autolinks https:// and http:// references" do
      body = Mailer::Renderer.new(@template, @substitutions).render
      body.must_match %r{href="https://www.belinkr.com/join"}

      @substitutions[:invitation_link] = "http://www.belinkr.com/join"

      body = Mailer::Renderer.new(@template, @substitutions).render
      body.must_match %r{href="http://www.belinkr.com/join"}
    end
  end # render
end # Mailer::Renderer
