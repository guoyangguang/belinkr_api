# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../workspace/orchestrator"
require_relative "../../../workspace/administrator/collection"
require_relative "../../../workspace/collaborator/collection"
require_relative "../../../user/member"
require_relative "../../../user/orchestrator"

$redis ||= Redis.new

describe Belinkr::Workspace::Administrator::Collection do
  describe "validations" do
    describe "#workspace_id" do
      it "must be present" do
        collection = Belinkr::Workspace::Administrator::Collection.new
        collection.valid?.must_equal false

        collection.errors[:workspace_id]
          .must_include "workspace must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Workspace::Administrator::Collection.new(
          workspace_id: "ad"
        )
        collection.valid?.must_equal false

        collection.errors[:workspace_id]
          .must_include "workspace must be a number"
      end
    end

    describe "#entity_id" do
      it "must be present" do
        collection = Belinkr::Workspace::Administrator::Collection.new
        collection.valid?.must_equal false

        collection.errors[:entity_id]
          .must_include "entity must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Workspace::Administrator::Collection.new(
          entity_id: "ad"
        )
        collection.valid?.must_equal false

        collection.errors[:entity_id]
          .must_include "entity must be a number"
      end
    end
  end # validations

  describe "#add" do
    it "must be in collaborators before adding to administators" do
      user            = Belinkr::User::Orchestrator.create(
                          Belinkr::User::Member
                            .new(first: "User", last: "111", entity_id: 1)
                        )
      workspace       = Belinkr::Workspace::Orchestrator.create(
                          Belinkr::Workspace::Member.new(name: "workspace 1"),
                          user
                        )

      collaborators   = Belinkr::Workspace::Collaborator::Collection
                          .new(workspace_id: workspace.id, entity_id: 1)

      administrators  = Belinkr::Workspace::Administrator::Collection
                          .new(workspace_id: workspace.id, entity_id: 1)

      lambda { 
        administrators.add user 
      }.must_raise Tinto::Exceptions::InvalidResource
    end
  end # add
  
  describe "#each" do
    it "yields User::Member instances" do
      user1         = Belinkr::User::Orchestrator.create(
                        Belinkr::User::Member
                          .new(first: "User", last: "111", entity_id: 1)
                      )
      user2         = Belinkr::User::Orchestrator.create(
                        Belinkr::User::Member
                          .new(first: "User", last: "112", entity_id: 1)
                      )
      workspace     = Belinkr::Workspace::Orchestrator.create(
                        Belinkr::Workspace::Member.new(name: "workspace 1"),
                        user1
                      )
      collaborators = Belinkr::Workspace::Collaborator::Collection.new(
                        workspace_id: workspace.id, entity_id: 1
                      )
      collaborators.add user1
      collaborators.add user2

      administrators = Belinkr::Workspace::Administrator::Collection.new(
                        workspace_id: workspace.id, entity_id: 1
                      )
      administrators.add user1
      administrators.add user2

      administrators.all.each do |administrator| 
        administrator.must_be_instance_of Belinkr::User::Member
      end
    end
  end # each
end # Workspace::Administrator::Collection
