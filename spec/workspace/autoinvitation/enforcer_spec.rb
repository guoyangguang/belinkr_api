#encoding: utf-8

require "minitest/autorun"
require 'redis'
require_relative '../../../workspace/autoinvitation/enforcer'
require_relative '../../../user/orchestrator'
require_relative '../../../workspace/orchestrator'
require_relative '../../../workspace/autoinvitation/member'

include Belinkr
$redis = Redis.new

describe Belinkr::Workspace::Autoinvitation::Enforcer do
  before do
    $redis.flushdb

    @user = User::Orchestrator.create(
      User::Member.new(entity_id: 1, first: 'User', last: '111'))

    @collaborator = User::Orchestrator.create(
      User::Member.new(entity_id: 1, first: 'User', last: '112')) 

    @admin = User::Orchestrator.create(
      User::Member.new(entity_id: 1, first: 'User', last: '113'))

    @workspace = Workspace::Orchestrator.create(
      Workspace::Member.new(entity_id: @admin.entity_id, name: 'Workspace 1'), 
        @admin)
    
    Workspace::Collaborator::Collection.new(entity_id: 1, 
      workspace_id: @workspace.id).add(@collaborator)

    @autoinvitation = Workspace::Autoinvitation::Member
      .new(entity_id: @workspace.entity_id, workspace_id: @workspace.id)
  end

  describe "#request_by?" do
    it "raises NotAllowed exception if the user is one of collaborators or admins" do
      lambda { Workspace::Autoinvitation::Enforcer
        .authorize(@collaborator, :request, @workspace, @autoinvitation) }
          .must_raise Tinto::Exceptions::NotAllowed

      lambda { Workspace::Autoinvitation::Enforcer
        .authorize(@admin, :request, @workspace, @autoinvitation) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#allow_by?" do
    it "raises NotAllowed exception if the user is not one of admins" do
      lambda { Workspace::Autoinvitation::Enforcer
        .authorize(@user, :allow, @workspace, @autoinvitation) }
          .must_raise Tinto::Exceptions::NotAllowed
      
      lambda { Workspace::Autoinvitation::Enforcer
        .authorize(@collaborator, :allow, @workspace, @autoinvitation) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#deny_by?" do
    it "raises NotAllowed exception if the user is not one of admins" do
      lambda { Workspace::Autoinvitation::Enforcer
        .authorize(@user, :deny, @workspace, @autoinvitation) }
          .must_raise Tinto::Exceptions::NotAllowed
      
      lambda { Workspace::Autoinvitation::Enforcer
        .authorize(@collaborator, :deny, @workspace, @autoinvitation) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end
end#Belinkr::Workspace::Autoinvitation::Enforcer

