# encoding: utf-8
require 'minitest/autorun'
require 'redis'
require_relative "../../../init"
require_relative '../../../workspace/autoinvitation/member'

include Belinkr
$redis = Redis.new

describe Belinkr::Workspace::Autoinvitation::Member do
  before do
    @autoinvitation = Workspace::Autoinvitation::Member
      .new(entity_id: 1, workspace_id: 1, requesting_id: 1)
  end

  describe "attributes" do
    it "has attributes #id, #entity_id, #workspace_id, #requesting_id, #state,
      #denied_at, #created_at, #updated_at, #deleted_at" do
       @autoinvitation.must_respond_to :id 
       @autoinvitation.must_respond_to :entity_id
       @autoinvitation.must_respond_to :workspace_id
       @autoinvitation.must_respond_to :requesting_id
       @autoinvitation.must_respond_to :state
       @autoinvitation.must_respond_to :denied_at
       @autoinvitation.must_respond_to :created_at
       @autoinvitation.must_respond_to :updated_at
       @autoinvitation.must_respond_to :deleted_at
    end
  end #attributes

  describe "validations" do
    describe "#entity_id" do
      it "must be present" do
        @autoinvitation.entity_id = nil
        @autoinvitation.valid?.must_equal false 
        @autoinvitation.errors[:entity_id]
          .must_include "entity must not be blank"
      end
      it "must be a number" do
        @autoinvitation.entity_id = 'num'
        @autoinvitation.valid?.must_equal false
        @autoinvitation.errors[:entity_id]
          .must_include "entity must be a number"
      end
    end

    describe "#workspace_id" do
      it "must be present" do
        @autoinvitation.workspace_id = nil
        @autoinvitation.valid?.must_equal false 
        @autoinvitation.errors[:workspace_id]
          .must_include "workspace must not be blank"
      end
      it "must be a number" do
        @autoinvitation.workspace_id = 'num'
        @autoinvitation.valid?.must_equal false
        @autoinvitation.errors[:workspace_id]
          .must_include "workspace must be a number"
      end
    end

    describe "#requesting_id" do
      it "must be present" do
        @autoinvitation.requesting_id = nil
        @autoinvitation.valid?.must_equal false 
        @autoinvitation.errors[:requesting_id]
          .must_include "requesting must not be blank"
      end
      it "must be a number" do
        @autoinvitation.requesting_id = 'num'
        @autoinvitation.valid?.must_equal false
        @autoinvitation.errors[:requesting_id]
          .must_include "requesting must be a number"
      end
    end
  end#validations

  describe "state" do
    it "returns the current state, the default state" do
      state_requested = Belinkr::Workspace::Autoinvitation::Member::STATES[0]
      @autoinvitation.state.must_equal state_requested
    end
  end

  describe "state=" do
    it "assigns the current state after it has been allowed" do
      state_allowed = Belinkr::Workspace::Autoinvitation::Member::STATES[1]
      @autoinvitation.state = state_allowed
      @autoinvitation.state.must_equal state_allowed
    end
  end
  describe 'methods' do
    it "must respond to #read, #save, #to_json" do
     @autoinvitation.must_respond_to :save
     @autoinvitation.must_respond_to :read
     @autoinvitation.must_respond_to :to_json
    end
    
    describe "#allow, #allowed?" do
      it "changes the state to 'allowed', and querys if it has 'allowed' state" do
        state_allowed = Belinkr::Workspace::Autoinvitation::Member::STATES[1]
        @autoinvitation.allow
        @autoinvitation.state.must_equal state_allowed
        @autoinvitation.allowed?.must_equal true
        @autoinvitation.must_equal @autoinvitation
      end
    end

    describe "#deny, #denied?" do
      it "changes the state to 'denied', and querys if it has 'denied' state" do
        autoinvitation = @autoinvitation.deny
        autoinvitation.state.must_equal 'denied'
        autoinvitation.denied?.must_equal true 
        autoinvitation.must_equal @autoinvitation
      end
    end
  end #methods
end #Belinkr::Workspace::Autoinvitation::Member
