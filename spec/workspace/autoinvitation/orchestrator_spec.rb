#encoding: utf-8
require "minitest/autorun"
require "redis"
require_relative "../../../user/orchestrator"
require_relative "../../../workspace/orchestrator"
require_relative "../../../workspace/autoinvitation/orchestrator"
require_relative "../../../activity/collection"

$redis = Redis.new

describe Belinkr::Workspace::Autoinvitation::Orchestrator do
  before do
    $redis.flushdb
    @user               = Belinkr::User::Orchestrator.create(
                            Belinkr::User::Member.new(
                              first: "User", last: "111", entity_id: 1)
                          )
    @admin              = Belinkr::User::Orchestrator.create(
                            Belinkr::User::Member.new(
                              first: "User", last: "112", entity_id: 1)
                          )
    @workspace          = Belinkr::Workspace::Orchestrator.create(
                            Belinkr::Workspace::Member.new(
                              entity_id: @admin.entity_id,
                              name: "Workspace1"), @admin
                          )
    autoinvitation      = Belinkr::Workspace::Autoinvitation::Member.new(
                            entity_id: @workspace.entity_id,
                            workspace_id: @workspace.id
                          )
     @autoinvitation    = Belinkr::Workspace::Autoinvitation::Orchestrator
       .request(@workspace, autoinvitation, @user)
  end

  describe ".request" do
    it "creates a autoinvitation with 'requested' state" do
      @autoinvitation.state.must_equal "requested"
      @autoinvitation.id.wont_be_nil
      Belinkr::Workspace::Autoinvitation::Collection
        .new(entity_id: 1, workspace_id: @workspace.id)
        .must_include @autoinvitation

      activity = Belinkr::Activity::Collection.new.all.to_a.first

      activity.actor.resource.id        .must_equal @user.id
      activity.action                   .must_equal "add"
      activity.object.resource.id       .must_equal @autoinvitation.id
    end
  end

  describe ".allow" do
    it "updates the state to 'allowed' and adds the user as a collaborator" do
      Belinkr::Workspace::Autoinvitation::Orchestrator
        .allow(@workspace, @autoinvitation, @admin)
      allowed_autoinvitation = Belinkr::Workspace::Autoinvitation::Member.new(
        entity_id:    @workspace.entity_id,
        workspace_id: @workspace.id,
        id:           @autoinvitation.id
      )
      allowed_autoinvitation.allowed?.must_equal true
      Belinkr::Workspace::Collaborator::Collection
        .new(entity_id: @workspace.entity_id, workspace_id: @workspace.id)
        .must_include @user

      activity  = Belinkr::Activity::Collection.new.all.to_a.first
      activity1 = Belinkr::Activity::Collection.new.all.to_a[1]

      activity.actor.resource.id        .must_equal @user.id
      activity.action                   .must_equal "join"
      activity.object.resource.id       .must_equal @workspace.id
      activity1.actor.resource.id       .must_equal @admin.id
      activity1.action                  .must_equal "allow"
      activity1.object.resource.id      .must_equal @autoinvitation.id
    end
  end # .allow

  describe ".deny" do
    it "update the autoinvitation's state to 'deny' " do
      deny_autoinvitation = Belinkr::Workspace::Autoinvitation::Orchestrator
                           .deny(@workspace, @autoinvitation, @admin)

      deny_autoinvitation.denied?.must_equal true
      Belinkr::Workspace::Collaborator::Collection
        .new(entity_id: @workspace.entity_id, workspace_id: @workspace.id)
        .wont_include @user

      activity = Belinkr::Activity::Collection.new.all.to_a.first

      activity.actor.resource.id        .must_equal @admin.id
      activity.action                   .must_equal "deny"
      activity.object.resource.id       .must_equal @autoinvitation.id
    end
  end # .deny
end # Belinkr::Workspace::Invitation::Orchestrator
