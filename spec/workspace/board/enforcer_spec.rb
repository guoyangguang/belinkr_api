#encoding: utf-8

require 'minitest/autorun'
require 'redis'
require_relative "../../../user/orchestrator"
require_relative "../../../workspace/orchestrator"
require_relative "../../../workspace/board/orchestrator"

include Belinkr
$redis = Redis.new
describe Workspace::Board::Enforcer do

  before do
    $redis.flushdb

    #the entity has one workspace, two users who are admin or collaborator 
    #of the workspace, and one board instance in the workspace
    @collaborator = User::Orchestrator.create(
      User::Member.new(entity_id: 1, first: 'user', last: '111'))

    @admin = User::Orchestrator.create(
      User::Member.new(entity_id: 1, first: 'user', last: '112'))
    
    @workspace = Workspace::Orchestrator.create(
      Workspace::Member.new(entity_id: 1, name: 'workspace 1'), @admin)
    
    Workspace::Collaborator::Collection.new(entity_id: 1, workspace_id: @workspace.id)
      .add(@collaborator).add(@admin)
    
    Workspace::Administrator::Collection.new(entity_id: 1, workspace_id: @workspace.id)
      .add(@admin)
    
    Workspace::Collaborator::Collection.new(entity_id: 1, workspace_id: @workspace.id)
      .remove(@admin)

    board = Workspace::Board::Member.new(name: 'board 1')
    @created_board = Workspace::Board::Orchestrator.create(@workspace, board, @admin)
    @board =  Workspace::Board::Member.new(name: 'board 2') 
  end

  describe "#create_by?" do
    it "must raise NotAllowed exception if the user is not one of the board's
      workspace's admins " do

      lambda { Workspace::Board::Enforcer.authorize(@collaborator,
        :create, @workspace, @board) }.must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#update_by?" do
    it "must raise NotAllowed exception if the user is not one of the board's
      workspace's admins" do
      lambda { Workspace::Board::Enforcer.authorize(@collaborator, 
        :update, @workspace, @board) }.must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#delete_by?" do
    it "must raise NotAllowed exception if the user is not one of the board's
      workspace's admins" do
      lambda { Workspace::Board::Enforcer.authorize(@collaborator,
        :delete, @workspace, @created_board) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#destroy_by?" do
    it "must raise NotAllowed exception if the user is not one of the board's
      workspace's admins" do
      lambda { Workspace::Board::Enforcer.authorize(@collaborator,
        :destroy, @workspace, @created_board) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#undelete_by?" do
    it "must raise NotAllowed exception if the user is not one of the board's
      workspace's admins" do
      deleted_board = @created_board.delete
      lambda { Workspace::Board::Enforcer.authorize(@collaborator,
        :undelete, @workspace, deleted_board) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end

  describe "#read_by?" do
    it "must raise NotAllowed exception if the user is not one of the board's
      workspace's admins or collaborators" do
      user = User::Orchestrator.create(
        User::Member.new(entity_id: 1, first: 'user', last: '113'))

      lambda { Workspace::Board::Enforcer.authorize(user, 
        :read, @workspace, @created_board) }
          .must_raise Tinto::Exceptions::NotAllowed
      end
  end

  describe "#collection_by?" do
    it "must raise NotAllowed exception if the user is not one of the 
      workspace's admins or collaborators" do
      user = User::Orchestrator.create(
        User::Member.new(entity_id: 1, first: 'user', last: '113'))
      boards = Workspace::Board::Collection
        .new(entity_id: 1, workspace_id: @workspace.id).page
     
      lambda { Workspace::Board::Enforcer.authorize(user, 
        :collection, @workspace, boards) }
          .must_raise Tinto::Exceptions::NotAllowed
    end
  end
end# Workspace::Board::Enforcer
