# encoding: utf-8
require 'minitest/autorun'
require_relative '../../../init'
require_relative '../../../workspace/board/member'

describe Belinkr::Workspace::Board::Member do
  describe "validations" do
    describe "name" do
      it "has more than 3 characters" do
        board = Belinkr::Workspace::Board::Member
                  .new(name: "ab", workspace_id: 1,
                       entity_id: 1, user_id: 1
                      )
        board.valid?.must_equal false
        board.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end

      it "has less than 250 characters" do
        board = Belinkr::Workspace::Board::Member
                  .new(name: 'a' * 251, workspace_id: 1,
                      entity_id: 1, user_id: 1
                      )
        board.valid?.must_equal false
        board.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end
    end # name

    describe "user_id" do
      it "must be present" do
        board = Belinkr::Workspace::Board::Member.new(
                  name: 'board 1', workspace_id: 1, entity_id: 1
                )
        board.valid?.must_equal false
        board.errors[:user_id].must_include "user must not be blank"
      end

      it "is an integer" do
        board = Belinkr::Workspace::Board::Member.new(
                  name: 'board 1', workspace_id: 1, entity_id: 1,
                  user_id: 'ab'
                )
        board.valid?.must_equal false
        board.errors[:user_id].must_include "user must be a number"
      end
    end # user_id

    describe "workspace_id" do

      it "is an integer" do
        board = Belinkr::Workspace::Board::Member.new(
                  name: 'board 1', workspace_id: 'aa', entity_id: 1,
                  user_id: 1
                )
        board.valid?.must_equal false
        board.errors[:workspace_id].must_include "workspace must be a number"
      end
    end # workspace_id

    describe "entity_id" do

      it "is an integer" do
        board = Belinkr::Workspace::Board::Member.new(
                  name: 'board 1', workspace_id: 1, entity_id: 'aa',
                  user_id: 1
                )
        board.valid?.must_equal false
        board.errors[:entity_id].must_include "entity must be a number"
      end
    end # entity_id

  end # validations
end
