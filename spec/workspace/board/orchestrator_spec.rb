# encoding: utf-8

require 'minitest/autorun'
require_relative '../../../user/orchestrator'
require_relative '../../../user/member'
require_relative '../../../user/collection'
require_relative '../../../workspace/orchestrator'
require_relative '../../../workspace/member'
require_relative '../../../workspace/collection'
require_relative '../../../workspace/board/orchestrator'
require_relative '../../../tinto/exceptions'



$redis ||= Redis.new

include Belinkr

describe Belinkr::Workspace::Board::Orchestrator do
  before do
    $redis.flushdb

    @user      = User::Orchestrator.create(
                   User::Member
                     .new(first: "User", last: "123", entity_id: 1)
                 )
    @workspace = Workspace::Orchestrator.create(
                   Workspace::Member
                     .new(name: "workspace 1", entity_id: @user.entity_id), @user
                )
    Workspace::Collaborator::Collection.new(entity_id: @user.entity_id, 
      workspace_id: @workspace.id).add(@user)
    
    Workspace::Administrator::Collection.new(entity_id: @user.entity_id,
      workspace_id: @workspace.id).add(@user)
    
    Workspace::Collaborator::Collection.new(entity_id: @user.entity_id,
      workspace_id: @workspace.id).remove(@user)

    @board = Workspace::Board::Member.new(
               name: 'board 1')
              
    @created_board = Workspace::Board::Orchestrator
                       .create(@workspace, @board, @user)
  end

  describe ".create" do
    it "create a board and add it's reference to board collection" do
      @created_board.must_be_instance_of Workspace::Board::Member
      @created_board.id.wont_be_nil
      @created_board.workspace_id.must_equal @workspace.id
      @created_board.created_at.wont_be_nil
      Workspace::Board::Collection
        .new(workspace_id: @workspace.id, entity_id: @user.entity_id)
        .must_include @created_board
    end
  end # .create

  describe ".update" do
    it "update a board" do
      @board.name = 'board update'

      Workspace::Board::Orchestrator
        .update(@workspace, @created_board, @board, @user)
      @created_board.name.wont_equal 'board 1'
      @created_board.name.must_equal 'board update'
    end
  end # .update

  describe ".delete" do
    it "update the deleted_at attribute and delete it's referentce in board collection from db" do
      dup_board = @created_board
      @created_board.deleted_at.must_be_nil
      Workspace::Board::Orchestrator
        .delete(@workspace, @created_board, @user)

      @created_board.deleted_at.wont_be_nil
      Workspace::Board::Collection
        .new(entity_id: @user.entity_id, workspace_id: @workspace.id)
          .wont_include dup_board
    end
  end # .delete


  describe ".destroy" do
    it "delete a board and it's reference in board collection from db" do
      board_id = @created_board.id
      dup_board = @created_board.dup
      Workspace::Board::Orchestrator
        .destroy(@workspace, @created_board, @user)

      @created_board.deleted_at.wont_be_nil 
      @created_board.id.must_be_nil  
      Workspace::Board::Collection.new(entity_id: @user.entity_id,
        workspace_id: @workspace.id).wont_include dup_board
      lambda {Workspace::Board::Member.new(entity_id: @user.entity_id,
        workspace_id: @workspace.id, id: board_id) }
          .must_raise Tinto::Exceptions::NotFound
    end
  end#destroy

  describe ".undelete" do
    it "unmark the deleted board and add it's reference into board collection" do
      deleted_board = Workspace::Board::Orchestrator
                        .delete(@workspace, @created_board, @user) 

      undeleted_board = Workspace::Board::Orchestrator
                          .undelete(@workspace, deleted_board, @user)

      undeleted_board.deleted_at.must_be_nil
      Workspace::Board::Collection.new(entity_id: @user.entity_id,
         workspace_id: @workspace.id)
           .must_include undeleted_board
    end
  end# undelete

  describe ".read" do
    it "returns the board" do
      board = Workspace::Board::Orchestrator
                .read(@workspace, @created_board, @user)
      board.must_equal @created_board 
    end
  end

  describe ".collection" do
    it "returns a board collection" do
      boards = Workspace::Board::Collection
        .new(entity_id: @user.entity_id, workspace_id: @workspace.id).page

      boards_collection= Workspace::Board::Orchestrator
        .collection(@workspace, boards, @user)
      
      boards_collection.must_equal boards
    end
  end
end # Belinkr::Workspace::Board::Orchestrator
