#encoding: utf-8

require 'minitest/autorun'
require_relative '../../../../workspace/board/story/collection'

include Belinkr
describe Belinkr::Workspace::Board::Story::Collection do
  
  before do
    @collection = Workspace::Board::Story::Collection
      .new(entity_id: 1, workspace_id: 1, board_id: 1)
  end

  describe "attributes" do
    it "has #entity_id,#workspace_id,#board_id" do
      @collection.must_respond_to :entity_id
      @collection.must_respond_to :workspace_id
      @collection.must_respond_to :board_id
    end
  end#attributes

  describe "validations" do
    describe "#entity_id" do
      it "must be present" do
        @collection.entity_id = nil
        @collection.valid?.must_equal false
        @collection.errors[:entity_id].must_include "entity must not be blank"
      end
      it "must be a number" do
        @collection.entity_id = "n"
        @collection.valid?.must_equal false
        @collection.errors[:entity_id].must_include "entity must be a number"
      end
    end

    describe "#workspace_id" do
      it "must be present" do
        @collection.workspace_id = nil
        @collection.valid?.must_equal false
        @collection.errors[:workspace_id].must_include "workspace must not be blank"
      end
      it "must be a number" do
        @collection.workspace_id = "n"
        @collection.valid?.must_equal false
        @collection.errors[:workspace_id].must_include "workspace must be a number"
      end
    end

    describe "#board_id" do
      it "must be present" do
        @collection.board_id = nil
        @collection.valid?.must_equal false
        @collection.errors[:board_id].must_include "board must not be blank"
      end
      it "must be a number" do
        @collection.board_id = "n"
        @collection.valid?.must_equal false
        @collection.errors[:board_id].must_include "board must be a number"
      end
    end
  end#validations

  describe "methods" do
    it "must has methods #all, #each, #size, #add, #delete, #remove" do
      @collection.must_respond_to :all
      @collection.must_respond_to :each
      @collection.must_respond_to :size
      @collection.must_respond_to :add
      @collection.must_respond_to :delete
      @collection.must_respond_to :remove
    end
  end#methods
end
