#encoding: utf-8
require 'minitest/autorun'
require 'redis'
require_relative '../../../../user/orchestrator'
require_relative '../../../../workspace/orchestrator'
require_relative '../../../../workspace/board/orchestrator'
require_relative '../../../../workspace/board/story/orchestrator'
$redis = Redis.new

include Belinkr

describe Workspace::Board::Story::Enforcer do
  before do
 
    $redis.flushdb

    #the entity has one workspace, two users who are admin or collaborator 
    #of the workspace,one board,and one story instance in the board
    @collaborator = User::Orchestrator.create(
      User::Member.new(entity_id:1, first: 'user', last: '111')) 
    @admin = User::Orchestrator.create(
      User::Member.new(entity_id:1, first: 'user', last: '112'))
    @workspace = Workspace::Orchestrator.create(
      Workspace::Member.new(name: 'workspace 1'), @admin)
    Workspace::Collaborator::Collection.new(entity_id: 1, 
      workspace_id: @workspace.id).add(@collaborator).add(@admin)
    Workspace::Administrator::Collection.new(entity_id: 1,
      workspace_id: @workspace.id).add(@admin)
    Workspace::Collaborator::Collection.new(entity_id: 1,
      workspace_id: @workspace.id).remove(@admin)
    @board = Workspace::Board::Orchestrator.create(@workspace,
      Workspace::Board::Member.new(name: 'board 1'), @admin) 
    @created_story = Workspace::Board::Story::Member.new(entity_id: 1, 
      workspace_id: @workspace.id, board_id: @board.id, name: 'story 1',
      start: Date.today, end: Date.today.next).save
    @story = Workspace::Board::Story::Member.new(name: 'story 2', 
      start: Date.today, end: Date.today.next)
  end
  
  describe "#create_by?" do
    it "must raise NotAllowed exception if the user is not one of the corresponding 
      workspace's admins" do
      lambda { Workspace::Board::Story::Enforcer.authorize(@collaborator, :create, 
        @workspace, @board, @story) }.must_raise Tinto::Exceptions::NotAllowed 
    end
  end# #create_by?

  describe "#delete_by?" do
    it "must raise NotAllowed exception if the user is not one of the corresponding 
      workspace's admins" do
      lambda { Workspace::Board::Story::Enforcer.authorize(@collaborator, :delete, 
        @workspace, @board, @created_story) }.must_raise Tinto::Exceptions::NotAllowed 
    end 
  end# #delete_by?

  describe "#undelete_by?" do
    it "must raise NotAllowed exception if the user is not one of the corresponding 
      workspace's admins" do
      deleted_story = @created_story.delete
      lambda { Workspace::Board::Story::Enforcer.authorize(@collaborator, :undelete, 
        @workspace, @board, deleted_story) }.must_raise Tinto::Exceptions::NotAllowed 
    end 
  end# #undelete_by?

  describe "#destroy_by?" do
    it "must raise NotAllowed exception if the user is not one of the corresponding 
      workspace's admins" do
      lambda { Workspace::Board::Story::Enforcer.authorize(@collaborator, :destroy, 
        @workspace, @board, @created_story) }.must_raise Tinto::Exceptions::NotAllowed 
    end 
  end# #destroy_by?

  describe "#read_by?" do
    it "must raise NotAllowed exception if the user is not one of the corresponding 
      workspace's admins or collaborator" do
      user = User::Orchestrator.create(
        User::Member.new(entity_id: 1, first: 'user', last: '113'))
      lambda { Workspace::Board::Story::Enforcer.authorize(user, :read, 
        @workspace, @board, @created_story) }.must_raise Tinto::Exceptions::NotAllowed 
    end 
  end# #read_by?
end #Workspace::Board::Story::Enforcer
