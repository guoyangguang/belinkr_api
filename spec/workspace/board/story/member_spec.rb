#encoding: utf-8

require_relative "../../../../workspace/board/story/member"
require "minitest/autorun"
require_relative "../../../../init"

include Belinkr
describe Workspace::Board::Story::Member do

  before do
    @story = Workspace::Board::Story::Member.new(entity_id: 1, workspace_id: 1, board_id: 1)
  end

  describe "attributes" do
    it "has #id, #entity_id, #workspace_id, #board_id, #name, #start, #end,
      #created_at, #updated_at, #delete_at attributes" do
      @story.must_respond_to :id
      @story.must_respond_to :entity_id
      @story.must_respond_to :workspace_id
      @story.must_respond_to :board_id
      @story.must_respond_to :name
      @story.must_respond_to :start
      @story.must_respond_to :end
      @story.must_respond_to :created_at
      @story.must_respond_to :updated_at
      @story.must_respond_to :deleted_at
    end
  end #attributes

  describe "validations" do
    describe "entity_id" do
      it "must be present" do
        @story.entity_id = nil
        @story.valid?.must_equal false
        @story.errors[:entity_id].must_include "entity must not be blank"
      end
      it "must be a number" do
        @story.entity_id = "n"
        @story.valid?.must_equal false
        @story.errors[:entity_id].must_include "entity must be a number"
      end
    end

    describe "workspace_id" do
      it "must be present" do
        @story.workspace_id = nil
        @story.valid?.must_equal false
        @story.errors[:workspace_id].must_include "workspace must not be blank"
      end
      it "must be a number" do
        @story.workspace_id = "n"
        @story.valid?.must_equal false
        @story.errors[:workspace_id].must_include "workspace must be a number"
      end
    end

    describe "board_id" do
      it "must be present" do
        @story.board_id = nil
        @story.valid?.must_equal false
        @story.errors[:board_id].must_include "board must not be blank"
      end
      it "must be a number" do
        @story.board_id = "n"
        @story.valid?.must_equal false
        @story.errors[:board_id].must_include "board must be a number"
      end
    end

    describe "name" do
      it "must be present" do
        @story.valid?.must_equal false
        @story.errors[:name].must_include "name must not be blank"
      end
    end
    
    describe "start" do
      it "must be present" do
        @story.valid?.must_equal false
        @story.errors[:start].must_include "StartDate must not be blank"
      end
      it "must be at least 3 characters long" do
        @story.name = "cr"
        @story.valid?.must_equal false
        @story.errors[:name].must_include "name must be between 3 and 200 characters long"
      end
      it "must be at most 200 characters long" do
        @story.name = "c" * 201
        @story.valid?.must_equal false
        @story.errors[:name].must_include "name must be between 3 and 200 characters long"
      end
    end

    describe "end" do
      it "must be present" do
        @story.valid?.must_equal false
        @story.errors[:end].must_include "EndDate must not be blank"
      end
    end
  end #validations

  describe "methods" do
    it "has method #to_hash" do
      @story.must_respond_to :to_hash
    end
    it "has methods #read, #save, #update, #delete, #undelete, #destroy" do
      @story.must_respond_to :read
      @story.must_respond_to :save 
      @story.must_respond_to :update
      @story.must_respond_to :delete
      @story.must_respond_to :undelete
      @story.must_respond_to :destroy
    end
  end#methods
end#Workspace::Board::Story::Member
