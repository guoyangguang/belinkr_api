#encoding: utf-8

require "redis"
require "minitest/autorun"
require_relative "../../../../user/orchestrator"
require_relative "../../../../workspace/orchestrator"
require_relative "../../../../workspace/board/orchestrator"
require_relative "../../../../workspace/board/story/orchestrator"

$redis = Redis.new
include Belinkr

describe Belinkr::Workspace::Board::Story::Orchestrator do

  before do 
    $redis.flushdb
    
    @user = User::Orchestrator.create(User::Member
              .new(entity_id: 1, first: 'user', last: '111'))

    @workspace = Workspace::Orchestrator.create(Workspace::Member
                   .new(entity_id: @user.entity_id, name: "workspace 1"),@user)
                   
    Workspace::Collaborator::Collection.new(entity_id: @user.entity_id,
      workspace_id: @workspace.id).add(@user)
    Workspace::Administrator::Collection.new(entity_id: @user.entity_id, 
      workspace_id: @workspace.id).add(@user)
    Workspace::Collaborator::Collection.new(entity_id: @user.entity_id,
      workspace_id: @workspace.id).remove(@user)

    @board = Workspace::Board::Orchestrator.create(@workspace, Workspace::Board::Member 
               .new(entity_id: @user.entity_id, workspace_id: @workspace.id,
                 name: "board 1"), @user)

    @story = Workspace::Board::Story::Member
               .new(name: "first story",
                  start: Date.today, end: Date.today.next)
  end

  describe ".create" do
    it "create a story and add it's reference into story collection" do
      @created_story =  Workspace::Board::Story::Orchestrator.create(@workspace, @board, @story, @user)
      @created_story.id.wont_be_nil
      @created_story.created_at.wont_be_nil
      @created_story.updated_at.wont_be_nil

      Workspace::Board::Story::Collection
        .new(entity_id: @user.entity_id, workspace_id: @workspace.id, 
          board_id: @board.id).must_include @created_story 
    end
  end#.create
end
