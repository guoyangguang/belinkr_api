# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../workspace/collaborator/collection"
require_relative "../../../workspace/orchestrator"
require_relative "../../../user/member"
require_relative "../../../user/orchestrator"

$redis = Redis.new

describe Belinkr::Workspace::Collaborator::Collection do
  describe "validations" do
    describe "#workspace_id" do
      it "must be present" do
        collection = Belinkr::Workspace::Collaborator::Collection.new

        collection.valid?.must_equal false
        collection.errors[:workspace_id]
          .must_include "workspace must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Workspace::Collaborator::Collection
                      .new(workspace_id: "ad")

        collection.valid?.must_equal false
        collection.errors[:workspace_id]
          .must_include "workspace must be a number"
      end
    end #workspace_id

    describe "entity_id" do
      it "must be present" do
        collection = Belinkr::Workspace::Collaborator::Collection.new
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        collection = Belinkr::Workspace::Collaborator::Collection
          .new(entity_id: "ad")
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id
  end # validations

  describe "#each" do
    it "yields User::Member instances" do
      user1         = Belinkr::User::Orchestrator.create(
                        user = Belinkr::User::Member
                          .new(first: "User", last: "111", entity_id: 1)
                      )
      user2         = Belinkr::User::Orchestrator.create(
                        Belinkr::User::Member
                          .new(first: "User", last: "112", entity_id: 1)
                      )
      workspace     = Belinkr::Workspace::Orchestrator.create(
                        Belinkr::Workspace::Member.new(name: "workspace 1"),
                        user1
                      )

      collaborators = Belinkr::Workspace::Collaborator::Collection
                        .new(workspace_id: workspace.id, entity_id: 1)
      collaborators.add user1
      collaborators.add user2

      collaborators.all.each do |collaborator| 
        collaborator.must_be_instance_of Belinkr::User::Member
      end
    end
  end # each
end # Belinkr::Workspace::Collaborator::Collection
