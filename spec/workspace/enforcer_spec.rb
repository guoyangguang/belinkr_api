# encoding: utf-8
require "minitest/autorun"
require_relative "../../workspace/enforcer"
require_relative "../../workspace/orchestrator"
require_relative "../../workspace/collaborator/collection"
require_relative "../../workspace/administrator/collection"
require_relative "../../user/member"
require_relative "../../user/orchestrator"

$redis ||= Redis.new

describe Belinkr::Workspace::Enforcer do
  describe "#promote?" do
    before do
      @user          = Belinkr::User::Orchestrator.create(
                          Belinkr::User::Member
                            .new(first: "User", last: "000", entity_id: 1)
                        )
      @user1          = Belinkr::User::Orchestrator.create(
                          Belinkr::User::Member
                            .new(first: "User", last: "111", entity_id: 1)
                        )
      @user2          = Belinkr::User::Orchestrator.create(
                          Belinkr::User::Member
                            .new(first: "User", last: "112", entity_id: 1)
                        )
      @workspace      = Belinkr::Workspace::Orchestrator.create(
                          Belinkr::Workspace::Member.new(name: "workspace 1"),
                          @user
                        )

      @collaborators  = Belinkr::Workspace::Collaborator::Collection
                          .new(workspace_id: @workspace.id, entity_id: 1)
      @administrators = Belinkr::Workspace::Administrator::Collection
                          .new(workspace_id: @workspace.id, entity_id: 1)
    end

    it "raises when a collaborator tries to promote another user" do
      @collaborators.add @user1
      @collaborators.add @user2

      lambda {
        Belinkr::Workspace::Orchestrator.promote(@workspace, @user2, @user1)
      }.must_raise Tinto::Exceptions::NotAllowed

      @administrators.wont_include(@user2)
    end
  
    it "allows an administrator to promote a collaborator" do
      @collaborators.add  @user1
      @collaborators.add  @user2
      @administrators.add @user1

      Belinkr::Workspace::Orchestrator.promote(@workspace, @user2, @user1)

      @administrators.must_include(@user2)
    end

    it "raises when a collaborator tries to demote another user" do
      @collaborators.add @user1
      @collaborators.add @user2

      lambda {
        Belinkr::Workspace::Orchestrator.demote(@workspace, @user2, @user1)
      }.must_raise Tinto::Exceptions::NotAllowed

      @administrators.wont_include(@user2)
    end

    it "allows and administrator to demote another administrator" do
      @collaborators.add  @user1
      @collaborators.add  @user2
      @administrators.add @user1
      @administrators.add @user2

      Belinkr::Workspace::Orchestrator.demote(@workspace, @user2, @user1)

      @administrators.wont_include(@user2)
    end
  end # promote?
end # Workspace::Enforcer
