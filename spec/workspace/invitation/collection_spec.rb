# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../workspace/invitation/collection"

describe Belinkr::Workspace::Invitation::Collection do
  describe "validations" do
    describe "workspace_id" do
      it "must be present" do
        collection = Belinkr::Workspace::Invitation::Collection.new
        collection.valid?.must_equal false
        collection.errors[:workspace_id]
          .must_include "workspace must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Workspace::Invitation::Collection.new
        collection.valid?.must_equal false
        collection.errors[:workspace_id]
          .must_include "workspace must be a number"
      end
    end #workspace_id

    describe "entity_id" do
      it "must be present" do
        collection = Belinkr::Workspace::Invitation::Collection.new
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must not be blank"
      end

      it "must be a number" do
        collection = Belinkr::Workspace::Invitation::Collection.new
        collection.valid?.must_equal false
        collection.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id
  end # validations
end # Belinkr::Workspace::Invitation::Collection
