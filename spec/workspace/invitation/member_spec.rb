# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../workspace/invitation/member"

$redis ||= Redis.new

describe Belinkr::Workspace::Invitation::Member do
  before { $redis.flushdb }

  describe "#validations" do
    describe "workspace_id" do
      it "must be present" do
        invitation = Belinkr::Workspace::Invitation::Member.new
        invitation.valid?.must_equal false
        invitation.errors[:workspace_id]
          .must_include "workspace must not be blank"
      end

      it "must be a number" do
        invitation = Belinkr::Workspace::Invitation::Member
                      .new(workspace_id: "string")
        invitation.valid?.must_equal false
        invitation.errors[:workspace_id]
          .must_include "workspace must be a number"
      end
    end # workspace_id

    describe "entity_id" do
      it "must be present" do
        invitation = Belinkr::Workspace::Invitation::Member.new
        invitation.valid?.must_equal false
        invitation.errors[:entity_id]
          .must_include "entity must not be blank"
      end

      it "must be a number" do
        invitation = Belinkr::Workspace::Invitation::Member
                      .new(entity_id: "string")
        invitation.valid?.must_equal false
        invitation.errors[:entity_id]
          .must_include "entity must be a number"
      end
    end # entity_id

    describe "inviter_id" do
      it "must be present" do
        invitation = Belinkr::Workspace::Invitation::Member.new
        invitation.valid?.must_equal false
        invitation.errors[:inviter_id].must_include "inviter must not be blank"
      end

      it "must be a Numeric" do
        invitation = Belinkr::Workspace::Invitation::Member
                       .new(inviter_id: "ab")
        invitation.valid?.must_equal false
        invitation.errors[:inviter_id].must_include "inviter must be a number"
      end
    end # inviter_id

    describe "invited_id" do
      it "must be present" do
        invitation = Belinkr::Workspace::Invitation::Member.new
        invitation.valid?.must_equal false
        invitation.errors[:invited_id].must_include "invited must not be blank"
      end

      it "must be a number" do
        invitation = Belinkr::Workspace::Invitation::Member
                       .new(invited_id: "ab")
        invitation.valid?.must_equal false
        invitation.errors[:invited_id].must_include "invited must be a number"
      end
    end # invited_id
  end # validation
  
  describe "#accept" do
    it "changes the state of the invitation to 'accepted'" do
      invitation = Belinkr::Workspace::Invitation::Member.new(
        inviter_id:   1, 
        invited_id:   2, 
        state:        "pending",
        workspace_id: 1, 
        entity_id:    1
      ).save

      invitation.accept
      invitation.state.must_equal "accepted"
      invitation.save
      
      persisted = Belinkr::Workspace::Invitation::Member.new(
        id:           invitation.id, 
        workspace_id: invitation.workspace_id,
        entity_id:    invitation.entity_id
      )

      persisted.state.must_equal "accepted"
    end
  end #accept

  describe "#reject" do
    it "changes the state of the invitation to 'rejected'" do
      invitation = Belinkr::Workspace::Invitation::Member.new(
        inviter_id:   1, 
        invited_id:   2, 
        state:        "pending",
        workspace_id: 1, 
        entity_id:    1
      ).save

      invitation.rejected_at.must_be_nil
      invitation.reject(Time.now)
      invitation.save
      invitation.state.must_equal "rejected"
      invitation.rejected_at.wont_be_nil

      persisted = Belinkr::Workspace::Invitation::Member.new(
        id:           invitation.id, 
        workspace_id: invitation.workspace_id,
        entity_id:    invitation.entity_id
      )
      persisted.state.must_equal "rejected"
      persisted.rejected_at.wont_be_nil
    end
  end #reject

  describe "#accepted?" do
    it "returns true if state is 'accepted'" do
      invitation = Belinkr::Workspace::Invitation::Member.new(
        inviter_id:   1, 
        invited_id:   2, 
        workspace_id: 1, 
        entity_id:    1, 
        state:        "pending"
      ).save
      invitation.accept
      invitation.accepted?.must_equal true
    end
  end #accepted?

  describe "#rejected?" do
    it "returns true if state is 'rejected'" do
      invitation = Belinkr::Workspace::Invitation::Member.new(
        inviter_id:   1, 
        invited_id:   2, 
        workspace_id: 1, 
        entity_id:    1, 
        state:        "pending"
      ).save
      invitation.reject(Time.now)
      invitation.rejected?.must_equal true
    end
  end #rejected?
end # Belinkr::Workspace::Invitation::Member
