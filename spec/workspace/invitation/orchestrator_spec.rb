# encoding: utf-8
require "minitest/autorun"
require_relative "../../../workspace/orchestrator"
require_relative "../../../workspace/invitation/orchestrator"
require_relative "../../../workspace/collaborator/collection"
require_relative "../../../user/member"
require_relative "../../../user/collection"
require_relative "../../../user/orchestrator"

$redis ||= Redis.new

describe "invitations" do
  before do
    $redis.flushdb

    @workspace = Belinkr::Workspace::Member
                  .new(name: "workspace 1", entity_id: 1)
    @inviter    = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new(first: "User", last: "123", entity_id: 1)
                  )
    @invited    = Belinkr::User::Orchestrator.create(
                    Belinkr::User::Member
                      .new(first: "User", last: "234", entity_id: 1)
                  )
    
    Belinkr::Workspace::Orchestrator.create(@workspace, @inviter)
  end

  describe ".invite" do
    it "invite user to a workspace" do
      invitation  = Belinkr::Workspace::Invitation::Orchestrator
                      .invite(@workspace, @invited, @inviter)

      invitation.workspace_id .must_equal @workspace.id
      invitation.inviter_id   .must_equal @inviter.id
      invitation.invited_id   .must_equal @invited.id
      invitation.state        .must_equal "pending"

      Belinkr::Workspace::Invitation::Collection
        .new(workspace_id: @workspace.id, entity_id: @workspace.entity_id)
        .must_include invitation
    end

    it "registers an activity" do
      invitation = Belinkr::Workspace::Invitation::Orchestrator
                     .invite(@workspace, @invited, @inviter)
      activity = Belinkr::Activity::Collection.new.all.to_a.first
      activity.actor.resource.id.must_equal @inviter.id
      activity.action.must_equal 'invite'
      activity.object.resource.id.must_equal @invited.id
      activity.target.resource.id.must_equal @workspace.id
    end
  end # .invite

  describe ".accept" do 
    it "marks an invitation as accepted" do
      invitation  = Belinkr::Workspace::Invitation::Orchestrator
                      .invite(@workspace, @invited, @inviter)

      Belinkr::Workspace::Invitation::Orchestrator
        .accept(@workspace, invitation, @invited)

      invitation.accepted?.must_equal true 
      invitation =  Belinkr::Workspace::Invitation::Member.new(
                      id:           invitation.id, 
                      workspace_id: invitation.workspace_id,
                      entity_id:    invitation.entity_id
                    )

      invitation.accepted?.must_equal true 
    end

    it "adds the invited user to the collaborators of this workspace" do
      invitation  = Belinkr::Workspace::Invitation::Orchestrator
                      .invite(@workspace, @invited, @inviter)

      Belinkr::Workspace::Invitation::Orchestrator
        .accept(@workspace, invitation, @invited)

      invitation =  Belinkr::Workspace::Invitation::Member.new(
                      id:           invitation.id, 
                      workspace_id: invitation.workspace_id,
                      entity_id:    invitation.entity_id
                    )
                     
      collaborators = Belinkr::Workspace::Collaborator::Collection.new(
                        workspace_id: invitation.workspace_id,
                        entity_id:    invitation.entity_id
                      )

      collaborators.must_include @invited
    end

    it "registers two activities" do
       invitation = Belinkr::Workspace::Invitation::Orchestrator
                     .invite(@workspace, @invited, @inviter)

       invitation = Belinkr::Workspace::Invitation::Orchestrator
                      .accept(@workspace, invitation, @invited) 
      
       accept_activities = Belinkr::Activity::Collection.new.all
         .select {|member| member.action == 'accept'} 
       accept_activities[0].actor.resource.id.must_equal @invited.id
       accept_activities[0].object.resource.id.must_equal invitation.id
       accept_activities[0].target.resource.id.must_equal @workspace.id

       join_activities = Belinkr::Activity::Collection.new.all
         .select {|member| member.action == 'join'}
       join_activities[0].actor.resource.id.must_equal @invited.id
       join_activities[0].object.resource.id.must_equal @workspace.id
    end
  end # .accept

  describe ".reject" do 
    it "marks the invitation as 'rejected'" do
      invitation  = Belinkr::Workspace::Invitation::Orchestrator
                      .invite(@workspace, @invited, @inviter)

      Belinkr::Workspace::Invitation::Orchestrator
        .reject(@workspace, invitation, @inviter)

      invitation  = Belinkr::Workspace::Invitation::Member.new(
                      id:           invitation.id, 
                      workspace_id: invitation.workspace_id,
                      entity_id:    invitation.entity_id
                    )

      invitation.rejected?    .must_equal true
      invitation.rejected_at  .wont_be_nil
    end

    it "register an activity" do
      invitation = Belinkr::Workspace::Invitation::Orchestrator
                     .invite(@workspace, @invited, @inviter)

      invitation = Belinkr::Workspace::Invitation::Orchestrator
                     .reject(@workspace, invitation, @invited)

      activity = Belinkr::Activity::Collection.new.all.first
      activity.actor.resource.id.must_equal @invited.id
      activity.action.must_equal 'reject'
      activity.object.resource.id.must_equal invitation.id
      activity.target.resource.id.must_equal @workspace.id
    end
  end # .reject
end # Belinkr::Workspace::Invitation::Orchestrator
