# encoding: utf-8
require "minitest/autorun"
require_relative "../../init"
require_relative "../../workspace/member"

describe Belinkr::Workspace::Member do
  describe "validations" do
    describe "#name" do
      it "must be present" do
        workspace = Belinkr::Workspace::Member.new
        workspace.valid?.must_equal false
        workspace.errors[:name].must_include "name must not be blank"
      end

      it "has minimum length of 3 characters" do
        workspace = Belinkr::Workspace::Member.new(name: "ab")
        workspace.valid?.must_equal false
        workspace.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end

      it "has maximum length of 250 characters" do
        workspace = Belinkr::Workspace::Member.new(name: "a" * 251)
        workspace.valid?.must_equal false
        workspace.errors[:name]
          .must_include "name must be between 3 and 250 characters long"
      end
    end #name

    describe "entity_id" do
      it "must be present" do
        workspace = Belinkr::Workspace::Member.new
        workspace.valid?.must_equal false
        workspace.errors[:entity_id].must_include "entity must not be blank"
      end

      it "is a integer" do
        workspace = Belinkr::Workspace::Member.new(entity_id: "ad")
        workspace.valid?.must_equal false
        workspace.errors[:entity_id].must_include "entity must be a number"
      end
    end #entity_id
  end # validations
end # Belinkr::Workspace::Member
