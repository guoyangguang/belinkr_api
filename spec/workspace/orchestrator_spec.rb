# encoding: utf-8
require "minitest/autorun"
require_relative "../../workspace/orchestrator"
require_relative "../../workspace/collaborator/collection"
require_relative "../../workspace/administrator/collection"
require_relative "../../user/member"
require_relative "../../user/collection"
require_relative "../../user/orchestrator"
require_relative "../../tinto/exceptions"
require_relative "../../activity/collection"

$redis ||= Redis.new

describe Belinkr::Workspace::Orchestrator do
  before do
    @workspace = Belinkr::Workspace::Member
                  .new(name: "workspace 1", entity_id: 1)
    @user      = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User", last: "123", entity_id: 1)
                 )
  end

  def activity_collection
    Belinkr::Activity::Collection.new
  end

  describe ".create" do
    it "creates a workspace" do
      Belinkr::Workspace::Orchestrator.create(@workspace, @user)
      @workspace            .id.wont_be_nil
      @workspace.name       .must_equal "workspace 1"
      @workspace.created_at .wont_be_nil

      Belinkr::Workspace::Collection.new(entity_id: @workspace.entity_id)
        .must_include @workspace
      Belinkr::Workspace::Administrator::Collection.new(
        workspace_id: @workspace.id,
        entity_id:    @workspace.entity_id
      ).must_include @user

      activity = activity_collection.all.to_a.first
      activity.actor.resource.id      .must_equal @user.id
      activity.action                 .must_equal 'add'
      activity.object.resource.id     .must_equal @workspace.id
    end
  end

  describe ".read" do
    it "read a workspace content" do
      wk = Belinkr::Workspace::Orchestrator.create(@workspace, @user)
      wk_one = Belinkr::Workspace::Member
                .new(id: wk.id, entity_id: @user.entity_id)
      wk = Belinkr::Workspace::Orchestrator.read(wk_one, @user)
      wk.id.wont_be_nil
      wk.name.must_equal 'workspace 1'
    end
  end

  describe ".update" do
    it "updates the workspace attributes" do
      Belinkr::Workspace::Orchestrator.create(@workspace, @user)
      updated       = @workspace.dup
      updated.name  = "updated name"

      Belinkr::Workspace::Orchestrator.update(@workspace, updated, @user)
      Belinkr::Workspace::Member
        .new(id: @workspace.id, entity_id: @user.entity_id)
        .name.must_equal "updated name"

      activity = activity_collection.all.to_a.first
      activity.actor.resource.id      .must_equal @user.id
      activity.action                 .must_equal 'update'
      activity.object.resource.id     .must_equal updated.id
      activity.object.resource.name   .must_equal "updated name"
    end
  end

  describe ".delete" do
    it "deletes a workspace" do
      Belinkr::Workspace::Orchestrator.create(@workspace, @user)
      @workspace.deleted_at.must_be_nil     
      Belinkr::Workspace::Orchestrator .delete(@workspace, @user)

      @workspace.deleted_at .wont_be_nil  
      @workspace.id         .must_equal @workspace.id
      @workspace.name       .must_equal @workspace.name

      activity = activity_collection.all.to_a.first
      activity.actor.resource.id         .must_equal @user.id
      activity.action                    .must_equal 'delete'
      activity.object.resource.id        .must_equal @workspace.id
      activity.object.resource.deleted_at.wont_be_nil
    end
  end

  describe "promotion, demotion, leaving and removing" do
    before do
      @workspace      = Belinkr::Workspace::Orchestrator
                          .create(@workspace, @user)
      @collaborators  = Belinkr::Workspace::Collaborator::Collection
                          .new(workspace_id: @workspace.id, entity_id: 1)
      @administrators = Belinkr::Workspace::Administrator::Collection
                          .new(workspace_id: @workspace.id, entity_id: 1)
      @user1          = Belinkr::User::Orchestrator.create(
                          Belinkr::User::Member
                            .new(first: "User", last: "123", entity_id: 1)
                        )
      @user2          = Belinkr::User::Orchestrator.create(
                          Belinkr::User::Member
                            .new(first: "User", last: "234", entity_id: 1)
                        )
    end

    describe ".promote" do 
      it "promotes a collaborator to administator" do
        @collaborators.add @user1
        @collaborators.add @user2
        @administrators.add @user1

        Belinkr::Workspace::Orchestrator.promote(@workspace, @user2, @user1)
        
        @administrators.must_include @user2

        activity = activity_collection.all.to_a.first
        activity.actor.resource.id      .must_equal @user1.id
        activity.action                 .must_equal 'promote'
        activity.object.resource.id     .must_equal @user2.id
      end

      it "removes the collaborator from the collaborator collection" do
        @collaborators.add @user1
        @collaborators.add @user2
        @administrators.add @user1

        Belinkr::Workspace::Orchestrator.promote(@workspace, @user2, @user1)
        @collaborators.wont_include @user2
      end
    end

    describe ".demote" do
      it "demotes an administrator to collaborator in a workspace" do
        @collaborators.add @user1
        @collaborators.add @user2
        @administrators.add @user1
        @administrators.add @user2

        Belinkr::Workspace::Orchestrator.demote(@workspace, @user2, @user1)
        @collaborators.must_include(@user2)

        activity = activity_collection.all.to_a.first
        activity.actor.resource.id      .must_equal @user1.id
        activity.action                 .must_equal 'demote'
        activity.object.resource.id     .must_equal @user2.id
      end

      it "removes the administrator from the administrator collection" do
        @collaborators.add @user1
        @collaborators.add @user2
        @administrators.add @user1
        @administrators.add @user2

        Belinkr::Workspace::Orchestrator.demote(@workspace, @user2, @user1)
        @administrators.wont_include @user2
      end
    end

    describe ".leave" do
      describe "allows the user to leave the workspace" do
        it "as administrator" do
          @collaborators .add(@user1).add(@user2)
          @administrators.add(@user1).add(@user2)
          @collaborators.remove(@user1).remove(@user2)
          
          @administrators.must_include @user2

          Belinkr::Workspace::Orchestrator.leave(@workspace, @user2) 
          @administrators.wont_include @user2

          activity = activity_collection.all.to_a.first
          activity.actor.resource.id      .must_equal @user2.id
          activity.action                 .must_equal 'leave'
          activity.object.resource.id     .must_equal @workspace.id
        end

        it "as collaborator" do
          @collaborators .add(@user1)
          @collaborators .add(@user2)

          Belinkr::Workspace::Orchestrator.leave(@workspace, @user2) 
          @collaborators.wont_include @user2

          activity = activity_collection.all.to_a.first
          activity.actor.resource.id      .must_equal @user2.id
          activity.action                 .must_equal 'leave'
          activity.object.resource.id     .must_equal @workspace.id
        end
      end # allows the user to leave the workspace

      it "raises if the last user in the workspace 
      (which must be an administrator) wants to leave" do

        lambda {
          Belinkr::Workspace::Orchestrator.leave(@workspace, @user)
        }.must_raise Tinto::Exceptions::NotAllowed 

        @administrators.must_include(@user)
      end
    end # .leave

    describe ".remove" do
      describe "allows an administrator to remove a user from the workspace" do
        it "removes an administor user" do
          @collaborators.add @user1
          @collaborators.add @user2
          @administrators.add @user1
          @administrators.add @user2

          Belinkr::Workspace::Orchestrator.remove(@workspace, @user2, @user1)
          @administrators.wont_include @user2

          activity = activity_collection.all.to_a.first
          activity.actor.resource.id      .must_equal @user1.id
          activity.action                 .must_equal 'remove'
          activity.object.resource.id     .must_equal @user2.id
        end

        it "removes a collaborator user" do
          @collaborators.add @user1
          @collaborators.add @user2
          @administrators.add @user1

          Belinkr::Workspace::Orchestrator.remove(@workspace, @user2, @user1)
          @collaborators.wont_include @user2

          activity = activity_collection.all.to_a.first
          activity.actor.resource.id      .must_equal @user1.id
          activity.action                 .must_equal 'remove'
          activity.object.resource.id     .must_equal @user2.id
        end
      end
    end # .remove
  end # promoting, demoting, leaving and removing
end # Belinkr::Workspace::Orchestrator
