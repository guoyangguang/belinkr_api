# encoding: utf-8
require "minitest/autorun"
require_relative "../../../init"
require_relative "../../../workspace/reply/member"


describe Belinkr::Workspace::Reply::Member do

  describe "#validations" do
    describe "workspace_id" do
      it "must be present" do
        reply = Belinkr::Workspace::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:workspace_id]
          .must_include "workspace must not be blank"
      end

      it "must be a number" do
        reply = Belinkr::Workspace::Reply::Member
                      .new(workspace_id: "string")
        reply.valid?.must_equal false
        reply.errors[:workspace_id]
          .must_include "workspace must be a number"
      end
    end # workspace_id

    describe "user_id" do
      it "must be present" do
        reply = Belinkr::Workspace::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:user_id].must_include "user must not be blank"
      end

      it "must be a number" do
        reply = Belinkr::Workspace::Reply::Member
                       .new(invited_id: "ab")
        reply.valid?.must_equal false
        reply.errors[:user_id].must_include "user must be a number"
      end
    end # user_id

    describe "status_id" do
      it "must be present" do
        reply = Belinkr::Workspace::Reply::Member.new
        reply.valid?.must_equal false
        reply.errors[:status_id].must_include "status must not be blank"
      end

      it "must be a number" do
        reply = Belinkr::Workspace::Reply::Member
                       .new(invited_id: "ab")
        reply.valid?.must_equal false
        reply.errors[:status_id].must_include "status must be a number"
      end
    end # status_id

    describe "text" do
      it "has more than 3 characters" do
        reply = Belinkr::Workspace::Reply::Member.new(text: "ab")
        reply.valid?.must_equal false
        reply.errors[:text]
          .must_include "text must be between 3 and 250 characters long"
      end

      it "has less than 250 characters" do
        reply = Belinkr::Workspace::Reply::Member.new(text: "a" * 251)
        reply.valid?.must_equal false
        reply.errors[:text]
          .must_include "text must be between 3 and 250 characters long"
      end
    end #text
  end #validations
end # Belinkr::Workspace::Reply::Member
