#encoding: utf-8

require 'minitest/autorun'
require 'redis'
require_relative '../../../user/orchestrator'
require_relative '../../../workspace/orchestrator'
require_relative '../../../workspace/reply/orchestrator'
require_relative '../../../workspace/status/orchestrator'

$redis = Redis.new

describe Belinkr::Workspace::Reply::Orchestrator do
  before do
   $redis.flushdb
   @user = Belinkr::User::Orchestrator.create(
             Belinkr::User::Member.new(
               first: 'User', last: '111', entity_id: 1))

   @admin = Belinkr::User::Orchestrator.create(
              Belinkr::User::Member.new(
                first: 'User', last: '112', entity_id: 1))

   @workspace = Belinkr::Workspace::Orchestrator.create(
                  Belinkr::Workspace::Member.new(
                    entity_id: @admin.entity_id,
                    name: 'Workspace'), @admin)
   @status = Belinkr::Workspace::Status::Orchestrator
     .create(@workspace, Belinkr::Workspace::Status::Member
     .new(entity_id: 1, workspace_id: @workspace.id, text: 'status'),
        @admin)

   @reply =   Belinkr::Workspace::Reply::Member.new(
                user_id: @user.id,
                workspace_id: @workspace.id,
                text: "reply workspace")
   @replied = Belinkr::Workspace::Reply::Orchestrator.create(
                @status, @reply, @user
              )
  end

  describe ".create" do
    it "create a new workspace's reply" do
      @replied.id.wont_be_nil
      @status.replies.must_include @reply
    end
  end # .create

  describe ".update" do
    it "updates an existing workspace's reply" do
      updated = @replied.dup
      updated.text = "updated reply"

      updated = Belinkr::Workspace::Reply::Orchestrator
                  .update(@status, updated, @user)

      updated.id       .must_equal @reply.id
      updated.user_id  .must_equal @reply.user_id
      updated.text     .must_equal "updated reply"
    end
  end

  describe ".delete" do
    it "deletes an existing workspace'reply" do
      deleted = Belinkr::Workspace::Reply::Orchestrator
                  .delete(@status, @replied, @user)

      deleted.deleted_at .wont_be_nil
      deleted.id         .must_equal @reply.id
      deleted.text       .must_equal @reply.text
      @status.replies   .must_include deleted
    end
  end

  describe ".undelete" do
    it "restores a previously deleted workspace'reply" do
      deleted = Belinkr::Workspace::Reply::Orchestrator
                  .delete(@status, @replied, @user)

      deleted.deleted_at .wont_be_nil
      restored = Belinkr::Workspace::Reply::Orchestrator
                  .undelete(@status, deleted, @user)
      
      restored.deleted_at.must_be_nil
      @status.replies   .must_include restored
    end
  end

  describe ".destroy" do
    it "destroy the persent workspace's reply" do
      delete_status = Belinkr::Workspace::Reply::Orchestrator
                      .delete(@status, @replied, @user)
      destroy_status = Belinkr::Workspace::Reply::Orchestrator
                       .destroy(@status, delete_status, @user)

      @status.replies.wont_include destroy_status
      @status.replies.first.must_be_nil
    end
  end #describe .destroy

end#Belinkr::Workspace::Reply::Orchestrator
