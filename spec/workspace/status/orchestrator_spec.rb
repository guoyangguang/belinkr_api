# encoding: utf-8
require "minitest/autorun"
require 'i18n'
require_relative "../../../workspace/status/orchestrator"
require_relative "../../../workspace/orchestrator"
require_relative "../../../user/orchestrator"


$redis ||= Redis.new

describe Belinkr::Workspace::Status::Orchestrator do
  before do
    @workspace = Belinkr::Workspace::Member
                  .new(name: "workspace 1", entity_id: 1)
    @user      = Belinkr::User::Orchestrator.create(
                  Belinkr::User::Member
                    .new(first: "User", last: "123", entity_id: 1)
                 )
    Belinkr::Workspace::Orchestrator.create(@workspace, @user)
    @status     = Belinkr::Workspace::Status::Member.new text:("status 1")
  end


  describe ".collection" do
    it "acquiescently gets the kind of 'workspace' statuses collection 
      if successfully" do
      status = Belinkr::Workspace::Status::Orchestrator
        .create(@workspace, @status, @user)       
      
      statuses = Belinkr::Workspace::Status::Orchestrator
        .collection(@workspace, @user)
      
      statuses.kind.must_equal 'workspace'
      statuses.must_include status
    end

    it "gets the kind of 'council' statuses collection 
      if passed the 'council' option" do
      status = Belinkr::Workspace::Status::Orchestrator
        .create(@workspace, @status, @user, {kind: 'council'})       
      
      statuses = Belinkr::Workspace::Status::Orchestrator
        .collection(@workspace, @user, {kind: 'council'})
      
      statuses.kind.must_equal 'council'
      statuses.must_include status
    end
  end#.collection

  describe ".create" do
    it "acquiescently creates a kind of 'workspace' status if successfully" do
      status = Belinkr::Workspace::Status::Orchestrator
                 .create(@workspace, @status, @user)
      
      status.must_be_instance_of Belinkr::Workspace::Status::Member

      status.id            .wont_be_nil
      status.workspace_id  .must_equal @workspace.id
      status.created_at    .wont_be_nil

      Belinkr::Workspace::Status::Collection.new(workspace_id: @workspace.id,
         entity_id: @workspace.entity_id, kind: 'workspace')
           .must_include status
    end

    it "creates a kind of 'council' status if passed 'council' option" do
      status = Belinkr::Workspace::Status::Orchestrator
                 .create(@workspace, @status, @user, {kind: 'council'})
      
      status.must_be_instance_of Belinkr::Workspace::Status::Member

      status.id            .wont_be_nil
      status.workspace_id  .must_equal @workspace.id
      status.created_at    .wont_be_nil

      Belinkr::Workspace::Status::Collection.new(workspace_id: @workspace.id,
         entity_id: @workspace.entity_id, kind: 'council')
           .must_include status
    end
  end # .create

  describe ".update" do
    it "acquiescently updates a kind of 'workspace' status if successfully " do
      status = Belinkr::Workspace::Status::Orchestrator
                  .create(@workspace, @status, @user)
      updated = Belinkr::Workspace::Status::Member.new(text: 'updated status')

      status = Belinkr::Workspace::Status::Orchestrator
        .update(@workspace, status, updated, @user)

      status.text.must_equal "updated status"
    end

    it "updates a kind of 'council' status if passed 'council' option" do
      status = Belinkr::Workspace::Status::Orchestrator
                  .create(@workspace, @status, @user, {kind: 'council'})
      updated = Belinkr::Workspace::Status::Member.new(text: 'updated status')

      status = Belinkr::Workspace::Status::Orchestrator
        .update(@workspace, status, updated, @user, {kind: 'council'})

      status.text.must_equal "updated status"
    end
  end # .update

  describe ".delete" do
    it "acquiescently removes a kind of 'workspace' status if successfully" do
      status = Belinkr::Workspace::Status::Orchestrator
                 .create(@workspace, @status, @user)

      status = Belinkr::Workspace::Status::Orchestrator
                 .delete(@workspace, status, @user)

      status.deleted_at.wont_be_nil

      Belinkr::Workspace::Status::Collection
        .new(workspace_id: @status.workspace_id, entity_id: @status.entity_id,
          kind: 'workspace').wont_include @status
    end

    it "removes a kind of 'council' status if passed 'council' option" do
      status = Belinkr::Workspace::Status::Orchestrator
                 .create(@workspace, @status, @user, {kind: 'council'})

      status = Belinkr::Workspace::Status::Orchestrator
                 .delete(@workspace, status, @user, {kind: 'council'})

      status.deleted_at.wont_be_nil

      Belinkr::Workspace::Status::Collection
        .new(workspace_id: @status.workspace_id, entity_id: @status.entity_id,
          kind: 'council').wont_include @status
    end
  end # .delete

  describe ".undelete" do
    it "acquiescently undelete a kind of 'workspace' status if successfully" do
      created_status = Belinkr::Workspace::Status::Orchestrator
                         .create(@workspace, @status, @user)
      deleted_status = Belinkr::Workspace::Status::Orchestrator
                         .delete(@workspace, created_status, @user)

      deleted_status.deleted_at.wont_be_nil

      undeleted_status = Belinkr::Workspace::Status::Orchestrator
                           .undelete(@workspace, deleted_status, @user)

      undeleted_status.deleted_at.must_be_nil
      Belinkr::Workspace::Status::Collection
        .new(workspace_id: @workspace.id, entity_id: @user.entity_id,
          kind: 'workspace').must_include undeleted_status
    end

    it "undelete a kind of 'council' status if passed 'council' option" do
      created_status = Belinkr::Workspace::Status::Orchestrator
                         .create(@workspace, @status, @user, {kind: 'council'})
      deleted_status = Belinkr::Workspace::Status::Orchestrator
                         .delete(@workspace, created_status, @user, 
                           {kind: 'council'})

      deleted_status.deleted_at.wont_be_nil

      undeleted_status = Belinkr::Workspace::Status::Orchestrator
                           .undelete(@workspace, deleted_status, @user, {kind: 'council'})

      undeleted_status.deleted_at.must_be_nil
      Belinkr::Workspace::Status::Collection
        .new(workspace_id: @workspace.id, entity_id: @user.entity_id,
          kind: 'council').must_include undeleted_status
    end
  end # .undelete

  describe ".read" do
    it "acquiescently gets a kind of 'workspace' status if sccessfully" do
      created_status = Belinkr::Workspace::Status::Orchestrator
                         .create(@workspace, @status, @user)
      status = Belinkr::Workspace::Status::Orchestrator
                 .read(@workspace, created_status, @user)
      
      status.must_equal created_status
    end

    it "gets a kind of 'council' status if passed 'council' option" do
      created_status = Belinkr::Workspace::Status::Orchestrator
                         .create(@workspace, @status, @user, {kind: 'council'})
      status = Belinkr::Workspace::Status::Orchestrator
                 .read(@workspace, created_status, @user, {kind: 'council'})
      
      status.must_equal created_status
    end
  end# .read

end # Belinkr::Workspace::Status::Orchestrator
