# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Status
    class Collection
      extend Forwardable
      include Virtus
      include Aequitas
      include Enumerable

      MODEL_NAME  = "status"
      KINDS       = %w{ own general replies }

      attribute :kind,            String
      attribute :user_id,         Integer
      attribute :entity_id,       Integer

      validates_presence_of       :user_id, :entity_id, :kind
      validates_numericalness_of  :user_id, :entity_id
      validates_within            :kind, set: KINDS

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, 
                     :merge, :score

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Status::Member, storage_key)
      end

      def member_init_params
        { entity_id: entity_id, user_id: user_id }
      end

      private

      def storage_key
        "entities:#{entity_id}:timelines:#{user_id}:#{kind}"
      end
    end # Collection
  end # Status
end # Belinkr
