# encoding: utf-8
require_relative "../tinto/exceptions"

module Belinkr
  module Status
    class Enforcer
      include Tinto::Exceptions

      def self.authorize(user, action, resource)
        new(resource).send :"#{action}_by?", user
      end

      def initialize(resource)
        @resource = resource
      end

      def create_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @resource.entity_id
      end

      def read_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @resource.entity_id
      end


      def update_by?(user)
        raise NotAllowed unless user.id
        raise NotAllowed unless user.entity_id == @resource.entity_id
        raise NotAllowed unless user.id == @resource.user_id
      end

      alias_method :collection_by?, :read_by?
      alias_method :delete_by?,     :update_by?
      alias_method :undelete_by?,   :update_by?
      alias_method :destroy_by?,    :update_by?

    end # Enforcer
  end # Status
end # Belinkr
