# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "enforcer"
require_relative "../follow/collection"

module Belinkr
  module Status
    class Orchestrator
      def self.collection(statuses, user)
        Enforcer.authorize(user, :collection, statuses)
        statuses
      end

      def self.create(status, user)
        status = sanitize(status, user)
        Enforcer.authorize(user, :create, status)
        status.save

        follows = follows_for(status.user_id, status.entity_id)
        $redis.multi { add_to_timelines(status, follows) }

        yield status if block_given?
        status
      end 

      def self.read(status, user)
        Enforcer.authorize(user, :read, status)
        status
      end
          
      def self.update(status, changed, user)
        old_status = status.dup
        Enforcer.authorize(user, :update, status)
        status.update(changed)

        follows = follows_for(status.user_id, status.entity_id)

        $redis.multi do
          own_timeline_for(status.user_id, status.entity_id)
            .score(status, status.score)

          general_timeline_for(status.user_id, status.entity_id)
            .score(status, status.score)

          follows.each do |follow|
            general_timeline_for(follow.follower_id, status.entity_id)
              .score(status, status.score)
          end
        end

        yield old_status, status if block_given?

        status
      end

      def self.delete(status, user)
        status = sanitize(status, user)
        #Enforcer.authorize(user, :delete, status)
        follows = follows_for(status.user_id, status.entity_id)

        $redis.multi do
          status.delete
          remove_from_timelines(status, follows)
        end

        status
      end

      def self.undelete(status, user)
        status = sanitize(status, user)
        Enforcer.authorize(user, :undelete, status)
        follows = follows_for(status.user_id, status.entity_id)

        $redis.multi do
          status.undelete
          add_to_timelines(status, follows)
        end

        status
      end

      private

      def self.follows_for(user_id, entity_id)
        follows = Follow::Collection.new(
          user_id: user_id, entity_id: entity_id, kind: "followers"
        ).all.to_a
        follows
      end

      def self.add_to_timelines(status, follows)
        add_to_author_timelines status
        add_to_follower_timelines status, follows
      end

      def self.remove_from_timelines(status, follows)
        remove_from_author_timelines status
        remove_from_follower_timelines status, follows
      end

      def self.add_to_author_timelines(status)
        own_timeline_for(status.user_id, status.entity_id).add status
        general_timeline_for(status.user_id, status.entity_id).add status
      end

      def self.remove_from_author_timelines(status)
        own_timeline_for(status.user_id, status.entity_id).remove status
        general_timeline_for(status.user_id, status.entity_id).remove status
      end

      def self.add_to_follower_timelines(status, follows)
        follows.each do |follow|
          general_timeline_for(follow.follower_id, follow.entity_id).add status
        end
      end

      def self.remove_from_follower_timelines(status, follows)
        follows.each do |follow|
          general_timeline_for(follow.follower_id, follow.entity_id)
            .remove status
        end
      end

      def self.own_timeline_for(user_id, entity_id)
        Collection.new(user_id: user_id, entity_id: entity_id, kind: "own")
      end

      def self.general_timeline_for(user_id, entity_id)
        Collection.new(user_id: user_id, entity_id: entity_id, kind: "general")
      end

      def self.sanitize(resource, user)
        resource.user_id    = user.id
        resource.entity_id  = user.entity_id
        resource
      end
    end # Orchestrator
  end # Status
end # Belinkr
