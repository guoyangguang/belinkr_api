# encoding: utf-8
require "json"
require_relative "../reply/presenter"
require_relative "../tinto/utils"

module Belinkr
  module Status
    module Presenter
      class Collection
        def initialize(resource)
          @resource = resource
        end
        
        def as_poro
          @resource.map { |member| Presenter::Member.new(member).as_poro }
        end

        def as_json
          "[#{as_poro.map { |i| i.to_json }.join(",")}]"
        end
      end # Collection

      class Member
        BASE_PATH = "/statuses"

        def initialize(resource)
          @resource = resource
        end

        def as_json
          as_poro.to_json
        end

        def as_poro
          user = OpenStruct.new(timezone: "Europe/Madrid")
          local_created_at = 
            Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
          local_updated_at = 
            Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)

          {
            id:         @resource.id,
            text:       @resource.text,
            user_id:    @resource.user_id,
            created_at: local_created_at,
            updated_at: local_updated_at
          }
          .merge! mentions
          .merge! attachments
          .merge! replies
          .merge! errors
          .merge! links
          .merge! files
        end

        private

        def files
          {
            files: @resource.files
          }
        end

        def mentions
          {}
        end

        def attachments
          {}
        end

        def links
          {
            links:
              {
                self:     "#{BASE_PATH}/#{@resource.id}",
                replies:  "#{BASE_PATH}/#{@resource.id}/replies",
                user:     "/users/#{@resource.id}"
              }
          }
        end

        def errors
          @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
        end

        def replies
          { 
            replies: Reply::Presenter::Collection.new(
              Reply::Collection.new(@resource.embedded_replies)
            ).as_poro 
          }
        end
      end # Member
    end # Presenter
  end # Status
end # Belinkr
