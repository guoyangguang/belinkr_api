# encoding: utf-8
require_relative "exceptions"
require_relative "presenter"

module Tinto
  class Dispatcher
    def initialize(current_user, resource=nil, &block)
      @current_user = current_user
      @resource     = resource 
      @operation    = block
    end

    def handle(exception)
      case exception
      when Exceptions::NotAllowed       then [404]
      when Exceptions::InvalidResource  then [400, present(@resource)]
      when Exceptions::NotFound         then [404]
      else raise exception
      end
    end

    def collection
      [200, response_body]
    rescue => exception
      handle exception
    end

    def create
      [201, response_body]
    rescue => exception
      handle exception
    end

    def read
      [200, response_body]
    rescue => exception
      handle exception
    end

    def update
      [200, response_body]
    rescue => exception
      handle exception
    end

    def delete
      @operation.call
      [204]
    rescue => exception
      handle exception
    end

    private

    def response_body
      present(@operation.call)
    end

    def present(resource)
      Tinto::Presenter.determine_for(resource.class).new(resource).as_json
    end
  end # Dispatcher
end # Tinto
