# encoding: utf-8
require 'redis'
require 'json'

module Tinto
  class FileScheduler
    QUEUE_KEY = "file_mover"

    def self.enqueue(job)
      $redis.rpush QUEUE_KEY, job
    end

    def self.enqueue_for_create(*args)
      return false if args.empty?
      resource = args.first
      return false unless resource_valid?(resource)
      if resource.files.any?
        job = { action: 'create' }
        job.merge!(resource: { type: resource.class.name, id: resource.id })
        job.merge!(files: resource.files.collect { |file|
          file["original"].split("/").last
        })
        enqueue(job.to_json)
        true
      else
        false
      end
    end

    def self.enqueue_for_update(*args)
      return false if args.empty?
      old_resource, new_resource = *args
      return false unless resource_valid?(old_resource)
      return false unless resource_valid?(new_resource)
      return false if old_resource.id != new_resource.id

      if new_resource.files.any?
        files_changed = new_resource.files - old_resource.files
        if files_changed.any?
          job = { action: 'create' } # still create
          job.merge!(resource: {
            type: new_resource.class.name, id: new_resource.id
          })
          job.merge!(files: files_changed.collect { |file|
            file["original"].split("/").last
          })
          enqueue(job.to_json)
          true
        else
          false
        end
      else
        false
      end
    end

    def self.enqueue_for_delete(*args)

    end

    def self.enqueue_for_destroy(*args)

    end

    def self.resource_valid?(resource)
      return false if (!resource.respond_to?(:valid?)) || (!resource.valid?)
      return false if resource.id.nil?
      return false unless resource.respond_to?(:files)
      return false unless resource.files.is_a?(Array)
      true
    end

    # check from resource files content_type(white list)
    def self.to_pdf_job(resource)

    end
  end
end # Tinto