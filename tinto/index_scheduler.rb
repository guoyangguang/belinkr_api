# encoding: utf-8
require "redis"
require "json"

module Tinto
  class IndexScheduler
    QUEUE_KEY = "elasticsearch"

    def initialize(resource)
      @resource = resource
    end

    def schedule_for_indexing
      return unless can_index?
      $redis.rpush QUEUE_KEY, @resource.to_hash.merge(
        "_index"      => index,
        "_index_path" => index_path
      ).to_json
    end

    def can_index?
      @resource.class.const_get "MODEL_NAME"
      return true if  (@resource.respond_to? :entity_id) ||
                      (@resource.respond_to? :user_id)   ||
                      (@resource.respond_to? :search_index)
      return false
    rescue NameError
      false
    end

    def index
      return "entity_#{@resource.entity_id}" if @resource
                                              .respond_to? :entity_id
      return "user_#{@resource.user_id}"     if @resource.respond_to? :user_id
      return @resource.search_index
    end

    def index_path
      kind = @resource.class.const_get "MODEL_NAME"
      "#{kind}/#{@resource.id}"
    end
  end # IndexScheduler
end # Tinto
