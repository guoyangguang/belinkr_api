# encoding: utf-8
require "json"
require "redis"
require_relative "exceptions"
require_relative "index_scheduler"
require_relative "utils"

module Tinto
  class Member
    include Exceptions

    attr_accessor :resource

    def initialize(resource)
      @resource = resource
      read if @resource.id
    end
    
    def ==(other)
      @resource.attributes == other.attributes
    end

    def score
      @resource.updated_at ? @resource.updated_at.to_f : 0
    end

    def to_json(*args)
      @resource.attributes.to_hash.dup.to_json
    end

    def read
      raise NotFound unless json_data = $redis.get(id_key)
      @resource.attributes = JSON.parse(json_data)
      @resource
    end

    def save
      raise InvalidResource unless @resource.valid?
      this_moment                   = Time.now
      @resource.send :id=,          next_id     unless @resource.id
      @resource.send :created_at=,  this_moment unless @resource.created_at
      @resource.send :updated_at=,  this_moment 

      persist

      if @resource.respond_to?(:files) && @resource.files.any?
        Tinto::Utils.remove_from_sorted_set(
          Belinkr::Config::FILES_CLEANUP_KEY,
          @resource.files.collect { |file| file["id"] || file[:id] }
        )
      end

      @resource
    end

    def update(other)
      @resource.attributes.each do |k, v|
        unless [:created_at, :updated_at, :id, :entity_id].include? k
          @resource.send :"#{k}=", other.send(k)
        end
      end
      save
    end

    def delete
      raise InvalidResource unless @resource.valid? && @resource.created_at
      @resource.deleted_at  ||= Time.now
      persist
      @resource
    end

    def undelete
      raise InvalidResource unless @resource.valid? && @resource.deleted_at
      @resource.deleted_at = nil

      persist
      @resource
    end

    def destroy
      raise InvalidResource unless @resource.valid? && @resource.deleted_at
      $redis.del id_key

      if @resource.respond_to?(:files) && @resource.files.any?
        Tinto::Utils.add_to_sorted_set(
          Belinkr::Config::FILES_CLEANUP_KEY,
          @resource.files.collect { |file| file["id"] || file[:id] }
        )
      end

      @resource.id = nil
      @resource
    end

    private

    def persist
      $redis.set id_key, to_json
      IndexScheduler.new(@resource).schedule_for_indexing
    end

    def id_key
      "#{resource.storage_key}:#{@resource.id}"
    end

    def next_id
      $redis.incr serial_key
    end

    def serial_key
      "#{resource.storage_key}:serial"
    end
  end # Member
end # Collection
