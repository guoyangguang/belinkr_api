# encoding: utf-8
module Tinto
  module Presenter

    def self.determine_for(klass)
      raise ArgumentError, 'klass must be a Class' unless klass.is_a?(Class)
      chains = klass.name.split("::")
      chains.insert(-2, "Presenter")
      object = Object.const_get(chains.shift)
      while const = chains.shift
        object = object.const_get(const)
      end
      object.is_a?(Class) ? object : raise("resource:<#{klass.name}>'s presenter(Class) not present'")
    end

  end # Presenter
end # Tinto