# encoding: utf-8
require "tzinfo"
require "execjs"
require "bcrypt"

module Tinto
  module Utils
    def self.local_time_for(utc_time, timezone)
      TZInfo::Timezone.get(timezone)
        .utc_to_local(utc_time)
        .strftime("%Y-%m-%d %H:%M:%S")
    end

    def self.local_js_time_for(utc_time, timezone)
      local_time        = TZInfo::Timezone.get(timezone).utc_to_local(utc_time)
      seconds           = local_time.to_i 
      milliseconds      = seconds * 1000
      ExecJS.eval "(new Date(#{milliseconds}))"
    end

    def self.timezones
      TZInfo::Timezone.all_identifiers
    end

    def self.bcrypted?(password)
      BCrypt::Password.new(password)
      true
    rescue BCrypt::Errors::InvalidHash
      false
    end

    def self.in_multi?
      if $redis.client.respond_to?(:commands)
        $redis.client.commands.first == [:multi]
      else
        false
      end
    end

    def self.add_to_sorted_set(key, members = [], with_scores = false)
      raise ArgumentError, "sorted set's key is missing" if key.nil?
      members = [members] unless members.is_a?(Array)
      return if members.empty?
      $redis.multi do
        members.each do |item|
          if with_scores
            member = (item[:member] || item['member'])
            score  = item[:score] || item['score']
          else
            member, score = item, Time.now.to_f
          end
          if member && score.is_a?(Numeric)
            $redis.zadd key, score, member.to_s
          end
        end # each
      end # multi
    end # .add_to_sorted_set

    def self.remove_from_sorted_set(key, members = [])
      raise ArgumentError, "sorted set's key is missing" if key.nil?
      members = [members] unless members.is_a?(Array)
      return if members.empty?

      $redis.multi do
        members.each do |member|
          $redis.zrem key, member
        end
      end
    end # .remove_from_sorted_set

  end # Utils
end # Tinto
