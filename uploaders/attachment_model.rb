# encoding: utf-8
require 'digest'

module Belinkr
  # This model is a bit different with FileServer model
  # Currently both of POST and PUT requests save a new file(if files specify)
  class AttachmentModel

    def self.for_identity(identity)
      raise ArgumentError, "identity is missing" unless identity
      new(identity)
    end

    def initialize(identity=nil)
      @timestamp = Time.now
      @identity = identity
    end

    def random_seed
      rand
    end

    def identity
      @identity || (@identity = Digest::MD5.hexdigest(
          random_seed.to_s + @timestamp.to_f.to_s)
      )
    end

    alias :id :identity

  end # AttachmentModel
end # Belinkr