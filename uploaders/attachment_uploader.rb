# encoding: utf-8
require 'carrierwave'
require 'mini_magick'
require 'mime/types'

require 'carrierwave/processing/mime_types'

require_relative "attachment_model"

CarrierWave::SanitizedFile.sanitize_regexp= /\s/

module Belinkr
  class AttachmentUploader < CarrierWave::Uploader::Base
    permissions 0666
    include CarrierWave::MimeTypes
    include CarrierWave::MiniMagick

    #set_content_type# default override false
    process :set_content_type

    storage :file

    # here: not to use initialize->super
    def self.get_instance
      new(AttachmentModel.new)
    end

    def self.for_identity(identity)
      new(AttachmentModel.for_identity(identity))
    end

    def self.for(model)
      new(model)
    end

    def image_content_type_white_list
      %w(image/gif image/png image/jpeg)
    end

    def store_dir
      flag = storage_identity
      "public/files/#{flag}"
    end

    def filename
      storage_identity
    end

    def storage_identity
      model_validate!
      if model.respond_to?(:id)
        model.id
      elsif model.respond_to?(:identity)
        model.identity
      end
    end

    def image?(file=file)
      if file && !file.content_type.empty?
        image_content_type_white_list.include?(file.content_type)
      else
        false
      end
    end

    version :mini_thumbnail, :if => :image? do
      process :resize_to_fit => [32, 32]
    end

    version :thumb, :if => :image? do
      process :resize_to_fit => [128, 128]
    end

    version :preview, :if => :image? do
      process :resize_to_fit => [800, 600]
    end

    version :medium, :if => :image? do
      process :resize_to_fit => [320, 320]
    end

    private
    def model_validate!
      raise "model not present" unless model
      unless model.respond_to?(:id) || model.respond_to?(:identity)
        raise "model incorrect"
      end
    end

  end # AttachementUploader
end # Belinkr