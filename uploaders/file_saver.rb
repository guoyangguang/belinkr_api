# encoding: utf-8
require 'json'
require 'fileutils'

require_relative "attachment_uploader"

module Belinkr
  class FileSaver
    def self.chew!(files)
      # raise or rescue error message return to JSON?
      raise ArgumentError, 'files must be a array' unless files.is_a?(Array)
      raise ArgumentError, 'files is empty' unless files.any?
      errors = []
      upload_store_dirs = []
      uploaded = files.inject([]) do |ary, file|
        begin
          if errors.empty?
            result = {
                storage: :local,
                original_filename: file[:filename],
                content_type: file[:type]
            }
            uploader = AttachmentUploader.get_instance
            upload_store_dirs << uploader.store_dir
            uploader.store!(file)

            do_nothing! #dont remove!

            result.merge!(id: uploader.storage_identity)
            result.merge!(original: uploader.store_path)
            result.merge!(size: uploader.file.size)
            if uploader.file.content_type
              result.update(content_type: uploader.file.content_type)
            end
            if uploader.image?
              version_list = AttachmentUploader.versions
                          .keys.inject([]) do |version_ary, version|
                version_loader = uploader.send(version)
                hash = {
                  name: version,
                  original: version_loader.store_path,
                  size: version_loader.file.size
                }
                version_ary << hash
                version_ary
              end
              result.merge!(versions: version_list)
            end
            ary << result
          end
        rescue Exception => e
           errors << "#{file[:filename]} upload error: #{e.message}"
        end
        ary
      end

      if errors.empty?
        uploaded.to_json
      else
        upload_store_dirs.each { |store_dir| remove_dir(store_dir) }
        { error: errors.first }.to_json
      end

    end # .chew!

    # stub for help testing, dont remove!
    def self.do_nothing!;end

    def self.remove_dir(store_dir)
      begin
        FileUtils.rm_r store_dir if Dir.exist?(store_dir)
      rescue Exception => e
        e.message # LOGGER required
      end
    end
  end # FileSaver
end # Belinkr