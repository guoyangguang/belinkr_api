# encoding: utf-8
require "csv"
require_relative "member"
require_relative "orchestrator"
require_relative "../tinto/exceptions"

module Belinkr
  module User
    class BatchLoader
      def self.load(csv_file, entity_id)
        new_users = []
        CSV.open(csv_file).each do |line|
          user = User::Member.new(
            first: line[0],
            last: line[1],
            entity_id: entity_id
          )
          raise Tinto::Exceptions::InvalidResource unless user.valid?
          new_users << user
        end
        new_users.each do |user|
          User::Orchestrator.create(user)
        end
      end
    end # Orchestrator
  end # Invitation
end # Belinkr
