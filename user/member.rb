# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "../tinto/member"
require_relative "../tinto/utils"
require_relative "../config"

module Belinkr
  module User
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "user"

      attribute :id,              Integer
      attribute :first,           String
      attribute :last,            String
      attribute :entity_id,       Integer
      attribute :timezone,        String, default: Config::DEFAULT_TIMEZONE
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of       :first, :last, :entity_id
      validates_length_of         :first, min: 3, max: 150
      validates_length_of         :last, min: 3, max: 150
      validates_numericalness_of  :entity_id
      validates_within            :timezone, set: Tinto::Utils.timezones

      def_delegators :@member, :==, :score, :to_json, :read, :save, :delete
      alias_method :to_hash, :attributes

      def initialize(*args)
        super *args
        @member = Tinto::Member.new self
      end

      def storage_key
        "entities:#{entity_id}:users"
      end
    end # Member
  end # User
end # Belinkr
