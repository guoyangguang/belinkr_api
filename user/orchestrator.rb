# encoding: utf-8
require 'i18n'
require_relative "collection"
require_relative "../status/collection"
require_relative "../scrapbook/orchestrator"

module Belinkr
  module User
    class Orchestrator
      def self.create(user)
        user.save

        raise "user timelines already exist" if 
          general_timeline_for(user).exists? || own_timeline_for(user).exists?

        User::Collection.new(entity_id: user.entity_id).add user
        create_default_scrapbooks_for user
        user
      end

      def self.followers(follows, user)
        follower_ids = follows.map(&:follower_id)
        User::Collection.new({entity_id: user.entity_id}, follower_ids)
      end

      def self.followings(follows, user)
        followed_ids = follows.map(&:followed_id)
        User::Collection.new({entity_id: user.entity_id}, followed_ids)
      end

      private

      def self.general_timeline_for(user)
        Status::Collection.new(
          kind:       "own", 
          user_id:    user.id, 
          entity_id:  user.entity_id
        )
      end

      def self.own_timeline_for(user)
        Status::Collection.new(
          kind:       "general", 
          user_id:    user.id, 
          entity_id:  user.entity_id
        )
      end

      def self.create_default_scrapbooks_for(user)
        Config::INITIAL_SCRAPBOOKS.each do |name|
          Scrapbook::Orchestrator.create(
              Scrapbook::Member.new(
                  user_id: user.id, name: I18n.t("scrapbook.#{name}")
              ), user
          )
        end
      end
    end # Orchestrator
  end # User
end # Belinkr
