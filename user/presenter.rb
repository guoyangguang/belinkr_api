# encoding: utf-8
require "json"
require_relative "../tinto/utils"

module Belinkr
  module User
    module Presenter
      class Collection
        def initialize(resource)
          @resource = resource
        end

        def as_poro
          @resource.all.map { |member| Presenter::Member.new(member).as_poro }
        end

        def as_json
          "[#{as_poro.map { |i| i.to_json }.join(",")}]"
        end
      end #Collection

      class Member
        attr_accessor :resource
        BASE_PATH = '/users'

        def initialize(resource)
          @resource = resource
        end

        def as_json
          as_poro.to_json
        end

        def as_poro
          user = OpenStruct.new(timezone: "Europe/Madrid")
          local_created_at =
            Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
          local_updated_at =
            Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)
          
          {
            id:                @resource.id,
            first:             @resource.first,
            last:              @resource.last,
            created_at:        local_created_at,
            updated_at:        local_updated_at
          }.merge! errors
           .merge! links
        end

        private

        def links
          user_base_path = "#{BASE_PATH}/#{@resource.id}"
          {
              self: user_base_path,
              timeline: "#{user_base_path}/statuses",
              followers: "#{user_base_path}/followers",
              following: "#{user_base_path}/following"
          }
        end

        def errors
          @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
        end
      end #Member
    end # Presenter
  end # User
end # Belinkr
