# encoding: utf-8
module Belinkr
  module Config
    class FileMover

      class << self
        attr_accessor :api_url, :request_params, :files_root, :root
      end

      def self.configure
        yield self
      end

      def self.use_defaults!
        configure do |cfg|
          cfg.api_url     = "http://localhost:4000"
          cfg.request_params = {
              post: :files,
              delete: :id,
              put: { id: :id, file: :file }
          }
          cfg.files_root  = File.expand_path('.', Dir.pwd)
          cfg.root        = File.expand_path('.', Dir.pwd)
        end
      end

      def self.suite!(config_root=nil)
        use_defaults!
        self.root = config_root.to_s if config_root
        paths = [
            File.expand_path("config/file_mover.rb", root),
            File.expand_path(".file_mover", root),
            "#{ENV["HOME"]}/.file_mover"
        ]
        paths.each { |path| load(path) if File.exist?(path) }
      end

    end # FileMover
  end # Config
end # Belinkr

Belinkr::Config::FileMover.suite!