# encoding: utf-8
require "eventmachine"
require "typhoeus"
require "fileutils"
require "json"

require_relative "config"

# TODO LIST
#  add resource files key_name to FileMoverConfig. for eg: update_resource
#  method: resource_read from RESOURCE(for update the resource)?
#  file_server GET PUT DELETE with exception

module Belinkr
  class FileMover

    SELF_CONFIG = Belinkr::Config::FileMover

    def self.next
      msg = dequeue
      operate(msg) unless msg.to_s.empty?
      EM.next_tick(&method(:next))
    end

    def self.dequeue

    end

    def self.operate(msg)
      pkg = JSON.parse(msg)
      action = pkg.delete("action")
      send(action.to_sym, pkg)
    end

    # JSON request
    def self.request_json(url, params)
      Typhoeus::Request.run(
        url,
        params.merge!(headers: {
          "Content-type" => 'application/json',
          'Accept' => "application/json"}
        )
      )
    end

    # This used for deliver messages to administrators
    # file logger || mail || sms ?
    def self.message_notify(msg, type="error")
      # pending
    end

    # Update the resource with file changes
    # options { attr: value }
    # Orchestrator? System USER?
    def self.update_resource(resource, options)
      # pending
    end

    # Remove local storage folders
    def self.remove_local_storage_folders(folders)
      folders.collect! {|folder|
        path = File.join(SELF_CONFIG.files_root, folder)
        Dir.exist?(path) ? path : nil
      }
      folders.compact!
      folders.each do |folder|
        FileUtils.rm_r folder
      end
      true
    rescue Exception => e
      message_notify e.message
    end

    # CREATE TODO:
    # 1.update_resource for create just update the storage mark(local/remote)
    # 2 request_json add :wurf param(value: 'belinkr')
    ## options[Hash]
    #   files : Array of file md5s
    #   resource : { type: 'Status', id: 1 }
    def self.create(options)
      check_options_for_create!(options)
      option_files = options.delete("files")
      files = local_storage_files(option_files)
      params = { params: {} }
      files_hash = files.each_with_index.inject({}) do |hash, (file, index)|
        hash[index] = File.new(file)
        hash
      end
      params[:params].update(
          SELF_CONFIG.request_params[:post].to_sym => files_hash
      )
      params[:method] = :post
      response = request_json(SELF_CONFIG.api_url, params)
      result = JSON.parse(response.body)

      if result.is_a?(Hash) && result["error"]
        message_notify(result["error"])
      elsif result.is_a?(Array) && result.any?
        if update_resource(options.delete("resource"), { files: result })
          remove_local_storage_folders(option_files)
        end
      end

    rescue Exception => e
      message_notify(e.message)
    end

    ## options[Hash]
    #  file: file's md5
    def self.update(options)
      check_options_for_update!(options)
      file = options.delete("file")
      # if raise, let it be.
      local_file = local_storage_files(file).first
      params = { params: {} }
      params[:params].update(
          SELF_CONFIG.request_params[:put][:id].to_sym => file,
          SELF_CONFIG.request_params[:put][:file].to_sym => File.new(local_file)
      )
      params[:method] = :put

      response = request_json(SELF_CONFIG.api_url, params)
      result = JSON.parse(response.body)
      if result.is_a?(Hash)
        if result["error"]
          message_notify(result["error"])
        else
          if update_resource(options.delete("resource"), { files: result })
            remove_local_storage_folders([file])
          end
        end
      else
        raise "PUT request returns an error specifications"
      end
    rescue Exception => e
      message_notify e.message
    end # .update

    ## options[Hash]
    #  file: file's md5
    def self.delete(options)
      check_options_for_delete!(options)
      file = options.delete("file")
      # delete local storage file
      # it might already deleted by .create >>> check again
      remove_local_storage_folders([file])

      # delete the remote file
      params = { params: {
          SELF_CONFIG.request_params[:delete].to_sym => file
      }}
      params[:method] = :delete
      response = request_json(SELF_CONFIG.api_url, params)
      if response.code != 204
        result = JSON.parse(response.body)
        message_notify(result["error"]) if result.is_a?(Hash) && result["error"]
      end
    rescue Exception => e
      message_notify(e.message)
    end

    ## options[Hash]
    #   files : Array of file md5s
    def self.destroy(options)
      check_options_for_destroy!(options)
      files = options.delete('files')
      files.each { |file| delete(file) }
    rescue Exception => e
      message_notify(e.message)
    end

    # Check the options is a Hash
    def self.check_options_is_hash!(options)
      raise ArgumentError, "Arguments must be a Hash" unless options.is_a?(Hash)
    end

    # Check the options contains a resource
    def self.check_options_resource!(options)
      unless options.has_key?("resource")
        raise ArgumentError, "Resource Must be present"
      end

      if options["resource"]["type"].nil? || options["resource"]["id"].nil?
        raise ArgumentError, "Resource Must be present"
      end
    end

    # Check POST request file fields
    def self.check_options_for_create!(options)
      check_options_is_hash!(options)
      check_options_resource!(options)
      raise ArgumentError, "POST files missing" unless options.has_key?("files")

      unless options["files"].is_a?(Array)
        raise ArgumentError, "POST files must be a Array"
      end

      if options["files"].empty?
        raise ArgumentError, "POST files can not be empty"
      end
    end

    def self.check_options_for_update!(options)
      check_options_is_hash!(options)
      check_options_resource!(options)
      raise ArgumentError, "UPDATE file missing" unless options.has_key?("file")
    end

    def self.check_options_for_delete!(options)
      check_options_is_hash!(options)
      raise ArgumentError, "DELETE file missing" unless options.has_key?("file")
    end

    def self.check_options_for_destroy!(options)
      check_options_is_hash!(options)
      unless options.has_key?("files")
        raise ArgumentError, "DESTROY files missing"
      end

      unless options["files"].is_a?(Array)
        raise ArgumentError, "DESTROY files must be a Array"
      end

      if options["files"].empty?
        raise ArgumentError, "DESTROY files can not be empty"
      end
    end

    # Get local storage files
    def self.local_storage_files(files)
      files = [files] unless files.is_a?(Array)
      files = files.collect { |file|
        File.join(SELF_CONFIG.files_root, file, file)
      }.select { |file| File.exist?(file) }
      files.empty? ? raise('local files was empty') : files
    end

  end # FileMover
end # Belinkr

EM.run do
  Belinkr::FileMover.next
end if ENV["WORKER_ENV"] == "production"
