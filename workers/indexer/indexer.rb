# encoding: utf-8
require "eventmachine"
require "em-hiredis"
require "typhoeus"
require "json"

module Belinkr
  class Indexer
    def self.redis
      @redis ||= EM::Hiredis.connect("redis://localhost:6379")
    end

    def self.next
      redis.blpop("elasticsearch", 0).callback do |list, json|
        resource  = JSON.parse(json)
        path      = resource.delete "_index_path"
        index     = resource.delete "_index"

        p "#{index}/#{path}"
        response = Typhoeus::Request.put(
          "http://localhost:9200/#{index}/#{path}", body: resource.to_json
        )
      end
      EM.next_tick(&method(:next))
    end
  end
end

EM.run do
  Belinkr::Indexer.next
end 
