# encoding: utf-8
require "cassandra"

module Belinkr 
  module Worker
    class Logger

      KEY_SPACE =  "Belinkr"

      def self.persist(activity_in_json)
        unless client.column_families.include? key
          client.add_column_family(column_family) 
        end
        activity = JSON.parse(activity_in_json)
        row_key  = activity.delete("id").to_s
        sanitized_activity = sanitize(activity)
        client.insert(key.to_sym, row_key, sanitized_activity)
        row_key
      end

      def self.sanitize(activity)
        h      = {}
        activity.each do |k, v|
          v    = v.to_json if v.kind_of? Hash
          h[k] = v unless v.nil?
        end
        h
      end

      #there are two kinds of CF: Standard and Super,
      #whereas the default is Standard
      def self.column_family
        Cassandra::ColumnFamily.new(:keyspace => KEY_SPACE,
                                    :name     => key)
      end

      def self.key
        "Activities"
      end

      def self.client
        @client ||= Cassandra.new(KEY_SPACE)
      end

    end #Logger
  end #Worker
end #Belinkr
