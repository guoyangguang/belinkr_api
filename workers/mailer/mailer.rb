# encoding: utf-8
require "eventmachine"
require "em-hiredis"
require "pony"
require "json"
require_relative "../../config"
require_relative "renderer"

module Belinkr
  module Worker
    class Mailer
      def self.redis
        @redis ||= EM::Hiredis.connect("redis://localhost:6379")
      end

      def self.next
        redis.blpop("sendmail", 0).callback do |list, json|
          message       = JSON.parse(json)
          template_file = template_file_for(message)
          substitutions = message.fetch("substitutions")

          html_body = Renderer.new(template_file, substitutions).render

          Pony.mail(
            from:               message.fetch("from"),
            to:                 message.fetch("to"),
            subject:            "test",
            html_body:          html_body,
            #charset:            "UTF-8",
            #text_part_charset:  "UTF-8",
            via:                :smtp,
            via_options:        Config::SMTP
          )
        end
        EM.next_tick(&method(:next))
      end

      def self.template_file_for(message)
        template  = message.fetch("template").to_sym
        locale    = message.fetch("locale").to_sym

        Config::MAILER_TEMPLATES.fetch(template).fetch(locale)
      end
    end # Mailer
  end # Worker
end # Belinkr

EM.run do
  Belinkr::Worker::Mailer.next
end 
