# encoding: utf-8
require "rinku"

module Belinkr
  module Worker
    class Mailer
      class Renderer
        def initialize(template, substitutions={})
          @template       = template.dup
          @substitutions  = substitutions
        end
          
        def render(substitutions={})
          check_substitutions_for(@template)
          @substitutions.each { |k, v| @template.gsub! "{{#{k}}}", v }
          Rinku.auto_link(@template)
        end


        def check_substitutions_for(text)
          needed = text.scan(/{{[\w_]+}}/).map { |k| k.gsub!(/[{|}]/, "") }
          passed = @substitutions.keys.map(&:to_s)

          (needed - passed).each do |k|
            raise ArgumentError, "a substitution for #{k} is required"
          end
          
          (passed - needed).each do |k|
            raise ArgumentError, "#{k} is not used in the template"
          end
        end
      end # Renderer
    end # Mailer
  end # Worker
end # Belinkr
