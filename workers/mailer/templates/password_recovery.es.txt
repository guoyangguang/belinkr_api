Estimado {{user_name}}:

Hemos recibido una petición para restablecer su contraseña.

Si ha iniciado esta petición, por favor haga click en {{password_reset_link}} para restablecer su contraseña.

Si usted no ha iniciado esta petición, por favor ignore este mensaje. Su cuenta está segura y en perfecto estado. Estaremos encantados de atender sus preguntas en nuestra área de soporte: http://help.belinkr.com

Atentamente,

El equipo de belinkr.
