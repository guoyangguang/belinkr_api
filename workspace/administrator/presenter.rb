# encoding: utf-8
require "json"
require_relative "../../tinto/utils"

module Belinkr
  module Workspace
    module Administrator
      module Presenter 
        include Belinkr::User::Presenter
      end # Presenter
    end # Administrator
  end # Workspace
end # Belinkr
