#encoding: utf-8
require_relative "../../tinto/exceptions"

module Belinkr
  module Workspace
    module Autoinvitation
      class Enforcer
        include Tinto::Exceptions

        def initialize(workspace, autoinvitation)
          @workspace, @autoinvitation = workspace, autoinvitation
        end

        def self.authorize(user, action, workspace, autoinvitation)
          new(workspace, autoinvitation).send(:"#{action}_by?", user)
        end
       
        private

        def request_by?(user)
          raise NotAllowed unless user.id
          raise NotAllowed unless user.entity_id == @workspace.entity_id

          raise NotAllowed if admins_for(@workspace).include?(user) || 
            collaborators_for(@workspace).include?(user)
        end
        
        def allow_by?(user)
          raise NotAllowed unless user.id
          raise NotAllowed unless user.entity_id == @workspace.entity_id &&
            user.entity_id == @autoinvitation.entity_id 
          
          raise NotAllowed unless @workspace.id == @autoinvitation.workspace_id
          
          raise NotAllowed unless admins_for(@workspace).include?(user)  
        end
      
        alias_method :deny_by?, :allow_by?

        def collaborators_for(workspace)
          Workspace::Collaborator::Collection.new(
            entity_id: workspace.entity_id, workspace_id: workspace.id)
        end

        def admins_for(workspace)
          Workspace::Administrator::Collection.new(
            entity_id: workspace.entity_id, workspace_id: workspace.id)
        end
      end#Enforcer
    end
  end
end
