#encoding: utf-8

require 'virtus'
require 'aequitas'
require 'forwardable'
require "statemachine"

require_relative '../../tinto/member'

module Belinkr
  module Workspace
    module Autoinvitation
      class Member
        include Virtus
        include Aequitas
        extend Forwardable

        MODEL_NAME = 'autoinvitation'

        STATES = %w{ requested allowed denied }

        attribute :id,            Integer
        attribute :entity_id,     Integer
        attribute :workspace_id,  Integer
        attribute :requesting_id, Integer
        attribute :state,         String
        attribute :denied_at,     Time
        attribute :created_at,    Time
        attribute :updated_at,    Time
        attribute :deleted_at,    Time

        validates_presence_of :entity_id, :workspace_id, :requesting_id
        validates_numericalness_of :entity_id, :workspace_id, :requesting_id
        validates_within :state, set: STATES                           
        
        alias_method :to_hash, :attributes
        
        def_delegators :@state_machine, :allow
        def_delegators :@member, :==, :score, :to_json, :read, :save


        def initialize(*args)
          super(*args)
          @member = Tinto::Member.new(self)

          @state_machine = Statemachine.build do
            state :requested
            state :allowed
            state :denied

            trans :requested, :allow, :allowed
          end

          @state_machine.state = @state if @state
        end

        def state
          sync_state
          super
        end

        def state=(state)
          @state_machine.state = state if @state_machine
          super(state)
        end

        def allowed? 
          state == 'allowed'
        end
        
        def deny
          self.state = 'denied'
          self.denied_at = Time.now
          self.save
        end
        
        def denied?
          state == 'denied'
        end

        def member_init_params
          { entity_id: entity_id, workspace_id: workspace_id }
        end

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:autoinvitations"
        end

        private

        def sync_state
          self.state = @state_machine.state.to_s
        end

      end#Member
    end
  end
end
