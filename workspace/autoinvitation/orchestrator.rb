# encoding: utf-8
require_relative "collection"
require_relative "enforcer"
require_relative "../../user/member"
require_relative "../collaborator/collection"
require_relative "../../activity/collection"

module Belinkr
  module Workspace
    module Autoinvitation
      class Orchestrator
        def self.collection(workspace, autoinvitations, user)
          #TODO enforcer
          autoinvitations
        end

        def self.request(workspace, autoinvitation, user)
          sanitize(workspace, autoinvitation, user)
          Enforcer.authorize(user, :request, workspace, autoinvitation)
          autoinvitation.save
          
          activity = Activity::Member.new(
                       actor:  user,
                       action: "add",
                       object: autoinvitation
                    ).save

          $redis.multi do
            autoinvitations_for(workspace).add(autoinvitation)
            activities.add activity
          end
          autoinvitation
        end
        
        def self.allow(workspace, autoinvitation, admin)
          sanitize(workspace, autoinvitation, admin)
          Enforcer.authorize(admin, :allow, workspace, autoinvitation)
          
          collaborator = Belinkr::User::Member.new(
            id:         autoinvitation.requesting_id, 
            entity_id:  workspace.entity_id
          )
          autoinvitation.allow
          autoinvitation.save

          activity = Activity::Member.new(
                       actor:  admin,
                       action: "allow",
                       object: autoinvitation
                    ).save
          activity1 = Activity::Member.new(
                       actor:  collaborator,
                       action: "join",
                       object: workspace
                    ).save

          $redis.multi do
            collaborators_for(workspace).add collaborator
            activities.add activity
            activities.add activity1
          end

          autoinvitation
        end

        def self.deny(workspace, autoinvitation, admin)
          sanitize(workspace, autoinvitation, admin)
          Enforcer.authorize(admin, :deny, workspace, autoinvitation)
          autoinvitation.deny

          activity = Activity::Member.new(
                       actor:  admin,
                       action: "deny",
                       object: autoinvitation
                    ).save

          $redis.multi do
            activities.add activity
          end

          autoinvitation
        end

        private

        def self.sanitize(workspace, autoinvitation, user)
          autoinvitation.entity_id      = user.entity_id
          autoinvitation.workspace_id   = workspace.id
          autoinvitation.requesting_id  ||= user.id
          autoinvitation.state          = "requested"
          autoinvitation
        end

        def self.autoinvitations_for(workspace)
          Collection.new(
            entity_id:    workspace.entity_id,
            workspace_id: workspace.id
          )
        end
         
        def self.collaborators_for(workspace)
          Belinkr::Workspace::Collaborator::Collection
            .new(entity_id: workspace.entity_id, workspace_id: workspace.id) 
        end

      def self.activities
        Activity::Collection.new
      end
      end # Orchestrator
    end # Autoinvitation
  end # Workspace
end # Belinkr
