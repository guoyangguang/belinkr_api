# encoding: utf-8
require 'forwardable'
require 'virtus'
require 'aequitas'
require_relative '../../tinto/sorted_set'
require_relative 'member'


module Belinkr
  module Workspace
    module Board
      class Collection
        extend Forwardable
        include Enumerable
        include Virtus
        include Aequitas

        MODEL_NAME = 'board'

        attribute :workspace_id,    Integer
        attribute :entity_id,       Integer

        validates_presence_of       :workspace_id, :entity_id
        validates_numericalness_of  :workspace_id, :entity_id

        def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                       :exists?, :all, :page, :<<, :add, :remove, :delete,
                       :merge, :score

        def initialize(*args)
          super *args
          @sorted_set = Tinto::SortedSet.new(self, Board::Member, storage_key)
        end

        def member_init_params
          { entity_id: entity_id, workspace_id: workspace_id }
        end

        private

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:boards"
        end
      end # class Collection
    end # module Board
  end # module Workspace
end # module Belinkr
