# encoding: utf-8
require_relative '../../tinto/exceptions'
require_relative "../collaborator/collection"
require_relative "../administrator/collection"

module Belinkr
  module Workspace
    module Board
      class Enforcer
        include Tinto::Exceptions

        def initialize(workspace, resource)
          @workspace = workspace
          @resource = resource
        end

        def self.authorize(user, action, workspace, resource)
          new(workspace, resource).send :"#{action}_by?", user
        end

        def create_by?(user)
          raise NotAllowed unless user.id
          raise NotAllowed unless user.entity_id == @workspace.entity_id &&  
             user.entity_id == @resource.entity_id

          raise NotAllowed unless @resource.workspace_id == @workspace.id
          raise NotAllowed unless admins_for(@workspace).include? user 
        end
       
        def read_by?(user)
          raise NotAllowed unless user.id
          raise NotAllowed unless user.entity_id == @workspace.entity_id &&
            user.entity_id == @resource.entity_id

          raise NotAllowed unless @resource.workspace_id == @workspace.id
          raise NotAllowed unless collaborators_for(@workspace).include?(user) ||
            admins_for(@workspace).include?(user)
        end

        alias_method :update_by?, :create_by?
        alias_method :delete_by?, :create_by?
        alias_method :destroy_by?, :create_by?
        alias_method :undelete_by?, :create_by?
        alias_method :collection_by?, :read_by?
        
        private

        def collaborators_for(workspace)
          Workspace::Collaborator::Collection.new(
            entity_id: workspace.entity_id, workspace_id: workspace.id)
        end

        def admins_for(workspace)
          Workspace::Administrator::Collection.new(
            entity_id: workspace.entity_id, workspace_id: workspace.id)
        end
      end # class Enforcer
    end # module Board
  end # module Workspace
end
