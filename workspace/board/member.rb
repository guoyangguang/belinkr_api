# encoding: utf-8
require 'forwardable'
require 'virtus'
require 'aequitas'
require_relative '../../tinto/member'
require_relative '../../tinto/exceptions'


module Belinkr
  module Workspace
    module Board
      class Member
        extend Forwardable
        include Virtus
        include Aequitas

        MODEL_NAME = 'board'

        attribute :id,              Integer
        attribute :name,            String
        attribute :workspace_id,    Integer
        attribute :user_id,         Integer
        attribute :entity_id,       Integer
        attribute :created_at,      Time
        attribute :updated_at,      Time
        attribute :deleted_at,      Time

        validates_presence_of       :name, :workspace_id, :user_id, :entity_id
        validates_length_of         :name, min: 3, max: 250
        validates_numericalness_of  :user_id, :workspace_id, :entity_id

        def_delegators :@member, :==, :score, :to_json, :read, :save,
                                  :delete, :undelete, :destroy

        alias_method :to_hash, :attributes

        def initialize(*args)
          super *args
          @member = Tinto::Member.new self
        end

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:boards"
        end
      end # Member
    end # Board
  end # Workspace
end # Belinkr
