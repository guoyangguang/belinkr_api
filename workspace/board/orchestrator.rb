# encoding: utf-8
require_relative 'collection'
require_relative '../../tinto/exceptions'
require_relative 'enforcer'


module Belinkr
  module Workspace
    module Board
      class Orchestrator

        def self.create(workspace, board, user)
          board = sanitize(workspace, board, user)
          Enforcer.authorize user, :create, workspace, board 
          board.save
          boards_for(workspace).add board
          board
        end

        def self.update(workspace, board, updated, user)
          sanitized = sanitize(workspace, updated, user) 
          sanitized.id = board.id
          Enforcer.authorize user, :update, workspace, sanitized 
          board.attributes = sanitized.attributes
          board.save
          board
        end

        def self.delete(workspace, board, user)
          Enforcer.authorize user, :delete, workspace, board
          board.delete
          boards_for(workspace).remove board
          board
        end
        
        def self.destroy(workspace, board, user)
          Enforcer.authorize user, :destroy, workspace, board
          $redis.multi do
            board.delete
            boards_for(workspace).remove board
            board.destroy
          end
          board
        end
      
        def self.undelete(workspace, board, user)
          Enforcer.authorize user, :undelete, workspace, board
          $redis.multi do
            board.undelete
            boards_for(workspace).add(board)
          end
          board
        end
       
        def self.read(workspace, board, user)
          Enforcer.authorize user, :read, workspace, board
          board
        end
       
        def self.collection(workspace, boards, user)
          Enforcer.authorize(user, :collection, workspace, boards)
          boards
        end
        private

        def self.boards_for(workspace)
          Collection.new(workspace_id: workspace.id,
                         entity_id: workspace.entity_id
                        )
        end
         
        def self.sanitize(workspace, board, user)
          board.entity_id = user.entity_id
          board.workspace_id = workspace.id
          board.user_id = user.id
          board
        end

      end # class Orchestrator
    end # module Board
  end # module Workspace
end # module Belinkr
