#encoding: utf-8

require_relative "member"
require_relative "../../../tinto/sorted_set" 

module Belinkr
  module Workspace
    module Board
      module Story
        class Collection
          include Virtus
          include Aequitas
          extend Forwardable 
          include Enumerable

          MODEL_NAME = "story" 

          attribute :entity_id, Integer
          attribute :workspace_id, Integer
          attribute :board_id, Integer
          
          validates_presence_of :entity_id, :workspace_id, :board_id
          validates_numericalness_of :entity_id, :workspace_id, :board_id

          def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                         :exists?, :all, :page, :<<, :add, :remove, :delete, 
                         :merge, :score

          def initialize(*args)
            super(*args)
            @sorted_set = Tinto::SortedSet.new(self, Story::Member, storage_key)
          end

          private
          def storage_key
            "entities:#{entity_id}:workspaces:#{workspace_id}:boards:#{board_id}:stories"
          end
        end#Collection
      end
    end
  end
end
