#encoding: utf-8

require_relative '../../../tinto/exceptions'
require_relative '../../administrator/collection'
require_relative '../../collaborator/collection'

module Belinkr
  module Workspace
    module Board
      module Story
        class Enforcer
          include Tinto::Exceptions

          def initialize(workspace, board, story)
            @workspace, @board, @story = workspace, board, story
          end

          def self.authorize(user, action, workspace, board, story )
            new(workspace, board, story).send(:"#{action}_by?", user)
          end

          def create_by?(user)
            raise NotAllowed unless user.id
            raise NotAllowed unless user.entity_id == @workspace.entity_id &&
              user.entity_id == @board.entity_id && user.entity_id == @story.entity_id
            
            raise NotAllowed unless @workspace.id == @board.workspace_id &&
              @workspace.id == @story.workspace_id
            
            raise NotAllowed unless @board.id == @story.board_id
            raise NotAllowed unless admins_for(@workspace).include? user            
          end
          
          alias_method :delete_by?, :create_by?
          alias_method :undelete_by?, :create_by?
          alias_method :destroy_by?, :create_by?
         
         def read_by?(user)
           raise NotAllowed unless user.id
           raise NotAllowed unless user.entity_id == @workspace.entity_id &&
             user.entity_id == @board.entity_id && user.entity_id == @story.entity_id
           
           raise NotAllowed unless @workspace.id == @board.workspace_id &&
             @workspace.id == @story.workspace_id
           
           raise NotAllowed unless @board.id == @story.board_id
           raise NotAllowed unless admins_for(@workspace).include?(user) ||
             collaborators_for(@workspace).include?(user)
         end

        private

        def collaborators_for(workspace)
          Workspace::Collaborator::Collection.new(
            entity_id: workspace.entity_id, workspace_id: workspace.id)
        end

        def admins_for(workspace)
          Workspace::Administrator::Collection.new(
            entity_id: workspace.entity_id, workspace_id: workspace.id)
        end

        end #Enforcer
      end
    end
  end
end
