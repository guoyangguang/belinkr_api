#encoding: utf-8

require 'virtus'
require 'aequitas'
require 'forwardable'
require_relative '../../../tinto/member'

module Belinkr
  module Workspace
    module Board
      module Story
        class Member
          include Virtus
          include Aequitas
          extend Forwardable

          MODEL_NAME = 'story'
          
          attribute :id, Integer
          attribute :entity_id, Integer
          attribute :workspace_id, Integer
          attribute :board_id, Integer
          attribute :name, String
          attribute :start, Date
          attribute :end, Date
          attribute :created_at, Time
          attribute :updated_at, Time
          attribute :deleted_at, Time
          
          validates_presence_of :entity_id, :workspace_id, :board_id, :name, 
                                :start, :end
          validates_numericalness_of :entity_id, :workspace_id, :board_id
          validates_length_of :name, min: 3, max: 200

          def_delegators :@member, :==, :score, :to_json, :read, :save, :update, 
                                :delete, :undelete, :destroy             
          alias_method :to_hash, :attributes

          def initialize(*args)
            super *args
            @member = Tinto::Member.new self
          end

          def storage_key
            "entities:#{entity_id}:workspaces:#{workspace_id}:boards:#{board_id}:stories"
          end
        end # Member
      end #Story
    end # Board
  end # Workspace
end # Belinkr
