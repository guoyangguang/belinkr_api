#encoding: utf-8

require_relative "collection"
require_relative 'enforcer'

module Belinkr
  module Workspace
    module Board
      module Story
        class Orchestrator
          
          def self.create(workspace, board, story, user)
            #TODO how to rollback here? 
            story = sanitize(workspace, board, story, user)
            Enforcer.authorize(user, :create, workspace, board, story)
            story.save
            stories_for(board).add(story)
            story 
          end

          private

          def self.stories_for(board)
            Collection.new(entity_id: board.entity_id, 
              workspace_id: board.workspace_id, board_id: board.id)
          end

          def self.sanitize(workspace, board, story, user)
            story.entity_id = user.entity_id
            story.workspace_id = workspace.id
            story.board_id = board.id
            story
          end
        end#Orchestrator
      end
    end
  end
end
