# encoding: utf-8
require "json"
require_relative "../../tinto/utils"

module Belinkr
  module Workspace
    module Collaborator
      module Presenter
        include Belinkr::User::Presenter
      end # Presenter
    end # Collaborator
  end # Workspace
end # Belinkr
