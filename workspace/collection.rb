# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../tinto/sorted_set"

module Belinkr
  module Workspace
    class Collection
      extend Forwardable
      include Virtus
      include Aequitas
      include Enumerable
      
      MODEL_NAME = "workspace"

      attribute                   :entity_id, Integer

      validates_presence_of       :entity_id
      validates_numericalness_of  :entity_id

      def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                     :exists?, :all, :page, :<<, :add, :remove, :delete, 
                     :merge, :score

      def initialize(*args)
        super *args
        @sorted_set = Tinto::SortedSet.new(self, Workspace::Member, storage_key)
      end

      def member_init_params
        { entity_id: entity_id }
      end

      private

      def storage_key
        "entities:#{entity_id}:workspaces"
      end
    end # Collection
  end # Workspace
end # Belinkr
