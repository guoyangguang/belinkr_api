# encoding: utf-8
require_relative "../workspace/administrator/collection"
require_relative "../tinto/exceptions"

module Belinkr
  module Workspace
    class Enforcer
      include Tinto::Exceptions

      def initialize(workspace)
        @workspace = workspace
      end

      def promote?(promoter, promoted)
        administrators  = Administrator::Collection.new(
                            workspace_id: @workspace.id, 
                            entity_id:    @workspace.entity_id
                          )

        raise NotAllowed unless administrators.include?(promoter)
      end

      alias_method :demote?, :promote?

      def leave?(user)
        raise NotAllowed if 
          administrators_for(@workspace).include?(user) &&
          administrators_for(@workspace).length <= 1
      end

      def remove?(remover, removed)
        promote?(remover, nil)
        raise NotAllowed if remover.id == removed.id
      end

      def administrators_for(workspace)
        Workspace::Administrator::Collection
          .new(workspace_id: workspace.id, entity_id: workspace.entity_id)
      end
    end # Enforcer
  end # Workspace
end # Belinkr
