# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "member"
require_relative "../../tinto/sorted_set"

module Belinkr
  module Workspace
    module Invitation
      class Collection
        extend Forwardable
        include Virtus
        include Aequitas
        include Enumerable

        MODEL_NAME = "invitation"

        attribute :workspace_id,    Integer
        attribute :entity_id,       Integer

        validates_presence_of       :workspace_id, :entity_id
        validates_numericalness_of  :workspace_id, :entity_id

        def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                        :exists?, :all, :page, :<<, :add, :remove, :delete, 
                        :merge

        def initialize(*args)
          super *args
          @sorted_set = Tinto::SortedSet
                          .new(self, Invitation::Member, storage_key)
        end

        def member_init_params
          { entity_id: entity_id, workspace_id: workspace_id }
        end

        private

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:invitations"
        end
      end # Collection
    end # Invitation
  end # Workspace
end # Belinkr
