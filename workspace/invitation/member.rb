# encoding: utf-8
require "virtus"
require "aequitas"
require "statemachine"
require_relative "../../tinto/member"

module Belinkr
  module Workspace
    module Invitation
      class Member
        extend Forwardable
        include Virtus
        include Aequitas

        MODEL_NAME  = "invitation"
        STATES      = %w{ pending accepted rejected }

        attribute :id,              Integer
        attribute :workspace_id,    Integer
        attribute :entity_id,       Integer
        attribute :inviter_id,      Integer
        attribute :invited_id,      Integer
        attribute :state,           String
        attribute :created_at,      Time
        attribute :updated_at,      Time
        attribute :rejected_at,     Time

        validates_presence_of       :workspace_id, :entity_id,
                                    :inviter_id, :invited_id
        validates_numericalness_of  :workspace_id, :entity_id,
                                    :inviter_id, :invited_id

        def_delegators :@member, :==, :to_json, :read, :save, :score
        def_delegators :@state_machine, :accept, :reject

        alias_method :to_hash, :attributes

        def initialize(*args)
          super *args
          @member = Tinto::Member.new self
          
          @state_machine = Statemachine.build do
            state :pending
            state :accepted
            state :rejected do
              on_entry :rejected_at=
            end

            trans :pending, :accept, :accepted
            trans :pending, :reject, :rejected
          end
          @state_machine.context = self
          @state_machine.state = @state if @state
        end
       
        def state
          sync_state
          super
        end
  
        def state=(state)
          @state_machine.state = state if @state_machine
          super(state)
        end
        
        def accepted?
          state == "accepted"
        end

        def rejected?
          state == "rejected"
        end

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:invitations"
        end

        private 

        def sync_state
          self.state = @state_machine.state.to_s 
        end
      end # Member
    end # Invitation
  end # Workspace
end # Belinkr
