# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "../../user/member"
require_relative "../../activity/collection"

module Belinkr
  module Workspace
    module Invitation
      class Orchestrator
        def self.collection(workspace, invitations, user)
          invitations
        end

        def self.invite(workspace, invited, inviter)
          # sanitize
          # Enforcer.authorize inviter, :invite, workspace, invited
          invitation = Invitation::Member.new(
            inviter_id:   inviter.id,
            invited_id:   invited.id,
            state:        "pending",
            workspace_id: workspace.id,
            entity_id:    workspace.entity_id
          ).save
          Invitation::Collection.new(
            workspace_id: workspace.id, 
            entity_id:    workspace.entity_id
          ).add(invitation)
          
          activity = Belinkr::Activity::Member.new(
            actor: inviter,
            action: 'invite',
            object: invited,
            target: workspace
          ).save
          Belinkr::Activity::Collection.new.add(activity)

          invitation
        end

        def self.accept(workspace, invitation, user)
          #sanitize
          # Enforcer.authorize user, :accept, workspace, invitation
          invitation.accept
          invitation.save
          accept_activity = Belinkr::Activity::Member.new(
            actor: user,
            action: 'accept',
            object: invitation,
            target: workspace
           ).save

          collaborators_for(workspace).add user
          join_activity = Belinkr::Activity::Member.new(
            actor: user,
            action: 'join',
            object: workspace
            ).save
          
          
          Belinkr::Activity::Collection.new.add(accept_activity)
            .add(join_activity)

          invitation
        end

        def self.reject(workspace, invitation, invited)
          # sanitize
          # Enforcer.authorize user, :reject, workspace, invitation
          invitation.reject(Time.now)
          invitation.save

          activity = Belinkr::Activity::Member.new(
            actor: invited,
            action: 'reject',
            object: invitation,
            target: workspace
          ).save
          Belinkr::Activity::Collection.new.add(activity)

          invitation
        end

        private

        def self.administrators_for(workspace)
          Workspace::Administrator::Collection
            .new(workspace_id: workspace.id, entity_id: workspace.entity_id)
        end
        
        def self.collaborators_for(workspace)
          Workspace::Collaborator::Collection
            .new(workspace_id: workspace.id, entity_id: workspace.entity_id)
        end
      end # Orchestrator
    end # Invitation
  end # Workspace
end # Belinkr
