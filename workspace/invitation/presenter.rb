# encoding: utf-8
require 'json'
require_relative '../../tinto/utils'

module Belinkr
  module Workspace
    module Invitation
      module Presenter

        class Collection
          def initialize(resource)
            @resource = resource
          end

          def as_poro
            @resource.map { |member| Presenter::Member.new(member).as_poro}
          end

          def as_json
            "[#{as_poro.map { |i| i.to_json }.join(",")}]"
          end
        end #class Collection

        class Member
          def initialize(resource)
            @resource = resource
          end

          def as_poro
            user = OpenStruct.new(timezone: "Europe/Madrid")
            local_created_at =
              Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
            local_updated_at =
              Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)

            {
              id:                 @resource.id,
              workspace_id:       @resource.workspace_id,
              entity_id:          @resource.entity_id,
              inviter_id:         @resource.inviter_id,
              invited_id:         @resource.invited_id,
              state:              @resource.state,
              created_at:         local_created_at,
              updated_at:         local_updated_at,
              rejected_at:        @resource.rejected_at
            }.merge! errors
             .merge! links
          end

          def as_json
            as_poro.to_json
          end

          private

          def errors
            @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
          end

          def links
            {
                links: {
                    self: "/workspaces/#{@resource
                                        .workspace_id}/invitations/#{
                                        @resource.id}",
                    workspace: "/workspaces/#{@resource.workspace_id}"
                }
            }
          end

        end # class Member
      end # module Presenter
    end # module Invitation
  end # module Workspace
end # module Belinkr