# encoding: utf-8
require "virtus"
require "aequitas"
require_relative "../tinto/member"

module Belinkr
  module Workspace
    class Member
      extend Forwardable
      include Virtus
      include Aequitas

      MODEL_NAME = "workspace"

      attribute :id,              Integer
      attribute :name,            String
      attribute :entity_id,       Integer
      attribute :created_at,      Time
      attribute :updated_at,      Time
      attribute :deleted_at,      Time

      validates_presence_of       :name, :entity_id
      validates_length_of         :name, min: 3, max: 250
      validates_numericalness_of  :entity_id

      def_delegators :@member, :==, :score, :to_json, :read, :save, 
                                :delete, :undelete

      alias_method :to_hash, :attributes

      def initialize(*args)
        super *args
        @member = Tinto::Member.new self
      end
      
      def storage_key
        "entities:#{entity_id}:workspaces"
      end
    end # Member
  end # Workspace
end # Belinkr
