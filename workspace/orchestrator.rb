# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "enforcer"
require_relative "collaborator/collection"
require_relative "administrator/collection"
require_relative "../activity/collection"

module Belinkr
  module Workspace
    class Orchestrator
      def self.collection(workspaces, user)
        #Enforcer.authorize user, :collection, workspace
        workspaces
      end

      def self.read(workspace, user)
        #Enforcer.authorize user, :read, workspace
        workspace
      end

      def self.create(workspace, user)
        sanitize(workspace, user)
        #Enforcer.authorize user, :create, workspace
        workspace.save

        collection = Workspace::Collection.new(entity_id: workspace.entity_id)
        #$redis.multi do
          collection.add workspace
          collaborators_for(workspace).add user
          administrators_for(workspace).add user
          collaborators_for(workspace).remove user

          add_activity actor: user, action: 'add', object: workspace
        #end
        workspace
      end

      def self.update(workspace, updated, user)
        sanitize(updated, user)
        #Enforcer.authorize user, :update, workspace
        workspace.attributes = updated.attributes
        workspace.save
        add_activity actor: user, action: 'update', object: workspace

        workspace
      end

      def self.delete(workspace, user)
        #Enforcer.authorize user, :delete, workspace
        workspace.delete
        add_activity actor: user, action: 'delete', object: workspace
        workspace
      end

      def self.promote(workspace, promoted, promoter)
        Enforcer.new(workspace).promote?(promoter, promoted)
        administrators_for(workspace).add promoted
        collaborators_for(workspace).remove promoted
        add_activity actor: promoter, action: 'promote', object: promoted

        promoted
      end

      def self.demote(workspace, demoted, demoter)
        Enforcer.new(workspace).demote?(demoter, demoted)
        collaborators_for(workspace).add demoted
        administrators_for(workspace).remove demoted
        add_activity actor: demoter, action: 'demote', object: demoted
        demoted
      end

      def self.leave(workspace, user)
        Enforcer.new(workspace).leave?(user)
        unlink(workspace, user)
        add_activity actor: user, action: 'leave', object: workspace
        workspace
      end

      def self.remove(workspace, removed, remover)
        Enforcer.new(workspace).remove?(remover, removed)
        unlink(workspace, removed)
        add_activity actor: remover, action: 'remove', object: removed
        workspace
      end

      private

      def self.sanitize(workspace, user)
        workspace.entity_id = user.entity_id
      end

      def self.unlink(workspace, user)
        administrators_for(workspace).remove(user) if
          administrators_for(workspace).include?(user)

        collaborators_for(workspace).remove(user) if
          collaborators_for(workspace).include?(user)
      end

      def self.administrators_for(workspace)
        Workspace::Administrator::Collection
          .new(workspace_id: workspace.id, entity_id: workspace.entity_id)
      end
      
      def self.collaborators_for(workspace)
        Workspace::Collaborator::Collection
          .new(workspace_id: workspace.id, entity_id: workspace.entity_id)
      end

      def self.add_activity(attr={})
        activity = Activity::Member.new(
                   actor: attr.fetch(:actor),
                   action: attr.fetch(:action),
                   object: attr.fetch(:object)
                 ).save
        Activity::Collection.new.add activity
      end

    end # Orchestrator
  end # Workspace
end # Belinkr
