# encoding: utf-8
require 'json'
require_relative '../tinto/utils'

module Belinkr
  module Workspace
    module Presenter

      class Collection
        def initialize(resource)
          @resource = resource
        end

        def as_poro
          @resource.map { |member| Presenter::Member.new(member).as_poro }
        end

        def as_json
          "[#{as_poro.map { |i| i.to_json }.join(",")}]"
        end
      end # Collection

      class Member
        BASE_PATH = '/workspaces'

        def initialize(resource)
          @resource = resource
        end

        def as_poro
          user = OpenStruct.new(timezone: "Europe/Madrid")
          local_created_at =
            Tinto::Utils.local_time_for(@resource.created_at, user.timezone)
          local_updated_at =
            Tinto::Utils.local_time_for(@resource.updated_at, user.timezone)

          {
            id:         @resource.id,
            name:       @resource.name,
            entity_id:  @resource.entity_id,
            created_at: local_created_at,
            updated_at: local_updated_at
          }.merge! errors
          .merge! links
        end


        def as_json
          as_poro.to_json
        end

        private 

        def links
          user_base_path = "#{BASE_PATH}/#{@resource.id}"
          {
            links:
              {
                self: user_base_path,
                invitations: "#{user_base_path}/invitations",
                users: "#{user_base_path}/users",
                collaborators: "#{user_base_path}/collaborators",
                administrators: "#{user_base_path}/administrators",
                boards: "#{user_base_path}/boards"
              }
          }
        end

        def errors
          @resource.errors.empty? ? {} : { errors: @resource.errors.to_hash }
        end

      end #Member
    end # Presenter
  end # Workspace
end # Belinkr
