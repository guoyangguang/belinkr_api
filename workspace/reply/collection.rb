# encoding: utf-8
require_relative "member"
require_relative "../../tinto/exceptions"
require "json"

module Belinkr
  module Workspace
    module Reply
      class Collection
        include Enumerable

        MODEL_NAME = "reply"

        def initialize(members=[])
          @members = members.map { |reply| coerce(reply) }
        end

        def each
          @members.compact.select { |reply| yield validate(coerce(reply)) }
        end

        def create(reply)
          reply             = coerce(reply)
          reply.id          = @members.size
          this_moment       = Time.now
          reply.created_at  = this_moment
          reply.updated_at  = this_moment

          @members << validate(reply)
          @members  = @members.each_with_index do |reply, index|
                        reply.id = index; reply
                      end
          self
        end
        alias_method :add, :create
        alias_method :<<,  :create


        def update(reply)
          reply               = coerce(reply)
          reply.updated_at    = Time.now
          @members[reply.id]  = validate(reply)
          self
        end

        def delete(reply)
          reply               = coerce(reply)
          reply.deleted_at    = Time.now
          @members[reply.id]  = validate(reply)
          self
        end

        def undelete(reply)
          reply               = coerce(reply)
          raise Tinto::Exceptions::InvalidResource unless reply.deleted_at

          reply.deleted_at    = nil
          @members[reply.id]  = validate(reply)
          self
        end

        def destroy(reply)
          raise Tinto::Exceptions::InvalidResource unless reply.deleted_at
          @members[reply.id]  = nil
          self
        end

        def to_json(*args)
          "[#{@members.map { |m| m.to_json }.join(", ")}]"
        end

        private

        def coerce(reply)
          reply.is_a?(Member) ? reply : Member.new(reply)
        end

        def validate(reply)
          raise Tinto::Exceptions::InvalidResource unless reply.valid?
          reply
        end

      end # Collection
    end # Reply
  end# Workspace
end # Belinkr
