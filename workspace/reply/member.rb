# encoding: utf-8
require "virtus"
require "aequitas"
require "json"

module Belinkr
  module Workspace
    module Reply
      class Member
        include Virtus
        include Aequitas

        MODEL_NAME = "reply"

        attribute :id,              Integer
        attribute :text,            String
        attribute :user_id,         Integer
        attribute :workspace_id,    Integer
        attribute :status_id,       Integer
        attribute :created_at,      Time
        attribute :updated_at,      Time
        attribute :deleted_at,      Time

        validates_presence_of       :text, :user_id, :workspace_id, :status_id
        validates_numericalness_of  :user_id, :workspace_id, :status_id
        validates_length_of         :text, min: 3, max: 250

        alias_method :to_hash, :attributes

        def initialize(*args)
          super(*args)
        end

        def to_json
          attributes.to_json
        end

      end # Member
    end # Reply
  end #Workspace
end # Belinkr
