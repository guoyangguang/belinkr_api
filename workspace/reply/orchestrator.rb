# encoding: utf-8
require_relative "member"
require_relative "collection"
require_relative "../status/orchestrator"

module Belinkr
  module Workspace
    module Reply
      class Orchestrator
        def self.collection(replies, user)
          replies
        end
        
        def self.create(status, reply, user)
          #TODO enforcer
          status.embedded_replies =
              status.replies << sanitize(status, reply, user)
          status.save
          reply
        end

        def self.update(status, reply, user)
          #TODO enforcer
          status.embedded_replies =
              status.replies.update sanitize(status, reply, user)
          status.save
          reply
        end

        def self.delete(status, reply, user)
          #TODO enforcer
          status.embedded_replies = status.replies.delete reply
          status.save
          reply
        end

        def self.destroy(status, reply, user)
          #TODO enforcer
          status.embedded_replies = status.replies.destroy reply
          status.save
          reply
        end

        def self.undelete(status, reply, user)
          #TODO enforcer
          status.embedded_replies = status.replies.undelete reply
          status.save
          reply
        end

        private

        def self.sanitize(status, reply, user)
          reply.user_id      = user.id
          reply.workspace_id = status.workspace_id
          reply.status_id    = status.id
          reply
        end

      end # Orchestrator
    end # Reply
  end # Workspace
end # Belinkr
