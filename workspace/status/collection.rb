# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require_relative "../../tinto/sorted_set"
require_relative "member"

module Belinkr
  module Workspace
    module Status
      class Collection
        extend Forwardable
        include Enumerable
        include Virtus
        include Aequitas

        MODEL_NAME = "status"
        
        KINDS =  %w{workspace council}
        
        attribute :kind, String
        attribute :workspace_id,    Integer
        attribute :entity_id,       Integer

        validates_presence_of       :workspace_id, :entity_id, :kind
        validates_numericalness_of  :workspace_id, :entity_id
        validates_within :kind, set: KINDS 

        def_delegators :@sorted_set, :each, :size, :length, :include?, :empty?,
                       :exists?, :all, :page, :<<, :add, :remove, :delete, 
                       :merge, :score

        def initialize(*args)
          super *args
          @sorted_set = Tinto::SortedSet.new(self, Status::Member, storage_key)
        end

        def member_init_params
          { entity_id: entity_id, workspace_id: workspace_id }
        end

        private

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:statuses:#{kind}"
        end
      end # Collection
    end # Status
  end # Workspace
end # Belinkr
