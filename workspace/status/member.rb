# encoding: utf-8
require "forwardable"
require "virtus"
require "aequitas"
require_relative "../../tinto/member"
require_relative "../reply/collection"

module Belinkr
  module Workspace
    module Status
      class Member
        extend Forwardable
        include Virtus
        include Aequitas

        MODEL_NAME = "status"

        attribute :id,                Integer
        attribute :text,              String
        attribute :user_id,           Integer
        attribute :workspace_id,      Integer
        attribute :entity_id,         Integer
        attribute :created_at,        Time
        attribute :updated_at,        Time
        attribute :deleted_at,        Time
        attribute :embedded_replies,  EmbeddedValue, model: Reply::Collection,
                                      default: Reply::Collection.new

        validates_presence_of       :text, :user_id, :workspace_id, :entity_id
        validates_length_of         :text, min: 3, max: 5000
        validates_numericalness_of  :user_id, :workspace_id, :entity_id

        def_delegators :@member, :==, :score, :to_json, :read, :save, 
                                  :delete, :undelete

        alias_method :to_hash, :attributes

        def initialize(*args)
          super *args
          @member = Tinto::Member.new self
        end

        def replies
          Reply::Collection.new(embedded_replies)
        end

        def storage_key
          "entities:#{entity_id}:workspaces:#{workspace_id}:statuses"
        end
      end # Member
    end # Status
  end # Workspace
end # Belinkr
