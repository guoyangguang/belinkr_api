# encoding: utf-8
require_relative "collection"
require_relative "member"

module Belinkr
  module Workspace
    module Status
      class Orchestrator

        def self.collection(workspace, user, options = {})
          statuses_for workspace, options 
        end

        def self.create(workspace, status, user, options = {})
          status = sanitize(status, workspace, user)
          statuses = statuses_for(workspace, options)

          status.save
          statuses.add status
          status
        end

        def self.update(workspace, status, updated, user, options = {})
          status = sanitize(status, workspace, user)
          #TODO need statuses in enforcer
          statuses = statuses_for(workspace, options)

          status.text = updated.text
          status.save
          status
        end

        def self.delete(workspace, status, user, options = {})
          statuses = statuses_for(workspace, options)
          $redis.multi do
            status.delete
            statuses.remove status
          end
          status
        end

        def self.undelete(workspace, status, user, options = {})
          statuses = statuses_for(workspace, options)  
          $redis.multi do
            status.undelete
            statuses.add status
          end
          status
        end

        def self.read(workspace, status, user, options = {})
          #TODO need statuses in enforcer
          statuses = statuses_for(workspace, options)
          status
        end

        private 

        def self.sanitize(status, workspace, user)
          status.user_id      = user.id
          status.workspace_id = workspace.id
          status.entity_id    = user.entity_id
          status
        end

        def self.statuses_for(workspace, options = {})
          kind = options[:kind] ? options[:kind] : 'workspace'
          Workspace::Status::Collection.new(workspace_id: workspace.id,
             entity_id: workspace.entity_id, kind: kind)
        end
      end # Orchestrator
    end # Status
  end # Workspace
end # Belinkr
