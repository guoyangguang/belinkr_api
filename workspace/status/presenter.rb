#encoding: utf-8
require 'json'

module Belinkr
  module Workspace
    module Status
      module Presenter

      class Collection 
        def initialize(resource)
          @resource = resource
        end
        
        def as_poro
          @resource.map { |member| Presenter::Member.new(member).as_poro}
        end

        def as_json
          "[#{as_poro.map { |i| i.to_json }.join(",")}]"
        end
      end #Collection
        
        class Member
          def initialize(resource)
            @resource = resource
          end
          
          def as_json
            @resource.to_hash.merge!(errors).to_json 
          end

          def as_poro
            @resource.to_hash
          end
         
          private
          def errors
            (@resource.errors.empty?) ? {} : {errors: @resource.errors.to_hash}
          end
        end#Member

      end #Presenter
    end #Status
  end #Workspace
end #Belinkr
